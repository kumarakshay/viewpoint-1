class ReportsController < ApplicationController

  # generated reports displayed on the index page
  # for testing power report drop downs use "The McGraw-Hill Companies Ltd"
  before_filter :set_body_class
  before_filter lambda { help_menu_name('reports') }
  
  def index
    @body_class = "reports single-column"
    #
    # TODO: this list must now be populated from the pyxie API calls
    # TODO: the index page should show the file as a clickable item
    #
    # http://localhost:3006/pyxie - available buckets by current user
    # http://localhost:3006/pyxie/suba_ehealth - all files for a bucket
    #  
    @reports = Report.all_reports_list(current_user.company_guid)    
    # required for form display
    @form_type = "new"
    @req = Req.new
    @group_dat = Group.dat_for_dropdown(current_user.company_guid)
    @device_dat = Device.dat_for_dropdown(current_user.asset_list)
    @device_sysedge = Device.dropdown_by_device_class(current_user.asset_list,"SysEdge")
    @device_server = Device.dropdown_by_device_class(current_user.asset_list,"Server")

    # hidden form fields 
    @current_user_email = current_user.email
    @current_user_company_guid = current_user.company_guid
    @current_user_guid = current_user.guid

    @area_dat = Power.find_distinct_area(current_user.company_guid)
    @cabinet_dat = Power.find_distinct_cabinet(current_user.company_guid)    
    respond_to do |format|
      format.html
    end
  end

  def notifications
    @body_class = "reports single-column"
    @notification_tools = "reports single-column"
  end

  # TODO: based on the new functionality in pyxie, this method can be removed
  # the file should be clickable in the index grid
  def show
    company_guid = current_user.company_guid
    report_name = params[:id]  
    report_format = params[:ext]  
    f = Report.construct_path(report_name, company_guid, report_format)
    if params[:format] == "txt"
      send_file f, :type => "application/txt", :x_sendfile => true
    else  
      send_file f, :type => "application/pdf", :x_sendfile => true
    end
  end

  private

  def set_body_class
    add_body_class "reports single-column"
  end

  def add_body_class(new_class)
    @body_class ||= ""
    @body_class << "#{new_class}"
  end

end