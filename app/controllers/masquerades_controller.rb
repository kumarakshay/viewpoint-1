class MasqueradesController < ApplicationController

  before_filter lambda { help_menu_name('masquerade') }  
  # TODO: This should be all active companies
  def index
    authorize! :masquerade, sign_in_user
    @body_class = "masquerades index single-column"
    @errors = []
    @companies = []
    if (can_masquerade? || session[:masq] = "true")
      if params[:search].blank?
        @companies = Company.all(:limit => 50)
      else
       @companies = Company.search_by_name(params[:search])
      end
    else
      @errors << "should not be allowed to masquerade."
    end
  end

  # display users sort by email
  # display company name in the toolbar
  def show
    @company_name = Company.where(guid: params[:id]).first.name
    @users = User.where(company_guid: params[:id], active: true).sort(:email.asc).all
    @body_class = "masquerades index single-column"
  end

  def new
    if (current_user.can_masquerade? || session[:masq] = "true")
      session.delete(:company_has_childs)  
      mu = MasqueradeUser.new(current_user.real_user)
      mu.masquerade_as(params[:id])
      if mu.masqueraded_is_sungard?
        #Sungard user cannot masquerade as another sungard user
        @blocked_user_email = mu.email
      else
        #session[:operator_guid] = mu.guid
        session[:operator_guid] = params[:id]
        # This is used in context of company masquerade, that too when Sungard employee masquerades as 
        # someone who is having switch company rights.
        session[:user_mas_guid] = params[:id]
        session[:masquerade] = "true"
        @masquerade = true
        @masquerade_user = mu.user
        session[:is_omi] = @masquerade_user.is_omi?
        ##  set has childs if mu has child companies
        set_has_childs_to_session(@masquerade_user)
      end      
    end
    @o = @u = nil #resetting current_user
    @body_class = "masquerades index single-column"
  end

  def destroy    
    session[:operator_guid] = session[:user_guid]
    session.delete(:company_mas_guid) 
    session.delete(:user_mas_guid) 
    session.delete(:company_has_childs)
    session[:masquerade] = "false"
    session.delete(:is_omi)
    @masquerade = false
    @o = @u = nil #resetting current_user    
    redirect_to home_path
  end

  private 

  def can_masquerade?
    if @masquerade and @masquerade_user
      @masquerade_user.can_masquerade?
    else
      current_user.can_masquerade?
    end
  end
end
