class Sungard::RolesController <  VpAdminController
  skip_before_filter :authorize
  load_and_authorize_resource
  before_filter :load_instance, :only => [:show, :edit,:update, :destroy]
  
  def index
    @toolname = ': Roles'
    @body_class = "single-column"
    @roles = Role.active_roles(current_user.roles)
  end 
  
  def show
    @toolname = ': Roles'
    @body_class = "single-column"
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new
    @toolname = ': new role'
    @body_class = "single-column"
    @role = Role.new()    
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    @toolname = ': edit role'
    @body_class = "single-column"
  end  

  def create  
    @toolname = ': new role'  
    @role = Role.new(params[:role])    
    respond_to do |format|
      if @role.save
        format.html { redirect_to [:sungard, @role], notice: 'role item was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = ': edit role'
    respond_to do |format|
      params["role"].delete('name') # avoiding changes in name 
      if @role.update_attributes(params[:role])
        format.html { redirect_to [:sungard, @role], notice: 'role was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @role.destroy
        format.html { redirect_to sungard_roles_url }
      else
        format.html { redirect_to sungard_roles_url, notice: 'Unable to delete group as it may be referenced in subscriptions' }
      end
    end    
  end

  private 
  def load_instance
    begin
      @role = Role.find(params[:id]) 
      if @role.blank?
        Rails.logger.warning("Unable to load role with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.warning("Unable to load role with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end
end
