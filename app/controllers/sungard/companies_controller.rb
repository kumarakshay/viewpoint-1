class Sungard::CompaniesController <  VpAdminController

  before_filter lambda { help_menu_name('masquerade') }  

  def index
    @body_class = "masquerades index single-column"
    @toolname = ': Company Management'
    @errors = []
    @companies = []
    if (can_masquerade? || session[:masq] = "true")
      if params[:search].blank?
        @companies = Company.all(:limit => 50, :deleted_at => nil )
      else
        @companies = Company.search_by_name(params[:search])
      end
    else
      @errors << "should not be allowed to masquerade."
    end
  end

  def show
    @body_class = "single-column"
    @company = Company.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company }
    end
  end


  def edit
    @body_class = "single-column"
    @company = Company.find(params[:id])
  end

  def new
    @body_class = "single-column"
    @company = Company.new
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # companies should not be created from the UI
  def create
  end

  def update
    @body_class = "single-column"
    @company = Company.find(params[:id])
    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html {
          flash[:notice] = 'Company was successfully updated.'
          render :edit
        }
      else
        format.html {
          flash[:notice] = 'Company was successfully updated, not.'
          render :edit
        }
      end
    end
  end

  def destroy
    @company = Company.find_by_guid(params[:id])
    if @company
      @users = User.where(:company_guid => params[:id])
      unless @users.blank?
        begin
          res = MyOpenid.delete_company(params[:id])
        rescue  Net::HTTPError => e 
          logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
        end
      end
      # This is soft delete we are updating deleted_at with Time.now
      Company.destroy_all(:guid => params[:id] )
      redirect_to sungard_companies_path, :notice => "Company was successfully updated."
    else
      redirect_to sungard_companies_path, :notice => "Company was successfully updated, not."
    end

  end
  #    
  

  private 

  def can_masquerade?
    if @masquerade and @masquerade_user
      @masquerade_user.can_masquerade?
    else
      current_user.can_masquerade?
    end
  end
end