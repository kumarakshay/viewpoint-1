class Sungard::HelpsController <  VpAdminController
  load_and_authorize_resource
  before_filter :load_instance, :only => [:show, :edit,:update, :destroy]

  def index
    @helps = Help.all
    @toolname = ': Help Management'
    @body_class = "single-column"
  end 
  
  def show
    @body_class = "single-column"
    respond_to do |format|
      format.html # show.html.erb
      @toolname = ': Help Management'
    end
  end

  def new
    @help = Help.new()    
    @toolname = ': new help'
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    @toolname = ': edit help'
    @body_class = "single-column"
  end  

  def create    
    @help = Help.new(params[:help])    
    @toolname = ': new help'
    respond_to do |format|
      if @help.save
        format.html { redirect_to [:sungard, @help], notice: 'help was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = ': edit help'
    respond_to do |format|
      if @help.update_attributes(params[:help])
        format.html { redirect_to [:sungard, @help], notice: 'Help was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @toolname = ': delete help'
  	@help.destroy    
    respond_to do |format|
      format.html { redirect_to sungard_helps_url }
    end    
  end

  private 
  def load_instance
    begin
      @help = Help.find(params[:id]) 
      if @help.blank?
        Rails.logger.info("Unable to load Help with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load Help with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

end
