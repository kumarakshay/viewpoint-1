require 'sgtools/zenoss/device'
require 'sgtools/zenoss_client'
require 'sgtools/zenoss/company'
require 'sgtools/zenoss/component_type'

class Sungard::StatusController < ToolsTeamController
  layout 'blank'

  def login_count
    s = SystemInfo.new
    render json: s.login_count
  end

  def echo
    m = 'echo'
    m = params[:m] if(params[:m])
    render json: m
  end

  # @return ok when Company.acl_monitor is greater than success threshold
  # @return problem when Company.acl_monitor is less than success threshold
  def acl
    m = Company.acl_monitor
    render json: m
  end

  def index
    @toolname = ': Health'
    @body_class = "single-column"
    @start_time = DateTime.now()
    @success = {
      mongo: false,
      zenoss: false,
      sgs: false
    }

    @errors = {
      mongo: "",
      zenoss: "",
      sgs: ""
    }

   # test mongo by looking for a record
   begin
     c = Device.first
     raise "unable to reach mongo" if c.nil?
     @success[:mongo] = true unless c.nil?
   rescue Exception => e
     @success[:mongo] = false
     @errors[:mongo] = e.message
   end

   # test sgs by looking for a device record
   begin
     d = Device.first
     raise "unable to reach sgs" if c.nil?
     @success[:sgs] = true unless c.nil?  
   rescue Exception => e     
     @success[:sgs] = false 
     @errors[:sgs] = e.message  
   end

    # test zenoss by loading a device via the sgtools_gem
    begin
      mongo_device = Device.find_by_name("phlnu481v")
      d = SGTools::Zenoss::Device.new("phlnu481v", :z_instance => mongo_device.zenoss_instance).load!
      raise "unable to reach zenoss" if d.nil?
      @success[:zenoss] = true unless d.nil?
    rescue Exception => e
      @success[:zenoss] = false
      @errors[:zenoss] = e.message
    end

    @finish_time = DateTime.now()

    if request.format.json?
      render json: {:systems => @success, :errors => @errors}
    end
  end
end
