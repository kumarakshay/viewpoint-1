class Sungard::MenusController <  VpAdminController
  load_and_authorize_resource
  before_filter :load_instance, :only => [:show, :edit,:update, :destroy]

  def index
    @menus = Menu.all
    @toolname = ': Menu Management'
  end 
  
  def show
    @toolname = ': Menu Management'
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new
    @menu = Menu.new()    
    @toolname = ': new menu'
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
   @toolname = ': edit menu'
  end  

  def create 
    @toolname = ': new menu'   
    @menu = Menu.new(params[:menu])    
    respond_to do |format|
      if @menu.save
        format.html { redirect_to [:sungard, @menu], notice: 'menu item was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = ': edit menu'
    respond_to do |format|
      if @menu.update_attributes(params[:menu])
        format.html { redirect_to [:sungard, @menu], notice: 'Menu was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @menu.destroy
        format.html { redirect_to sungard_menus_url }
      else
        format.html { redirect_to sungard_menus_url, notice: 'Unable to delete group as it may be referenced in help management' }
      end
    end    
  end

  private 
  def load_instance
    begin
      @menu = Menu.find(params[:id]) 
      if @menu.blank?
       Rails.logger.info("Unable to load menu with id  #{params[:id]}")
       render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load Menu with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

end
