class Sungard::ComplianceComputeController <  VpAdminController
  # load_and_authorize_resource
  # skip_before_filter :authorize
  before_filter :load_instance, :only => [:update,:destroy]

  def index
    @compliance_groups = ComplianceCompute.sort(:name.asc).all
    @toolname = 'Compliance Compute Management'
    @body_class = "single-column"
  end

  def new
    @compliance_compute = ComplianceCompute.new()
    @toolname = 'New Compliance Group'
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def create
    @toolname = 'New Compliance Group'
    @compliance_compute = ComplianceCompute.new(params[:compliance_compute])
    respond_to do |format|
      if @compliance_compute.save
        format.html { redirect_to '/sungard/compliance_compute', notice: 'compliance group was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = ': Generate Compliance Report'
    begin
      respond_to do |format|
        if @compliance_compute.update_attributes(params[:compliance_compute])
          hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => "executions", :method => "POST" , :query => compliance_params })
          response = hpClient.fetch
          executionId = response["executionId"]
          if executionId
            pathname = "executions/" + executionId +  "/execution-log"
            get_hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => pathname, :method => "GET" , :query => nil})
            get_response = get_hpClient.execute_long_running
            if get_response["flowOutput"]
              @compliance_compute.vcenter_compliant = get_response["flowOutput"]["VCENTER_Compliant"].to_i rescue 0
              @compliance_compute.vcenter_total = get_response["flowOutput"]["VCENTER_Total"].to_i  rescue 0
              @compliance_compute.esxi_compliant = get_response["flowOutput"]["ESXI_Compliant"].to_i rescue 0
              @compliance_compute.esxi_total = get_response["flowOutput"]["ESXI_Total"].to_i rescue 0
              if @compliance_compute.save
                insert_compliance_devices(get_response["flowOutput"],@compliance_compute)
              end
            end
            notice = params[:commit] == "Check Compliance" ? "Compliance report check has been completed successfully. Please check the result for more details" : "Compliance report request has been submitted successfully(#{executionId}). Please <a href='/ui/index.html#/reports'>click here </a> to download the report"
            format.html { redirect_to '/sungard/compliance_compute', notice: notice }
          else
            format.html { redirect_to '/sungard/compliance_compute', notice: 'There is an issue with running HPOO flow' }
          end
        else
          format.html { redirect_to '/sungard/compliance_compute', notice: "There is an issue with updating compliance version: #{@compliance.errors.full_messages}" }
        end
      end
    rescue Exception =>e
      Rails.logger.info "*******Compliance Reporting Exception*********************"
      Rails.logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      format.html { redirect_to '/sungard/compliance_compute', notice: e.message }
    end
  end

  def destroy
    @toolname = 'Delete Help'
    @compliance_compute.destroy
    respond_to do |format|
      format.html { redirect_to "/sungard/compliance_compute" }
    end
  end

  private

  def insert_compliance_devices(dataString,obj)
    unless dataString["VCENTER_List"].blank?
      existing_vcenter_records = ComplianceDevice.where(:parent => obj.name,:group => obj.group_name1,:company_guid => current_user.company_guid.downcase).all
      existing_vcenter_records.map(&:delete)
      version = obj.vCenter_Version.blank? ? "" : obj.vCenter_Version
      batch1 = prepare_record(dataString["VCENTER_List"], obj.name, obj.group_name1, version)
      ComplianceDevice.collection.insert(batch1) unless batch1.empty?
    end
    unless dataString["ESXI_List"].blank?
      existing_esxi_records = ComplianceDevice.where(:parent => obj.name,:group => obj.group_name2,:company_guid => current_user.company_guid.downcase).all
      existing_esxi_records.map(&:delete)
      batch2 = prepare_record(dataString["ESXI_List"],obj.name,obj.group_name2,obj.Esxi_Version)
      version = obj.Esxi_Version.blank? ? "" : obj.Esxi_Version
      ComplianceDevice.collection.insert(batch2) unless batch2.empty?
    end
  end

  def prepare_record(recordStr,parent,group,version)
    batch = []
    strArray = recordStr.split("||")
    strArray.each do |element|
      deviceObj = {}
      objArray = element.split("|")
      unless objArray.blank?
        compliant = objArray[1].match(version).present?
        deviceObj["name"] = objArray[0]
        deviceObj["version"] = objArray[1]
        deviceObj["compliant_version"] = version
        deviceObj["build"] = objArray[2]
        deviceObj["type"] = "Compute"
        deviceObj["parent"] = parent
        deviceObj["group"] = group
        deviceObj["company_guid"] = current_user.company_guid
        deviceObj["is_compliant"] = compliant
        deviceObj["created_at"] =  DateTime.now.strftime('%Y-%m-%dT%H:%M:%S%z')
        deviceObj["updated_at"] =  DateTime.now.strftime('%Y-%m-%dT%H:%M:%S%z')
        batch.push(deviceObj)
      end
    end
    return batch
  end

  def load_instance
    begin
      @compliance_compute = ComplianceCompute.find(params[:id])
      if @compliance_compute.blank?
        Rails.logger.info("Unable to load compliance with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load compliance with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def compliance_params
    default_params = {
      "uuid" => "025de47d-5a54-47f0-8a54-cd842c5e629e",
      "runName"=> "ViewPoint - Compute Compliance Report"
    }
    input = {"GroupName" => "#{@compliance_compute.name} #{@compliance_compute.group_name1}", "vCenter_Version" => @compliance_compute.vCenter_Version, "Esxi_Version" => @compliance_compute.Esxi_Version}
    input.merge!("pyxieRequired" => params[:commit] && params[:commit] != "Check Compliance" ? "Yes" : "No")
    default_params.merge!("inputs" => input)
    default_params
  end
end
