class Sungard::ApiKeysController <  VpAdminController

  def index
    @api_keys = ApiKey.all
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def new
    @api_key = ApiKey.new
  end

  def create
    @api_key = ApiKey.new(params[:api_key])
    if @api_key.save
      redirect_to sungard_api_keys_url, :notice => "Created"
    else
      render "new"
    end
  end

  def edit
    @api_key = ApiKey.find(params[:id])
  end

  def update
    @api_key = ApiKey.find(params[:id])
    respond_to do |format|
      if @api_key.update_attributes(params[:api_key])
        format.html { redirect_to [:sungard, @api_key], notice: 'API Key was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def show
    @api_key = ApiKey.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def destroy
    @api_key = ApiKey.find(params[:id])
    @api_key.destroy
    respond_to do |format|
      format.html { redirect_to sungard_api_keys_url }
    end
  end
end