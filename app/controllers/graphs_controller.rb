class GraphsController < ApplicationController

  before_filter  :load_instance, :only => [:show, :edit, :update, :destroy]
  before_filter  :set_body_class
  before_filter  :populate_dashboard_id

  def index
    @graphs = Graph.all
  	@toolname = ''
  end

  def show
  	@toolname = ': list'
  end

  def edit
  	@toolname = ': edit'
  end

  def new        
    @graph_new = Graph.new(:asset_list =>[""])
  	@toolname = ': new'
    @dash_id = params[:dash_id] 
  end

  def create
    @graph_new = Graph.new(params[:graph])    
    respond_to do |format|
      if @graph_new.save        
        format.html do 
          if @dash_id.blank?
            redirect_to @graph_new, notice: 'Graph was successfully created.'
          else 
            add_to_parent_and_update_position
            redirect_to dashboard_path(:id => @dash_id) 
          end
        end
      else
        # TODO need to handle this properly perhaps redirect to dashboard. with a notice..        
        format.html { render action: "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @graph_new.update_attributes(params[:graph])
        format.html { redirect_to dashboard_path(:id => @dash_id), notice: 'Graph was successfully updated.' }

      else
        format.html { render action: "edit" }
      end
    end
  end  

  def populate_dashboard_id    
    @dash_id = params[:dash_id] if params[:dash_id]
  end
  private 

  def add_to_parent_and_update_position
    position = Dashboard.add_graph(@dash_id,@graph_new.id.to_s)
    @graph_new.update_attribute(:order, position)
  end

  def set_body_class
  	@body_class = 'graphs single-column'
  end

  def load_instance
    begin
      @graph_new = Graph.find(params[:id]) 
      if @graph_new.blank?        
       render 'errors/not_found',  :status => :not_found
      end
    rescue      
      render 'errors/not_found',  :status => :not_found
    end
  end
end
