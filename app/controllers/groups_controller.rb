class GroupsController < ApplicationController
  before_filter :load_instance, :only => [:show, :edit, :update, :destroy]

  def index
    @groups = Group.all(company_guid: current_user.company_guid, :active => true)
    @body_class = "groups index single-column"
  end 

  def beta_groups
    @groups = Group.all(company_guid: current_user.company_guid, :active => true)
    @body_class = "groups index single-column"
  end
  
  def show
    respond_to do |format|
      @body_class = "groups show single-column"
      format.html # show.html.erb
    end
  end

  def new
    @group = Group.new(:asset_list => [""])    
    respond_to do |format|
      @body_class = "groups new single-column"
      format.html # new.html.erb
    end
  end

  def edit
   # @group = Group.find(params[:id])
    @body_class = "groups edit single-column"
  end  

  def create    
    @group = prepare_group (params)    
    audit = {}
    audit["user_email"] = current_user.real_user.email
    audit["action"] = "create"
    audit["auditable_type"] = 'Group' 

    respond_to do |format|
      if @group.save
        @body_class = "groups save single-column"
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render json: @group, status: :created, group: @group}
      else
        @body_class = "groups create single-column"
        format.html { render action: "new" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
     params[:group][:asset_list] = params[:group][:asset_list] || []
     respond_to do |format|
      if @group.update_attributes(params[:group])
      @body_class = "groups update single-column"
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { head :no_content }
      else
        @body_class = "groups update single-column"
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @group.destroy
      @body_class = "groups destroy single-column"
        format.html { redirect_to groups_url }
      else
        @body_class = "groups destroy single-column"
        format.html { redirect_to groups_url, notice: 
          "Unable to delete group -#{@group.name} as it is referenced in subscriptions" }
      end
    end    
  end

  def beta_groups
    @body_class = "groups index single-column"
  end

  private 
  def load_instance
    begin
      @group = Group.find(params[:id]) 
      if @group.blank?
        Rails.logger.info("Unable to load group with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load group with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def prepare_group(params)
    group = Group.new(params[:group])
    group.company_guid = current_user.company_guid
    group
  end
end
