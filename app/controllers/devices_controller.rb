require 'csv'

class DevicesController < ApplicationController
  before_filter lambda { help_menu_name('devices') }
  before_filter :omi_user_redirect, only: :index

  VALID_DEVICE_TYPES = ['Router', 'Switch', 'Firewall', 'Server', 'System', 'Circuit']

  def index
    @help = "device"
    if current_user.is_sungard?
      @body_class = "devices single-column"
      render :file => 'shared/unauthorized_masq', :layout => 'application'
      return
    else
      @devices = Device.find_all_by_asset_guid(current_user.asset_list, :order => "name")
      @assets_list = @devices.collect(&:asset_guid)
      respond_to do |format|
        @body_class = "devices single-column"
        format.html
      end
    end
  end

  def core_devices
    r = Device.search do |s|
      s.query do |q|
        p = params
        q.boolean do |b|
          b.must {string p[:query]} if p[:query].present?
          b.must {string 'description:*type\=core*'}
          b.must {string 'istatus:Installed-Active'}
          b.must {string 'type:router OR switch' }
        end
      end
    end
    render json: r
  end

  def show
    authorize! :access_non_omi_pages, current_user, session[:is_omi]
    authorize! :device_access, current_user, params[:id]
    @body_class = "device"
    @event_api = "events"
    # store all errors from RPC's
    @errors = []
    unless (view_device_details?(params[:id]) || current_user.is_sungard?)
      @errors << "Unable to retrieve device"
      @body_class = 'single-column unauthorized'
      render :file => 'shared/unauthorized', :layout => 'application'
      return
    end
    @device = Device.find_by_asset_guid(params[:id])
    raise ActiveRecord::RecordNotFound unless @device

    # raise InvalidProfileDevice unless @device.type.in?(VALID_DEVICE_TYPES)
    # @zenoss_details = @device.zenoss_details

    # @body_class = 'core' if @device.core_device?
    # @body_class = 'core'

    #Capture recently searched assets without masquerading as user
    if @masquerade_user.blank?
      TicketLanding::RecentAsset.save_searched_asset(@device,current_user)
    end

    # incident count
    @incident_count = nil
    begin
      # @incident_count = @device.incident_count
      Rails.logger.info "Begin SGTools::ITSM Incident pull for #{@device.asset_guid}"
      puts @device.asset_guid
      total = SGTools::ITSM::Incident.find_all_by_device_id(@device.asset_guid, "company.u_portal_id" => current_user.company_guid, active: true).total
      Rails.logger.info "End SGTools::ITSM Incident pull"
      @incident_count = total
    rescue Exception => e
      @errors << "Unable to retrieve incident count from Service Now."
    end

    # change count
    @change_count = nil
    begin
      # @change_count = @device.change_count
      Rails.cache.fetch("device_model_change_count_#{@device.asset_guid}", :expires_in => 1.minutes) do
        Rails.logger.info "Begin SGTools::ITSM Change Request pull for #{@device.asset_guid}"
        total = SGTools::ITSM::ChangeRequest.find_all_by_device_id(@device.asset_guid, "company.u_portal_id" => current_user.company_guid, active: true).total
        Rails.logger.info "End SGTools::ITSM Change Request pull"
      end
      @change_count = total
    rescue Exception => e
      @errors << "Unable to retrieve change count from Service Now."
    end

    # MX Window count
    @active_mx_window_count = nil
    begin
      # @active_mx_window_count = @device.active_mx_window_count
      @active_mx_window_count = SGTools::ITSM::ScheduleSpan.find_all_by_device_id(@device.asset_guid, "company.u_portal_id" => current_user.company_guid, active: true).count
    rescue Exception => e
      @errors << "Unable to retrieve MX Window count from Service Now."
    end

    # Event Count
    # @events_count = nil
    # begin
    #   # @events_count = @device.events.count
    #   @event_count = Event.where(assetGuid: @device.asset_guid).count
    # rescue Exception => e
    #   @errors << "Unable to retrieve Zenoss event counts."
    # end

    # component list from zenoss
    @component_list = nil
    begin
      @component_list = @device.component_tree
      #rescue Exception => e
      #  @errors << "Unable to retrieve Zenoss component list. #{e}"
      #  @component_list = []
    end

    # pass all of the errors forward
    if @errors.count > 0
      flash[:error] = @errors
    end

    #### thresholds
    #@dats = JSON.parse(SGTools::Zenoss::Device.thresholds(@device.asset_guid))
    
    @dats = @device.zenoss_device.thresholds

    @cpu_memory = []
    @mountpoints = []

    @dats.each do |d|
      if d["type"] == "filesystem"
        @mountpoints.push(d)
      else
        # CPU Node
        if d["type"] == "cpu" || d["type"] == "memory"
          @cpu_memory.push(d)
        end
      end
    end

  end

  def device_details
    unless params[:global_id].blank?
      device = OmiDevice.find_by_global_id(params[:global_id])
      unless device.blank?
        redirect_to "/ui/index.html#/devices/#{device.asset_guid}"
      else
        redirect_to "/ui/index.html#/os-app-dashboard"
      end
    else
      redirect_to "/ui/index.html#/os-app-dashboard"
    end
  end

  private

  # Checks is user is allowed to view device details. A check is made if masquerade setting is
  # on then, asset list of masquerade user is checked for details.
  def view_device_details?(id)
    if @masquerade and @masquerade_user
      @masquerade_user.all_asset_list.include?(id)
    else
      current_user.all_asset_list.include?(id)
    end
  end

  def omi_user_redirect
    redirect_to "/ui/index.html#/devices" if session[:is_omi]
  end
end
