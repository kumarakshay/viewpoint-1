class VpAdminController < ApplicationController
  before_filter  :authorize
  layout 'admin'
  include ApplicationHelper

  rescue_from CanCan::AccessDenied do |exception|
    puts '---got AccessDenied from CanCan---'
    #flash[:error] = "Unauthorized Access"
    redirect_to welcome_path, notice: "Unauthorized Access"
  end
  
  private

  def authorize
    if sign_in_user.vp_admin_role?
      return true
    end
    redirect_to my_board_path, notice: "Unauthorized Access"
  end
end