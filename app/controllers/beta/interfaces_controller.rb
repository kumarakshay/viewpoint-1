class Beta::InterfacesController < SGTools::Rails::REST.api_v1_controller_class
  before_filter lambda { help_menu_name('interfaces') }

  def index
  	@body_class = "dash single-column"
  end  

end