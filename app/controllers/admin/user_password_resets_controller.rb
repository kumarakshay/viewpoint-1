class Admin::UserPasswordResetsController < ApplicationController
  #layout 'reset'

  def create
    begin
     @user = User.first(:email => { :$regex => /^#{params[:email]}$/i }, :active => true)
      if @user and  @user.send_password_reset!
        send_email @user 
        msg = 'Email sent with password reset instructions'
      else
        msg = "Unable to reset password for #{params[:email]}"
      end 
      redirect_to_edit(msg)
     rescue Exception => e 
       logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
       redirect_to_edit('Unable to reset password')
     end
  end

  private

  def redirect_to_edit(message)
    respond_to do |format|
      format.html { 
        flash[:notice] = message
        if @user.blank?
          redirect_to welcome_path
        else
          redirect_to(edit_admin_user_path(@user))
        end

      }
    end
  end

  def send_email(user)
    UserMailer.reset_password(user).deliver  unless APP_CONFIG["open_id_version"] == 'V2'
  end

end