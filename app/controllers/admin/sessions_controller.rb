class Admin::SessionsController < ApplicationController
  layout 'basic'
  skip_before_filter  :authenticated? unless Rails.env == 'production'

  # required to render the login form
  def new
    #This is double check to avoid some one misusing this link.
    redirect_to(new_oid_path) if Rails.env == 'production' 
  end

  def create
    #This is double check to avoid some one misusing this link.
    redirect_to(new_oid_path) if Rails.env == 'production'   
    email = params[:email]
    password = params[:password]
    #only active user can able to login
    u = User.first(:email => { :$regex => /^#{email}$/i }, :active => true) 
    if u.blank?
      logger.info("user not found in viewpoint db for #{email}")
      render 'new', notice: "Email address provided is invalid!"
    else
      session.delete(:user_company)
      session.delete(:user_snt)
      session[:user_guid] = u.id
      # operator_guid is used by pyxie for auth
      session[:operator_guid] = u.id
      #for backdoor login set masquerade flag to flase in session
      session[:masquerade] = "false"
      logger.info("authorized access for #{email}")      
      # if current_user.is_sungard? || !Access.can_access_SN(current_user.company_guid) || current_user.is_omi?
      #   redirect_to home_path
      # else
      #   redirect_to "/managedservice/#ticket_landing"
      # end
      redirect_to my_board_path
    end
  end

end