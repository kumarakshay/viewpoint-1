class ToolsController < ApplicationController
  before_filter :set_body_class
  layout 'application'

  private 

  def set_body_class
    add_body_class "tools single-column"
  end

  def add_body_class(new_class)
    @body_class ||= ""
    @body_class << "#{new_class}"
  end
end
