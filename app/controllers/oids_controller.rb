class OidsController < ApplicationController
  include "OidAspects::#{APP_CONFIG['open_id_version']}::OidsHelper".constantize
  layout 'basic'
  skip_before_filter  :authenticated?, :select_branding

  # redirects it to the openid server.
  def new
    oid_redirect
  end

  def destroy
    Rails.cache.delete("user-#{session[:user_guid]}")
    @masquerade = false
    reset_session
    #TODO logout from openid session.
    redirect_to root_url
  end
end
