class ErrorsController < ApplicationController
  layout 'basic'

  def unauthorized
    # Will render the app/views/errors/unauthorized.html.erb template
  end
 
  def not_found
    # Will render the app/views/errors/not_found.html.erb template
  end
 
  def error
    # Will render the app/views/errors/error.html.erb template
  end

  def error_500
    render file: "#{Rails.root}/public/500.html", layout: false, status: 500
  end

  protected
 
  # The exception that resulted in this error action being called can be accessed from
  # the env. From there you can get a backtrace and/or message or whatever else is stored
  # in the exception object.
  def the_exception
    @e ||= env["action_dispatch.exception"]
  end
end