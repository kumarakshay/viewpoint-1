class Services::ConfirmationsController < ApplicationController
	layout 'reset'
  skip_before_filter :authenticated?, :select_branding

  def index
  end

  def show
    redirect_to :edit_services_confirmation
  end

  def edit
  	@user = User.find_by_confirmation_token(params[:id])
    if @user.blank?
      @user = User.new
      flash[:notice] = "Invalid confirmation token"
      render :edit
    end
  end 

  def update
    msg='Unable to confirm user'
    @user = User.find_by_confirmation_token(params[:id])
    if @user
      params['user'].delete('confirmation_token')
      @user.attributes=params['user']
      if @user.confirm!
        session[:user_guid] = @user.guid 
        session[:operator_guid] = @user.guid
        redirect_to home_path          
        return
      else
        if @user.sso_errors.nil?          
          flash[:notice] = msg
        else
          flash[:notice] = "Please check with administrator, either your account/activation token has expired"
        end
      end
    else
      @user = User.new(params["user"])
      flash[:notice] = msg
    end
    render :edit 
  end
end
