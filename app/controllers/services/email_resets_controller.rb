class Services::EmailResetsController < ApplicationController
  layout 'reset'
  skip_before_filter :authenticated?, :select_branding

	def index
  end

  def show
    redirect_to :edit_services_email_reset
  end
  
  def edit
    @user = User.find_by_confirmation_token(params[:id])
  end

  def update
    begin
      @user = User.find_by_confirmation_token(params[:id])
      if @user
        if @user.confirm_email!
          flash[:notice] = "Your email has been successfully changed"
          redirect_to home_path
        else
          flash[:notice] = @user.sso_errors[:message]  
          render_edit
        end 
      else
         flash[:notice] = "Please check with administrator, either your account/activation token has expired"
         render_edit
      end   
     rescue Exception => e 
       logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
       flash[:notice]= "There is some problem with updating data. Please contact administrator"
       redirect_to home_path 
     end
  end

  private

   def render_edit
    @user = nil
    render :edit
   end 
end
