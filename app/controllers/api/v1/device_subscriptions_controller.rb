module Api
  module V1
    class DeviceSubscriptionsController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          report = params[:id]
          @metadata = "device subscription"
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/msa.json")
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceSubscription.new(r)
          end
        else
          report = params[:id]
          frequency = params[:frequency]
          @metadata = "device subscription"
          case report
          when "1"
            DeviceSubscription.bulk_load(report, frequency)
            @dats = DeviceSubscription.all  
          when "2"
            DeviceSubscription.bulk_load(report, frequency)
            @dats = DeviceSubscription.all
          when "3"
            DeviceSubscription.bulk_load(report, frequency)
            @dats = DeviceSubscription.all
          when "4"
            DeviceSubscription.bulk_load(report, frequency)
            @dats = DeviceSubscription.all
          else
            DeviceSubscription.bulk_load(["1","2","3","4"],frequency)
            @dats = DeviceSubscription.all  
          end
        end  
      end
    end
  end
end