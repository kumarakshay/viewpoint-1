require 'sgtools/zenoss/metric'

class Api::V1::MetricsController < SGTools::Rails::REST.api_v1_controller_class

  def company_devices
    if APP_CONFIG['stub_webservice']
      Rails.logger.info("stubbed data")
      guid = "stubbed subaru"
      dat = File.read("#{Rails.root}/test/fixtures/metrics/company_devices.json")
    else
      Rails.logger.info("live data")
      guid = Company.where(name: /subaru/i).first.guid
      dat = SGTools::Zenoss::Metric.company_devices(guid)
    end  
    @metadata = guid
    @dat = JSON.parse(dat)
  end

  def company_interfaces
    if APP_CONFIG['stub_webservice']
      guid = "stubbed subaru"
      dat = File.read("#{Rails.root}/test/fixtures/metrics/company_interfaces.json")
    else
      guid = Company.where(name: /subaru/i).first.guid
      dat = SGTools::Zenoss::Metric.company_interfaces(guid)
    end
    @metadata = guid 
    @dat = JSON.parse(dat) 
  end

  def device_metrics
    if APP_CONFIG['stub_webservice']
      device_guid = "stubbed: 7152131E-9B6B-C86C-E040-A2A8567D4EA1"
      dat = File.read("#{Rails.root}/test/fixtures/metrics/device_metrics.json")
    else
      device_guid = "7152131E-9B6B-C86C-E040-A2A8567D4EA1"
      dat = SGTools::Zenoss::Metric.device_metrics(device_guid)
    end
    @metadata = device_guid
    @dat = JSON.parse(dat) 
  end

  def interface_metrics
    if APP_CONFIG['stub_webservice']
      interface_name = "stubbed: ALPIR1.GE-0/0/9.109"
      dat = File.read("#{Rails.root}/test/fixtures/metrics/interface_metrics.json")
    else
      interface_name = "ALPIR1.GE-0/0/9.109"
      dat = SGTools::Zenoss::Metric.interface_metrics(interface_name)
    end
    @metadata = interface_name
    @dat = JSON.parse(dat) 
  end

  def metric_data
    if APP_CONFIG['stub_webservice']
      @metadata = "stubbed SOA-104.subaru1.com/os/filesystems/siedbprd02/usedBlocks_usedBlocks.rrd"
      dat = File.read("#{Rails.root}/test/fixtures/metrics/metric_data.json")
    else
      device = Device.find_by_asset_guid(params[:device_id])
      @metric = SGTools::Zenoss::Metric.new(params[:metric_id],
        :z_instance => device.zenoss_instance).load!
    end
    @metadata = {}
  end
end