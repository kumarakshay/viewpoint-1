##
# The locations service allows you to search for SunGard location data
# @url Locations
# @topic Locations
#
class Api::V1::LocationsController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :check_query_access, only: ["search", "index"]

  ##
  # Returns a list of SunGard location data based on search options.
  # The default fields are 'location', 'location_name', 'city', 'state', 'country', 'sungard_code'
  # 
  # @url [GET] /api/locations?[arguments]
  # @url [POST] /api/locations
  # 
  # @argument [String] auth_token Authentication token for user.
  # @optional_argument [Boolean] datacenter Return only records that are flagged as datacenter locations
  # @optional_argument [String] state Limit to locations in specified state.
  # @optional_argument [Integer] limit Maximum number of records to return. Default is 100.
  # @optional_argument [Integer] start Return records starting at this number. Default is 0.
  # @optional_argument [Array] select Array of strings specifying column names to return.
  #
  # @example_response
  #   {
  #        total: 80,
  #        data: [
  #                  {
  #                   "location":"AZ.PHOENIX.010",
  #                   "location_name":"PHX01 - PHOENIX ISC/IDC (INFLOW)",
  #                   "city":"Phoenix",
  #                   "state":"AZ",
  #                   "country":"US",
  #                   "sungard_code":"phx1"
  #                  },
  #                  {
  #                   "location":"AZ.SCOTTSDALE.001",
  #                   "location_name":"SCO01 - SCOTTSDALE ISC/IDC (SAS)",
  #                   "city":"Scottsdale",
  #                   "state":"AZ",
  #                   "country":"US",
  #                   "sungard_code":"sco1"
  #                  },
  #                ....
  #              ] 
  #    }
  # 
  # @example_response
  #   {"message":"Unable to find user."}
  #
  # @response_field [Integer] total Number of records returned by query.
  # @response_field [Array] data Array of location objects with fields matching select argument.
  #
  # @response_code 200 OK
  # @response_code 401 Authentication error
  # 
  def index
    data = []
    query = []
    default_columns = ['location', 'location_name', 'city', 'state', 
                       'country', 'sungard_code']
    options = {'sort' => "location",'dir' => "ASC"}

    dc = params[:datacenter] || nil
    unless dc.blank?
      case dc.downcase
      when 'f', 'false', 'n', 'no'
        dc = 'f'
      else
        dc = 't'
      end

      query.append({'name' => "attr_datacenter", 'value' => dc, 'op' => "eq"})
    end

    query.append({'name' => "state", 'value' => params[:state].upcase, 'op' => "eq"}) unless params[:state].blank?

    options['limit'] = params[:limit] || 100
    options['start'] = params[:start] || 0
    options['query'] = query
    options['select'] = params[:select] || default_columns
    
    data = Location.query_with(options)
    render json: data
  end

  def search
    data = IpHostname.query_with(@body)
    render json: data
  end
end
