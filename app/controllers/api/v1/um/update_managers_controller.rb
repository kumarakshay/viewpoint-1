class Api::V1::Um::UpdateManagersController < SGTools::Rails::REST.api_v1_controller_class

  # before_filter :authorize_call
  before_filter :get_upstream_data, :if => proc { !APP_CONFIG['stub_webservice']}
  BASE_URL =  APP_CONFIG['um_url']

  def scheduledEntries
  end

  def servers
  end

  def latestGoldPolicy
  end

  def goldPolicies
  end

  def patches
  end

  def tickets
  end

  def ticket
  end

  #Get ticket related comment
  def tickets_comments
  end

  def update_comment
  end

  private

  def get_upstream_data()
    url = BASE_URL + request.fullpath
    Rails.logger.info "************#{request.fullpath}"
    options = {}
    options["method"] = request.method
    unless params[:update_manager].blank?
      options["query"] = params[:update_manager]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
    render :text => @records.to_json, :content_type => "application/json"
  end

  def authorize_call
    if params[:company_guid]
      if  params[:company_guid] == current_user.company_guid
        return true
      else
        render :text => "Unauthorise Access", :content_type => "application/json"
      end
    end
  end

end