class Api::V1::VpInterfacesController < SGTools::Rails::REST.api_v1_controller_class

  def index
  	guid = current_user.company_guid
    @records = Interface.where(:$or => [{:company_guid =>  guid},{contracting_guid: guid},{owner_guid: guid}]).sort(:device_name)
  end

end