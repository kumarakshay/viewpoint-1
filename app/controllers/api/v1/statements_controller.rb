class Api::V1::StatementsController < SGTools::Rails::REST.api_v1_controller_class

  def index
  	@statements = Statement.order("position ASC").all
  end

  def groups
    @group_dat = Group.dat_for_dropdown(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @group_dat}
    end
  end

  def device_sysedge
    @device_sysedge =  Device.dropdown_by_device_class(current_user.asset_list,"SysEdge")
    respond_to do |format|
      format.json { render json: @device_sysedge}
    end
  end

  def device_server
    @device_server = Device.dropdown_by_device_class(current_user.asset_list,"Server")
    respond_to do |format|
      format.json { render json: @device_server}
    end
  end

  def areas
    @area = Power.find_distinct_area(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @area}
    end
  end

  def cabinets
    @cabinet = Power.find_distinct_cabinet(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @cabinet}
    end
  end
end