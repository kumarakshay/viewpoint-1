class Api::V1::EventSubscriptionsController < ToolsTeamController
  def index  	
    @event_subscriptions = EventSubscription.all_events
    if @event_subscriptions
      @metadata = {
                    "total_records" => @event_subscriptions.length,
                    "total_assets" => total_assets(@event_subscriptions)
                  }
    end
    render :stream => true
  end

  private
  def total_assets(events)
    childs = 0
    events.each do |ev|
      childs += ev.asset_list.length
    end
    childs
  end
end
