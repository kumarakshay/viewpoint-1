class Api::V1::BarsController < Api::V1::FoosController 
  include "ApplicationHelper".constantize

  def index
    if APP_CONFIG['stub_webservice']
      user_hash = JSON.parse(File.read("#{Rails.root}/test/fixtures/ticket_landing/current_user.json"))
    else

      user_hash = {}
      unless current_user.blank?
	      #Inser some general information
	      user_hash["masquerade"] = session[:user_guid] != session[:operator_guid] ? true : false
	      user_hash["company_masquerade"] = session.include?(:company_mas_guid)
	      user_hash["branding"] = @branding
	      #Insert current_user company informations
	      current_user_hash = current_user.as_json
	      current_user_hash.delete("roles")
	      user_hash.merge!({"current_user" => current_user_hash})
	      user_hash["current_user"]["is_sungard"] = current_user.is_sungard?
	      #Insert current user's abilities
	      ability = Ability.new(current_user)
	      ability_hash = {"abilities" => {}}
	      ability_hash["abilities"]["can_read_DNS"] = ability.can? :read, SGTools::Rails::Infoblox::Dns::Zone
	      ability_hash["abilities"]["can_masquerade"] = current_user.can_masquerade?
	      ability_hash["abilities"]["can_see_ticketing"] = Access.can_access_SN(current_user.company_guid)
	      user_hash["current_user"].merge!(ability_hash)
	      #Inser user viewpoint roles into current user object
	      user_hash["current_user"].merge!({"vp_roles" => {}})
	      vp_roles = Role.all.collect(&:name)
	      cu_roles = current_user.roles
	      vp_roles.each{|a| user_hash["current_user"]["vp_roles"][a] = cu_roles.include?(a) ? true : false }
	      #Insert current user's servicenow related roles and permission
	      sn_role_hash = {}
	      sn_role_hash.merge!({"sn_roles" => {"grant_access" => "0","notification" => "0","authorization" => "0"}})
	      client = ServiceNowClient.new
	      sn_permissions = client.get_user_roles(current_user.guid)
	      unless sn_permissions.blank?
	        sn_role_hash["sn_roles"]["grant_access"] = sn_permissions[:grant_access]
	        sn_role_hash["sn_roles"]["notification"] = sn_permissions[:notification]
	        sn_role_hash["sn_roles"]["authorization"] = sn_permissions[:authorization]
	      end
	      user_hash["current_user"].merge!(sn_role_hash)

	      #Insert servicenow base URL into current user object
	      url = current_user.is_allstream_user? ? APP_CONFIG['service_now_allstream'] : APP_CONFIG['service_now_url']
	      user_hash["current_user"].merge!({"sn_URL" => url})
	      ticketing_url = get_sn_url()
	      user_hash["current_user"].merge!({"ticketing_url" => ticketing_url})
	      user_hash["current_user"].merge!({"portal_url" => APP_CONFIG['legacy_portal']})
	    
	      #Insert sign in user details
	      user_hash.merge!({"sign_in_user" => {}})
	      user_hash["sign_in_user"]["first_name"] = sign_in_user.first_name
	      user_hash["sign_in_user"]["last_name"] = sign_in_user.last_name
	      user_hash["sign_in_user"]["email"] = sign_in_user.email
	      user_hash["sign_in_user"]["guid"] = sign_in_user.guid
	      user_hash["sign_in_user"]["company_name"] = sign_in_user.company_name
	      user_hash["sign_in_user"]["company_guid"] = sign_in_user.company_guid
	      user_hash["sign_in_user"]["is_sungard"] = sign_in_user.is_sungard?
	      #Insert session details
	      user_hash.merge!({"session" => {"company" => session[:user_company],"company_has_childs" => (session[:company_has_childs].blank? ? false : session[:company_has_childs])}})
	    end
  	end
    @record = OpenStruct.new(user_hash)
  end

end