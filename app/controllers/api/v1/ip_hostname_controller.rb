##
# The device service lets you search for SunGard asset data
# @url Assets
# @topic Devices
#
class Api::V1::IpHostnameController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :check_query_access, only: ["search", "index"]

  def index
    data = IpHostname.query_with(@body)
    render json: data
  end

  def search
    data = IpHostname.query_with(@body)
    render json: data
  end

  def device_by
    id = params[:id] || params[:device_id]
    d  = IpHostname.find_by_ip_host(id).device
    raise ActiveRecord::RecordNotFound.new("unable to find Device for #{id}") if d.blank?
    d
  end
end
