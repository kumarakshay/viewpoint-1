class Api::V1::FileSystemsController < SGTools::Rails::REST.api_v1_controller_class

  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/incidents.json")
    render :json => dat
  end

end