module Api
  module V1
    class CompanySubscriptionsController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          report = params[:id]
          @metadata = "Company Subscriptions"
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/msa.json")
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            CompanySubscription.new(r)
          end
        else
          report = params[:id]
          frequency = params[:frequency]
          @metadata = "Company based subscriptions"
          case report
          when "6"
            CompanySubscription.bulk_load("6",frequency)
            @dats = CompanySubscription.all
          when "7"
            CompanySubscription.bulk_load("7",frequency)
            @dats = CompanySubscription.all
          when "8"
            CompanySubscription.bulk_load("8",frequency)
            @dats = CompanySubscription.all
          when "9"
            CompanySubscription.bulk_load("9",frequency)
            @dats = CompanySubscription.all
          when "10"
            CompanySubscription.bulk_load("10",frequency)
            @dats = CompanySubscription.all
          when "11"
            CompanySubscription.bulk_load("11",frequency)
            @dats = CompanySubscription.all
          when "12"
            CompanySubscription.bulk_load("12",frequency)
            @dats = CompanySubscription.all
          when "13"
            CompanySubscription.bulk_load("13",frequency)
            @dats = CompanySubscription.all
          when "14"
            CompanySubscription.bulk_load("14",frequency)
            @dats = CompanySubscription.all  
          else
            CompanySubscription.bulk_load(["6","7","8","9","10","11","12","13","14","15","16"],frequency)
            @dats = CompanySubscription.all
          end
        end  
      end
    end
  end
end