class Api::V1::Omi::OmiController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_interface, :only => :interfaces
  before_filter :authorize_call
  before_filter :get_omi_data, :if => proc { !APP_CONFIG['stub_webservice']}
  before_filter :get_omi_data, :except => :tql
  after_filter :update_recent_asset

  BASE_URL = Figaro.env.omi_base_url
  AUTHORIZATION = Figaro.env.omi_authorization
  SEVERITY_MAP = {"normal" =>"information", "major" => "error", "minor" => "debug"}


  def node
    render_response
  end

  def node_related
    render_response
  end

  def nodes
    render_response
  end

  def interfaces
    @records = [] if no_data_found
    render_response
  end

  def companies
    render_response
  end

  def company_relations
    render_response
  end

  def company
    render_response
  end

  def company_relation
    render_response
  end

  def event
    render_response
  end

  def company_events
    # Logic to discard unknown severity events and mask the severity with user friendly value
    if @records && @records["events"]
      @records["events"].each do |event|
        unless event["severity"].downcase == 'unknown'
          mask = SEVERITY_MAP[event["severity"]]
          event["severity"] = mask if mask
        else
          @records["events"].delete(event)
        end
      end
    end
    @records = {events: []}  if @records.keys.empty?
    render_response
  end

  def company_events_count
    render_response
  end

  def events
    render_response
  end

  def tql
    @records = {}
    unless params[:asset_guid].blank?
      body = {
        tql_name: "API CI by GUID",
        tqlParams: {
          asset_guid: params[:asset_guid]
        }
      }
      result = post_omi_data(body)
      unless result.blank?
        @records =  result['cis'].group_by { |data| data["type"] }
      end
      render_response
    end
  end

  private

  def render_response
    render :text => @records.to_json, :content_type => "application/json"
  end

  def get_omi_data()
    url = BASE_URL + request.fullpath
    options = {}
    header = {"Authorization" => AUTHORIZATION}
    options["header"] = header
    options["method"] = request.method
    unless params[:omi].blank?
      options["query"] = params[:omi]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
  end

  def post_omi_data(body_params={})
    begin
      uri = URI.parse("#{BASE_URL}:443/cmdb/tql")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, nil)
      request["Authorization"] = AUTHORIZATION
      request["Content-Type"] = 'application/json'
      request.body = body_params.to_json
      response = http.request(request)
      result = JSON.parse(response.body)
    rescue Exception => e
      return {
        "cis"=> [],
        "relations" => []
      }
    end
  end

  def authorize_call
    if params[:company_guid]
      if  params[:company_guid].downcase == current_user.company_guid.downcase && !current_user.is_sungard?
        return true
      else
        render "api/v1/shared/unauthorize", :status => 401
        return false
      end
    end
  end

  def authorize_interface
    if params[:interface_filter]
      params[:company_guid] = params[:interface_filter].split("|").last
    end
  end

  def no_data_found
    !@records || (@records && @records.is_a?(Hash) && @records.key?("error") && @records["error"].include?("NoResultException"))
  end

  #Capture recently searched assets without masquerading as user
  def update_recent_asset
    if current_user.company.asset_list.include? params[:asset_guid]
      @device = OmiDevice.find_by_asset_guid(params[:asset_guid])
      TicketLanding::RecentAsset.save_searched_asset(@device,current_user)
    end
  end
end
