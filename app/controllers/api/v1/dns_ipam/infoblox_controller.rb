class Api::V1::DnsIpam::InfobloxController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :get_infoblox_data, except: [:get_ip_list, :get_zone_auth]
  before_filter :get_ip_list_data, only: [:get_ip_list]
  DNS_IPAM_CONFIG = eval(Figaro.env.dns_ipam)
  
  def get_dns_ipam
  end

  def post_dns_ipam
  end

  def put_dns_ipam
  end

  def delete_dns_ipam
  end

  def get_ip_list
  end

  def get_zone_auth
    fullpath = "/zone_auth?*customerguid=" + current_user.company_guid + "&_return_fields=fqdn,ns_group,dns_soa_email,soa_default_ttl,soa_email,soa_expire,soa_negative_ttl,soa_refresh,soa_retry,soa_serial_number,view,disable,zone_format,extattrs&_return_type=json"
    url = DNS_IPAM_CONFIG['base_url'] + DNS_IPAM_CONFIG['infoblox_path'] + fullpath
    options = {}
    header = header = {"Authorization" => DNS_IPAM_CONFIG['infoblox_authorization']}
    options["header"] = header
    options["method"] = request.method
    unless params[:infoblox].blank?
      infoblox_form = params[:infoblox][:zone] ? params[:infoblox].except(:zone) : params[:infoblox]
      options["query"] = infoblox_form
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    ns_group_mapping = {}
    records = fetch_data(rest_client, request.method)
    if records && records.length > 1
      records.each do |record|
        type = get_record_type(record, ns_group_mapping)
        record[:type] = type
      end
    end
    render :text => records.to_json, :content_type => "application/json"
  end

  private

  def get_infoblox_data
    url = DNS_IPAM_CONFIG['base_url'] + DNS_IPAM_CONFIG['infoblox_path'] + request.fullpath
    options = {}
    header = {"Authorization" => DNS_IPAM_CONFIG['infoblox_authorization']}
    options["header"] = header
    options["method"] = request.method
    unless params[:infoblox].blank?
      infoblox_form = params[:infoblox][:zone] ? params[:infoblox].except(:zone) : params[:infoblox]
      options["query"] = infoblox_form
    end 
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = fetch_data(rest_client, request.method)
    @records = @records.to_json unless @records.is_a? String
    render :text => @records, :content_type => "application/json"
  end

  def get_ip_list_data
    url = DNS_IPAM_CONFIG["base_url_iplist"] + DNS_IPAM_CONFIG['ip_list_path'] + request.fullpath
    options = {}
    header = {"X-IPM-Username" => DNS_IPAM_CONFIG['x_ipm_username'], "X-IPM-Password" => DNS_IPAM_CONFIG['x_ipm_password']}
    options["header"] = header
    options["method"] = request.method
    unless params[:infoblox].blank?
      infoblox_form = params[:infoblox][:zone] ? params[:infoblox].except(:zone) : params[:infoblox]
      options["query"] = infoblox_form
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = fetch_data(rest_client, request.method)
    render :text => @records.to_json, :content_type => "application/json"
  end

  def fetch_data(rest_client, http_method)
    begin
      httpclient = HTTPClient.new
      httpclient.ssl_config.verify_mode = OpenSSL::SSL::VERIFY_NONE
      if http_method == 'GET'
        response = httpclient.get(rest_client.url, :header => rest_client.common_header)
      elsif http_method == 'POST'
        response = httpclient.post(rest_client.url, :body => Yajl.dump(rest_client.query), :header => rest_client.common_header)
      elsif http_method == 'PUT'
        response = httpclient.put(rest_client.url, :body => Yajl.dump(rest_client.query), :header => rest_client.common_header)
      elsif http_method == 'DELETE'
        response = httpclient.delete(rest_client.url, :body => Yajl.dump(rest_client.query), :header => rest_client.common_header)
      end
      update_audit_logs if (http_method == 'POST' || http_method == 'PUT') && !valid_json?(response.body)
    rescue  Timeout::Error
      retry
    end
    return JSON.parse(response.body) unless response.body.blank? rescue response.body
  end

  def update_audit_logs
    zone_name = params[:infoblox] && params[:infoblox][:zone] ? params[:infoblox][:zone] : ""  rescue nil
    record = params[:infoblox] && (params[:infoblox][:name] || params[:infoblox][:ptrdname]) ? params[:infoblox][:name] || params[:infoblox][:ptrdname] : nil  rescue nil
    type = current_user.is_sungard? ? nil :  'customer-update'
    record_type = request.fullpath.split("/")[1].split(":")[1].upcase rescue ""
    dns_audit = DnsAudit.new(user: current_user.email, action: request.method.downcase, record_type: record_type,  zone: zone_name, 
      user_guid:current_user.guid, company_guid: current_user.company_guid, type: type, customer_name: current_user.company_name, record: record)
    dns_audit.save
  end

  def valid_json?(json)
    JSON.parse(json)
    return true
  rescue JSON::ParserError => e
    return false
  end

  def check_nsgroup_api(ns_group)
    fullpath = "/nsgroup?name=#{ns_group}&_return_fields=extattrs,external_primaries,external_secondaries,grid_primary,grid_secondaries&_return_type=json"
    url = DNS_IPAM_CONFIG['base_url'] + DNS_IPAM_CONFIG['infoblox_path'] + fullpath
    options = {}
    options["header"] = {"Authorization" => DNS_IPAM_CONFIG['infoblox_authorization']}
    options["method"] = "GET"
    rest_client = HttpClient.new(url, options)
    records = fetch_data(rest_client, "GET")
    if records[0] && records[0]["external_primaries"] && (records[0]["external_primaries"].length > 0)
      return true
    else
      return false
    end
  end

  def get_record_type(record, ns_group_mapping)
    if record["disable"]
      type = 'zone-disabled'
    elsif(!record.key?("ns_group"))
      type = 'zone-delegated'
    elsif ns_group_mapping.key?(record["ns_group"])
      type = ns_group_mapping[record["ns_group"]]
    elsif check_nsgroup_api(record["ns_group"])
      type = 'zone-slave'
      ns_group_mapping[record["ns_group"]] = 'zone-slave'
    else
      type = ''
      ns_group_mapping[record["ns_group"]] = ''
    end
     return type
  end
end
