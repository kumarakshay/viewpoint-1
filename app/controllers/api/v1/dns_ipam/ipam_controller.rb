class Api::V1::DnsIpam::IpamController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :get_data
  BASE_URL = Figaro.env.patch_management_base_url
  AUTHORIZATION = Figaro.env.patch_management_authorization


  def get_collection_by_name
  end

  def get_collection_by_id
  end

  def get_all_documents
  end

  def get_wildcard_values
  end

  def check_collection_exists
  end

  def check_field_exists
  end

  def check_value_exists
  end

  def add_document
  end

  def update_document
  end

  def delete_document
  end

  def get_collection_count
  end

  def get_doc_field
  end

  private

  def get_data()
    url = BASE_URL + request.fullpath
    options = {}
    header = {"Authorization" => AUTHORIZATION}
    options["header"] = header
    options["method"] = request.method
    unless params[:ipam].blank?
      options["query"] = params[:ipam]
    end
    Rails.logger.info "******data**#{params[:ipam]}"
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
    render :text => @records.to_json, :content_type => "application/json"
  end
end
