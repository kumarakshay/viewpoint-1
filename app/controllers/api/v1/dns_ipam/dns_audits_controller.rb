class Api::V1::DnsIpam::DnsAuditsController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_call
  before_filter :get_dns_audits_logs, only: [:index]

  def index
    render json: {status: 'SUCCESS', message: 'Loaded all audits logs', logs:  @records}, status: :ok
  end

  def create
    begin
      @audit = DnsAudit.new(params[:dns_audit])
      respond_to do |format|
        if @audit.save
          format.json { render json: @audit, status: :created }
        else
          format.json { render json: @audit.errors, status: :unprocessable_entity }
        end
      end
    rescue 
      render :json => { :errors => ["Can not process this request"]}, :status => 422
    end
  end



  private

  def get_dns_audits_logs
    @records = []
    begin
      @records = DnsAudit.for_customer(current_user.company_guid).sort(:created_at.desc).all
    rescue
    end
  end

  def authorize_call
    if current_user
      return true
    else
      render :text => "Unauthorise Access", :content_type => "application/json"
    end
  end
end
