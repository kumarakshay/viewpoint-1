class Api::V1::ProcessorsController < SGTools::Rails::REST.api_v1_controller_class

  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/processors.json")
    render :json => dat
  end
  
end