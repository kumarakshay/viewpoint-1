class Api::V1::Portal::CompanyMasqueradesController < SGTools::Rails::REST.api_v1_controller_class

  before_filter :load_parent_child_companies, :only =>[:index, :show, :new]
  
  def index    
    authorize! :company_masquerade, @real_user
    @records = []
    # TODO why is there a limit of 50 here?
    if params[:search].blank?        
      # @companies = Company.all(:limit => 50, :guid => {:$in => @guids})
      @records = Company.where(:guid => {:$in => @guids}).sort(:name.asc)
    else
      @records = Company.where(name: /#{params[:search]}/i, :guid => {:$in => @guids}).sort(:name.asc)
    end
  end

  def new
    authorize! :company_masquerade, @real_user
    #proxy = User.find_by_guid(PROXY_USR_GUID)
    @company = Company.find_by_guid(params[:id])
    if @company
      session[:masquerade_company] = current_user.company.name
      session[:masquerade_company_id] = current_user.company_guid
      session[:company_mas_guid] = params[:id]
      session[:operator_guid] = current_user.id
      @masquerade = true
      @masquerade_user = current_user
      session[:is_omi] = @masquerade_user.is_omi?
      @o = @u = nil #resetting current_user
      render :nothing => true, :status => 204,:content_type => "application/json"
    else
      render :json => { :errors => ["Can not process this request."]}, :status => 422
    end
  end

  def destroy
    # Check if previously masqueraded
    if session.include?(:user_mas_guid)
      session[:operator_guid] = session[:user_mas_guid]
    else
      session[:operator_guid] = session[:user_guid]
      session[:masquerade] = "false"
      @masquerade = false
    end
    session.delete(:company_mas_guid)
    session.delete(:company_has_childs)
    session.delete(:is_omi)
    session.delete(:masquerade_company)
    session.delete(:masquerade_company_id)
    #session.delete(:user_mas_guid)
    @o = @u = nil #resetting current_user
    render :nothing => true, :status => 204,:content_type => "application/json"
  end

  private 

  # Load all the parent child companies
  # -- if sungard user is masquerading then we need to hold the child companies from the 
  #       masquerading user
  # -- else if a normal user with masquerading permission logs in then we load the parent child
  #       company from his parent child relationship.
  def load_parent_child_companies
    begin
      @real_user = current_user
      if session.include?(:user_mas_guid)
        mu = User.find(session[:user_mas_guid])
        unless mu.blank?
          @real_user = mu
        end
      else
        current_user.reload
      end
      @guids = Company.parent_child_list(@real_user.company_guid)
      @guids.delete(@real_user.company_guid)
    rescue
      @guids  = []
    end
  end
end
