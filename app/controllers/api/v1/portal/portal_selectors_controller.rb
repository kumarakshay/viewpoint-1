class Api::V1::Portal::PortalSelectorsController < SGTools::Rails::REST.api_v1_controller_class
  def index
    results =  [] #user_allowed_applications(current_user.email)
    @portals = []
    results.each do |key, value|
      portal_node = {}
      portal_node["classname"] = PORTAL_HASH[key][0] rescue ""
      portal_node["title"] = PORTAL_HASH[key][1] rescue key
      portal_node["target"] = PORTAL_HASH[key][2] rescue "_self"
      portal_node["href"] = value
      @portals.push(portal_node)
    end
    if session[:user_snt] && ["webk","tfsu","hbsi","tr0i","k2sl","wedd","ceny","jbcc","ersl","chcp","irod","cdsl","exll","prpf"].include?(session[:user_snt].downcase)
      @portals.delete_if { |h| h["classname"] == "link-cloud" }
      @portals.push(
        {
          "classname" => "link-cloud",
          "title" => "Managed AWS",
          "target" => "_blank",
          "href" => "https://managedaws.sungardas.com"
        }
      )
    end
  end

  private

  def user_allowed_applications(email)
    begin
      records = {}
      if email.present?
        options = {}
        url = APP_CONFIG['up_wurst_url'] + "/up/ws/headerTools/portalselector/listOfPortals/#{email}"
        rest_client = HttpClient.new(url, options)
        records = rest_client.fetch
      end
    rescue  Timeout::Error
      retry
    end
  end

  PORTAL_HASH = {
    "Cloud" => ["link-cloud",  "Self-managed Cloud" "_self"],
    "Viewpoint" => ["link-managed-services active", "Managed Services","_self"],
    "Service Now" =>["", "Ticketing","_blank"],
    "RaaS" => ["link-recovery-service",  "Recovery Services","_self"],
    "AWS" => ["link-cloud", "Managed AWS","_blank"],
    "MRP" => ["","MRP Recovery Reports","_blank"]
  }
end
