class Api::V1::Portal::SearchController < SGTools::Rails::REST.api_v1_controller_class
  BLACKLIST = ["Group"]
	def search
		@records = []
		model_name = current_user.is_omi? && params["collection"] == 'device' ? 'OmiDevice' : params["collection"].camelize
		if model_exists?(model_name)
			model = model_name.constantize
			if model.respond_to?("portal_search") && params["query"].length >= 3
				if current_user.is_sungard? && BLACKLIST.exclude?(model_name)
				  @records = model.send(:portal_search, params["query"])
				else
					@records = model.send(:portal_search, params["query"], current_user.company_guid)
				end
			end
			 render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
		end
	end

	private

	def model_exists?(collection)
		files = Dir[Rails.root + 'app/models/*.rb']
		models = files.map{ |m| File.basename(m, '.rb').camelize }
		models.include? collection
	end
end

