module Api
  module V1
    class GroupGlancesController < ToolsTeamController

      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/group/group_glance.json")
          @metadata = "stubbed groups glance"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            Group.new(r)
          end
        else
          query = Group.active
          unless params[:company_guid].blank?
            query = query.where(:company_guid => params[:company_guid])          
          end
          unless params[:updated_after].blank?
            query = query.updated_after(params[:updated_after])          
          end
          @metadata = "groups glance"
          @dats = query.all
        end  
      end
    end
  end
end