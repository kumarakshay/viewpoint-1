##
# The authentication service lets you login, logout and get current user details.
# @url User Authentication
# @topic Authentication
#
class Api::V1::SessionsController < SGTools::Rails::REST.api_v1_controller_class
  prepend_before_filter :require_no_authentication, :only => [:create, :current_session]
  
  before_filter :ensure_params_exist, :only => [:create]

  respond_to :json

  def new
    login
  end
  
  def create
    login
  end

  def destroy
    logout
  end

  ##
  # Authenticate as a user with the API. This returns a token that is used to make
  # furhter requests against the API.
  # 
  # @url [GET] /api/login?[arguments]
  # @url [POST] /api/login
  # 
  # @argument [String] username The username for authentication.
  # @argument [String] password The password to use for authentication.
  #
  # @example_response
  #   {
  #     "login":"jdoe",
  #     "auth_token":"dRASesSWupRPpKWE9RUR"
  #   }
  # 
  # @example_response
  #   {"message":"Error with your login or password"}
  #
  # @response_field [String] login Username that was authenticated.
  # @response_field [String] auth_token Token that should be used for future requests
  # @response_field [String] message Error message
  #
  # @response_code 200 OK
  # @response_code 401 Authentication Error
  # 
  def login
    build_resource
    resource = ApiUser.authenticate_with_ldap({:login => params[:username], :password => params[:password]})
    # resource = ApiUser.find(1)
    
    if resource
      sign_in("api_user", resource)
      current_api_user.reset_authentication_token!
      # render :json=> {:login=>resource.login, :status => :ok}
      render :json => {:login=>resource.login, :auth_token => current_api_user.authentication_token }.to_json, :status => :ok
      return
    end
    invalid_login_attempt
  end
  
  ##
  # Sign out of API. This destroys the auth_token for the user.
  # 
  # @url [GET] /api/logout?[arguments]
  # 
  # @argument [String] auth_token Authentication token for user.
  #
  # @example_response
  #   {"status":"ok"}
  # 
  # @example_response
  #   {"message":"Unable to find user."}
  #
  # @response_field [String] status ok if success
  # @response_field [String] message Error message
  #
  # @response_code 200 OK
  # @response_code 401 Could not find user
  # 
  def logout
    current_api_user.reset_authentication_token!
    sign_out(resource_name)
    render :json => {:status => :ok}
  end

  ##
  # Get user credentials based on current token.
  # 
  # @url [GET] /api/current_user?[arguments]
  # 
  # @argument [String] auth_token Authentication token for user.
  #
  # @example_response
  #   {"login":"jdoe"}
  # 
  # @example_response
  #   {"message":"Unable to find user."}
  #
  # @response_field [String] login Username of current user.
  # @response_field [String] message Error message
  #
  # @response_code 200 OK
  # @response_code 401 Could not find user
  # 
  def current_session
    if current_api_user
      render :json => current_api_user
    else
      render :json=> {:message=>"Unable to find user."},
       :status=>401
    end
  end

  protected
  def ensure_params_exist
    missing = false
    missing = true if params[:username].blank? or params[:password].blank?
    return unless missing
    render :json=>{:message=>"missing authentication parameters"}, 
           :status=>422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render :json=> {:message=>"Error with your login or password"},
           :status=>401
  end
end