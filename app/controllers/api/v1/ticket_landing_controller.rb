class Api::V1::TicketLandingController < SGTools::Rails::REST.api_v1_controller_class
  include SGTools::Rails::REST::RestfulResources

  CACHE_EXPIRE_TIME = 60.minutes
  EH_CACHE_EXPIRE_TIME = 10.minutes

  def ticket_stats
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticket_landing/ticket_stats.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      @records = Ehealth::Ticketing.bulk_load(current_user.company_guid)
    end
  end

  def tickets
    @records = []
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures//ticket_landing/tickets.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      start_date = Date.today - 1.month
      end_date =  Date.today 
      company_id = current_user.company_guid
      type = params[:type]
      client = ServiceNowRestClient.new
      results = client.company_tickets(company_id,start_date,end_date,type)
      results.each do |result|
        @records << OpenStruct.new(result)
      end
    end
  end

  def recent_assets
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticket_landing/recent_assets.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      @records = TicketLanding::RecentAsset.where(:company_guid => current_user.company_guid,:user_guid => current_user.guid).sort(:updated_at.desc).limit(20);
    end
  end

  def asset_health
    @records = {"result" => {}}.to_json
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/environment_healths.json")
    else
      company = Company.where(:guid =>current_user.company_guid).first
      unless company.blank?
        z_instance = company.zenoss_instance
        unless z_instance.blank?
          severity = SGTools::Zenoss::MaxSeverity.new(company.guid,{:z_instance => z_instance})
          @records = severity.max_severities
        end
      end
    end
    render :text => '{"result":' + @records + '}', :content_type => "application/json"
  end

  def groups_health
    @record = {"result" => {}}.to_json
    if APP_CONFIG['stub_webservice']
      @record = File.read("#{Rails.root}/test/fixtures/ehealth/groups_environment_health.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      company = Company.where(:guid =>current_user.company_guid).first
      unless company.blank?
        @record = company.group_availability
      end
      @record
    end
  end

  def sn_user_roles
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticket_landing/sn_user_roles.json")
    else
      client = ServiceNowClient.new
      client.get_user_roles(current_user.guid)
      @records = client.get_user_roles(current_user.guid).to_json
    end
    render :text =>  @records, :content_type => "application/json"
  end
end
