class Api::V1::EventHistoriesCompanyController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :check_query_access

  def index_stub
    d = EventHistory.where(companyGuid: current_user.company_guid).all
    dat = File.read("#{Rails.root}/test/fixtures/mom/events.json")
    render :json => dat
  end

  def index
    @body["query"]["companyGuid"] = current_user.company_guid

    page = @body["page"].to_i > 0 ? @body["page"].to_i : 1
    per_page = @body["per_page"].to_i || 25

    where = {}
    query_parts = []
    query_values = []
    order = nil
    # fix up the column names
    if @body["query"].present?
     where = Hash[@body["query"].map {|k,v| [Event::FIELD_MAPPINGS[k] || k, v]}]
    end
    where.each do |k,v|
      if k.in?('lastTime', 'firstTime')
        times = v.split('-')
        if times.count > 0
          # times[0] = times[0].to_i / 1000 if times[0].length > 10
          times[0] = times[0].to_i           
          start_t = Time.at(times[0].to_i).to_i
          end_t = Time.now.to_i
          if times[1].present?
            # times[1] = times[1].to_i / 1000 if times[1].length > 10
            times[1] = times[1].to_i 
            end_t = Time.at(times[1].to_i).to_i
          end
          query_parts.append("#{k} > ? AND #{k} < ?")
          query_values.append(start_t)
          query_values.append(end_t)
        end
      elsif k.in?('eventState', 'severity', 'prodState')
        query_parts.append("#{k} in (?)")
        query_values.append(v)
      elsif k.in?('assetGuid', 'evid', 'companyGuid')
        query_parts.append("#{k}=?")
        query_values.append(v)
      elsif k.in?('count', 'DeviceImpact')
        search_term = v
        search_term.tr!(' ','')
        value = search_term.tr('><= ','').to_i
        if search_term.starts_with?('>=')
          query_parts.append("#{k} >= ?")
        elsif search_term.starts_with?('<=')
          query_parts.append("#{k} <= ?")
        elsif search_term.starts_with?('>')
          query_parts.append("#{k} > ?")
        elsif search_term.starts_with?('<')
          query_parts.append("#{k} < ?")
        else
          query_parts.append("#{k}=?")
        end
        query_values.append(value)
      else
        query_parts.append("#{k} LIKE ?")
        query_values.append("%#{v}%")
      end
    end
    if @body["sort_field"].present?
      sort_field = Event::FIELD_MAPPINGS[@body["sort_field"]] || @body["sort_field"]
      order = "#{sort_field} #{@body["sort_dir"] || desc}"
    end
    query_string = query_parts.join(' AND ')
    # Rails.logger.info("****** event_company query_string #{query_string}")
    # Rails.logger.info("****** event_company query_string #{query_values}")
    d = EventHistory.where(query_string, *query_values).order(order) 

    total = d.count
    total_pages = total > 0 ? ((total/per_page)+1).floor : 1
    page = page > total_pages ? total_pages : page || 1
    render json: {:total => total, :total_pages => total_pages, :page => page, :results => d.offset((page-1)*per_page).limit(per_page)}

  end
end