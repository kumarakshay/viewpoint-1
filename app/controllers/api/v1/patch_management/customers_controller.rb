class Api::V1::PatchManagement::CustomersController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_mfa
 
  def search
    authorize! :masquerade, sign_in_user
    @records = []
    if params[:search].blank?
      @records = Company.all(:limit => 50)
    else
      @records = Company.search_by_name(params[:search])
    end
  end

  private

  def authorize_mfa
    if MfaEvent.for_user(sign_in_user.guid).first.blank?
      render :json=> {:message=>"user is not mfa authenticated", :oneportal_base_url=>APP_CONFIG["oneportal_url"]}, :status=>401
    end
  end
end