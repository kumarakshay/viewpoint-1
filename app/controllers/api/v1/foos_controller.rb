# not authenticating at the application level 
# basic auth for tools team
class Api::V1::FoosController < ApplicationController 
  skip_before_filter :authenticated?

end
