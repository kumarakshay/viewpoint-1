class Api::V1::VpDevicesController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :set_model_class

  def index
    @records = @model_class.find_all_by_asset_guid(current_user.asset_list, :order => "name")
  end

  def show
    @record = nil
    if current_user.asset_list.include?(params[:id])
      @record = @model_class.where(:asset_guid => params[:id]).first
    end
  end

  def by_ip
  	@record = []
  	if params[:ip_address]
      record = @model_class.where(:ip_address_formatted => params[:ip_address]).first
      if record && current_user.asset_list.include?(record.asset_guid)
      	@record = record
      end
    end
  end

  private
  def set_model_class
    @model_class = session[:is_omi] ? OmiDevice : Device
  end
end
