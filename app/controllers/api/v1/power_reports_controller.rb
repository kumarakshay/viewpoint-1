module Api
  module V1
    # API controller for Power subscription
    #
    # @return [String] subscription_id
    # @return [String] subscription
    # @return [String] email
    # @return [String] company_guid
    # @return [String] frequency
    # @return [String] delivery_time
    # @return [String] cabinet
    # @return [String] area
    #
    # Using controller inheritance for authentication
    # @see http://www.therailsway.com/2009/4/6/controller-inheritance/
    #
    # @see Power
    # @see https://wiki.sungardas.corp/display/OSSTOOLS/Zenoss+Report+API+Hooks
    class PowerReportsController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/power_subscription.json")
          @metadata = "stubbed power reports"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            PowerReport.new(r)
          end
        else
          #override the report_id to 1 as we are keeping powerbar report_id as 1 in the statement_obrs collection
          PowerReport.bulk_load("1")
          @dats = PowerReport.where(:report_id => "1").all
          @metadata = "power report subscription"
        end  
      end
    end
  end
end