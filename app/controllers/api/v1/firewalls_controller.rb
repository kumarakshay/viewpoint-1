class Api::V1::FirewallsController < SGTools::Rails::REST.api_v1_controller_class

  ARCHIEVES_MONTHS = (Date.today - 3.month)
  def archive
    begin
      @records = []
      if params["ip_address"]
        httpclient = HTTPClient.new
        url = "#{APP_CONFIG['vp_base_url']}/pyxie/#{current_user.company_guid}-firewall_logs?token=ccd5995f61224fae48f78fcb5a17a82d"
        httpclient.ssl_config.verify_mode = OpenSSL::SSL::VERIFY_NONE
        response = httpclient.get(url, :header => common_header)
        records = JSON.parse(response.body)
        records["result"].each do |record|
          if record["filename"].match(params["ip_address"]+ "-") && (record["filename"].match(".zip") || record["filename"].match(".gz"))
            if(!record['meta'].blank?)
              meta = record['meta']
              date_s = "#{meta['report_day']}-#{meta['report_month']}-#{meta['report_year']}"
              date = Date.parse(date_s)
              if date > ARCHIEVES_MONTHS
                @records << record
              end
            end
          end
        end
      end
    rescue  Timeout::Error
      retry
    rescue Exception => e
      Rails.logger.info "*******Exception*********************"
      Rails.logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
    end
    render :text => '{"result":' + @records	.to_json + '}', :content_type => "application/json"
  end

  def common_header
    {'Content-type'=> 'application/json'}
  end

end
