module Api
  module V1
    # API controller for Network Usage subscription
    #
    # @return [String] subscription_id
    # @return [String] subscription
    # @return [String] email
    # @return [String] company_guid
    # @return [String] frequency
    #
    # Using controller inheritance for authentication
    # @see http://www.therailsway.com/2009/4/6/controller-inheritance/
    #
    # @see InternetUsage
    # @see https://wiki.sungardas.corp/display/OSSTOOLS/Zenoss+Report+API+Hooks
    class InternetUsagesController < ToolsTeamController
      def index1
        InternetUsage.bulk_load("3")
        @dats = InternetUsage.all
      end
      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/network_usage.json")
          @metadata = "stubbed network usage"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            InternetUsage.new(r)
          end
        else
          InternetUsage.bulk_load("3")
          @dats = InternetUsage.where(:report_id => "3").all
          @metadata = "network usage subscription"
        end  
      end
    end
  end
end