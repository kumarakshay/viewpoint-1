class Api::V1::CommandsController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :find_device

  def show
  end

  def device_command
    @device_name = @device.name
    @commands = @device.commands unless @device.blank?
  end

  def device_services
  end

  def device_objects
  end

  def device_object_groups
  end

  def device_groups
  end

  private

  def find_device
    if session[:is_omi]
  	  @device = OmiDevice.find_by_asset_guid(params[:asset_guid])
    else
      @device = Device.find_by_asset_guid(params[:asset_guid])
    end
  end
end
