module Api
  module V1
    class MsaSubscriptionsController < ToolsTeamController
      MSA_SUBSCRIPTION_TYPE = { 'Company Device Availability PDF' => '6', 'Company Device Availability CSV' => '7',
                                'Company Network Availability PDF' => '8','Company Network Availability CSV' => '9',
                                'Company Power Availability PDF' => '10', 'Company Power Availability CSV' => '11',
                                'Company Availability PDF' => '12', 'Company Availability CSV' => '13',
                                'Network Path Performance PDF' => '14', 'Group Availability' => '15',
                                'Network Path Performance CSV' => '16'
                              }
      def index
        report = params[:id]
        @metadata = MSA_SUBSCRIPTION_TYPE.key(report) || "unknown param"
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/msa.json")
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            MsaSubscription.new(r)
          end
        else
          MsaSubscription.bulk_load(report)
          @dats = MsaSubscription.where(:report_id => report).all
        end
      end

      MSA_SUBSCRIPTION_TYPE.keys.each do |type|
        # dynamic method for all types msa subscription reports
        define_method type.split(" ").join("").underscore do
          @metadata = type
          if APP_CONFIG['stub_webservice']
            source_records = JSON.parse(File.read("#{Rails.root}/test/fixtures/subscriptions/msa.json"))
            @dats = source_records.map do |r|
              MsaSubscription.new(r)
            end
          else
            MsaSubscription.bulk_load(MSA_SUBSCRIPTION_TYPE[type])
            @dats = MsaSubscription.where(:report_id => MSA_SUBSCRIPTION_TYPE[type]).all(select: [:delivery_time, :email, :company_guid, :subscription_id, :frequency])
          end
          respond_to do |format|
            format.json { render json: filter_json_data }
          end
        end
      end

      private

      def filter_json_data
        {meta: @metadata, status: 200, result: @dats }
      end
    end
  end
end