# not authenticating at the application level 
# basic auth for tools team
class ToolsTeamController < ApplicationController 
  skip_before_filter :authenticated?, :select_branding
  before_filter :authenticate
  protected
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == APP_CONFIG['api_user'] && password == APP_CONFIG['api_password']
    end
  end
end
