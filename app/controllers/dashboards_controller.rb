class DashboardsController < ApplicationController
  before_filter  :authenticated?
  #VUPT1956 Re-enable when required.
  #before_filter  :redirect_everything
  before_filter  :load_instance, :only => [:show, :edit, :update, :destroy]
  before_filter  :set_body_class

  def index
    @dashboards = Dashboard.all(:user_id => current_user.guid)
    @toolname = ' list'
  end

  def show    
    @toolname = 'show'
    @graph_new = Graph.new(:asset =>[""])
    @dash_id = params[:dash_id] 
  end

  def edit
  end

  def new
    @dashboard = Dashboard.new(:graphs =>[])
    @toolname = ': new'
  end

  def create
    @dashboard = Dashboard.new(params[:dashboard])  
    @dashboard.user_id = current_user.guid  
    @toolname = ''
    respond_to do |format|
      if @dashboard.save        
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = ''
    respond_to do |format|
      if @dashboard.update_attributes(params[:dashboard])
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @dashboard.destroy
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully deleted.' }
      else
        format.html { redirect_to @dashboard, notice: 'Unable to delete dashboard as it may be referenced in subscriptions' }
      end
    end    
  end

  private
  def redirect_everything
    redirect_to welcome_path
  end

  def set_body_class
  	@body_class = 'dashboard single-column'
  end

  def load_instance
    begin
      @dashboard_count = Dashboard.where(:user_id => current_user.guid).size
      dashboard_id = params[:id] 
      if (dashboard_id == 'default')
        @dashboard = Dashboard.first(:user_id => current_user.guid, :default => true)
      else 
        @dashboard = Dashboard.first(:id => dashboard_id, :user_id => current_user.guid)   
      end
      unless @dashboard
        @dashboard = Dashboard.new(
          :graphs => [], 
          :name => 'Default Dashboard', 
          :user_id => current_user.guid, 
          :default => true, 
          :description => 'this is your default dashboard.')
        @dashboard.save

      end

      Rails.logger.info("Unable to load dashboard with id  #{@dashboard}")

    rescue Exception => e
      Rails.logger.info("Unable to load dashboard with id  #{e}")
      render 'errors/not_found',  :status => :not_found
    end
  end
end