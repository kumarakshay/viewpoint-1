class WidgetsController < ApplicationController

  # zenoss recent alerts widget
  def recent_zenoss_alerts
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])
    # @events = []
    # @events = @device.events(5,'lastTime') unless @device.nil?
    # respond_to do |format|
    #   format.html  do
    #     render :partial => 'zenoss_recent_alerts'
    #   end
    # end
  end

  # zenoss event totals widge
  def zenoss_event_totals
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])        
    # respond_to do |format|
    #   format.html  do
    #     render :partial => 'zenoss_event_totals'
    #   end
    # end
  end

  # circuit same path widget
  def circuit_same_path
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])
    # respond_to do |format|
    #   format.html  do
    #     if @device and @device.type.eql?('Circuit')
    #       render :partial => 'circuit_same_path'
    #     else
    #       render :nothing => true 
    #     end
    #   end
    # end
  end

  # circuit a z info widget
  def circuit_a_z
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])
    # respond_to do |format|
    #   format.html  do
    #     if @device and @device.type.eql?('Circuit')
    #       render :partial => 'circuit_a_z'
    #     else
    #       render :nothing => true 
    #     end
    #   end
    # end
  end

  # def device cpu info
  def cpu
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])
    # respond_to do |format|
    #   format.html  do
    #     if @device and @device.zen_instance?
    #       z = Zenoss.new
    #       opts = {'object_type' => 'device', 'asset_guids' => [@device.asset_guid], 'start_date' => 'now-1h' }
    #       key = Base64.urlsafe_encode64("#{@device.asset_guid}-device_perf_1h")
    #       @perf_data = Rails.cache.fetch(key, expires_in: 15.minutes) do
    #         data = z.perf_report_router(@device.zen_instance, opts)
    #         data[0] || nil
    #       end
    #       render :partial => 'cpu'
    #     else
    #       render :nothing => true 
    #     end
    #   end
    # end
  end

  # def device memory info
  def memory
    render :nothing => true
    # render :nothing => true unless params[:device_id]
    # @device = Device.find_by_asset_guid(params[:device_id])
    # respond_to do |format|
    #   format.html  do
    #     if @device and @device.zen_instance?
    #       z = Zenoss.new
    #       opts = {'object_type' => 'device', 'asset_guids' => [@device.asset_guid], 'start_date' => 'now-1h' }
    #       key = Base64.urlsafe_encode64("#{@device.asset_guid}-device_perf_1h")
    #       @perf_data = Rails.cache.fetch(key, expires_in: 15.minutes) do
    #         data = z.perf_report_router(@device.zen_instance, opts)
    #         data[0] || nil
    #       end
    #       # This is a temp fix for routers returning incorrect memory util
    #       # see OSSTOOLS-609 - WGM
    #       if @perf_data['memory_available_bytes']['average'].blank? && @perf_data['memory_buffered_bytes']['average'].blank?
    #         @perf_data = nil
    #       end
    #       render :partial => 'memory' 
    #     else
    #       render :nothing => true 
    #     end
    #   end
    # end
  end
  

  # device_time
  def device_time
    respond_to do |format|
      format.html do
        render :partial => 'device_time'
      end
    end
   end

end