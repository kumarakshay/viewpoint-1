class EventsController < ApplicationController
  before_filter lambda { help_menu_name('events') }
  before_filter :omi_user_redirect, only: :index

  # all data is being populated via api
  def index
    if current_user.is_sungard?
      @event_api = "events_company"
      @body_class = "events single-column"
      render :file => 'shared/unauthorized_masq', :layout => 'application'
      return
    else
      @event_api = "events_company"
      @body_class = "events single-column"
    end
  end

  def show
  end

  private
  
  def omi_user_redirect
    redirect_to "/ui/index.html#/events" if session[:is_omi]
  end
end