class Help
  include MongoMapper::Document
  key :title, String
  key :body, String
  key :menu_id, String
  timestamps!

  validates :title, :menu_id, :body, presence: true

  validates :menu_id, :uniqueness => true

  def self.has_associated_menu?(menu_id)
  	Help.where(menu_id: menu_id).count > 0 ? true : false 
  end

end

