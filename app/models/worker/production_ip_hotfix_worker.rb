require 'yajl'
require 'em-synchrony'
require 'em-synchrony/em-http'
module Worker
  class ProductionIpHotfixWorker
    # URI_BASE = "#{SpotlightClient.config.site}/firewalls?production_state.description=Installed-Active"
    URI_BASE = "#{SpotlightClient.config.site}/asset_ip_records?description=Customer%20Production"
    URI_PARAMETERS = "&asset.guid=%s"
    SPOTLIGHT_TOKEN = "Token token=#{SpotlightClient.config.api_key}"

    def self.update_device_records(company_guid = "640A4921-4FCB-4E33-858D-F15C85D63B30")
      Rails.logger.info("Spotlight hotfix to update production ip for company #{company_guid} is started.")
      company = Company.find_by_guid(company_guid)
      if company
        assets = company.asset_list
        count = 0
        not_updated = []
        assets.each do |guid|
          begin
            device =  ::Device.find_by_asset_guid(guid)
            if device
              ip_address = self.get_production_ip_from_spotlight(guid)
              if ip_address
                Rails.logger.info("ip_address received from Spotlight #{ip_address}")
                device.update_attributes(:customer_production_ip => ip_address)
                count = count + 1
              else
                Rails.logger.info("Not able to find ip address for device #{guid}")
                not_updated.push(guid)
              end
            else
              Rails.logger.info("Unable to find device in db with asset_guid #{guid}")
            end
          rescue Exception => e
            Rails.logger.info("Unable to update device details from json asset_guid #{guid}: #{$!}
                \nBacktrace:\n\t#{e.backtrace.join("\n\t")}")
          end
        end
        Rails.logger.info("Number of devices updated for company #{company_guid} is #{count}")
        Rails.logger.info("Spotlight Ip not updated for devices #{not_updated}")
      else
        Rails.logger.info("Company not found with guid #{company_guid}")
      end
    end

    def self.get_production_ip_from_spotlight(asset_guid)
      uri_string = URI_BASE + URI_PARAMETERS % [asset_guid]
      uri = URI.parse(uri_string)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Get.new(uri.request_uri)
      req['Authorization'] = SPOTLIGHT_TOKEN
      begin
        fetch_start = Time.now()
        res = http.request(req)
      rescue SocketError => e
        puts "unable to contact Spotlight @ uri_string.  Giving up!"
      rescue Net::ReadTimeout => e
        puts "http.request() timed out(Net::ReadTimeout): #{e}"
        exit(-1)
      end
      fetch_end = Time.now()
      elapsed_time = fetch_end - fetch_start
      body = JSON.parse(res.body)
      if body["result"] && body["result"][0]["ip_address"]
        return body["result"][0]["ip_address"]
      else
        return false
      end
    end
  end
end
