module Worker
class DeviceDetailReportWorker 
  include MongoMapper::Document

  key :company_guid, String  
  key :group_name, String
  key :description, String
  key :asset_list, Array
  
  def self.bulk_load
    before_count = Worker::DeviceDetailReportWorker.count
    start_time = Time.now
    walk_the_table
    duration = Time.now - start_time
    after_count = Worker::DeviceDetailReportWorker.count
    print_log(DeviceDetailReportWorker,before_count, after_count, duration )
  end  

  def self.walk_the_table
    self.collection.remove({});
    subaru = "D9EFA335-F8E5-4271-9541-BB5D7590AEE5" 
    group_details = Group.collection.find({'company_guid'=>{'$in'=>[subaru]}})
    assets_name = []
    group_details.each do |g|
      assets = g['asset_list']
      assets.length.times do |i|
        y = get_device_name(assets[i])
        assets_name << y
      end 
      doc = {company_guid: g['company_guid'], group_name: g['name'], asset_list: assets_name}
      self.collection.insert(doc)
      assets_name = []
    end
  end

  def self.get_device_name(asset_guid)
    z = Device.where(asset_guid: asset_guid).first
    if z
      z.name
    end
  end
  
end
end