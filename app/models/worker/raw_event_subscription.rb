module Worker
  class RawEventSubscription
 	  include MongoMapper::Document
	  key :idsubscriptionid
	  key :companyname
	  key :companyguid
	  key :assetguid
	  key :assetname
	  key :severity
	  key :contactname
	  key :contactguid
	  key :emailtype
	  key :emailaddress
	  timestamps!
  end
end
