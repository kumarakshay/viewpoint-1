module Worker
  class RawCompanyAssetsWorker 
    include MongoMapper::Document
    extend WebServiceLoad

    self.web_service_class = ServiceCenter
    self.load_service_method = 'company_monitored_devices'

    key :ASSET_GUID
    key :NAME
    key :COMPANY_GUID
    key :CONTRACTING_GUID
    key :OWNER_GUID
    key :ASSET_TYPE

    BATCH_SIZE = 5000

    def self.populate(options)
      self.load_from_webservice(true, &Proc.new)
    end

    def self.bulk_load
      before_count = RawCompanyAssetsWorker.count
      start_time = Time.now
      logger.debug('starting')
      TmpRawCompanyAssetsWorker.delete_all
      big_bacon = []
      populate({}) do|row|
        big_bacon << row
        if big_bacon.size > BATCH_SIZE
          TmpRawCompanyAssetsWorker.collection.insert(big_bacon)
          big_bacon=[]
        end
      end
      unless big_bacon.blank?
        TmpRawCompanyAssetsWorker.collection.insert(big_bacon)
        big_bacon = nil
      end
      unless mailed_and_verify_record?(RawCompanyAssetsWorker)
        RawCompanyAssetsWorker.collection.drop
        TmpRawCompanyAssetsWorker.collection.rename("worker.raw_company_assets_workers")

        duration = Time.now - start_time 
        after_count = RawCompanyAssetsWorker.count
        print_log('RawCompanyAssetsWorker',before_count, after_count, duration )
        logger.debug("ending: #{Time.now - start_time}")
        Worker::RawCompanyAssetsWorker.collection.create_index([['COMPANY_GUID',  Mongo::ASCENDING]])
        Worker::RawCompanyAssetsWorker.collection.create_index([['CONTRACTING_GUID',  Mongo::ASCENDING]])
        Worker::RawCompanyAssetsWorker.collection.create_index([['OWNER_GUID',  Mongo::ASCENDING]])  
        return true  
      else
        Rails.logger.info "RawCompanyAssetsWorke bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
        p "RawCompanyAssetsWorke bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
        return false
      end
    end
  end
  class TmpRawCompanyAssetsWorker
    include MongoMapper::Document
    extend WebServiceLoad
    key :ASSET_GUID
    key :NAME
    key :COMPANY_GUID
    key :CONTRACTING_GUID
    key :OWNER_GUID
    key :ASSET_TYPE
  end
end