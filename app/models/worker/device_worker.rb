
require 'yajl'
require 'em-synchrony'
require 'em-synchrony/em-http'
require 'sgtools/zenoss/device'


module Worker
class DeviceWorker 
  include MongoMapper::Document
  BATCH_SIZE = 5000
  extend WebServiceLoad
  
  key :asset_guid
  key :company_use
  key :name
  key :ip_address_formatted
  key :collector
  key :ip_address
  key :location
  key :location_uid
  key :ping_flag
  key :priority
  key :production_state
  key :serial_number
  key :snmp_flag
  key :tag_number
  key :zenoss_instance
  key :clear
  key :critical, Integer, default: 0
  key :debug, Integer, default: 0
  key :error, Integer, default: 0
  key :information, Integer, default: 0
  key :ordered, Integer, default: 0
  key :warning, Integer, default: 0
  key :snmp_sys_name
  key :customer_production_ip
  key :command_ids, Array
  key :na_DeviceID
  timestamps!

  DEVICES = []
  
  # avoid overhead and just assign to a hash vs d.attributes
  def self.hash_from_zenoss(h={})
    d = {}
    d[:name] = h['name']
    d[:ip_address] = h['ipAddress']
    d[:ip_address_formatted] = h['ipAddressString']
    d[:uid] = h['uid']
    d[:zenoss_instance] = h['zenoss_instance']
    d[:priority] = h['priority']
    d[:serial_number] = h['serialNumber']
    d[:production_state] = h['productionState']
    d[:tag_number] = h['tagNumber']
    d[:collector] = h['collector']
    d[:asset_guid] = h['cAssetGUID']
    d[:snmp_sys_name] = h['snmpSysName']
    # d[:company_use]  = CompanyAsset.company_use_for_asset_guid(d[:asset_guid])
    unless h['location'].blank?
      d[:location] =  h['location']['name']
      d[:location_uid] = h['location']['uid']
    end
    unless h['hwManufacturer'].blank?
      d[:hw_manufacturer] = h['hwManufacturer']['name']
      d[:hw_manufacturer_uid] = h['hwManufacturer']['uid']
    end
    unless h['hwModel'].blank?
      d[:hw_model] =  h['hwModel']['name']
      d[:hw_model_uid] = h['hwModel']['uid']
    end
    unless h['osManufacturer'].blank?
      d[:os_manufacturer] =  h['osManufacturer']['name']
      d[:os_manufacturer_uid] = h['osManufacturer']['uid']
    end
    unless h['osModel'].blank?
      d[:os_model] =  h['osModel']['name']
      d[:os_model_uid] = h['osModel']['uid']
    end
    unless h['systems'].blank?
      s = h['systems'].first['name']
      m = s.match(/\/([^\/]+)$/)
      if m
        d[:company_use] = m[1]
      else
        d[:company_use] = s
      end
      d[:system_uid] = h['systems'].first['uid']
    end
    self::DEVICES << d
    d
  end

  # the data is being pulled from zenoss
  def self.build_new_collection
    start_time = Time.now
    logger.info('starting DeviceWorker bulk_load')
    before_device_count = Device.count
    self.delete_all
    common_header =     {
        'Content-type'=> 'application/json',
        'Authorization'=> 'Basic ' + Viewpoint::Application::ZENOSS_CONFIG_WORKER.auth
        }
    EventMachine.synchrony do 
      api_path = '/zport/devices_stream'
      totals={}  
      http_response = {}
      big_bacon = {}
      parser={}
      done_processing={}
      Viewpoint::Application::ZENOSS_CONFIG_WORKER.urls.each do |z_instance, url|
        big_bacon[z_instance] =[]
        parser[z_instance] = Yajl::Parser.new
        parser[z_instance].on_parse_complete = lambda{|o| 
          o['zenoss_instance'] = z_instance
          h = self.hash_from_zenoss(o)
          big_bacon[z_instance] << h
          if big_bacon[z_instance].size > BATCH_SIZE
            # p "big bacon size is :#{z_instance}  #{big_bacon[z_instance].size}"
            Rails.logger.info("big bacon size is :#{z_instance}  #{big_bacon[z_instance].size}")
              self.collection.insert(big_bacon[z_instance])
              big_bacon[z_instance]= []
          end
        }
        http_response[z_instance] = EventMachine::HttpRequest.new(url + api_path, {verify_peer: false}).aget head: common_header
        http_response[z_instance].errback do
          Rails.logger.error(http_response[z_instance].response_header.status)
          EM.stop
        end        
        http_response[z_instance].stream do |chunkie|
          begin
            parser[z_instance] << chunkie
          rescue Exception=> e
            Rails.logger.error(e)
          end
        end
        http_response[z_instance].callback{
          unless big_bacon[z_instance].blank?
            self.collection.insert(big_bacon[z_instance])
            # p "big bacon size is :#{z_instance}  #{big_bacon[z_instance].size}"
            Rails.logger.info("big bacon size is :#{z_instance}  #{big_bacon[z_instance].size}")
            big_bacon[z_instance]= nil
          end
          done_processing[z_instance]=true
          done_proc = Viewpoint::Application::ZENOSS_CONFIG_WORKER.urls.reject{|z_instance,urls| done_processing[z_instance]}
          EM.stop if done_proc.blank?
        }
      end
    end
    # write detail about invalid devices 
    File.open("log/invalid_devices.log", "w") do |f|
      devices =   DeviceWorker.where(:$or => [{:asset_guid => nil },{:asset_guid => ''}, {:asset_guid =>' '}])
      f.write("------------------#{Time.now}---------------------")
      devices.each do |device|
        f.write(device.inspect)
      end
    end
    DeviceWorker.collection.remove({asset_guid: ' '})
    DeviceWorker.collection.remove({asset_guid: ''})
    DeviceWorker.collection.remove({asset_guid: nil})
    device_count = self::DEVICES.count
    invalid_device = self::DEVICES.count - DeviceWorker.count
    self.ensure_index(:name)
    self.ensure_index(:ip_address)
    self.ensure_index(:asset_guid)
    unless mailed_and_verify_record?(Device)
      #This will find all the firewall devices from Spotlight and mark them in viewpoint.
      Worker::FirewallWorker.decorate_firewall_devices
      # # drop the production collection and rename the worker to production
      Worker::ProductionIpWorker.decorate_production_ip
      # This bulk load add command collection also add device related supperting collection  
      Worker::CommandRunnerWorker.build_new_collection(self)
      Device.collection.drop
      DeviceWorker.collection.rename('devices')
      duration = Time.now - start_time
      after_count = Device.count
      logger.info("ending: #{duration}")
      Rails.logger.info("Before bulk load Device Count: #{before_device_count}")
      Rails.logger.info("After bulk load Device Count: #{after_count}")
      p "Before bulk load Device Count: #{before_device_count}"
      p "After bulk load Device Count: #{after_count}"
      Rails.logger.info "Total No of device comming from Zenos: #{device_count}  " 
      p "Total No of device comming from Zenos: #{device_count}"
      Rails.logger.info "Total No of devices whose asset_guid is either nil or '' or ' ': #{invalid_device}"
      p "Total No of devices whose asset_guid is either nil or '' or ' ': #{invalid_device}"
      Rails.logger.info "Time taken to complete device bulk load: #{duration}"
      p "Time taken to complete device bulk load: #{duration}"
    else
      Rails.logger.info "DeviceWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
      p "DeviceWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
    end
  end

  def self.remove_crufty_devices
    Device.find_each{|d| r = RawCompanyAssetsWorker.first(ASSET_GUID: d.asset_guid); delete_it << d.asset_guid if r.blank?}
    Device.collection.remove({asset_guid:{:$in=>delete_it}})
  end
end
end
