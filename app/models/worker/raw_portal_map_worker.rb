module Worker
class RawPortalMapWorker
  include MongoMapper::Document
  extend WebServiceLoad

  key :companys_po
  key :parent_company_oracle_id
  key :company_name
  key :total_records
  key :current_record
  key :u_status
  key :companys_poid
  key :status_code_description
  key :id_company_type_id
  key :parent_portal_name
  key :parent_portal_record_status
  key :parent
  key :parent_portal_description
  key :portal_record_status
  key :parent_company_snt
  key :id_status_code_id
  key :parent_company_name
  key :portal_create_date
  key :description
  key :company_is_active_flag
  key :modified_date
  key :company_type_description
  key :id_company_id
  key :u_guid
  key :u_portal
  timestamps!

  self.web_service_class = ServiceCenter
  self.load_service_method = 'portal_map'
  BATCH_SIZE = 5000

  def self.load_from_service_center
    bulk_load(ignore_keys: true)
  end

  def self.populate(options)
    self.load_from_webservice(true){|o|yield self.load_translation(o)}
  end

  def self.build_new_collection
    before_count = PortalMapWorker.count
    start_time = Time.now
    RawPortalMapWorker.bulk_load
    unless mailed_and_verify_record?(::PortalMapWorker) 
      PortalMapWorker.collection.drop
      RawPortalMapWorker.collection.rename('portal_map_workers')
      duration = Time.now - start_time
      # Adding indexex to new table.
      PortalMapWorker.collection.create_index([['parent_company_snt',  Mongo::ASCENDING]])
      after_count = PortalMapWorker.count
      p "Starting Bulk load: #{Time.now}"
      print_log(PortalMapWorker,before_count, after_count, duration )
    else
      Rails.logger.info "PortalMapWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']} "
      p "PortalMapWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
    end
  end

  def self.load_translation(h={})
    tn = Time.now
    { companys_po: h['COMPANYSPO'],
      parent_company_oracle_id: h['PARENTCOMPANYORACLEID'],
      company_name: h['COMPANYNAME'],
      total_records: h['TOTAL_RECORDS'],
      current_record: h['CURRENT_RECORD'],
      u_status: h['U_STATUS'],
      companys_poid: h['COMPANYSPOID'],
      status_code_description: h['STATUSCODEDESCRIPTION'],
      id_company_type_id: h['IDCOMPANYTYPEID'],
      id_company_id: h['IDCOMPANYID'],
      parent: h['PARENT'],
      parent_company_name: h['PARENTCOMPANYNAME'],
      parent_portal_name: h['PARENTPORTALNAME'],
      parent_portal_description: h['PARENTPORTALDESCRIPTION'],
      parent_portal_record_status: h['PARENTPORTALRECORDSTATUS'],
      parent_company_snt: h['PARENTCOMPANYSNT'],
      portal_record_status: h['PORTAL_RECORDSTATUS'],
      id_status_code_id: h['IDSTATUSCODEID'],
      portal_create_date: h['PORTALCREATEDATE'],
      description: h['DESCRIPTION'],
      company_is_active_flag: h['COMPANY_ISSACTIVEFLAG'],
      modified_date: h['MODIFIEDDATE'],
      company_type_description: h['COMPANYTYPEDESCRIPTION'],
      u_guid: h['U_GUID'],
      u_portal: h['U_PORTAL'],
      created_at: tn, 
      updated_at: tn }
  end

  def self.bulk_load
    start_time = Time.now
    logger.debug('starting')
    self.delete_all
    big_bacon = []
    populate({}) do|row|
      big_bacon << row
      if big_bacon.size > BATCH_SIZE
        self.collection.insert(big_bacon)
        big_bacon=[]
      end
    end
    unless big_bacon.blank?
      self.collection.insert(big_bacon)
      big_bacon = nil
    end
    logger.debug("ending: #{Time.now - start_time}")
  end

  # loop through the collection and display the parent / child relationship
  def self.walk_the_collection
    dat = PortalMapWorker.collection.find({'parent_company_snt'=>{'$nin'=>['sung']}}).sort('parent_company_name')
    dat.each do |p|
      Rails.logger.info("#{p['parent_company_name']} (#{p['parent_company_snt']}) => #{p['company_name']}")
    end
    Rails.logger.info("Finished processing #{dat.count} rows.")
  end

  def self.describe_relationship(parent_company_snt)
    dat = PortalMapWorker.collection.find({'parent_company_snt'=>{'$in'=>[parent_company_snt]}}).sort('parent_company_name')
    dat.each do |p|
      Rails.logger.info("#{p['parent_company_name']} (#{p['parent_company_snt']}) => #{p['company_name']}")
    end
    Rails.logger.info("Total children: #{dat.count} rows.")
  end

  def self.describe_child_relationship(company_snt)
    dat = PortalMapWorker.collection.find({'u_portal'=>{'$in'=>[company_snt]}}).sort('parent_company_name')
    dat.each do |p|
      Rails.logger.info("#{p['parent_company_name']} (#{p['parent_company_snt']}) => #{p['company_name']}")
    end
    Rails.logger.info("Total children: #{dat.count} rows.")
  end

  def self.data_for_export
    dat = PortalMapWorker.all(:parent_company_snt.ne => 'sung')
  end
end
end