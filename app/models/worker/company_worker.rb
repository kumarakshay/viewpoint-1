require 'em-synchrony/em-mongo'
require 'csv'
module Worker
  class CompanyWorker

    include MongoMapper::Document
    # extend FilterableMongo
    extend WebServiceLoad

    key :guid
    key :oracle_id
    key :name
    key :support_org
    key :sg_id
    key :site_url
    key :sfdc_product_code
    key :asset_list, Array
    key :active, Boolean, default: true
    key :deleted_at, Time
    key :zenoss_presence, Boolean, default: false
    key :user_count, Integer, default: 0
    key :us_only, Boolean, default: false
    key :is_tdr, Boolean, default: false
    key :is_apm, Boolean, default: false
    key :sys_id
    timestamps!

    self.web_service_class = ServiceCenter
    self.load_service_method = 'company_list'
    BATCH_SIZE = 5000

    def self.load_translation(h={})
      tn = Time.now
      sfdc_code = h['SFDC_PRODUCT_CODE'].empty? ? [] : h['SFDC_PRODUCT_CODE']
      h['VCHCOMPANYNAME'] = "Sungard AS" if h['IDCOMPANYID'] && h['IDCOMPANYID'] == "76C6F530-40C1-446D-A5D5-A66E78605149"
      {name: h['VCHCOMPANYNAME'].strip, guid: h['IDCOMPANYID'], snt: h['VCHSNT'], sfdc_product_code: sfdc_code, sg_id: h['SGID'], site_url: h['SITEURL'], oracle_id: h['VCHORACLEID'], support_org: h['SUPPORTORG'], sys_id: h['SNCOMPSYSID'], created_at: tn, updated_at: tn}
    end

    def self.ignore_columns
      ['active']
    end

    def self.load_from_service_center
      bulk_load(ignore_keys: true)
    end

    def self.populate(options)
      self.load_from_webservice(true){|o|yield self.load_translation(o)}
    end

    def self.build_new_collection
      before_count = Company.count
      start_time = Time.now
      # asset_load_successfully =RawCompanyAssetsWorker.bulk_load
      CompanyWorker.bulk_load
      unless mailed_and_verify_record?(Company)
        CompanyWorker.build_assets
        # CompanyWorker.add_children
        CompanyWorker.add_user_count
        #Update the company collection and set us_only flag to true for the companies received from SN with rest API
        CompanyWorker.marked_us_based_companies
        #PORTALOMS-2495 - capture correct oracle Id for cgi companies
        CompanyWorker.fix_oracle_id_for_companies
         #PORTALOMS-1573
        CompanyWorker.mark_tdr_companies
        #PORTALOMS-3529
        CompanyWorker.mark_apm_companies
        # Discussed with Amit - the call to process_old_companies should not be in posthaste
        # CompanyWorker.process_old_companies
        # CompanyWorker.zenoss_presence
        CompanyWorker.marked_cloud_portal_companies
        CompanyWorker.swap_collections
        duration = Time.now - start_time
        after_count = Company.count
        print_log(Company,before_count, after_count, duration )
      else
        Rails.logger.info "CompanyWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
        p "CompanyWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
      end
    end

    def self.bulk_load
      self.delete_all
      big_bacon = []
      populate({}) do|row|
        big_bacon << row
        if big_bacon.size > BATCH_SIZE
          self.collection.insert(big_bacon)
          big_bacon=[]
        end
      end
      unless big_bacon.blank?
        self.collection.insert(big_bacon)
        big_bacon= nil
      end
      self.ensure_index(:guid)
    end

    def self.public_columns_for_select
      exclude = []
      s = (self.column_names - exclude)
      s.map do |c|
        "#{c}"
      end.join(',')
    end

    def self.build_assets
      batch_size = 1000
      companies = self.collection.find({'guid'=>{'$nin'=>['76C6F530-40C1-446D-A5D5-A66E78605149','E71312DC-AAF1-4BF2-941D-B297B1D326AB']}},{fields: ['guid'], batch_size: batch_size})
      unless companies.blank?
        companies.each do |c|
          build_asset(c['guid'])
        end
      else
        p ">>>>>>>>>>>>> No Companies Found >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
      end
    end

    def self.build_asset(guid)
      # assets = RawCompanyAssetsWorker.where(:$or=>[{COMPANY_GUID: guid},{CONTRACTING_GUID: guid},{OWNER_GUID: guid}]).fields('ASSET_GUID')
      assets = OmiDevice.where(:company_use_guid => guid.downcase).all.collect(&:asset_guid)
      filtered_assets = assets.reject(&:blank?)
      al = []
      h = {:guid => guid}
      unless filtered_assets.blank?
        al = filtered_assets.map(&:downcase).uniq
        CompanyWorker.collection.update(h,{'$set'=> {'asset_list'=> al}})
      else
        p ">>>>>>>>>>>> NO Assets Found for the Company #{guid} >>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        CompanyWorker.collection.update(h,{:$set=>{delete_me: true}})
      end
    end

    def self.add_children
      batch_size = 1000
      companies = self.collection.find({'guid'=>{'$nin'=>['76C6F530-40C1-446D-A5D5-A66E78605149','E71312DC-AAF1-4BF2-941D-B297B1D326AB']}},{fields: ['guid'], batch_size: batch_size})
      companies.each do |c|
        build_children(c['guid'])
      end
    end

    def self.add_user_count
      batch_size = 1000
      companies = self.collection.find({},{fields: ['guid'], batch_size: batch_size})
      companies.each do |c|
        user_count = User.collection.find({company_guid: c['guid'], deleted_at: nil}).count
        CompanyWorker.collection.update({:guid => c['guid']},{:$set=>{user_count: user_count}})
      end
    end

    def self.marked_cloud_portal_companies
      file = "#{Rails.root}/db/data/cloud_portal/ecscust.csv"
      CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
        company_snt = row.to_hash['Customer SNT'].to_s.strip
        unless company_snt.blank?
          CompanyWorker.collection.update({:snt => company_snt},{:$set=>{cloud_portal: true}})
        end
      end
      puts "Total company updated for cloud portal is #{CompanyWorker.where(:cloud_portal => true).all.count}"
    end


    def self.fix_oracle_id_for_companies
      file = "#{Rails.root}/db/data/hotfix/CGI_customers.csv"
      CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
        company_guid = row.to_hash['Guid'].to_s.strip
        oracle_id = row.to_hash['Oracle_id'].to_s.strip
        unless company_guid.blank?
          CompanyWorker.collection.update({:guid => company_guid},{:$set=>{oracle_id: oracle_id}})
        end
      end
    end

    def self.marked_us_based_companies
      sn_client =  ServiceNowRestClient.new
      results = sn_client.us_based_companies
      unless results.blank?
        results.each do |result|
          unless result['u_portal_id'].blank?
            CompanyWorker.collection.update({:guid => result['u_portal_id']},{:$set=>{us_only: true}})
          end
        end
      end
    end

    def self.mark_tdr_companies
      file = "#{Rails.root}/db/data/import/tdr_companies.csv"
      snt_code = []
      begin
        if  Rails.env != 'production'
          CompanyWorker.collection.update({},{:$set => {:is_tdr => true}},{:multi => true})
        else
          CSV.foreach(file, :headers => true) do |row|
            snt_code.push(row.to_hash['SNT Code'].to_s) unless row.to_hash['SNT Code'].blank?
          end
        end
        unless snt_code.blank?
          CompanyWorker.collection.update({:snt => {:$in => snt_code}},{:$set => {:is_tdr => true}},{:multi => true})
          Rails.logger.info"^^^^^^^^^^^^^^^ Number of companies whitelisted for tdr ^^^^^#{snt_code.length}"
          Rails.logger.info"^^^^^^^^^^^^^^^^^^^^ Details of tdr flag updated companies ^^^#{snt_code}"
        end
      rescue Exception => e
        logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      end
    end

    def self.mark_apm_companies
      file = "#{Rails.root}/db/data/import/apm_companies.csv"
      snt_code = []
      begin
        CSV.foreach(file, :headers => true) do |row|
          snt_code.push(row.to_hash['SNT Code'].to_s) unless row.to_hash['SNT Code'].blank?
        end
        unless snt_code.blank?
          CompanyWorker.collection.update({:snt => {:$in => snt_code}},{:$set => {:is_apm => true}},{:multi => true})
          Rails.logger.info"^^^^^^^^^^^^^^^ Number of companies whitelisted for APM ^^^^^#{snt_code.length}"
          Rails.logger.info"^^^^^^^^^^^^^^^^^^^^ Details of APM flag updated companies ^^^#{snt_code}"
        end
      rescue Exception => e
        logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      end
    end

    def self.build_children(guid)
      parent_assets = []
      child_assets = []
      all_child_assets = []
      all_parent_assets = []
      c = self.where(guid: guid).first
      parent_assets = c.asset_list
      dat = PortalMapWorker.where(parent_company_snt: c.snt).all
      dat.each do |p|
        child_assets = get_asset_list(p['id_company_id'])
        if child_assets.any?
          all_child_assets = all_child_assets | child_assets
        end
      end
      if all_child_assets.any?
        all_parent_assets = parent_assets | all_child_assets
        h = {:guid => guid}
        CompanyWorker.collection.update(h,{'$set'=> {'asset_list'=> all_parent_assets}})
      end
    end

    def self.get_asset_list(guid)
      asset_list = CompanyWorker.where(guid: guid).first.asset_list rescue []
    end

    def self.zenoss_presence
      rc = SGTools::Zenoss::ReportingCompany.new("", :z_instance=> "cust2")
      res = JSON.parse(rc.all_company) unless rc.all_company.blank?
      unless res.empty?
        guids = res["result"]["data"].map(&:upcase) unless  res["result"]["data"].blank?
        companies = CompanyWorker.where(:guid => {:$in => guids})
        companies.each do |company|
          company.update_attributes({:zenoss_presence => true})
        end
      end
    end

    def self.swap_collections
      Company.collection.drop
      CompanyWorker.collection.rename('companies')
    end

    # def self.process_old_companies
    #   CompanyWorkerStash.build_record
    #   stash_companies = CompanyWorkerStash.all
    #   stash_companies.each do |company|
    #     worker_company = Worker::CompanyWorker.first(:guid => company.guid)
    #     unless worker_company.blank?
    #       worker_company.active = company.info["active"]
    #       worker_company.deleted_at = Time.zone.parse(company.info["deleted_at"])
    #       worker_company.save
    #     end
    #   end
    # end

    # # this is not being used
    # def self.count_users
    #   batch_size = 1000
    #   companies = self.collection.find({'guid'=>{'$nin'=>['76C6F530-40C1-446D-A5D5-A66E78605149','E71312DC-AAF1-4BF2-941D-B297B1D326AB']}},{fields: ['guid'], batch_size: batch_size})
    #   start_time = Time.now
    #   companies.each do |c|
    #     users = User.where(company_guid: c['guid']).count
    #     h = {:_id => c['_id']}
    #     if users == 0
    #       CompanyWorker.collection.update(h,{:$set=>{delete_me: true}})
    #     end
    #   end
    # end
  end
end
