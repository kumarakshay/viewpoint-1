module Worker
  class CommandRunnerDevice

  	def self.bulk_load(model)
      begin
        executionId = get_execution_id
        unless executionId.blank?
          response = fetch_variables(executionId)
          unless response.blank? or response["flowOutput"].blank?
            jsonClean = URI.decode(response["flowOutput"]["deviceResult"])
            records = JSON.parse(jsonClean)
            update_device_records(records, model) unless records.blank? 
          end 
        end
      rescue => e
        Rails.logger.info "Error on command runner device bulk load: " + e.to_s
      end
    end

    def self.get_execution_id
      hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => "executions", :method => "POST" , :query => fetch_device_params})
      post_res = hpClient.execute
      executionId = post_res["executionId"]
    end

    def self.fetch_variables(executionId)
      pathname = "executions/" + executionId +  "/execution-log"
      get_hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => pathname, :method => "GET" , :query => ""})
      response = get_hpClient.execute
    end

  	def self.update_device_records(records, model)
      count = 0
      records.each do |row|
        begin          
          unless row['primaryIPAddress'].blank?
            device =  model.find_by_ip_address_formatted(row['primaryIPAddress'])
            if device 
              command_ids = row["cmd_commands"].split(",").delete_if{|i| i=='0' or i=='null'}
              dev_objects = row["dev_objects"].split(",") unless row["dev_objects"].blank?
              dev_groups  = row["dev_groups"].split(",") unless row["dev_groups"].blank?
              dev_services = row["dev_services"].split(",") unless row["dev_services"].blank?
              device.update_attributes(:command_ids => command_ids, :dev_objects=> dev_objects,
                                       :dev_groups => dev_groups, :dev_services => dev_services, 
                                       :na_DeviceID => row['na_DeviceID'] )
              count+=1
            else
              Rails.logger.info("Unable to find device with ip_address_formatted #{row['primaryIPAddress']}")
            end
          end
        rescue Exception => e          
          Rails.logger.error "Unable to update device details from json #{row}: #{$!}
              \nBacktrace:\n\t#{e.backtrace.join("\n\t")}"
        end
      end
      return count
    end

    def self.fetch_device_params
      {
      "uuid"=>"97255f55-a462-4a8b-8e14-2225a0964366", 
      "runName"=> "ViewPoint - Fetch Qualified Devices",
      "logLevel"=> "DEBUG", 
      "inputs"=>{}
      }
    end 
    
  end
end
