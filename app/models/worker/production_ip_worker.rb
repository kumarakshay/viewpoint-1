require 'yajl'
require 'em-synchrony'
require 'em-synchrony/em-http'
module Worker
  class ProductionIpWorker
    # URI_BASE = "#{SpotlightClient.config.site}/firewalls?production_state.description=Installed-Active"
    URI_BASE = "#{SpotlightClient.config.site}/asset_ip_records?description=Customer%20Production"
    URI_PARAMETERS = "&__limit=%d&__offset=%d"
    SPOTLIGHT_TOKEN = "Token token=#{SpotlightClient.config.api_key}"
    LIMIT = 500
    OFFSET = 0
    
    def self.update_device_records(record)
      result = record['result']
      count = 0
      result.each do |row|
        begin          
          unless row['guid'].blank?
            device =  Worker::DeviceWorker.find_by_asset_guid(row['guid'])
            if device 
              device.update_attributes(:customer_production_ip => row['ip_address'])
              count+=1
            else
              Rails.logger.info("Unable to find device with asset_guid #{row['guid']}")
            end
          end
        rescue Exception => e          
          Rails.logger.error "Unable to update device details from json asset_guid #{row}: #{$!}
              \nBacktrace:\n\t#{e.backtrace.join("\n\t")}"
        end
      end
     return count
    end

    def self.decorate_production_ip
      begin 
        start_time = Time.now
        found = true
        firewall_count = 0
        offset = OFFSET
        limit = LIMIT
        vp_updated_record = 0
        begin
          record = get_production_ip_from_spotlight(limit, offset)
          if record['result'].blank?
            found = false
          end
          record['result'].each do |firewall|
            firewall_count += 1
          end
          offset += limit  
          vp_record = update_device_records(record)
          vp_updated_record += vp_record
        end while found == true  
        duration = Time.now - start_time
        Rails.logger.info "Time taken to complete producttion ip fetch bulk load: #{duration}"
        p "Time taken to complete producttion ip fetch bulk load: #{duration}"
        puts "Total records received from spotlight: #{firewall_count}"
        Rails.logger.info "Total asset ip records received from spotlight: #{firewall_count}"
        Rails.logger.info("Total production ip records updated in viewpoint #{vp_updated_record}")
      rescue => e
        Rails.logger.error "Unable to fetch production ip #{e.backtrace}"
      end

    end

    def self.get_production_ip_from_spotlight(limit, offset)
      uri_string = URI_BASE + URI_PARAMETERS % [limit, offset]
      uri = URI.parse(uri_string)
      Rails.logger.debug uri.request_uri
      
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Get.new(uri.request_uri)
      req['Authorization'] = SPOTLIGHT_TOKEN
     
      retry_attempts = 5 
      begin
        fetch_start = Time.now()
        res = http.request(req)
      rescue SocketError => e
        if retry_attempts > 0
          retry_attempts -= 1
          puts "Failed to contact Spotlight retrying (#{e})"
          sleep 3

          retry
        else
          puts "unable to contact Spotlight @ uri_string.  Giving up!"
          raise
        end
      rescue Net::ReadTimeout => e
        puts "http.request() timed out(Net::ReadTimeout): #{e}"
        exit(-1)
      end
      fetch_end = Time.now()
      elapsed_time = fetch_end - fetch_start
      body = JSON.parse(res.body)
      return body   
    end

  end  
end