module Worker
  class CommandRunnerVariable
    include MongoMapper::Document
    key :command_id
    key :name, String
    key :description, String
    key :variableData, String
 
    timestamps!

  	def self.bulk_load(model)
      begin
        CommandRunnerVariable.delete_all
        all_commands = all_command_ids(model)
        all_commands.each do |command_id|
          executionId = get_execution_id(command_id)
          unless executionId.blank?
            response = fetch_variables(executionId)
            unless response.blank? or response["flowOutput"].blank?
              records = response["flowOutput"]["DeviceResult"]
              update_command_collection(records) unless records.blank?  
            end
          end
        end
        Command.collection.drop
        CommandRunnerVariable.collection.rename('commands')
      rescue => e
         Rails.logger.info "Error on command runner variable bulk load: " + e.to_s
      end
    end

    def self.all_command_ids(model)
      all_command_ids = model.where({:command_ids => {:$exists => true, :$ne =>[]}}).collect(&:command_ids).flatten.uniq
    end

    def self.get_execution_id(command_id)
      updated_variable_params = update_variable_params(command_id)
      hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => "executions", :method => "POST" , :query => updated_variable_params})
      post_res = hpClient.execute
      executionId = post_res["executionId"]
    end

    def self.fetch_variables(executionId)
      pathname = "executions/" + executionId +  "/execution-log"
      get_hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => pathname, :method => "GET" , :query => ""})
      response = get_hpClient.execute
    end


    def self.update_command_collection(records)
      count = 0
      data = records.split("|")
      c = Worker::CommandRunnerVariable.new(:command_id => data[0], :name => data[2], :description => data[1], :variableData => data[3])     
      c.save
    end

    def self.update_variable_params(command_id)
      params = variable_params
      params["inputs"]["customScriptID"] = command_id 
      params
    end
    
    def self.variable_params
      {
        "uuid"=>"4a998e3d-2b1f-4447-8e27-274a0ccf3eca", 
        "runName"=> "ViewPoint - Fetch Command Variables",
        "logLevel"=> "DEBUG", 
        "inputs"=>
        {
          "customScriptID"=>""
        }
      }
    end

  end
end
