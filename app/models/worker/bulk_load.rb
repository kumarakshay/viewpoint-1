module Worker
	class BulkLoad
	  include MongoMapper::Document
	  key :name, String
	  key :success, Boolean
	  key :run_at, Time
	  key :count

	  timestamps!

		def self.run
			records = Worker::BulkLoad.where(:success => false, :run_at => {:$gt => Time.now.beginning_of_day})
			records.each do |record|
				p "Running bulk load #{record.name}"
				model = record.name
				model.constantize.send(:build_new_collection)
			end
		end
	end
end