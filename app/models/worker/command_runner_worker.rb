module Worker
  class CommandRunnerWorker

    include MongoMapper::Document
    extend WebServiceLoad

    def self.build_new_collection(model)
      start_time = Time.now
      count = Command.count
    	begin 
	    	Worker::CommandRunnerDevice.bulk_load(model) #it will update devices with supported comand_ids
	    	Worker::CommandRunnerVariable.bulk_load(model)
	   	rescue => e
	   		Rails.logger.info "command runner bulk load failed" + e.to_s 
	   	end
      duration = Time.now - start_time
      Rails.logger.info("Before bulk load Command Count: #{count}")
      Rails.logger.info("After bulk load Command Count: #{Command.count}")
      p "Before bulk load Command Count: #{count}"
      p "After bulk load Command Count: #{Command.count}"
      Rails.logger.info "Time taken to complete Command bulk load: #{duration}"
      p "Time taken to complete command bulk load: #{duration}"
    end
  end
end