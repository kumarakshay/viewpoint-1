module Worker
  class CompanyWorkerStash
    include MongoMapper::Document
    key :guid
    key :info, Hash
		 
    def self.build_record
      self.delete_all
      self.collection.insert(populate_hash)
	end

	def self.deleted_companies
	  Company.where(:active => false, :deleted_at => {"$ne" => nil} )
	end

	def self.populate_hash
      build_hash = deleted_companies.map do |company|
	    {
	      :guid => company.guid, 
	      :info => 
	    	{
	    	 :deleted_at => company.deleted_at.to_s,
	    	 :active => company.active
	    	}
	    }
	  end
	  build_hash
	end

  end
end