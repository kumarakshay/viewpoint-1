class Command
  include MongoMapper::Document

  key :command_id
	key :name, String
 	key :description, String
 	key :variableData, String
	timestamps!

  validates :command_id, :uniqueness => true
  
end
