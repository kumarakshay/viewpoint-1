Ticket Landing Dashboard 
====================
## Quick Links

To have some quick navigation menu on Ticket Landing dashboard we have added following quick links 

1. Dashboard: This will navigate to customer's default dashboard
1. Devices: This will navigate to device index page where customer can see list of all the devices
1. Events: This will navigate to event index page where customer can see list of all the events 
1. DNS: This will navigte to DNS management page if customer have appropriate rights
1. User Guide: This will navigate to user guide page

For future release the menu drawer expands to show customer a complete list of menu options available for them to choose.

## Create Links

These icons link to frequent functions in Service Now to give customers top-level access to the following

1. Incident:  Report a problem or outage in your environment
1. Change: Submit a request for the addition, modification or removal of something in your environment
1. Request: Get assistance with your environment, ask a question or submit an enhancement feature
1. LILO/Site Access:  Schedule a visit to your hosting facility
1. Notification Matrix: SN admin role required to access this page
1. Authorization Matrix: SN admin role required to access this page

## Asset Health

Asset Health component is render from data that has been populated from zenoss MaxSeverity class.
The component displays asset_type, count of same type of assets and their health percentage.
The component also shows overall health score in graphical format.

1. [Controller](https://bitbucket.org/mountdiablo/viewpoint/src/master/app/controllers/api/v1/ehealth_controller.rb)
1. [MaxSeverity Model](https://bitbucket.org/mountdiablo/sgtools_zenoss/src/master/lib/sgtools/zenoss/max_severity.rb)
1. [Data endpoint zenoss](https://bitbucket.org/mountdiablo/sgtools_zenoss/src/master/lib/sgtools/zenoss_endpoints.rb#cl-119)
1. [Mock json](https://bitbucket.org/mountdiablo/viewpoint/src/master/test/fixtures/ehealth/environment_healths.json)
1. [Calculation source](https://bitbucket.org/mountdiablo/sgtools_zenoss/src/master/lib/sgtools/zenoss/max_severity.rb)

## Asset Health Group View

Asset Health Group View component is render from the data that has been populated from Zenoss DeviceSeverities class.The component displays group_name, count of assets belongs to that group and their health percentage.
The component also shows overall health score in graphical format.

1. [Controller](https://bitbucket.org/mountdiablo/viewpoint/src/master/app/controllers/api/v1/ehealth_controller.rb#cl-66)
1. [DeviceSeverity Model](https://bitbucket.org/mountdiablo/sgtools_zenoss/src/develop/lib/sgtools/zenoss/device_severity.rb)
1. [Data endpoint zenoss](https://bitbucket.org/mountdiablo/sgtools_zenoss/src/develop/lib/sgtools/zenoss_endpoints.rb#cl-137)
1. [Mock json](https://bitbucket.org/mountdiablo/viewpoint/src/master/test/fixtures/ehealth/groups_environment_health.json)
1. [Calculation source](https://bitbucket.org/mountdiablo/viewpoint/src/master/app/models/ehealth/company.rb?at=jira/3018-refactor-sn-calls#cl-27)


## Ticket Statistics

The ticket statistics component is rendered from the data that has been populated through the ehealth ticketing API
The ehealth ticketing API is returning count in the form of past, active and future by calling ServiceNow Soap API CompanyTicketSummary from eureka instance. For Ticket Landing module we will only use count active as open and past as closed. Also, we will show the addition of these two count as a total count. This is a customer/company-wide count across all ticket types and maintenance. OVERDUE will not be available from Eureka for this release, so we will HIDE this OVERDUE column from the table.

1. [Controller](https://bitbucket.org/mountdiablo/viewpoint/src/676af28990af45eb8d648d37436e5dc718e9c5e9/app/controllers/api/v1/ehealth_controller.rb?at=feature%2Fticket-landing&fileviewer=file-view-default#ehealth_controller.rb-138)
1. [Viewpoint Model](https://bitbucket.org/mountdiablo/viewpoint/src/676af28990af45eb8d648d37436e5dc718e9c5e9/app/models/ehealth/ticketing.rb?at=feature%2Fticket-landing&fileviewer=file-view-default)
1. [Mock json](https://bitbucket.org/mountdiablo/viewpoint/src/676af28990af45eb8d648d37436e5dc718e9c5e9/test/fixtures/ehealth/ticketings.json?at=feature%2Fticket-landing&fileviewer=file-view-default)
1. [Calculation Source](https://bitbucket.org/mountdiablo/viewpoint/src/676af28990af45eb8d648d37436e5dc718e9c5e9/lib/service_now_client.rb?at=feature%2Fticket-landing&fileviewer=file-view-default#service_now_client.rb-79) 


## Tickets

This component will show all the list of company tickets and its details. All table columns are filterable and sortable. Tickets info is coming from ServiceNow Rest API [Postman Sample](https://www.getpostman.com/collections/168a5764e471666f25a7). While calling this API we are passing following paramters

1. company_id: Company guid
1. active: true
1. created_on:  this date is in between past 30 days and todays date

Source code details

1. [Controller](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/app/controllers/api/v1/ticket_landing_controller.rb?at=feature%2Fticket-landing&fileviewer=file-view-default#ticket_landing_controller.rb-15)
1. [Viewpoint Model/Calculation Source](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/lib/service_now_rest_client.rb?at=feature%2Fticket-landing&fileviewer=file-view-default#service_now_rest_client.rb-51)
1. [Mock json](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/test/fixtures/ticket_landing/tickets.json?at=feature%2Fticket-landing&fileviewer=file-view-default)


## Recent Assets

Displays the list of the last 20 device profile pages that were viewed by the user. This should exclude page views from someone masquerading as the user

1. [Controller](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/app/controllers/api/v1/ticket_landing_controller.rb?at=feature%2Fticket-landing&fileviewer=file-view-default#ticket_landing_controller.rb-32)
1. [Viewpoint Model](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/app/models/ticket_landing/recent_asset.rb?at=feature%2Fticket-landing&fileviewer=file-view-default)
1. [Mock json](https://bitbucket.org/mountdiablo/viewpoint/src/42382dee786ac480bfd79bf13fb84bdeebef4f8f/test/fixtures/ticket_landing/recent_assets.json?at=feature%2Fticket-landing&fileviewer=file-view-default)



