module TicketLanding
  class RecentAsset
    include MongoMapper::Document
    key :asset_guid
    key :host
    key :company_guid
    key :user_guid
    timestamps!


    def self.save_searched_asset(device,user)
      begin
        asset = self.find_or_initialize_by_asset_guid_and_user_guid(device.asset_guid, user.guid)
        asset.host = device.name
        asset.company_guid = user.company_guid
        asset.save!
      rescue Exception=> e
        Rails.logger.error(e)
      end
    end
  end
end
