require 'time_conversion'

class CompanySubscription
  include MongoMapper::Document
  key :report_id, String
  key :company_guid, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :report_type, String
  key :report_type_name, String
  key :zenoss_instance, String
  key :report_file_name, String
  key :group_guid, String
  key :job_id, String
  key :start_date, String
  key :end_date, String

  # # down case required for zenoss upgrade
  # def company_guid
  #   read_attribute(:company_guid).downcase  # No test for nil?
  # end

  def array_of_company_guids
    unless company_guid.blank?
      Company.parent_child_list(company_guid)
    end
  end

  def group_name
    unless group_guid.blank?
      Group.where(id: group_guid).first.name
    end
  end

  def group_asset_list
    unless group_guid.blank?
      Group.where(id: group_guid).first.asset_list
    end
  end

  def self.bulk_load(report_id, frequency)
    CompanySubscription.delete_all
    dat = Req.where(subscription: true, :report_id => report_id, :frequency => frequency).all
    dat.each do |d|
      report_id = d.report_id
      start_date,end_date = d.report_date_interval_for_bus
      self.walk_the_table(
        report_id,
        d.company_guid,
        d.email,
        d.frequency,
        d.delivery_time,
        d.time_zone,
        d.id,
        d.report_type_for_bus,
        d.zenoss_instance_for_bus,
        d.report_filename_for_bus,
        d.job_id,
        d.group_guid,
        start_date,
        end_date
      )
    end
  end

  def self.walk_the_table(report_id, company_guid, email, frequency, delivery_time, time_zone, id,
                          report_type, zenoss_instance, report_filename, job_id, group_guid,start_date, end_date)
    dat = self.create(
      :report_id => report_id,
      :subscription_id => id,
      :email => email,
      :company_guid => company_guid,
      :frequency => frequency,
      :report_type => report_type,
      :report_type_name => Req.report_type_name(report_id),
      :zenoss_instance => zenoss_instance,
      :report_file_name => report_filename,
      :job_id => job_id,
      :group_guid => group_guid,
      :start_date => start_date,
      :end_date => end_date,
      :delivery_time => TimeConversion.convert_to_utc(delivery_time, time_zone)
    )
    dat.save
  end
end
