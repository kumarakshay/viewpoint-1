require 'time_conversion'

class InternetUsage
  include MongoMapper::Document
  key :report_id, String
  key :company_guid, String
  key :device_guid, String
  key :group_guid, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :delivery_time_gmt, String
  key :subscription, Boolean
  key :area, String
  key :cabinet, String

  # down case required for zenoss upgrade
  def company_guid
    read_attribute(:company_guid).downcase  # No test for nil?
  end

  def self.bulk_load(report_id)
    InternetUsage.delete_all
    dat = Req.where(subscription: true, :report_id => report_id).all
    dat.each do |d|
      self.walk_the_table(d.report_id,d.group_guid, d.company_guid, d.email, d.device_guid, d.frequency, 
        d.delivery_time, d.time_zone, d.area, d.cabinet,d.id)
    end
  end  

  def self.walk_the_table(report_id,group_id,company_guid,email,device_guid,frequency,delivery_time,time_zone,area,cabinet,id)
    statement = Statement.where(report_id: report_id).first
    dat = self.create(
      :report_id => report_id,
      :subscription_id => id,
      :statement_name => statement.name,
      :email => email,
      :company_guid => company_guid,
      :frequency => frequency,
      :delivery_time => TimeConversion.convert_to_utc(delivery_time, time_zone)       
    )
    dat.save
  end
end