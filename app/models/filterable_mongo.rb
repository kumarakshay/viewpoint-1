module FilterableMongo

  def translate_column_name(col)
    col
  end

  def translate_bogus_column_name(col)
    col
  end

  def exclude_columns
    ['uid','asset_guid','zenoss_instance'] 
  end

  def public_columns
    (self.column_names - exclude_columns)
  end

  # returns rows only
  def where_with(body={},joins={})
    start = body['start'] || 0
    raw_sort = body['sort']
    sort_dir = body['dir'] || 'asc'
    if(raw_sort.respond_to?(:keys))
      sort = raw_sort.map do |k,v|
        [self.translate_bogus_column_name(k).to_sym,  v.to_sym]
      end
      sort_dir = nil
    elsif(raw_sort)
      sort  = [self.translate_bogus_column_name(raw_sort) , sort_dir.to_sym]
    else
      sort = []
    end
    where_filter = self.last_filters(body,joins) || {}
    rows = self.where(where_filter)
  end

  # returns data and counts
  def query_with(body={},joins={})
    limit = body['limit'] || 100
    if limit.to_i > 50000
      limit = 50000
    end
    start = body['start'] || 0
    raw_sort = body['sort']
    sort_dir = body['dir'] || 'asc'
    if(raw_sort.respond_to?(:keys))
      sort = raw_sort.map do |k,v|
        [self.translate_bogus_column_name(k).to_sym,  v.to_sym]
      end
      sort_dir = nil
    elsif(raw_sort)
      sort  = [self.translate_bogus_column_name(raw_sort) , sort_dir.to_sym]
    else
      sort = []
    end
    where_filter = self.last_filters(body,joins) || {}
    rows = self.where(where_filter)
    total = rows.count
    {total: total, data: rows.limit(limit).skip(start).sort(sort)}
  end

  # get query, base64 for unique id and put in cache
  def last_filters(body,joins={})
    query = body['query'] || []
    query_json = JSON::dump(query)
    sum = Digest::MD5.base64digest(query_json)
    Rails.cache.fetch("#{self.collection.name}_#{sum}", expires_in: 15.minutes) do
      self.filter_by(body,joins)
    end
  end

  # sets up the filter conditions
  def filter_by(body={},joins={})
    adv_filter = body['adv_query'] || false
    query = body['query'] || []
    filter = []
    query.each do |q|
      if q['name'] then
        val = q['value']
        if(self.keys[q['name']] && Integer ==self.keys[q['name']].type)
          val = val.to_i
        end
        case q['op']
          when "eq", "="
            filter << {q['name'] => val}
          when "gt", ">"
            filter << {q['name'] => {:$gt=> val}}
          when "lt", "<"
            filter << {q['name'] => {:$lt=> val}}
          when "like"
            filter << {q['name'] => {:$regex=> /#{val}/i}}
          when "in"
            filter << {q['name'] => {:$in=> Array.new(val)}}
          when "between"
            filter << {q['name'] => {:$gt=> val.first , :$lt=> val.last}}
          else
            filter << {q['name'] => val}
        end
      end
    end
    {:$and=> filter} unless filter.blank?
  rescue=> e
    logger.error(e.message)
  end
end