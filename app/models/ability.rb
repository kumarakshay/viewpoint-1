require 'sgtools/rails/base/ability_hooks'

class Ability
  include CanCan::Ability
  include SGTools::Rails::Base::AbilityHooks
  # include SGTools::Rails::Infoblox::AbilityHooks
  include SGTools::Rails::Splunk::AbilityHooks
  include SGTools::Rails::MongodbLogger::AbilityHooks
  include SGTools::Rails::Hpoo::AbilityHooks

  def initialize(user)
    if user.role?('viewpoint_admin')
      can  :manage, :all
      #Ability for contact users (Sungard/Viewpoint admin can not create contact user in self company)
      cannot :create_cu, SGTools::ITSM::SystemUserWriter do |contact|
        contact.company.blank? || user.company_guid == contact.company || contact.company == 'E71312DC-AAF1-4BF2-941D-B297B1D326AB' #VUPT-1545
      end
      cannot :manage, SGTools::Zenoss::Event
    elsif user.role?('sungard_admin')
      can  :manage, :all
      cannot :manage, [Role, Help, Menu]
      cannot :see, [:roles_management, :help_management]
      cannot :delete_user, User do |current_user, profile_user|
        profile_user.role?("viewpoint_admin")
      end
      cannot :manage, SGTools::Zenoss::Event
      #Ability for contact users (Sungard/Viewpoint admin can not create contact user in self company)
      cannot :create_cu, SGTools::ITSM::SystemUserWriter do |contact|
        contact.company.blank? || user.company_guid == contact.company || contact.company == 'E71312DC-AAF1-4BF2-941D-B297B1D326AB' #VUPT-1545
      end

    elsif user.role?('company_admin')
      #Ability for contact users (Company admin can only create contact user in self company)
      can :create_cu, SGTools::ITSM::SystemUserWriter do |contact|
        user.company_guid == contact.company
      end
    end
    can :read, OmiDevice

    # Abilities for everyone!
    can :read, SGTools::Zenoss::Device
    can :read, SGTools::Zenoss::Event, :companyGuid => user.company_guid.downcase unless user.is_sungard?

    #Abilities for masquerading
    can :masquerade, User do |sign_in_user|
      sign_in_user.is_sungard?
    end

    can :delete_user, User do |current_user, profile_user|
      current_user.has_higher_role_than_me?(profile_user)
    end

    #Abilities for masquerading companies
    can :company_masquerade, User do |sign_in_user|
      # sign_in_user.masquerade?
      sign_in_user.company_user_masquerade?
    end

    can :see_user_details, User do |luser|
      luser.vp_admin_role?
    end

    can :edit_user_details, User do |sign_in_usr, profile_usr|
      sign_in_usr.id == profile_usr.id || (sign_in_usr.admin_role? && sign_in_usr.company_guid == profile_usr.company_guid)
    end

    can :event_subscription, User do |luser|
      luser.vp_admin_role?
    end

    can :manage, Group
    can :read, Device
    can :manage, Req
    can :device_access, User do |sign_in_usr, device_id|
      sign_in_usr.all_asset_list.include?(device_id) or sign_in_usr.is_sungard?
    end
    can :access_non_omi_pages, User do |sign_in_usr, is_omi_flag|
      !is_omi_flag
    end

    if user.role?('infoblox_dns_customer_engineer')
      can :read, SGTools::Rails::Infoblox::Dns::Zone,
        :extensible_attributes => {:customerguid => user.company_guid.downcase }
      can :read, SGTools::Rails::Infoblox::Dns::Audit,
        :sgtools_rails_infoblox_dns => { :customer_guid => user.company_guid.downcase }

      # Check if the user has the infoblox_dns_customer_reporter role
    elsif user.role?('infoblox_dns_customer_reporter')
      can :read, SGTools::Rails::Infoblox::Dns::Zone,
        :extensible_attributes => {:customerguid => user.company_guid.downcase }
      can :read, SGTools::Rails::Infoblox::Dns::Audit,
        :sgtools_rails_infoblox_dns => { :customer_guid => user.company_guid.downcase }
    end

    # Check if the user has the infoblox_dns_sungard_engineer role
    if user.role?('infoblox_dns_sungard_engineer')
      can :manage, SGTools::Rails::Infoblox::Dns::Zone
      can :manage, SGTools::Rails::Infoblox::Dns::Audit

      # Check if the user has the infoblox_dns_sungard_reporter role
    elsif user.role?('infoblox_dns_sungard_reporter')
      can :manage, SGTools::Rails::Infoblox::Dns::Audit
      can :read, SGTools::Rails::Infoblox::Dns::Zone
    end

    # Include abilities from engines
    run_hook :define_ability, user
  end

  private

  # Override any imported private methods here

end
