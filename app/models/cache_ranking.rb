class CacheRanking
  include MongoMapper::Document
  key :identifier, String
  key :key, String
  timestamps!

  validates_presence_of :identifier
  validates_presence_of :key
end