class EventStat
  include MongoMapper::Document

  key :asset_guid
  key :critical, Integer, default: 0
  key :debug, Integer, default: 0
  key :error, Integer, default: 0
  key :information, Integer, default: 0
  key :ordered, Integer, default: 0
  key :warning, Integer, default: 0

  SEVERITY_MAP = {5 => 'critical', 4 => 'error', 3 => 'warning', 2 => 'information', 1 => 'debug', 0 => 'clear'}

  class ProcessingError < RuntimeError
  end

  BATCH_SIZE=5000
  
  def self.bulk_load
    start_time = Time.now
    logger.debug('starting')
    self.collection.remove
    big_bacon = []
    populate({}) do|row|
      big_bacon << row
      if big_bacon.size > BATCH_SIZE
        self.collection.insert(big_bacon)
        big_bacon=[]
      end
    end
    unless big_bacon.blank?
      self.collection.insert(big_bacon)
      big_bacon= nil
    end
    self.ensure_index('ordered')
    self.ensure_index('asset_guid')
    logger.debug("ending: #{Time.now - start_time}")
  end

  def self.populate(h={})
    dat = Event.get_all_status_grouped
    dat.each do |d|
      r = {"critical" => 0, "error" => 0, "warning" => 0, "information" => 0, "clear" => 0, "debug" => 0}
      a = d['severity_list'].split(",").map { |s| s.to_i }
      r["asset_guid"] = d["assetGuid"]
      a.each {|v|
        r[SEVERITY_MAP[v]]+=1
      }
      r['ordered'] = r['critical'] * 1000000000 + r['error'] * 1000000 + r['warning'] * 1000 + r['information']
      yield r
    end
  end

  # Calls bulk_load if the cache timestamp is greater than a minute
  def self.load_if_needed(force=false, asset_list)
    d = Digest::MD5.new
    sum = d.digest(asset_list.join())
    enc = Base64.urlsafe_encode64(sum)
    key = "event_stats-#{enc}"
    processing_key = "processing-#{key}"
    Rails.cache.delete(key) if force
    raise ProcessingError.new("processing #{key}") if Rails.cache.exist? processing_key
    Rails.cache.fetch(key, expires_in: 1.minute, race_condition_ttl:10 ) {
      Rails.cache.write(processing_key,true)
      logger.info("wrote load_if_needed: #{key}")
      self.bulk_load
      self.update_devices(asset_list)  
    }
  ensure 
    Rails.cache.delete(processing_key)
  end

  def self.totals(asset_list)
    opts = {}
    unless asset_list.blank?
      opts = {asset_guid: asset_list}
    end
    es = EventStat.where(opts)
    t = es.inject({critical: 0,error: 0, warning: 0, information: 0}){|h,evt| h.keys.each{|k| h[k]+=evt[k]};h}
    SEVERITY_MAP.map do|id,name|
      {id: id, name:name, total: t[name.to_sym] || 0}
    end
  end

  def self.update_devices(asset_list)
    start_time = Time.now
    logger.debug('starting')
    opts = {}
    unless asset_list.blank?
      opts = {asset_guid: asset_list}
    end
    Device.collection.update(opts,{:$set=>{critical: 0, error: 0, warning: 0, information: 0, ordered: 0}},{multi: true})
    self.where(opts).each do|row|
      d = Device.collection.update({asset_guid: row['asset_guid']},{:$set=>{critical: row['critical'], error: row['error'], warning: row['warning'], information: row['information'], ordered: row['ordered']}})
    end
    Device.ensure_index('ordered')
    Device.ensure_index(:asset_guid)
    logger.debug("ending: #{Time.now - start_time}")
  end
end