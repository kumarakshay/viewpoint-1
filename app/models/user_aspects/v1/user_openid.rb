module UserAspects
  module V1
	  module UserOpenid

		  module ClassMethods
		   # Updates identifier URL for the user depending on his company_guid, and email. 
			  def self.update_identifier(params={})        
			    user = User.first(:email => { :$regex => /^#{params[:email]}$/i })
			    unless user.blank?
			      user.update_attribute("last_login_at", Time.now)
			    end
			    user    
			  end     
		    end
		  
		  module InstanceMethods		    

		  	def register!
			    if valid?
			      res = sso_user_create
			      if valid_sso_result?(res)
			        set_attributes(res)
			        self.user_sso_as_hash = res['user'].to_hash
			        self.save!
			        return true        
			      end
			      self.sso_errors= {:message => "Unable to register user #{self.email}"}
			    end
	    	    false
	  		end

			  def confirm!
			    Rails.logger.info("------user #{self} ------")
			    self.respond_action = 'reset_password'
			    if active? and valid? and !self.confirmation_token.nil?
			      res = sso_user_confirmation
			      if valid_sso_result?(res)
			        self.confirmation_token = nil            
			        self.last_login_at = Time.now
			        self.user_sso_as_hash = res['user'].to_hash
			        return self.save
			      end
			      self.sso_errors={:message => "Unable to confirm user #{self.email}"}
			    end
			    false
			  end

			  def update!(new_email)
			    if valid?
			      res = sso_user_update
			      if valid_sso_result?(res)
			          self.user_sso_as_hash = res['email'].to_hash
			          #Change email flow
			          if new_email.present? && self.email != new_email 
			            self.pending_email = new_email
			            res = sso_change_email
			            if valid_sso_result?(res)
			              self.confirmation_token = res["confirmation_token"]  
			              Rails.logger.info "*****Email updated and confirmation token generated is :- #{res["confirmation_token"]}****"
			            else
			              self.sso_errors={:message => "Unable to update user #{self.email}"}
			              return false
			            end
			          end  
			          return self.save 
			      end
			      self.sso_errors={:message => "Unable to update user #{self.email}"}
			    end
			    false
			  end

			  def update_email!
			    if self.pending_email.present? && valid?
			      res = sso_change_email
			      if valid_sso_result?(res)
			        self.user_sso_as_hash = res['user'].to_hash
			        self.confirmation_token = res['confirmation_token']
			        return self.save
			      end
			      self.sso_errors={:message => 'Unable to change user email address'}
			    end  
			    false
			  end

			  def confirm_email!
			    if active? and valid? and !self.confirmation_token.nil?
			      res = sso_confirm_email
			      if valid_sso_result?(res)
			        self.confirmation_token = nil      
			        self.email = self.pending_email
			        self.pending_email = nil      
			        self.user_sso_as_hash = res['user'].to_hash
			        return self.save
			      end
			    end
			    self.sso_errors={:message => "Unable to confirm user email address"}
			    false
			  end  

			  def resend_confirmation!
			    res = sso_resend_confirmation
			    if valid_sso_result?(res)
			      self.user_sso_as_hash = res['user'].to_hash
			      self.confirmation_token = res['user']['confirmation_token']
			      return self.save
			    end
			    self.sso_errors={:message => 'Unable to send confirmation email'}
			    false
			  end  

			  def toggle_unlock!
			    res = sso_unlock_user
			    if valid_sso_result?(res)
			      self.user_sso_as_hash = res['email'].to_hash
			      return self.save
			    end
			    self.sso_errors={:message => "Unable to unlock user #{self.email}"}
			    false
			  end  

			  def delete_user!
			    res = sso_delete_user
			    if valid_sso_result?(res)
			      return self.destroy
			    end
			    self.sso_errors={:message => "Unable to delete user #{self.email}"}
			    false
			  end  

			  def send_password_reset! 
			    res = sso_password_reset
			    if valid_sso_result?(res)
			      self.user_sso_as_hash = res['user'].to_hash
			      self.reset_password_token = res["user"]['reset_password_token']
			      return self.save
			    end
			    self.sso_errors={:message => "Unable to reset password for user #{self.email}"}
			    false
			  end  

			  def confirm_password_reset!
			    self.respond_action = 'reset_password'
			    self.validate_password 
			    if errors.blank? and self.reset_password_token.present?
			      res = sso_confirm_password_reset
			      email = res["email"]
			      if valid_sso_result?(res)
			        user = User.first(:email => { :$regex => /^#{email}$/i })
			        user.reset_password_token = nil
			        user.user_sso_as_hash = res['email']
			        return user.save
			      end
			      self.sso_errors={:message => "Invalid reset token"}
			    end
			    false
			  end	

	  	  ##************user create**************** 
	  	  def sso_user_create
	  	  	OpenidManager.access_openid(APP_CONFIG['open_id_api_url'], registration_hash)
	   	  end

	   	   def registration_hash
	      	user_data = user_hash.merge!(create_hash("user", split=false, "guid"))
	  	    default_data.merge!(user_data).merge!(persona_hash)
	  	  end
	  	  
	  	  ## ************** User confirmation ********************
	   	  def sso_user_confirmation
	   	  	OpenidManager.access_openid(APP_CONFIG['open_id_confirmation_url'], confirmation_hash)
	   	  end

	   	  def confirmation_hash
	  	  	default_data.merge!(password_hash).merge!(create_hash("user", split=false, "confirmation_token"))
	  	  end

	  	  ##*****************User Update***************
	  	  def sso_user_update
	  	     OpenidManager.access_openid(APP_CONFIG['open_id_update_url'], update_hash,'Put')
	  	  end

	  	  def update_hash
	  	   	default_data.merge!(user_hash).merge!(persona_hash("persona"))
	  	  end

	  	  ##*****************User toggle unlock ***************
	  	  def sso_unlock_user
	  	  	OpenidManager.access_openid(APP_CONFIG['open_id_update_url'], toggle_unlock_hash,'Put')
	  	  end

	  	  def toggle_unlock_hash
	  	   	default_data.merge!(
	  	   	  {
			        'user[email]' => self.email,
			        'user[failed_attempts]' => 0,
			        'user[locked_at]' => nil,
			      }
	  	   	)
	  	  end

		  	  ##*****************User resend confirmation ***************
		  	def sso_resend_confirmation
		  	  res = OpenidManager.access_openid(APP_CONFIG['open_id_resend_confirm_url'], reset_hash,'Put')
			  end

	  	  def reset_hash
	  	   	default_data.merge!({'user[email]' => self.email})
	  	  end

	  	  ##*****************User Change Email ***************
	  	  def sso_change_email
	  	  	OpenidManager.access_openid(APP_CONFIG['open_id_change_email_url'], change_email_hash,'Put')
	  	  end	
	  	  
	  	  def change_email_hash
	  	  	default_data.merge!(
	  	  	  {
		          'user[email]' => self.email,
		          'user[new_email]' => self.pending_email
		        }
		      )
	  	  end

	  	  ##*****************User Confirm Email ***************
	  	  def sso_confirm_email
	  	  	OpenidManager.access_openid(APP_CONFIG['open_id_confirm_email_url'], confirm_email_hash)
	  	  end	
	  	  
	  	  def confirm_email_hash
	  	  	change_email_hash.merge!({'user[confirmation_token]' => self.confirmation_token })
	  	  end

	  	  ##*****************User delete ***************
	  	  def sso_delete_user
	  	  	 openid_delete_url = APP_CONFIG['open_id_user_delete_url'] + "/" + self.email
	  	  	 OpenidManager.access_openid(openid_delete_url, default_data,'Delete')
	  	  end
	  	  
	  	  ##*****************User reset password ***************
	  	  def sso_password_reset
		  	OpenidManager.access_openid(APP_CONFIG['open_id_reset_url'], reset_password_hash)
	  	  end

	  	  def reset_password_hash
	  	   	default_data.merge!({ 'user[email]' => self.email})
	  	  end

	  	  ##*****************User confirm reset password ***************
	  	  def sso_confirm_password_reset
		  	OpenidManager.access_openid(APP_CONFIG['open_id_reset_url'], confirm_password_hash,'Put')
	  	  end

	  	  def confirm_password_hash
	  	   	default_data.merge!(password_hash).merge!({'user[reset_password_token]' => self.reset_password_token})
	  	  end

	  	  #**********************Default Hash ***********************************

	      def user_hash
	      	create_hash("user", split=false, "email","active","company_name","company_guid")
	      end

	      def persona_hash(persona=nil)
	      	create_hash(persona, split=true, "first_name","last_name","time_zone")
	      end

	      def password_hash
	  	  	create_hash("user", split=false, "password", "password_confirmation")
	  	  end

	      def default_data
	      	{'auth_token' => APP_CONFIG['open_id_auth_token']}
	      end
	      

	   	  def create_hash(name, split, *args)
		    	data_hash = {}
		    	args.each do |i|
		    	  attribute = split ? i.tr('_','') : i
		    	  key = name ? "#{name}[#{attribute}]" : "#{attribute}"
		    	  data_hash.merge!( key => self.send(i))
		        end
		        data_hash
	      end

			  def valid_sso_result?(res)
			    if res.nil?
			      false
			    else
			      'success' == (res['status'] || res['message']) ? true : false 
			    end		  
		    end

			  def set_attributes(json_res)
			    if json_res
			      self.confirmation_token = json_res["user"]["confirmation_token"]
			      self.reset_password_token = json_res["user"]["reset_password_token"]
			      self.unlock_token = json_res["user"]["unlock_token"]
			      self.public_persona_id = json_res["user"]["public_persona_id"]
			      #self.guid = self.id.to_s
			    end
  			 end

		  end
		  
		  def self.included(receiver)
		    receiver.extend         ClassMethods
		    receiver.send :include, InstanceMethods		    
		  end	
	  end
  end
end
