class ComplianceCompute
  include MongoMapper::Document
  key :name, String
  key :group_name1, String
  key :group_name2, String
  key :vCenter_Version, String
  key :Esxi_Version, String
  key :vcenter_compliant, Integer, default: 0
  key :esxi_compliant, Integer, default: 0
  key :vcenter_total, Integer, default: 0
  key :esxi_total, Integer, default: 0
  timestamps!

  validates :name, :group_name1, :group_name2, :presence => {:message => 'cannot be empty'}
  validates :vCenter_Version, :Esxi_Version, presence: true, on: :update

  def vcenter_non_compliant
    vcenter_total -  vcenter_compliant rescue 0
  end

  def esxi_non_compliant
    esxi_total -  esxi_compliant rescue 0
  end

  def self.get_compliance_widget
    records = []
    sets = ComplianceCompute.order(:name).all
    sets.each do |obj|
      a = Hash.new
      a["product"] = obj.name + "-" + obj.group_name1
      a["compliant"] = obj.vcenter_compliant
      a["noncompliant"] = obj.vcenter_total - obj.vcenter_compliant
      records  << a
      b = Hash.new
      b["product"] = obj.name + "-" + obj.group_name2
      b["compliant"] = obj.esxi_compliant
      b["noncompliant"] = obj.esxi_total - obj.esxi_compliant
      records  << b
    end
    return records
  end
end
