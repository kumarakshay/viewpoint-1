class ApiKey
  include MongoMapper::Document
  before_create :generate_access_token
  key :access_token, String
  key :email, String
  timestamps!
  
private
  
  def generate_access_token
    begin
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token)
  end
  
end