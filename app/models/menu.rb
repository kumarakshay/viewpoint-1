class Menu
  include MongoMapper::Document
  key :title, String  
  timestamps!

  validates :title, presence: true

  before_destroy :validate_dependancies

  def validate_dependancies
	  if Help.has_associated_menu?(self.id.to_s)
	    self.errors.add :base, 'Cannot delete this menu as it is associated to a help'
	    return false
    end
  end
end