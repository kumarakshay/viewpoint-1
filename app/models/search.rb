class Search 
  include MongoMapper::Document
  key :keywords
  
  # Search is made for assets based on query params provided, 
  # and only those assets are returned that are part of the given company asset list.
  def self.search_with_params(company_guid, query_param)
  	dat = Device.search(query_param).all    
    unless dat.empty?
  	  all_asset =Company.where(guid: company_guid).first.asset_list      
      dat.delete_if {|dev| !(all_asset.include?(dev.asset_guid)) }   
    end
    dat
  end
end