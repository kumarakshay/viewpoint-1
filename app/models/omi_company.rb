class OmiCompany
  include MongoMapper::Document
  key :snt, String
  key :guid
  key :name
  key :asset_list, Array
  key :active, Boolean, default: true
  key :deleted_at, Time
  key :zenoss_presence, Boolean, default: false
  key :user_count, Integer, default: 0
  timestamps!

end
