require 'net/telnet'

class SystemInfo
  #attr_reader :redis_store

  # def initialize 
  #   @redis_store = Rails.cache.instance_variable_get(:@data)
  # end
  
  def memory_stats
    `ps -Ao rss=`.split.map(&:to_i).inject(&:+)
  end

  def passenger_stats
    `cd #{Rails.root};bundle exec passenger-status`
  end

  # def redis_stats
  #   host = redis_store.client.host
  #   port = redis_store.client.port
  #   h = Net::Telnet.new('Host'=>host, 'Port'=>port)
  #   h.puts('INFO')
  #   x = h.waitfor(/db\d+/)
  #   return {} if x.blank?
  #   a = x.split("\n")
  #   a.shift
  #   a.inject({}) do |h,item|
  #     temp = item.split(":")
  #     h[temp.first]  = temp.last
  #     h
  #   end
  # ensure
  #   h.close() if h  
  # end

  # def redis_stream
  #   raise 'not gonna work' unless block_given?
  #   host = redis_store.client.host
  #   port = redis_store.client.port
  #   h = Net::Telnet.new('Host'=>host, 'Port'=>port)
  #   x = h.cmd('MONITOR'){|b| yield b}
  # ensure
  #   h.close() if h
  # end

  def mongo_stats
  end

  def mongo_slow_queries
  end

  # def reports_running
  #   match_key = 'reports-*'
  #   keys = redis_store.keys(match_key)
  #   a = []
  #   keys.each do |k| 
  #     if(c = redis_store.get(k))
  #         a << {key: k, value: c.value}
  #     end
  #   end
  #   a
  # end

  # simple count of logins stored in the Audit collection
  def login_count
    Audit.where(action: "login").count
  end

end