# this class is used just as lookup table for population of adhoc report screen.
class CustomReport
  include MongoMapper::Document
  key :name, String
  key :display_name, String
  key :description, String
  key :report_id, String
  key :type, String
  key :meta_type, String
  key :file_name, String
  key :ext, String  
end