class ReqObr
  include MongoMapper::Document

  before_create :set_group_or_device_name, :set_email_me

  ADHOCH_REPORT_PATH="adhoch-user-reports"

  key :report_id, String
  key :company_guid, String
  key :user_guid, String
  key :report, String
  key :device_guid, String
  key :group_guid, String
  key :start_date, String
  key :end_date, String
  key :email, String # sign in user email or subscription email
  key :frequency, String # in words eg daily, monthly, weekly
  key :time_zone, String
  key :delivery_time, String
  key :subscription, Boolean # Need to update email also in case subscription true
  key :is_masqueraded , Boolean, default: false# true if sign in user is sungard
  key :area, String
  key :cabinet, String
  key :job_status, String,:default => 'pending'
  key :job_id, String
  key :email_me, Boolean, default: false
  key :acknowledged, Boolean, default: false
  key :obr_schedule_id, String # id from obr response
  key :destination, String  #This field has been updated with the filepath received from pyxie
  key :group, String
  key :device, String
  key :payload
  key :time, String
  key :isCancelled, Boolean, default: false
  key :report_type, String
  timestamps!

  scope :created_after,
  lambda { |val|
    where(:created_at => {:$gte =>  Time.at(val.to_i).utc + 1.second})
  }
  scope :subscriptions
  where(subscription: true)

  def report_name
    obr_report_collection.report_name
  end

  def obr_report_collection
    ObrStatement.find_by_report_id(report_id)
  end

  # return asset_list for given group
  def devices_for_group_guid()
    Group.find_by_id(self.group_guid).asset_list
  end

  # Before creating a req object it will generates a message_id and assign it to req object
  def self.initialize_job_id(prefix='')
    SecureRandom.hex(5)
  end

  private
  #This method will update the device/group name into req collection based 
  #on selected group/device id
  def set_group_or_device_name
    unless self.group_guid.blank?
      self.group = Group.where(id: self.group_guid).first.name rescue ""
    end

    unless self.device_guid.blank?
      self.device = OmiDevice.where(asset_guid: self.device_guid).first.name rescue ""
    end
  end

  # The pyxie destination created using users company_id, report filename and job_id
  # This destination is used to stoare report in pyxie
  def set_email_me
    self.email_me = self.subscription || self.is_masqueraded
    true
  end
end
