class Capacity
  include MongoMapper::Document
  key :name, String
  key :type, String
  key :device_type, String
  key :location, String
  key :unit, String
  key :total, Integer, default: 0
  key :used, Float, default: 0.0
  key :available, Float, default: 0.0
  timestamps!

  scope :for_device_type,
  lambda { |type|
    where(:device_type => type.downcase)
  }
end
