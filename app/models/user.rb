class User
  require 'csv'
  include UserAspects::UserRoles
  include MongoMapper::Document
  
  include "UserAspects::#{APP_CONFIG['open_id_version']}::SnWebApi".constantize
  include "UserAspects::#{APP_CONFIG['open_id_version']}::UserOpenid".constantize
  
  before_save :upcase_company_guid, :sanitize_roles
  after_create :increase_user_count

  attr_accessor :proxy_user, :real_user, :company_guid, :password, :password_confirmation,\
                :user_sso_as_hash, :sso_errors, :respond_action
  key :guid
  key :email
  key :company_guid
  # key :company_name
  key :portal_name
  key :app_settings, Hash
  key :active, Boolean, default: true
  key :time_zone, String
  key :vp_admin, Boolean
  key :old_contact_guid
  #key :phone_number, String
  key :roles, Array
  key :first_name, String
  key :last_name, String
  key :job_title, String
  key :created_by, String
  key :last_login_at, Time
  key :confirmation_token, String
  key :reset_password_token, String
  key :unlock_token, String
  key :public_persona_id
  key :service_now_guid, String 
  key :deleted_at, Time
  key :pending_email
  key :masquerade, Boolean
  key :provider, String
  key :oauth_token, String
  key :oauth_expires_at,Time

  timestamps!

  validates :company_guid, :email, :first_name, :last_name, :presence => {:message => 'cannot be empty'}
  
  validates :email, 
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message => 'invalid format' },            
            :unless  => lambda {|u| u.errors[:email].any? }

  #validation will fire if active user count is greater than 0          
  validates :email, :uniqueness => {:case_sensitive => false}

  # validates :pending_email, 
  #           :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message => 'invalid format' },            
  #           :on => :update,
  #           :unless => Proc.new {|c| c.pending_email.blank?}

  validate :validate_password , 
           :if => lambda {|s| (s.confirmation_token.present? || s.reset_password_token.present? ) and  (s.respond_action == 'confirm' || s.respond_action == 'reset_password' )}

  def self.query(options={})
    super.tap {|query| 
      query[:deleted_at] = nil
    }
  end
    
  def nickname
    email.split('@').first  
  end

  def role?(role)
    self.roles.include?role
  end

  def real_user
    self
  end

  def full_name
    name = ''
    name = name + first_name + ' ' unless first_name.blank?
    name = name + last_name unless last_name.blank?
    name
  end

  # Sungard employees can be identifed by their association with specific portal_names
  def sungard    
    (portal_name == "sungard" || portal_name == "sasuk" || portal_name == "admin" || portal_name =="SASI" || portal_name == "sas5" || portal_name == "sas7")
  end 

  def is_sungard?
     sungard
  end

  #Check the user company has access to OMI menu
  def is_omi?
    true
    # self.company.is_omi?
  end

  # check if user company have access to Portal Patching
  def portal_patching_access?
    self.company.portal_patching_access?
  end


  #Check user is allstream user or not
  def is_allstream_user?
    if PortalMapWorker.is_child?("mtai", self.company_guid) || self.company_guid == "1DA8A657-21A8-42EE-83A7-B4E32C8B480C"
      true
    else
      false
    end
  end

  # only sungard users can masquerade 
  def can_masquerade?
    is_sungard?
  end
  
  # Checks if this user is allowed to masquerade. Internally this method checks 
  # if user is sungard or non-sungard having masquerading permission.  
  def allow_masquerade_activity?
    can_masquerade? or company_user_masquerade?
  end

  def is_masquerading?
    false
  end

  def self.search_by_email(email)  
   User.where(:email => { :$regex => /#{email}/i }).limit(8).all
  end 

  # Getting highest role position of a user
  def find_highest_role_position
    role_position = {}
    Role.all_active_roles.each do |role|
      role_position.merge!({role.name => role.position})
    end
    position = []
    self.roles.each do |role|
      position << role_position[role]
    end
    highest_position = position.compact.sort.first
   end

  # Any user allow to edit other user profile when he has higher role than other user. 
  def has_higher_role_than_me?(user)
    # if compared to one self.
    if self.id == user.id
      return true
    else
      if user.find_highest_role_position.blank?
         return true
      else
        if self.find_highest_role_position.blank?
          return false
        else
          return self.find_highest_role_position <= user.find_highest_role_position
        end
      end  
    end
  end  

  # Checking User is admin or not
  def admin_role?
    admin_roles = ['company_admin','sungard_admin','viewpoint_admin','hpc_network_admin']
    admin_roles.each do |role|
      return true if role?(role)       
    end
    false
  end

  # Checking User is admin or not
  def vp_admin_role?
    admin_roles = ['sungard_admin','viewpoint_admin']
    admin_roles.each do |role|
      return true if role?(role)       
    end
    false
  end
  
  # VUPT-4281 Checking User access for invoices & contracts
  def access_restricted?
    roles.include?("view_billing_and_contracting")
  end

  def command_access?
    command_roles = ["command_runner_show_command", "command_runner_change_command"]
    command_roles.each do |role|
      return true if role?(role)       
    end
    false
  end

  def all_asset_list
    assets = []
    begin
      assets = Company.where(guid: self.company_guid).first.asset_list
      return assets
    rescue Exception => e
      return []
    end
    # if self.is_omi? && !APP_CONFIG['vp_cauldron']
    #   omi_company = OmiCompany.find_by_guid(self.company_guid)
    #   if omi_company.present?
    #     omi_company.asset_list
    #   else
    #     Company.where(guid: self.company_guid).first.asset_list
    #   end
    # else
    #   Company.where(guid: self.company_guid).first.asset_list
    # end
  end

  def asset_list(group_ids=[])
    if group_ids.blank?
      all_asset_list
    else
      Rails.cache.fetch(group_ids.join('_'),expires_in: 1.hour) do
        company_group_assets(group_ids)
      end
    end
  end

  def asset_list_string
    self.all_asset_list.map { |i| "'" + i.to_s + "'" }.join(",")
  end

  def self.set_as_company_admin(email)
    user = User.first(:email => { :$regex => /^#{email}$/i })
    if not user.blank? and not user.role?('company_admin')
      user.roles << 'company_admin' 
      user.save
    else
      false
    end
  end
 
  def validate_password
    if password.blank? || password_confirmation.blank?
      errors.add(:password,'cannot be empty')
    end
    if not password =~ /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$/ 
      errors.add(:password,'must be a minimum of 8 characters long and contain at least one upper case, lower case and number')
    end    
    if password != password_confirmation
      errors.add(:password,"and Confirmation does not match") 
    end
  end

  def phone_number_cc
    sn_user.blank? ? nil : sn_user.u_business_phone_cc
  end

  def phone_number_cc=(val)
    sn_user.u_business_phone_cc = val if sn_user
  end

  def phone_number_ext
    sn_user.blank? ? nil : sn_user.u_business_phone_ext
  end

  def phone_number_ext=(val)
    sn_user.u_business_phone_ext = val if sn_user
  end

  def phone_number
    sn_user.blank? ? nil : sn_user.phone  rescue nil
  end

  def phone_number=(val)
    # Commenting out following for due to issue #VUPT-3058
    # sn_user.phone = val if sn_user
  end

  def business_phone_notes
    sn_user.blank? ? nil : sn_user.u_business_phone_notes
  end

  def business_phone_notes=(val)
    sn_user.u_business_phone_notes = val if sn_user
  end

  def cell_phone_cc
    sn_user.blank? ? nil : sn_user.u_mobile_phone_cc
  end

  def cell_phone_cc=(val)
    sn_user.u_mobile_phone_cc = val if sn_user
  end

  def cell_phone_ext
    sn_user.blank? ? nil : sn_user.u_mobile_phone_ext
  end

  def cell_phone_ext=(val)
    sn_user.u_mobile_phone_ext = val if sn_user
  end

  def cell_phone
    sn_user.blank? ? nil : sn_user.mobile_phone
  end

  def cell_phone=(val)
    sn_user.mobile_phone = val if sn_user
  end

  def cell_phone_notes
    sn_user.blank? ? nil : sn_user.u_mobile_phone_notes
  end

  def cell_phone_notes=(val)
    sn_user.u_mobile_phone_notes = val if sn_user
  end

  def phone_3_cc
    sn_user.blank? ? nil : sn_user.u_alternate_phone_1_cc
  end

  def phone_3_cc=(val)
    sn_user.u_alternate_phone_1_cc = val if sn_user
  end

  def phone_3_ext
    sn_user.blank? ? nil : sn_user.u_alternate_phone_1_ext
  end

  def phone_3_ext=(val)
    sn_user.u_alternate_phone_1_ext = val if sn_user
  end

  def phone_3
    sn_user.blank? ? nil : sn_user.u_alternate_phone_1
  end

  def phone_3=(val)
    sn_user.u_alternate_phone_1 = val if sn_user
  end

  def phone_3_notes
    sn_user.blank? ? nil : sn_user.u_alternate_phone_1_notes
  end

  def phone_3_notes=(val)
    sn_user.u_alternate_phone_1_notes = val if sn_user
  end

  def phone_4_cc
    sn_user.blank? ? nil : sn_user.u_alternate_phone_2_cc
  end

  def phone_4_cc=(val)
    sn_user.u_alternate_phone_2_cc = val if sn_user
  end

  def phone_4_ext
    sn_user.blank? ? nil : sn_user.u_alternate_phone_2_ext
  end

  def phone_4_ext=(val)
    sn_user.u_alternate_phone_2_ext = val if sn_user
  end

  def phone_4
    sn_user.blank? ? nil : sn_user.u_alternate_phone_2
  end

  def phone_4=(val)
    sn_user.u_alternate_phone_2 = val if sn_user
  end

  def phone_4_notes
    sn_user.blank? ? nil : sn_user.u_alternate_phone_2_notes
  end

  def phone_4_notes=(val)
    sn_user.u_alternate_phone_2_notes = val if sn_user
  end

  def secondary_email
    sn_user.blank? ? nil : sn_user.u_alternate_email
  end

  def secondary_email=(val)
    sn_user.u_alternate_email = val if sn_user
  end

  # Service now user that is cached for the current request.
  def sn_user
    @sys_user ||= sys_user rescue nil
  end

  
  # Returns true if a non sungard user is having masquerading permission.
  def company_user_masquerade?    
    is_sungard? ? false : (self.role?('switch_company'))
  end

  #This is soft delete, we are updating deleted_at with current time 
  def delete
    self.deleted_at = Time.now
    self.active = false
    if self.save :validate => false
      self.decrease_user_count
      reqs = Req.where(user_guid: self.guid)
      reqs.each do |req|
        req.destroy
      end
    end
  end

  alias :destroy :delete

  def decrease_user_count
    self.company.update_attribute("user_count", (self.company.user_count - 1))
  end

  def set_guid(guid=nil)
    if guid.blank?
      uuid = UUID.new #This is used for SSO v1       
      self.guid = uuid.generate
    else
      self.guid = guid
    end
  end 

  def timezone
    if APP_CONFIG["open_id_version"] == 'V2'    
      tz = Timezone.where('label' => self.time_zone).first    
      tz.iana_value if tz
    end
  end
  

  def is_firewall_viewer?
    role?('firewall_viewer')
  end

  def company_name
    company.name if company
  end

  def company
    Company.where(:guid => company_guid).first
  end
  
  private
  def upcase_company_guid
    self.company_guid = self.company_guid.upcase
  end

  def roles_included
    active_roles = Role.all_active_roles.inject([]){|ar,r| ar << r.name}
    self.roles.each do |role|
      self.errors[:roles] << 'Valid roles not selected' unless active_roles.include?(role)
    end
  end
  
  # Adds viewpoint_user role to the user as a default one if any role is missing.
  def add_viewpoint_role
    self.roles << 'viewpoint_user' unless self.roles.include?('viewpoint_user')
  end
   
  # Disables possibility of non-sungard user getting sungard roles.
  def sanitize_roles
    if self.company_guid && !self.roles.blank?
      self.roles.uniq!
      unless is_sungard?
        self.roles = self.roles - [Role::VIEWPOINT_ADMIN, Role::SUNGARD_ADMIN]
      end         
    end
  end

  def increase_user_count
    self.company.update_attribute("user_count", self.company.user_count + 1) 
  end
end
