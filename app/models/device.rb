require 'sgtools/zenoss/device'

class Device 
  include MongoMapper::Document
  key :asset_guid  
  key :company_use
  key :name
  key :ip_address_formatted
  key :collector
  key :ip_address
  key :location
  key :location_uid
  key :ping_flag
  key :priority
  key :production_state
  key :serial_number
  key :snmp_flag
  key :tag_number
  key :zenoss_instance
  key :clear
  key :critical, Integer, default: 0
  key :debug, Integer, default: 0
  key :error, Integer, default: 0
  key :information, Integer, default: 0
  key :ordered, Integer, default: 0
  key :warning, Integer, default: 0
  key :atype
  key :management_ip
  key :cma_name
  key :snmp_sys_name
  key :customer_production_ip
  key :command_ids, Array
  key :dev_objects, Array
  key :dev_objectgroups, Array
  key :dev_groups, Array
  key :dev_services, Array
  key :na_DeviceID
  key :root_class
  key :ci_type
  timestamps!

  #VUPT-4209 - command_id 19901 is the actual change command id (Insert Access List Rule).
  # This code has been moved into application.yml as change commands are environment specific
  # CHANGECOMMNAD="19901"

  #These are the commands having issue and we are not going to show these commands on show form
  #VUPT-4167
  EXCEPTONACOMMANDS = ["19901","20001"]

  #This method return the asset_guid in lowercase associated with Device.
  def lowercase_asset_guid
    self[:asset_guid].downcase unless self[:asset_guid].blank?
  end  
  
  alias :asset_guid :lowercase_asset_guid

  scope :devices_in,
    # lambda { |str| where(:asset_guid => { :$in => str.split(',') } )}
    lambda { |str| where(:asset_guid => { :$in => str } )}

  scope :for_company,
    lambda { |str| 
       asset_list = Company.where(guid: str).first.asset_list
       where(:asset_guid => asset_list)
    }

  scope :name_starts_with,
    lambda { |str| where :name => { :$regex => /#{str}/i } }

  # search devices based on group id.
  scope :for_group,
    lambda { |group_id|
      group = Group.find(group_id)      
      assets = group.blank? ? [] : group.asset_list      
      where(asset_guid: { :$in => assets })
    }

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
        all.each do |device|
          csv << device.attributes.values_at(*column_names)
        end
    end
  end

  def self.thresholds
    guid = "BAD5076F-F6C7-C1ED-E040-A2A8567D5406"
    z = Zenoss::Device.device_thresholds(guid)
  end
   
  def self.search(id)
    # if(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/.match(id.to_s))
    #   dat = Device.where(:ip_address_formatted => Regexp.new(id))
    # else
    #   dat = Device.where(:name => Regexp.new(id, Regexp::IGNORECASE))
    # end   
    if (id =~ /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/)
      id = id
    else
      id = Regexp.escape(id)
    end
     Device.where(:$or => [
      {:name => Regexp.new(id, Regexp::IGNORECASE)}, 
      {:ip_address_formatted => Regexp.new(id)},
      {:customer_production_ip => Regexp.new(id)}
      ]) 
  end

  def self.portal_search(*args)
    if args.length == 1
      search(args[0]).all
    else
      obj = search(args[0]).all
      unless obj.empty?
        all_asset =Company.where(guid: args[1]).first.asset_list      
        obj.delete_if {|dev| !(all_asset.include?(dev.asset_guid)) }   
      end
      obj
    end

  end

  def self.dat_for_dropdown(asset_list)
    dat = Device.where(:asset_guid => asset_list).sort(:name.asc).all
  end

  def self.dropdown_by_device_class(asset_list, device_class)
    Rails.logger.info("calling dropdown_by_device_class #{device_class}")
    case device_class
      when "Server" 
        dat = Device.where(:asset_guid => asset_list, :uid => Regexp.new('\/zport\/dmd\/Devices\/Server')).sort(:name.asc).all
      when "SysEdge"
        dat = Device.where(:asset_guid => asset_list, :uid => Regexp.new('\/zport\/dmd\/Devices\/Server\/SysEdge')).sort(:name.asc).all
    end
  end

   # append the zenoss_instance attribute to the name using # as the separator
  def self.device_name_with_zenoss_instance(guid)
    z = Device.where(asset_guid: guid).first
    if z
      z.name + "%23" + z.zenoss_instance
    else
      'na'
    end
  end

  # append the zenoss_instance attribute to the guid using # as the separator
  def self.device_guid_with_zenoss_instance(guid)
    z = Device.where(asset_guid: guid).first
    if z
      z.asset_guid + "%23" + z.zenoss_instance
    else
      'na'
    end
  end

  # for an array of devices return the name of the highest occurring zenoss instances
  def self.get_max_zenoss_instance(group_id)
    g = Group.where(id: group_id).first
    assets = g.asset_list
    dat = Device.where(asset_guid: assets).fields(:zenoss_instance).all
    count_array = []
    dat.each do |entry|
      count_array << entry['zenoss_instance'] 
    end
    h = count_array.inject(Hash.new(0)){|total, e|total[e] +=1; total}
    d = h.max_by{|k,v|v}[0] 
  end

  # interate through a group and append the zenoss_instance attribute 
  # to the name using # as the separator
  def self.group_device_name_with_zenoss_instance(group_id)
    g = Group.where(id: group_id).first
    assets_by_name = []
    assets = g.asset_list
    assets.length.times do |i|
      y = device_name_with_zenoss_instance(assets[i])
      assets_by_name << y
    end
    assets_by_name 
  end

  # interate through a group and append the zenoss_instance attribute 
  # to the guid using # as the separator
  def self.group_guid_with_zenoss_instance(group_id)
    g = Group.where(id: group_id).first
    assets_by_name = []
    assets = g.asset_list
    assets.length.times do |i|
      y = device_guid_with_zenoss_instance(assets[i])
      assets_by_name << y
    end
    assets_by_name 
  end

  def zenoss_device
    @zenoss_device ||= SGTools::Zenoss::Device.new(
      asset_guid, 
      :c_asset_guid => asset_guid,
      :z_instance => zenoss_instance
    )
  end

  def component_tree
    zenoss_device.load!.component_tree
  end

  def zenoss_instance_name
    self.zenoss_instance
  end
  
  #This method decides which type of search needs to perform on Splunk based on cma_name value.
  def splunk_search_type
    self.cma_name.blank? ? "syslog" : "opsec"
  end

  # return the type of asset, currently it returns server or router.
  def type
    if self && !self.uid.blank?
      return 'server' if self.uid.match(/\/zport\/dmd\/Devices\/Server/)
      return 'router' if self.uid.match(/\/zport\/dmd\/Devices\/Server\/SysEdge/)
    end
    ''
  end

  def commands
    white_list_commands = command_ids - EXCEPTONACOMMANDS
    Command.where(:command_id => white_list_commands).all.uniq
  end

  def support_show_command?
    !command_ids.blank?
  end

  def support_firewall_change_command?
    (command_ids & APP_CONFIG['cr_change_command_ids']).any?
  end

  def display_ip
    customer_production_ip.presence || ip_address_formatted
  end
end
