module Ehealth
  class ActiveAsset
    include MongoMapper::Document
    key :guid
    key :host
    key :incidents
    key :req_changes
    key :mx_windows
    key :avail
    key :status
    key :company_guid
    key :asset_type
    key :group_ids, Array
    timestamps!

    def self.bulk_load(company_guid)
      begin
        company = ::Company.where(:guid =>company_guid).first
        z_instance = company.zenoss_instance
        unless z_instance.blank?
          asset_summery = fetch_asset_summary(company_guid)
          start_date = (Date.today - 1.month).strftime("%m/%d/%Y")
          end_date =  (Date.today + 1).strftime("%m/%d/%Y")
          company_avail = SGTools::Zenoss::CompanyAvailability.new(
            company_guid.downcase,{:z_instance => z_instance,:start => start_date,:end=> end_date} #TODO: Moved this to conf file
          )
          availability = company_avail.availability
          events = all_events(company_guid, z_instance)
          found_assets = []
          count = 0
          availability.each do |result|
            begin
              asset_guid = result["asset_guid"]
              unless asset_guid.blank?
                active_asset = OpenStruct.new
                active_asset.guid = asset_guid
                active_asset.host = result["title"]
                active_asset.company_guid = company_guid          
                active_asset.avail = result["availability"]
                fetched_asset = find_asset(asset_summery, asset_guid) unless asset_summery.blank?
                unless fetched_asset.blank?
                  active_asset.incidents = fetched_asset[2]
                  active_asset.req_changes = fetched_asset[3]
                  active_asset.mx_windows = fetched_asset [4]
                end
                active_asset.status = self.calculate_active_asset_status(events,asset_guid)
                active_asset.asset_type = result["device_class"].split("/").reverse.join(" ").squish
                # active_asset.group_ids = active_asset.assign_groups
                active_asset.save!
                found_assets.append(active_asset)
                count+= 1
              end
            rescue Exception => e
               Rails.logger.error "Error during creating/updating new asset: #{$!}
                \nBacktrace:\n\t#{e.backtrace.join("\n\t")}"           
            end
          end
          Rails.logger.info "Total assets received from zenoss- #{asset_summery.count}"
          return found_assets
        else
          Rails.logger.info "No zenoss_instance associated with this ehealth company."
        end
      rescue Exception=> e
        Rails.logger.error(e)
        return []
      end
    end

    def self.calculate_active_asset_status(events, guid)
      begin
        events_hash = self.find_event(events, guid)
        unless events_hash.blank?
          #Logic for calculating health for an active_asset
          if events_hash["critical"] > 0
            status = "Critical"
          elsif events_hash["error"] > 0
            status = "Error"
          elsif events_hash["warning"] > 0
            status = "Warning"
          else
            status = "Healthy"
          end
        end
        return status
      rescue Exception => e
        Rails.logger.error(e)
      end
    end

    def self.fetch_asset_summary(company_guid)
      asset_summery = []
      begin
        sn = ServiceNowClient.new
        asset_summery = sn.company_asset_summary(company_guid)
      rescue Exception => e
        Rails.logger.error(e)
      end
      asset_summery
    end 

    def self.find_availability(availability, guid)
       avail = availability.select {|x| x["asset_guid"].downcase ==  guid.downcase }
       avail[0] unless avail.blank?
    end

    def self.find_asset(source, guid)
      source.select{ |x| x[1].downcase == guid.downcase }[0]
    end

     def self.all_events(company_guid, z_instance)
       all_events = SGTools::Zenoss::DevicesByCompany.new(company_guid, {:z_instance => z_instance})
       all_events.devices_by_company
    end

    def self.find_event(events, guid)
      events_hash = {}
      unless events.blank?
        seach_event = events.find{|key,value| key["cAssetGUID"] == guid }
        unless seach_event.blank?
          seach_event['events'].each do |key,val|
            events_hash.merge!({key => val['count']})
          end
          events_hash
        end
      end
      events_hash
    end

  end
end
