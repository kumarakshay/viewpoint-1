class Interface
  include MongoMapper::Document
  key :asset_guid
  key :company_guid
  key :contracting_guid
  key :company_use
  key :component_id
  key :description
  key :device_name
  key :device_guid
  key :internet
  key :interface_id
  key :ip_interface
  key :logical_name
  key :network_name
  key :owner_guid
  key :parent_guid
  key :usage
  key :subtype
  key :billing_port
  key :standard_config
  timestamps!

  def self.report_dropdown(guid)
   dat = Interface.where(company_guid: guid).all
  end

  def lowercase_device_guid
    self[:device_guid].downcase
  end
  
  alias :device_guid :lowercase_device_guid  

end  

#<Interface _id: BSON::ObjectId('50ca312a0e6a51768a0058d6'), 
# billing_port: "router", 
# company_guid: "9D9F65DA-A69F-452C-87F5-A8F36014BCE7", 
# company_use: "NEW CINGULAR WIRELESS PCC LLC", 
# contracting_guid: "9D9F65DA-A69F-452C-87F5-A8F36014BCE7", 
# created_at: Thu, 13 Dec 2012 19:48:02 UTC +00:00, 
# description: "inct_I", 
# device_guid: "7D899F9F-9855-EE85-E040-A2A8567D3CF4", 
# device_name: "smyir2", 
# interface_id: "leJUEDmC8cDAbVojXKEkkg==", 
# internet: "yes", 
# ip_interface: "ge-0/0/5.119", 
# logical_name: "SMYIR2.GE-0/0/5.119", 
# network_name: "RD.7381:6759", 
# standard_config: "yes", 
# subtype: "router", 
# updated_at: Thu, 13 Dec 2012 19:48:02 UTC +00:00> 
