require 'time_conversion'

class DeviceGlance
  include MongoMapper::Document
  key :report_id, String
  key :company_guid, String
  key :device_guid, String
  key :group_guid, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :delivery_time_gmt, String
  key :subscription, Boolean
  key :area, String
  key :cabinet, String

  # down case required for zenoss upgrade
  def company_guid
    read_attribute(:company_guid).downcase  # No test for nil?
  end

  def self.bulk_load(report_id)
    DeviceGlance.delete_all
    dat = Req.where(subscription: true, :report_id => report_id).all
    dat.each do |d|
      self.walk_the_table(d.report_id,d.group_guid, d.company_guid, d.email, d.device_guid, d.frequency, 
        d.delivery_time, d.time_zone, d.area, d.cabinet, d.id)
    end
  end  

  def self.walk_the_table(report_id,group_id,company_guid,email,device_guid,frequency,delivery_time,time_zone,area,cabinet,id)
    statement = Statement.where(report_id: report_id).first
    if group_id != ""
      group = Group.where(id: group_id)
      assets_by_name = []
      group.each do |g|
        assets = g['asset_list']
        assets.length.times do |i|
          y = get_device_name(assets[i])
          assets_by_name << y
        end 
      end
    end
    if device_guid != ""
      assets_by_name = []
      assets_by_name << get_device_name(device_guid)
    end
    dat = self.create(
      :report_id => report_id,
      :subscription_id => id,
      :statement_name => statement.name,
      :email => email,
      :company_guid => company_guid,
      :frequency => frequency,
      :delivery_time => TimeConversion.convert_to_utc(delivery_time, time_zone),
      :asset_list => assets_by_name        
    )
    dat.save
  end

  def self.get_device_name(asset_guid)
    z = Device.where(asset_guid: asset_guid).first
    if z
      z.name + "#" + z.zenoss_instance
    else
      'na'
    end
  end
end