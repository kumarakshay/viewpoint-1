define([
  'sgtools/rails/ui/collections/base',
  '../models/type'
], function(
  BaseCollection,
  TypeModel
){

  var TypeCollection = BaseCollection.extend({
    model: TypeModel,
    url: '/api/ehealth/asset_types',

    initialize: function () {
      // Default sort field and direction
      this.sortField = "name";
      this.sortDirection = "ASC";
    }

  });
  // You don't usually return a collection instantiated
  return TypeCollection;
});
