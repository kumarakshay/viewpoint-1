define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/backup'
], function(_, BaseCollection, BackupModel){
  var BackupCollection = BaseCollection.extend({
    model: BackupModel,
    url: '/api/ehealth/backups'
  });
  // You don't usually return a collection instantiated
  return BackupCollection;
});
