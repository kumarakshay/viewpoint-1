define([
  'sgtools/rails/ui/collections/base',
  '../models/backup_month'
], function(
  BaseCollection,
  BackupMonthModel
){

  var BackMonthCollection = BaseCollection.extend({
    model: BackupMonthModel,
    url: '/api/ehealth/backup_months',

    initialize: function () {
      // Default sort field and direction
      this.sortField = "filename";
      this.sortDirection = "DESC";
    }

  });
  // You don't usually return a collection instantiated
  return BackMonthCollection;
});
