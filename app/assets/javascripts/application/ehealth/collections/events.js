define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/event'
], function(_, BaseCollection, EventModel){
  var EventCollection = BaseCollection.extend({
    model: EventModel,
    url: '/api/ehealth/events'
  });
  // You don't usually return a collection instantiated
  return EventCollection;
});
