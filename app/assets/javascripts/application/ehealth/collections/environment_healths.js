define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/environment_health'
], function(_, BaseCollection, EnvironmentHealthModel){
  var EnvironmentHealthCollection = BaseCollection.extend({
    model: EnvironmentHealthModel,
    url: '/api/ehealth/environment_healths',

    comparator: function(model){
      return model.get('asset_type');
    },

    initialize: function () {
      var self = this;
       // Default sort field and direction
    },

    parse: function(response){
      if (response.result.result) {
        obj = response.result.result.data;
        return _.map(obj, function (value, key) {
          // convert type key to value then parse out the string
          // added to handle json directly from zenoss
          if (key.substring(0, 1) === "/") {
            obj[key].asset_type = key.split("/").reverse().join(' ');
          } else {
            obj[key].asset_type = key;
          }
          return obj[key];
        });
      }
    },

  });
  // You don't usually return a collection instantiated
  return EnvironmentHealthCollection;
});
