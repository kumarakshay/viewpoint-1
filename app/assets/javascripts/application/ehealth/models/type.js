define([
  'sgtools/rails/ui/models/base'
], function(BaseModel){
  var TypeModel = BaseModel.extend({
    parse: function(data) {
      data.id = encodeURIComponent(data.name);
      data.api = "active_assets_by_type";
      return data;
    }
  });
  // Return the model for the module
  return TypeModel;
});

