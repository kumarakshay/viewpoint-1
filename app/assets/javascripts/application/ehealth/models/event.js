define([
	 'sgtools/rails/ui/models/base'
	],
	function(BaseModel){
		var EventModel = BaseModel.extend({
			urlRoot: "/api/ehealth/events",
		})
		return EventModel
	})