define([
       'underscore',
       'underscore.string',
       'backbone',
       'backbone.marionette',
       '../collections/ticketings',
       '../collections/active_assets',
       '../collections/backups',
       '../collections/environment_healths',
       '../collections/events',
       '../views/environment_health',
       '../views/backups',
       '../views/events',
       '../views/ticketing',
       '../views/assets',
       '../models/current_user',
       '../views/unauthorized',
      
], function(
  _,
  _s,
  Backbone,
  Marionette,
  TicketingCollection,
  ActiveAssetCollection,
  BackupsCollection,
  EnvironmentHealthCollection,
  EventCollection,
  EnvironmentHealthView,
  BackupsView,
  EventsView,
  TicketingView,
  AssetsView,
  CurrentUserModel,
  UnauthorizedView
 
) {
  return Marionette.Controller.extend({

    initialize: function(options) {
      var self = this;
      this.app = options.app;
      this.moduleLayout = options.moduleLayout;
      this.pageLayout = options.pageLayout;
    },

    index: function() {
      var self = this;
      this.moduleLayout.on('show', function () {
        var current_user = new CurrentUserModel();
        var omi_user;
        current_user = current_user.fetch({'async':false}).done(function(data){
          omi_user =  data.result.current_user.is_omi ;
        });
        if (!omi_user) {
          self.moduleLayout.ehealth.show(self.pageLayout);
          var ticketing_collection = new TicketingCollection();
          var active_asset_collection = new ActiveAssetCollection();
          var backups_collection = new BackupsCollection();
          var environment_health_collection = new EnvironmentHealthCollection();
          var event_collection = new EventCollection();
        

          var environment_health_view = new EnvironmentHealthView({
            collection: environment_health_collection
          });
          var backups_view = new BackupsView({
            collection: backups_collection
          });
          var events_view = new EventsView({
            collection: event_collection
          });
          var assets_view = new AssetsView({
            collection: active_asset_collection
          });
          var ticketing_view = new TicketingView({
            collection: ticketing_collection
          });

          backups_collection.listenTo(backups_view,"backupMonthChange", function(){
            backups_collection.url = backups_view.urlRoot;
            backups_collection.reset();
            backups_collection.fetch();
          });
         

          environment_health_collection.listenTo(environment_health_view, "choiceChange", function(){
            environment_health_collection.url = environment_health_view.urlRoot;
            environment_health_collection.reset();
            environment_health_collection.fetch();
            // environment_health_view.sync();
          });

          active_asset_collection.listenTo(assets_view, "groupChange", function(){
            active_asset_collection.url = assets_view.urlRoot;
            active_asset_collection.reset();
            active_asset_collection.fetch();
            // environment_health_view.sync();
          });

          self.pageLayout.environment_health.show(environment_health_view);
          self.pageLayout.backups.show(backups_view);
          self.pageLayout.events.show(events_view);
          self.pageLayout.ticketing.show(ticketing_view);
          self.pageLayout.assets.show(assets_view);
        } else {
          var unauthorized_view = new UnauthorizedView({
          });
          self.moduleLayout.ehealth.show(unauthorized_view);
        }
      });
    },

    show: function(id) {
      var self = this;
      //var model = IpBlockModel.findOrCreate({id: id});
    }

  });

});