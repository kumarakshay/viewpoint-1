define([
      'backbone',
      'jquery',
      'underscore',
      'backbone.marionette',
      'hbs!../templates/environment_health',
      'hbs!../templates/environment_health_row'
], function(
  Backbone,
  $,
  _,
  Marionette,
  EnvironmentHealthTmpl,
  EnvironmentHealthRowTmpl
) {

  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: EnvironmentHealthRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var EnvironmentHealthView = Marionette.CompositeView.extend({
    template: EnvironmentHealthTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.environment_health_table tbody',

    ui: {
      choice: '.health_choice',
      choices: '.health_choices > div'
    },

    events: {
      'click @ui.choices': 'set_health_choice'
    },

    urlRoot: "/api/ehealth/environment_healths",

    set_health_choice: function(e){
      var self = this,
      target = $(e.currentTarget);
      // get this text and set span text
      self.ui.choice.text(target.text());
      // set collection
      self.urlRoot = "/api/ehealth/" + target.attr('class');
      // render
      this.trigger('choiceChange');
    },

    thresholds: {
      green : [98,'#379e5f'],
      yellow: [95,'#fabe2c'],
      orange: [90,'#FF6600'],
      red: [0,'#a71117']
    },

    total_color: '',

    initialize: function(){
      var self = this;
      var ajax = self.collection.fetch().done(function(data){
        self.onshow();
      }).fail(function(x,s,e){

      });
      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          c.setSortField("asset_type", "ASC");
          c.sort();
          c.trigger('reset');
        }
      });
    },

    onshow: function(){
      var self = this;
      var count = 0, percentage = 0;
      $('.environment_health_table .asset_health_percentage').each(function(){
        percentage = percentage + (parseFloat($(this).attr('data-health-percentage'))*parseInt($(this).attr('data-health-assets'),10));
        count = count + parseInt($(this).attr('data-health-assets'),10);

      });

      this.plot_health_chart(percentage/count);
    },

    plot_health_chart: function(total){
      var self = this;
      total = parseInt(total,10);
        if (total >= self.thresholds.green[0]) {
         self.total_color = self.thresholds.green[1];
        } else if (total >= self.thresholds.yellow[0])  {
          self.total_color = self.thresholds.yellow[1];
        } else if (total >= self.thresholds.orange[0])  {
          self.total_color = self.thresholds.orange[1];
        } else if (total >= self.thresholds.red[0]) {
          self.total_color = self.thresholds.red[1];
        }


      var health_chart_percentage;
      var that = this;

      if (isNaN(total)) {
        $('#env_dash_health .module_loading').hide();
        this.health_chart = $.plot('#health_chart_canvas', [{data:0, color: self.total_color}, {data:100, color: '#f7f7f7'}], {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.78,
                    stroke: {width: 0},
                }
            }
        });
      $('#health_chart_percentage').html('&ndash; &ndash;');
      } else {
        health_chart_percentage = Math.floor(total);
        this.health_chart_end_percentage = health_chart_percentage;
        this.health_chart_current_percentage = 0;
        this.health_chart_current_frame = 1;
        $('#env_dash_health .module_loading').hide();
        this.health_chart = $.plot('#health_chart_canvas', [{data:0, color: self.total_color}, {data:100, color: '#f7f7f7'}], {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.78,
                    stroke: {width: 0},
                }
            }
        });
        this.health_chart_interval = setInterval(function(){
          that.animate_health_chart();
        }, 10); //1000ms - 100 frames
        return false;
      }
    },

    animate_health_chart: function(){
      var self = this;
      this.health_chart_current_percentage = this.ease_out_expo(this.health_chart_current_frame, 0, this.health_chart_end_percentage, 100); //100 frames
      if(this.health_chart_current_percentage >= this.health_chart_end_percentage){ //if past or at the end percentage, clear the timer
        clearInterval(this.health_chart_interval);
      }
      var plot_percentage = Math.ceil(this.health_chart_current_percentage);
      
      $('#health_chart_percentage').text(plot_percentage + '%');
      this.health_chart.setData([{data:this.health_chart_current_percentage, color: self.total_color}, {data:100-this.health_chart_current_percentage, color: '#f7f7f7'}]);
      this.health_chart.draw();
      this.health_chart_current_frame++;
    },
    ease_out_expo: function (t, b, c, d) {
      return c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b;
    },

  });
  return EnvironmentHealthView;
});