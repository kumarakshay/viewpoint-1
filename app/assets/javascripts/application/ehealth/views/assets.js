define([
      'backbone',
      'underscore',
      'backbone.marionette',
      '../collections/groups',
      '../collections/types',
      '../views/option_view',
      'hbs!../templates/assets',
      'hbs!../templates/active_asset_row'
], function(
  Backbone,
  _,
  Marionette,
  GroupsCollection,
  TypesCollection,
  OptionListView,
  AssetsTmpl,
  ActiveAssetRowTmpl
) {
  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: ActiveAssetRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var AssetsView =  Marionette.CompositeView.extend({
    template: AssetsTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.active_asset_table tbody',

    initialize: function(){
      var self = this;

      self.sort_dir = "ASC";
      self.groups = new GroupsCollection();
      self.types_collection = new TypesCollection();
      self.collection.fetch().done(function(){
        self.cloned = self.collection.clone();
        self.filteredData = self.cloned.RefineCollection();
        self.groups.fetch({reset: true});
        self.types_collection.fetch({reset: true}).done(function(){});
        self.onshow();
      });

      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        }
      });
    },

    ui: {
      status: 'input[name="status"]',
      host: 'input[name="host"]',
      incident: 'input[name="incident"]',
      changes: 'input[name="changes"]',
      mx_windows: 'input[name="mx_windows"]',
      avail: 'input[name="avail"]',
      text_search_filter: '.text_search_filter',
      sort: '.sort',
      group: 'select[name="group_choices"]',
      group_optgroup: 'optgroup.groups',
      type_optgroup: 'optgroup.types'
    },

    events: {
      'keyup @ui.text_search_filter': 'applyFilter',
      'click @ui.sort': 'doSort',
      'change @ui.group': 'set_group_choice'
    },

    onshow: function(){
      var self = this;
      $('#env_dash_assets .module_loading').hide();
      self.ui.group.fadeTo(500,1,function(){
        self.ui.group.prop('disabled',null).attr('style','');
      });
      var groupOptionsView = new Marionette.CollectionView({
        el: this.ui.group_optgroup,
        itemView: OptionListView,
        collection: this.groups
      });

      groupOptionsView.render();

      var typesOptionsView = new Marionette.CollectionView({
        el: this.ui.type_optgroup,
        itemView: OptionListView,
        collection: this.types_collection
      });

      typesOptionsView.render();
    },

    applyFilter: function(e){
      var self = this;
      var status = this.ui.status.val();
      var host = this.ui.host.val();
      var incident = this.ui.incident.val();
      var changes = this.ui.changes.val();
      var mx_windows = this.ui.mx_windows.val();
      var avail = this.ui.avail.val().replace('%','');
      var filterCollection = self.filteredData.where({status: status, host: host,
        incident: incident, changes: changes, mx_windows: mx_windows, avail: avail });
      self.collection.reset(filterCollection.models);
      if (filterCollection.models.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
      
    },

    doSort: function(e) {
      var self = this;
      var modelAttribute = $(e.currentTarget).data('sortby');
      self.sort_dir  = self.sort_dir === "ASC"? "DESC" : "ASC";
      self.collection.setSortField(modelAttribute, self.sort_dir);
      self.collection.sort();
      self.collection.trigger('reset');
      $(e.currentTarget).siblings().removeClass('asc desc');
      $(e.currentTarget).removeClass('asc desc').addClass(self.sort_dir.toLowerCase());
    },

    set_group_choice: function(e){
      var self = this;
      target = $(e.currentTarget);
      var group_guid =  target.val();
      if (group_guid === '')
      {
        self.urlRoot = "/api/ehealth/active_assets";
      }else {
        self.urlRoot = "/api/ehealth/" + group_guid;
      }
      this.trigger('groupChange');
    },
  });

  return AssetsView;
});
  