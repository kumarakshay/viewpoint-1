define([
      'backbone',
      'underscore',
      'backbone.marionette',
      'hbs!../templates/unauthorized_masq'
], function(
  Backbone,
  _,
  Marionette,
  UnauthorizedMasqTmpl
) {
  var UnauthorizedMasqView = Marionette.ItemView.extend({
    template:  UnauthorizedMasqTmpl
  });
  return UnauthorizedMasqView
})