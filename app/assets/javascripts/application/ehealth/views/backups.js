define([
       'backbone',
       'underscore',
       'jquery',
       'domReady',
       'backbone.marionette',
       '../views/month_option_view',
       '../collections/backup_months',
       'hbs!../templates/backups'
], function(
  Backbone,
  _,
  $,
  domReady,
  Marionette,
  OptionListView,
  BackupMonthCollection,
  BackupsTmpl
) {
  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var BackupsView = Marionette.ItemView.extend({
    tagName: 'div',
    template:  BackupsTmpl,
    backups_data: [],

    initialize: function(){
      var self = this;
      this.month_names= ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      this.backup_data = [];
      self.backup_month_collection = new BackupMonthCollection();
      self.backup_month_collection.fetch({reset: true}).done(function(){});
      self.collection.fetch().done(function(m){
        if (m.result.length !== 0){
            self.preProcessGraphData(self.collection);
        } else {
          self.$el.parent().hide();
        }
      });
      self.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          self.preProcessGraphData(c)
        }
      });
    },

    preProcessGraphData: function(c){
      var self = this;
      self.backups_data = [];
      c.each(function(model){
        var date_string = model.get('date_string');
        var success_rate = model.get('success_rate');
        self.backups_data.push([date_string, success_rate]);
      });
      self.onshow();
    },
   
    onshow: function()
    {
      var self = this;
      self.ui.toggle_graph.find('span.left').removeClass("on");
      self.ui.toggle_graph.find('span.right').addClass("on");
      self.plot_backups_chart("month");
      self.redraw_charts_db = _.debounce(self.redraw_charts, 50);
      $(window).on("resize.ehealth", function(){
        self.redraw_charts_db();
      });
    },

    ui: {
      toggle_graph: '.chart_toggle',
      backup_months: 'select[name="month_choice"]'

    },

    events:{
      "click @ui.toggle_graph": "toggleBackupGraph",
      'change @ui.backup_months': 'set_backup_month_choice'
    },

    set_backup_month_choice: function(e){
      var self = this;
      target = $(e.currentTarget);
      var filename =  target.val();
      if (filename !== '')
      {
        // Hide the mode option for last month backup graph
        var temp = filename.split("-")
        var file_date = new Date(temp[3] + "/" + temp[4] + "/" + temp[2]);
        var today = new Date();
        if(file_date < today) {
          self.ui.toggle_graph.hide();
        } else{
          self.ui.toggle_graph.show();
        }
        self.urlRoot = "/api/ehealth/backups_for_month?filename=" + filename;
        this.trigger('backupMonthChange');
      }
    },

    toggleBackupGraph: function(event){
      event.preventDefault();
      var self = this,
        clicked = $(event.target);
      if(clicked.hasClass("on")) {
          return false;
      } else {
        self.ui.toggle_graph.find('span').toggleClass("on");
        var mode = clicked.data("mode");
        self.plot_backups_chart(mode);
      }
        return false;
    },

    plot_backups_chart: function(mode){
      var that = this,
        self = this;
        plot_data = [];

      // if no data,return
      if(self.backups_data.length === 0){
        return false;
      } else {
        $('#env_dash_backups').fadeIn();
      }
      
      //setup plot data

      for (i = self.backups_data.length-1; i > 0; i--) {

        // condition inside current loop
        if ( mode === "week" && i < (self.backups_data.length-7) ) break;
        if ( mode === "month" && i < (self.backups_data.length-31) ) break;

        plot_data.push([ self.backups_data[i][0], self.backups_data[i][1] ]);

      }

      //setup ticks array
      var ticks = [];
      for(var i = 0; i < plot_data.length; i++) {
          ticks.push(plot_data[i][0]);
      }

      //setup ticks formatter
      var tick_formatter = null, xaxis_counter;
      if(mode == "month"){
        tick_formatter = function(val, axis){
          if(val == axis.options.ticks[0] || val == axis.options.ticks[axis.options.ticks.length -1] ||
             val == axis.options.ticks[Math.floor(axis.options.ticks.length/2)]) {
            var d = new Date(val);
             return (d.getUTCMonth()+1) + "/" + d.getUTCDate();
          } else {
            return ' ';
          }
        };
      } else {
        tick_formatter = function(val, axis){
          if(val == axis.options.ticks[0] || val == axis.options.ticks[axis.options.ticks.length -1] ||
             val == axis.options.ticks[Math.floor(axis.options.ticks.length/2)]) {
             var d = new Date(val);
             return (d.getUTCMonth()+1) + "/" + d.getUTCDate();
          } else {
            return ' ';
          }
        };
      }
      
      $('#env_dash_backups .module_loading').hide();
      $.plot("#backups_chart_canvas", [plot_data], {
          series: {
            lines: { show: true, fill: true, fillColor: "rgba(235,245,239,0.6)" },
          },
          xaxis: {
            mode: "time",
            ticks: ticks,
            labelWidth: 40,
            tickFormatter: tick_formatter,
            align: "center"
          },
          yaxis:{
            show: false,
            max: 100
          },
          colors: ['#379e5f'],
          grid: {
              show: true,
              color: '#000000',
              borderWidth: {top: 0, left: 1, right: 1, bottom: 1},
              borderColor: '#e4e4e4',
              hoverable: true,
              autoHighlight: false,
              margin: 0
          }
      });

      //bind hover on plot data points
      $('#env_dash_backups_tooltip').remove();
      $("<div id='env_dash_backups_tooltip'></div>").appendTo("#env_dash_backups");
      that.show_backups_chart_tooltip_debounced = _.debounce(that.show_backups_chart_tooltip, 10);
      $("#backups_chart_canvas").bind("plothover", function (event, pos, item) {
          if (item) {
            var date = new Date(item.datapoint[0]);
            var axisLabel = (date.getUTCMonth()+1) + "/" + date.getUTCDate();
            var data = Math.floor(item.datapoint[1]) + ', ' + axisLabel;
            that.show_backups_chart_tooltip_debounced(data,item);
          }
          else{
            $("#env_dash_backups_tooltip").hide();
          }
      });
    },

    show_backups_chart_tooltip: function(data, item){
      var backups_chart_offset = $('#env_dash_backups').offset();
      var item_page_x = item.pageX-11-backups_chart_offset.left;
      var item_page_y = item.pageY-27-backups_chart_offset.top;
      $("#env_dash_backups_tooltip").html(data).css({top: item_page_y, left: item_page_x}).show();
    },

    redraw_charts: function(){
      var backups_mode = $('#env_dash_backups .chart_toggle .toggle.on').data("mode");
      this.plot_backups_chart(backups_mode);
    },

    onRender: function(){
      var self = this;
      var monthOptionsView = new Marionette.CollectionView({
        el: this.ui.backup_months,
        itemView: OptionListView,
        collection: this.backup_month_collection
      });
    },

  });
  return BackupsView;
});