define([
       'underscore',
       'backbone.marionette',
       'hbs!../templates/options_list'
], function(_,Marionette,optionListTmpl) {

  var OptionView = Marionette.ItemView.extend({
    tagName: "option",
    initialize: function(options) {
    },
    attributes: function() {
      return {
        value: this.model.get('id') + '/' + this.model.get('api')
      };
    },
    template: optionListTmpl,
    serializeData: function() {
      if (this.optName) {
        return {
          name: this.optName
        };
      }
      return this.model.toJSON({withAllRelations: true});
    }
  });
  return OptionView;
});
