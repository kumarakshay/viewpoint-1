define([
       'backbone',
       'underscore',
       'jquery',
       'backbone.marionette',
        'hbs!../templates/events'
], function(
  Backbone,
  _,
  $,
  Marionette,
  EventsTmpl
) {
 
  var EventsView = Marionette.ItemView.extend({
    template: EventsTmpl,

    events_data: [],

    initialize: function(){
      var self = this;
      self.events_data = [];
      self.month_names= ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      self.collection.fetch().done(function(){
        self.generateEventData();
        self.onshow();
      });
    },
    
    generateEventData: function(){
      var self = this;
      self.collection.each(function(model){
        var events_data = {};
        var date = model.get("date");
        events_data.critical = model.get("critical");
        events_data.error = model.get("error");
        events_data.warning = model.get("warning");
        events_data.info = model.get("information");
        self.events_data.push([date, events_data]);
      });
    },

    onshow: function(){
      var self = this;
      this.plot_events_chart("month");
      
      self.redraw_charts_db = _.debounce(self.redraw_charts, 50);
      $(window).on("resize.ehealth", function(){
        self.redraw_charts_db();
      });
    },

    ui: {
      events_critical: "#events_critical",
      events_error: "#events_error",
      events_warning: "#events_warning",
      events_info: "#events_info",
      toggle_graph: '.chart_toggle'
    },

    events: {
      "change @ui.events_critical": "changeEventGraph",
      "change @ui.events_error": "changeEventGraph",
      "change @ui.events_warning": "changeEventGraph",
      "change @ui.events_info": "changeEventGraph",
      "click @ui.toggle_graph": "toggleEventGraph"
    },

    toggleEventGraph: function(event){
      event.preventDefault();
      var self = this,
        clicked = $(event.target);
      if(clicked.hasClass("on")) {
          return false;
      } else {
        self.ui.toggle_graph.find('span').toggleClass("on");
        var mode = clicked.data("mode");
        self.plot_events_chart(mode);
      }
        return false;
    },

    changeEventGraph: function(){
      var mode = $('#env_dash_events .chart_toggle .toggle.on').data("mode");
      this.plot_events_chart(mode);
      this.redraw_charts();
    },

    plot_events_chart: function(mode){

      var self = this;
      var i;

      var data = {};

      data.critical_plot = [];
      data.error_plot = [];
      data.warning_plot = [];
      data.info_plot = [];

      //if no events data, return
      if(self.events_data.length === 0){
        return false;
      }

      for (i = self.events_data.length-1; i > 0; i--) {

        // condition inside current loop
        if ( mode === "week" && i < (self.events_data.length-7) ) break;

        data.critical_plot.push([ self.events_data[i][0], self.events_data[i][1]['critical'] ]);
        data.error_plot.push([ self.events_data[i][0], self.events_data[i][1]['error'] ]);
        data.warning_plot.push([ self.events_data[i][0], self.events_data[i][1]['warning'] ]);
        data.info_plot.push([ self.events_data[i][0], self.events_data[i][1]['info'] ]);

      }

      //setup ticks
      var ticks = [];
      for(i = 0; i < data.critical_plot.length; i++) {
          ticks.push(data.critical_plot[i][0]);
      }

      //setup ticks formatter
      var that = this;
      var tick_formatter = null;
      if(mode == "month"){
        var xaxis_counter = 0;
        tick_formatter = function(val, axis){
          if((xaxis_counter-1)%7 === 0 || xaxis_counter === 1){
            xaxis_counter++;
            var d = new Date(val);
            return that.month_names[d.getUTCMonth()] + "&nbsp;" + d.getUTCDate();
          }
          else{
            xaxis_counter++;
            return ' ';
          }
        };
      }
      else{
        tick_formatter = function(val, axis){
          var d = new Date(val);
            return that.month_names[d.getUTCMonth()] + "&nbsp;" + d.getUTCDate();
        };
      }

      //construct plot data based on which checkboxes are checked
      var plot_data = [];
      var line_colors = [];
      var fill_colors = [];
      var fill = null;

      if($('#events_critical').is(":checked")){
        plot_data.push(data.critical_plot);
        line_colors.push('#a71117');
        fill_colors.push({ opacity: 0.15 });
      }

      if($('#events_error').is(":checked")){
        plot_data.push(data.error_plot);
        line_colors.push('#fb9056');
        fill_colors.push({ opacity: 0.15 });
      }

      if($('#events_warning').is(":checked")){
        plot_data.push(data.warning_plot);
        line_colors.push('#dba012');
        fill_colors.push({ opacity: 0.15 });
      }

      if($('#events_info').is(":checked")){
        plot_data.push(data.info_plot);
        line_colors.push('#5291ae');
        fill_colors.push({ opacity: 0.15 });
      }

      //if no events selected, show placeholder
      if(plot_data.length === 0)
        $('#events_chart_placeholder').fadeIn();
      else
        $('#events_chart_placeholder').hide();

      if(line_colors.length === 1){
        fill_colors = null;
        fill = 0.15;
      }
      else{
        fill = true;
        fill_colors = { colors: fill_colors };
      }

      //plot the data
      $('#env_dash_events .module_loading').hide();
      $.plot("#events_chart_canvas", plot_data, {
          series: {
            lines: {
              show: true,
              fill: fill,
              fillColor: fill_colors
            },
            shadowSize: 0
          },
          xaxis: {
            mode: "time",
            ticks: ticks,
            labelWidth: 40,
            tickFormatter: tick_formatter
          },
          yaxis:{
            show: false
          },
          colors: line_colors,
          grid: {
              show: true,
              color: '#000000',
              borderWidth: {top: 0, left: 1, right: 1, bottom: 1},
              borderColor: '#e4e4e4',
              hoverable: true,
              autoHighlight: false,
              margin: 0
          }
      });

      //bind hover on plot data points
      $('#env_dash_events_tooltip').remove();
      $("<div id='env_dash_events_tooltip'></div>").appendTo("#env_dash_events");
      that.show_events_chart_tooltip_debounced = _.debounce(that.show_events_chart_tooltip, 10);
      $("#events_chart_canvas").bind("plothover", function (event, pos, item) {
          if (item) {
            var data = item.datapoint[1];
            that.show_events_chart_tooltip_debounced(data,item);
          }
          else{
            $("#env_dash_events_tooltip").hide();
          }
      });

    },

     show_events_chart_tooltip: function(data, item){
      var events_chart_offset = $('#env_dash_events').offset();
      var item_page_x = item.pageX-11-events_chart_offset.left;
      var item_page_y = item.pageY-27-events_chart_offset.top;
      $("#env_dash_events_tooltip").html(Math.floor(data)).css({top: item_page_y, left: item_page_x}).show();
    },

    redraw_charts: function(){
      var events_mode = $('#env_dash_events .chart_toggle .toggle.on').data("mode");
      this.plot_events_chart(events_mode);
    },

  });
  return EventsView;
});