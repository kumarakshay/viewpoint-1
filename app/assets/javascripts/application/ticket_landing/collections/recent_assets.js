define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/recent_asset'
], function(_, BaseCollection, RecentAssetModel){
  var RecentAssetsCollection = BaseCollection.extend({
    model: RecentAssetModel,
    url: '/api/ticket_landing/recent_assets'
  });
  // You don't usually return a collection instantiated
  return RecentAssetsCollection;
});
