define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/current_user'
], function(_, BaseCollection, CurrentUserModel){
  var CurrentUsersCollection = BaseCollection.extend({
    model: CurrentUserModel,
    url: '/api/current_users'
  });
  // You don't usually return a collection instantiated
  return CurrentUsersCollection;
});
