define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/ticket'
], function(_, BaseCollection, TicketModel){
  var TicketCollection = BaseCollection.extend({
    model: TicketModel,
    url: '/api/ticket_landing/tickets'
  });
  // You don't usually return a collection instantiated
  return TicketCollection;
});