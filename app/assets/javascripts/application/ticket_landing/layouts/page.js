define([
 'backbone.marionette',
 'hbs!../templates/main_page'
 ], function(Marionette, mainTemplate) {
  var MainLayout =  Marionette.Layout.extend({
    template: mainTemplate,
    regions: {
      quick_link: ".quick-links",
      sn_link: '.big-buttons',
      ticket_stats: "#ticket_stat",
      asset_health: "#asset_health",
      recent_assets: ".recent-assets",
      tickets : '#tickets',
      grid_col_1 : '.grid_col_1',
      grid_col_2 : '.grid_col_2',
      top_buttons: '.top'
    }
  });
  return MainLayout;
});