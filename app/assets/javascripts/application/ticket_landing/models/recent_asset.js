define([
   'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var RecentAssetModel = BaseModel.extend({
      urlRoot: "/api/ticket_landing/recent_assets",
    });
    return RecentAssetModel;
  });