define([
   'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var TicketStateModel = BaseModel.extend({
      urlRoot: "/api/ticket_landing/ticket_stats",
      toJSON: function(){
        // get the standard json for the object
        var data = Backbone.Model.prototype.toJSON.apply(this, arguments);

        // check for specific types and change to --
        if (data.type === 'Maintenance') {
          data = null;
        }
        if(data && !data.open) {
          data.open = 0;
        }
         if(data && !data.closed) {
          data.closed = 0;
        }
        return data;
      }

    });
    return TicketStateModel;
  });