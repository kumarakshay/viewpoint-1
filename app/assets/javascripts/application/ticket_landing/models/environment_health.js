define([
  'sgtools/rails/ui/models/base'
],
function(BaseModel){
  var EnvironmentHealthModel = BaseModel.extend({

    urlRoot: "/api/ticket_landing/asset_health",
    thresholds: {
      green : [98,'#379e5f'],
      yellow: [95,'#fabe2c'],
      orange: [90,'#FF6600'],
      red: [0,'#a71117']
    },
    health_percentage: {
      critical: 0,
      error: 50,
      warning: 75,
      ok: 100
    },
    total: 0,
    calculate_health_score: function(data){
      var health = {},
        self = this;
      // calculate individual
      health.ok = self.health_percentage.ok * (data.total_devices - (data.sev_counts.warning + data.sev_counts.error + data.sev_counts.critical));
      // health.ok = self.health_percentage.ok * data.sev_counts.ok;
      health.warning = self.health_percentage.warning * data.sev_counts.warning;
      health.error = self.health_percentage.error * data.sev_counts.error;
      health.critical = self.health_percentage.critical * data.sev_counts.critical;
      health.total = ((health.ok+health.warning+health.error+health.critical)/data.total_devices).toFixed(1);
      return health.total;
    },
    calculate_indicator: function(percent){
      var indicator,
        self = this;
        if (percent >= self.thresholds.green[0]) {
          indicator = "indicator_green";
        } else if (percent >= self.thresholds.yellow[0])  {
          indicator = "indicator_yellow";
        } else if (percent >= self.thresholds.orange[0])  {
          indicator = "indicator_orange";
        } else if (percent >= self.thresholds.red[0]) {
          indicator = "indicator_red";
        }
        return indicator;
    },
    toJSON: function(){
      var self = this;
      // get the standard json for the object
      var data = Backbone.Model.prototype.toJSON.apply(this, arguments);
      // calculations
      data['asset_health_percentage'] = this.calculate_health_score(data);
      var total = this.total + data.asset_health_percentage;
      data.total = total;
      this.total = total;
      data.indicator = this.calculate_indicator(data.asset_health_percentage);
      return data;
    },
  });
  return EnvironmentHealthModel;
});