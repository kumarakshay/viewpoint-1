define([
   'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var CurrentUserModel = BaseModel.extend({
      urlRoot: "/api/current_users",
    });
    return CurrentUserModel;
  });