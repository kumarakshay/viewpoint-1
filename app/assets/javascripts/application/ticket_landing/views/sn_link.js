define([
  'backbone',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/sn_link'
], function(
  Backbone,
  _,
  Marionette,
  SnLinkTmpl
) {
  var SnLinkView =  Marionette.ItemView.extend({
    template: SnLinkTmpl,
     ui: {
      grant_access: ".link-site-access"
    },
    initialize: function(options) {
      var self = this;
      if(options) {
        self.sn_URL = options.sn_URL;
        self.grant_access = options.grant_access;  
      }
    },
    serializeData: function() {
      var self = this;
        return {
            sn_URL: self.sn_URL,
            grant_access: self.grant_access === '0' ? 'inactive': ''
        };
    },
    onRender: function() {
      if(this.grant_access === '0') {
        this.ui.grant_access[0].disabled = true;
      }
    }
  });

  return SnLinkView;
});