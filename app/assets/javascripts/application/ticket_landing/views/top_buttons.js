define([
       'backbone',
       'underscore',
       'backbone.marionette',
       'hbs!../templates/top_buttons'
], function(
  Backbone,
  _,
  Marionette,
  top_buttons
) {
  var TopButtons = Marionette.ItemView.extend({
    tagName: 'div',
    id: "top-buttons",
    template:  top_buttons,
    ui: {
      authorization: "#authorization-matrix",
      cti: "#cti",
    },
    initialize: function(options) {
      var self = this;
      if(options) {
        self.sn_URL = options.sn_URL;
        self.authorization = options.authorization;
        self.grant_access = options.grant_access;  
        self.company_guid = options.company_guid;
      }
    },
    events: {
    'click @ui.cti': 'displayCTI',
    },
    displayCTI: function() {
      var evt = document.createEvent('Event');
      evt.initEvent('displayCTI', true, true);
      document.dispatchEvent(evt);
    },
    serializeData: function() {
      var self = this;
        return {
            sn_URL: self.sn_URL,
            authorization: self.authorization === '1' ? true: false,
            grant_access: self.grant_access === '0' ? 'inactive': '',
            company_guid: self.company_guid
        };
    }
  });
  return TopButtons;
});