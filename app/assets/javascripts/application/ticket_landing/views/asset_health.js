define([
  'backbone',
  'jquery',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/environment_health',
  'hbs!../templates/environment_health_row'
], function(
  Backbone,
  $,
  _,
  Marionette,
  EnvironmentHealthTmpl,
  EnvironmentHealthRowTmpl
) {

  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: EnvironmentHealthRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td style="text-align: center;">No Results Found</td>')
  });

  var AssetHealthView = Marionette.CompositeView.extend({
    template: EnvironmentHealthTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.environment_health_table tbody',

    ui: {
      choices: 'select[name="health_choices"]',
      health_label: '#health_type'
    },

    events: {
      'change @ui.choices': 'set_health_choice'
    },

    urlRoot: "/api/ticket_landing/asset_health",

    set_health_choice: function(e) {
      var self = this,
        target = $(e.currentTarget);
      var selected_opt = target.val();
      if (selected_opt === 'Groups') {
        self.urlRoot = "/api/ticket_landing/groups_health";
        self.ui.health_label.text("All Groups");
      } else {
        self.urlRoot = "/api/ticket_landing/asset_health";
        self.ui.health_label.text("All Assets");
      }
      this.trigger('choiceChange');
    },

    thresholds: {
      green: [98, '#379e5f'],
      yellow: [95, '#fabe2c'],
      orange: [90, '#FF6600'],
      red: [0, '#a71117']
    },

    total_color: '',

    initialize: function() {
      var self = this;
      var ajax = self.collection.fetch().done(function(data) {
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        } else {
          self.collection.setSortField("asset_type", "ASC");
          self.collection.sort();
          self.collection.trigger('reset');
          self.plot_health_chart();
        }
      });

      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          self.collection.setSortField("asset_type", "ASC");
          self.collection.sort();
          self.collection.trigger('reset');
          self.plot_health_chart();
        } 
      });
    },

    onShow : function(view) {
      var self = this;
      if (self.collection.length > 0) {
        self.plot_health_chart();
      }
      $(window).on("resize.asset-health", function(){
        self.plot_health_chart();
      });
    },

    onClose : function(view) {
      $(window).off(".asset-health");
    },

    plot_health_chart: function() {
      var self = this;
      var count = 0,
        score = 0,
        percentage = 0;

      $('.environment_health_table .asset_health_percentage').each(function() {
        percentage = percentage + (parseFloat($(this).attr('data-health-percentage')) * 
            parseInt($(this).attr('data-health-assets'), 10));
        count = count + parseInt($(this).attr('data-health-assets'), 10);
      });
      score = Math.floor(percentage / count);
      if(!_.isNaN(score)) {
       if (score >= self.thresholds.green[0]) {
          self.total_color = self.thresholds.green[1];
        } else if (score >= self.thresholds.yellow[0]) {
          self.total_color = self.thresholds.yellow[1];
        } else if (score >= self.thresholds.orange[0]) {
          self.total_color = self.thresholds.orange[1];
        } else if (score >= self.thresholds.red[0]) {
          self.total_color = self.thresholds.red[1];
        }
        this.health_chart_end_percentage = score;
        this.health_chart_current_percentage = 0;
        this.health_chart_current_frame = 1;

        var data = [{
          data: [
            [1, score]
          ],
          color: self.total_color
        }, {
          data: [
            [1, 100 - score]
          ],
          color: '#CDCDCD'
        }];

        this.asset_health_chart = $.plot('#asset-health-chart', data, {
          series: {
            pie: {
              innerRadius: 0.82,
              show: true,
              stroke: {
                width: 0
              },
            }
          }
      });
      if(this.health_chart_interval) {
        clearInterval(this.health_chart_interval);
      }
      this.health_chart_interval = setInterval(function() {
        self.animate_health_chart();
      }, 10);
      }  
    },

    animate_health_chart: function() {
      var self = this;
      this.health_chart_current_percentage = this.ease_out_expo(this.health_chart_current_frame, 0, this.health_chart_end_percentage, 100); //100 frames
      if (Math.ceil(self.health_chart_current_percentage) >= self.health_chart_end_percentage) { //if past or at the end percentage, clear the timer
        clearInterval(self.health_chart_interval);
      }
      var plot_percentage = Math.ceil(this.health_chart_current_percentage);
      var info_p = $('.box.asset-health .chart-container p');
      info_p.children('span:first-child').text(plot_percentage + '%');
      info_p.children('span:last-child').text('Health Score');
      this.asset_health_chart.setData([{
        data: this.health_chart_current_percentage,
        color: self.total_color
      }, {
        data: 100 - this.health_chart_current_percentage,
        color: '#f7f7f7'
      }]);
      this.asset_health_chart.draw();
      this.health_chart_current_frame++;
    },
    ease_out_expo: function(t, b, c, d) {
      return c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },

  });
  return AssetHealthView;
});