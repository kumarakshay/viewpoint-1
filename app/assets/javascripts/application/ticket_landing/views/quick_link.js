define([
  'backbone',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/quick_link'
], function(
  Backbone,
  _,
  Marionette,
  QuickLinkTmpl
) {
  var QuickLinkView =  Marionette.ItemView.extend({
    template: QuickLinkTmpl,
    ui: {
        dns_link: ".dns-link"
    },
    initialize: function (options) {
        var self = this;
        if(options) {
           self.dns_link = options.dns_link;
           self.user_guide_link = options.user_guide_link;
        }
        // Homepage mobile quick links and new ticket
        $('#mobile-buttons').children('button').click(function () {
            if ($(this).is("#quick-links-button")) {
                $('main').after('<div class="clone-window quick-links"></div>');
                $('.quick-links').find('.links-list').clone().appendTo('.clone-window');
            }
            if ($(this).is("#new-ticket-button")) {
                $('main').after('<div class="clone-window big-buttons"><div class="content"></div></div>');
                $('.big-buttons').find('.column').each(function () {
                    $(this).clone().appendTo('.clone-window .content');
                });
            }

            var header_height = $('body').children('#header').outerHeight();
            var body_width = $('body').outerWidth();
            var main = $('main');
            var clone_window = $('.clone-window');

            clone_window.prepend('<a class="back">Dashboard</a>');
            $('html').addClass('overflow-x');


            clone_window.css({
                'padding-top': header_height
            }).animate({x: body_width}, 0);

            main.css({
                'width': body_width,
                'position': 'fixed'
            }).animate({x: -body_width}, function () {
            });

            clone_window.show().animate({x: 0}, function () {
                $(this).css('width', '100%');
                $('html').removeClass('overflow-x');
            });

            clone_window.find('a.back').click(function () {
                $('html').addClass('overflow-x');

                clone_window.animate({x: body_width}, function () {
                    $(this).remove();
                });

                main.animate({x: 0}, function () {
                    $('html').removeClass('overflow-x');
                    $(this).css({
                        'width': 'auto',
                        'position': 'static'
                    })
                });

            })
        });
        // Close on resize
        var windowWidth = $(window).width();
        $(window).resize(function(){
            if ($(window).width() != windowWidth) {
                windowWidth = $(window).width();
                if($('.clone-window').length){
                    $('.clone-window').find('a.back').click()
                }
            }
        });
    },
    serializeData: function() {
        var self = this;
        return {
            dns_link: self.dns_link ? ' ' : 'inactive',
            user_guide_link: self.user_guide_link
        };
    },
    onRender: function() {
      if(!this.dns_link) {
        this.ui.dns_link[0].disabled = true;
      }
    }
  });

  return QuickLinkView;
});