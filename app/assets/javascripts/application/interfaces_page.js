define([
  'sg/jquery',
  'sg/graphs',
  'datatables'
], function(
  $,
  sg_graphs
) {
  return({
    init: function() {
      // Datatables
      $('#devices_list').dataTable({
        "fnDrawCallback": function() {
          sg_graphs.interface_init();
        }
      });
    },
  });
});
