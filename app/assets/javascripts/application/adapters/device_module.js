define([
       'backbone.marionette',
       '../device_page'
], function (Marionette, page) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
      //This function gets called when user clicks on Command Runner icon
      //We are setting the stored_url in the cookie for rediecting properly after 2f authentication 
      $('#show_command').click(function(){
          var device_id = $(this).attr('rel');
          var date = new Date();
          date.setTime(date.getTime()+(300*1000));
          var expires = "; expires="+date.toGMTString();
          document.cookie="stored_url=index.html#\/cr-show\/" + device_id + expires;
          window.location = '/ui/2f/index.html#/cr-show/' + device_id; 
       })
    },
    onStart: function(options) {
      page.init(options.guid, options.company_guid);
    }
  });
});
