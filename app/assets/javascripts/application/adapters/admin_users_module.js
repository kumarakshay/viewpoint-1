define([
  'backbone.marionette',
  'application/admin/users_page',
  'application/admin/users_management'
], function (
  Marionette,
  page,
  user_mng
) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      // page.init();
      if (options.guid === undefined){
        user_mng.init();
      } else {
        user_mng.init(options.guid);
      }
      $("#show_disable_user:checkbox").click(function(){
        if (this.checked) {
          $(".disable_user").show();
          $(".active_user").hide();
        } else {
          $(".disable_user").hide();
          $(".active_user").show();
        }
      });

      $('#contact_user_new #admin_form').fadeIn('slow');
      $(document.body).on('submit',"#contact_user_form", function(event){
        event.preventDefault();
        user_mng.contact_user_submit($(this));
      });

      if (options.edit_user_id !== undefined){
        user_mng.fill_up_form(options.edit_user_id);
      }
    }
  });
});