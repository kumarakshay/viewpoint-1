define([
       'jquery',
       'backbone.marionette',
       '../events_page'
], function ($, Marionette, page) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      page.init();
      $('#events form').attr('rel','events_company');
    }
  });
});
