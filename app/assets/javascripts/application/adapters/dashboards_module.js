define([
       'backbone.marionette',
       '../dashboards_page'
], function (Marionette,page) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      page.init();
    }
  });
});
