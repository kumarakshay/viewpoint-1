define([
  'underscore',
  'backbone',
  'sgtools/rails/ui/models/base'
], function(
  _,
  Backbone,
  BaseModel
){
  var levels = new Backbone.Collection([
    { id: 0, name: 'emergency' },
    { id: 1, name: 'alert' },
    { id: 2, name: 'critical' },
    { id: 3, name: 'error' },
    { id: 4, name: 'warning' },
    { id: 5, name: 'notice' },
    { id: 6, name: 'info' },
    { id: 7, name: 'debug' }
  ]);

  return BaseModel.extend({
    defaults: {
      level: levels.get(6)
    },
    relations: [
      {
        type: 'HasOne',
        key: 'level',
        keySource: 'level_id'
      }
    ],
    initialize: function() {
      var l = this.get('level');
      if (l && (_.isNumber(l) || _.isString(l))) {
        this.setLevel(l);
      }
    },
    setLevel: function(l) {
      if(_.isNumber(l)) {
        this.set('level',levels.get(l));
      }
      else {
        this.set('level',levels.findWhere({name: l}));
      }
    }
  });
});

