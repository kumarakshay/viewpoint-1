define(['domReady', 'sg/jquery', './page','sg/ajax'], function(domReady, $, vp_page, ajax) {

  return {

    convert_portal_user_submit: function($form, id) {
      var self = this, $type, $path, $id;
      $id = id
      $('td.save').addClass('loading');
      $('td.save input').prop('disabled',true);
      
      if ($form.hasClass('new')) {
        $type = 'PUT';
        $path = '/api/itsm/system_users/' + $form.attr('rel');
      }

      var request = ajax.call(
        $path,
        // {data: $form.serialize() + "?&id=" + $form.attr('rel'), type: $type}
         {data: $form.serialize(), type: $type}
      );
      
      request.done(function($data){
        if ($data.status != 200){
          $('tr.message').addClass('ajax_error').slideDown('slow',function(){
            $(this).find('p.ajax_error').fadeIn('slow');
          });
        } else {
          if ($form.hasClass('new')) {
               $('div.error_messages p.success').html(
              'You have successfully converted to non-portal user'); 
              // var delete_request = ajax.call('/admin/users/' + $data.result.u_guid + '/cleanup'); 
              var delete_request = ajax.call('/admin/users/' + $id + '/cleanup');          
          } 
          $('tr.message').slideDown('slow',function(){
            $(this).find('p.success').fadeIn('slow');
          });
        }
      });

      request.fail(function(){
        $('tr.message').addClass('ajax_error').slideDown('slow',function(){
          $(this).find('p.ajax_error').fadeIn('slow');
        });
        // undo disable submit button and show loading icon
        $('#contact_user_form input[type=submit]').removeAttr('disabled').removeAttr('style');
      });

      request.always(function(){
          $('td.save').removeClass('loading');           
          $('td.save input').prop('disabled',false);
      })
    },


    init: function() {

      var self = this;
      vp_page.init();
    }
    // END init

  };
});
  