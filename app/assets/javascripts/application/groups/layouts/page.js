define([
       'backbone.marionette',
       'hbs!../templates/main_page'
], function(Marionette, mainTemplate) {
  var FormLayout =  Marionette.Layout.extend({
    template: mainTemplate,
    className: "group_form",
    regions: {
      leftForm: "#left_form",
      rightForm: '#right_form',
    },
  });

  return FormLayout;
});


