define([
       'underscore',
       'backbone.marionette',
       'hbs!../templates/form',
       'hbs!../templates/group_device_list',
       '../views/base_error',
       '../behaviors/placeholder.behavior',
       '../behaviors/clear_search.behavior'
], function(
  _,
  Marionette,
  groupFormTmpl,
  groupDevicesTmpl,
  BaseError,
  PlaceHolderBehavior,
  ClearSearchBehavior
) {

  var RowView = Marionette.ItemView.extend({
    tagName: 'li',
    template: groupDevicesTmpl,
    initialize : function (options) {
      var self = this;
    },
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    template: _.template('Loading...')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    template: _.template('No Devices Found')
  });

  var groupFormView =  Marionette.CompositeView.extend({
    template: groupFormTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: '.added_to_group',
    ui: {
      name: 'input[name="name"]',
      action: 'input[name="action"]',
      button: 'input[name="button"]',
      remove_device: '.remove_device',
      count: '.group_count',
      list_items: '.added_to_group'
    },
    events: {
      "click @ui.button" : "submitt_group",
      "click @ui.action" : "update_action",
      "click @ui.remove_device": "remove_device",
      "keyup @ui.name": "validate",
      "paste @ui.name": "validate"
    },
    behaviors: {
      PlaceHolderBehavior: {
        behaviorClass: PlaceHolderBehavior
      },      
      ClearSearchBehavior: {
        behaviorClass: ClearSearchBehavior
      }
    },
    initialize: function(options){
      var self = this;
      self.app = options.app;
      self.action = "save";
      self.model.on("sync", self.render, self);
      self.model.on("sync", self.collectionReload, self);
      if(self.model.isNew()){
        self.emptyView = NoResultsView;
        self.view = 'new';
      }
      self.listenTo(self.collection,'sync', function(c) {
        self.ui.button.removeClass('processing');
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          var name = "asset";
          if(self.collection.length > 1 ) {
           name = "assets";
          }
          self.ui.count.text(self.collection.length + " " + name);
        }
      });

      self.collection.listenTo(self.app.vent,'group:device:added',function(m) {
        self.collection.add(m);
        self.update_asset_list();
      });
    },

    collectionReload: function(){
      var self = this;
      this.collection.fetch({
        reset: true,
        data: {
          '__filter[for_group]': self.model.get("id")
        }
      });
    },

    onRender: function(){
      var self = this;
      if (this.view == 'new')
      {
        this.ui.button.removeClass('processing');
        self.validate();
      }
    },

    remove_device: function(e) {
      var self = this;
      model = self.collection.get($(e.currentTarget).attr("id"));
      wait: true;
      self.collection.remove(model);
      self.app.vent.trigger('group:device:removed', self.model, model);
      if (self.collection.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
      self.update_asset_list();
    },

    update_action: function(e){
      var self = this;
      self.action = $(e.currentTarget).val();
    },

    
    update_asset_list: function(){
      var self = this;
      var asset_guids = _.uniq(self.collection.pluck("asset_guid"));
      self.model.set({asset_list: asset_guids});
      self.bindUIElements();
      var name = "asset";
      if(self.collection.length > 1 ) {
       name = "assets";
      }
      self.validate();
      self.ui.count.text(self.collection.length + " " + name);
    },

    submitt_group: function(){
      var self = this;
      if(!self.model.isNew() && self.collection.length === 0 ){
        var ok = confirm("Are you sure you want to delete this group?");
        if(ok){
          self.model.destroy({
            success: function (model, response) {
              self.app.vent.trigger('app:notification',{
                message: 'Group was deleted successfully'
              });
              window.location = "/#groups";
            },
            error: function (model, response) {
              self.app.vent.trigger('app:notification',{
                message: 'Cannot delete this group as it is associated to a subscription'
              });
              model.fetch({
                  success: function (model) {
                    self.collection.fetch({
                      reset: true,
                      data: {
                       '__filter[for_group]': model.get("id")
                      }
                    });
                    self.render();
                  }
              });
            },
          });
        }
      }
      else{
       self.save_group();
      }
    },

    save_group: function(e) {
      var self = this;
      self.update_asset_list();
      self.setPending(true,"saving...");
      if (self.action === "save") {
         self.model.set({name: self.ui.name.val()});
      } else {
        var realModel = self.model;
        self.model = realModel.clone();
        self.model.set({name: self.ui.name.val()});
      }
      var saved = self.model.save({},{
        success: function(model, response){
          self.setPending(false,"save");
          BaseError.hideErrors();
          if (self.view === 'new') {
            self.app.vent.trigger('app:notification',{
              message: 'Group was successfully created'
            });
          } else {
            self.app.vent.trigger('app:notification',{
              message: 'Group was successfully updated'
            });
          }
          window.location = "#groups/" + model.get('id') + "/edit" ;
        },
        error: function(model, response){
          BaseError.validation_error(model, response);
        }
     });
     if (saved === false) {
      setTimeout(function(){
            self.setPending(false,"save");
      }, 500, self);
      BaseError.showErrors(self.model.errors);
     }
    },

    validate: function() {
      if (this.ui.name.val().length < 1 || this.collection.length === 0) {
        this.ui.button.prop('disabled', true);
      } else {
        this.ui.button.removeAttr('disabled');
      }
    },

    setPending: function(visible,text) {
      if (visible === true) {
        this.ui.button.val(text).prop('disabled', true);
        this.ui.button.addClass("processing");
        // a blocking overlay div prevents any activity on site
        // ui hash cannot be used effectively for top level elements 
        // straight javascript, no jquery        
        var block = document.createElement("div");
        block.id = "group_no_click";
        block.className = "overlay";
        document.body.appendChild(block);
      }
      else if (visible === false) {
        this.ui.button.val(text).removeAttr('disabled');
        this.ui.button.removeClass("processing");
        // remove the blocking overlay
        // straight javascript, no jquery
        var ele = document.getElementById("group_no_click");
        ele.parentNode.removeChild(ele);
      }
    },
  });
  return groupFormView;
});
