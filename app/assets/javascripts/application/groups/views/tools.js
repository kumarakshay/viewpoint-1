define([
       'backbone',
       'underscore',
       'backbone.marionette',
       'hbs!../templates/tools'
], function(
  Backbone,
  _,
  Marionette,
  toolsTmpl
) {
  var ToolsView = Marionette.ItemView.extend({
    tagName: 'span',
    className: "tools",
    template:  toolsTmpl,
    initialize: function(options) {
      this.optName = options.optName;
      this.optValue = options.optValue;
    },
    serializeData: function() {
      if (this.optName) {
        return {
          name: this.optName
        };
      }
    }
  });
  return ToolsView;
});