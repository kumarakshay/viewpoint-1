define([
       'underscore',
       'underscore.string',
       'backbone',
       'backbone.marionette',
       '../views/index',
       '../views/group_device_list',
       '../views/form',
       '../views/tools',
       '../models/group',
       '../collections/devices',
       '../collections/groups'
], function(
  _,
  _s,
  Backbone,
  Marionette,
  GroupIndexView,
  GroupDeviceListView,
  GroupFormView,
  ToolsView,
  GroupModel,
  DeviceCollection,
  GroupCollection
) {

  return Marionette.Controller.extend({

    initialize: function(options) {
      var self = this;
      this.app = options.app;
      this.moduleLayout = options.moduleLayout;
      this.formLayout = options.formLayout;
    },

    index: function() {
      var self = this;
      var toolsView = new ToolsView({'optName': ''});
      this.app.getRegion('subtools').show(toolsView);
      this.moduleLayout.on('show', function () {
        var groupIndexView  = new GroupIndexView({
          app: self.app
        });
        self.moduleLayout.groupsManager.show(groupIndexView);
      });
    },

    new_group: function() {
      var self = this;
      var model = new GroupModel();
      var collection = new DeviceCollection();
      var parents = new DeviceCollection();

      var toolsView = new ToolsView({'optName': 'back'});
      this.app.getRegion('subtools').show(toolsView);
      this.moduleLayout.on('show', function () {
       
        self.moduleLayout.groupsManager.show(self.formLayout);
        var group_new_form = new GroupDeviceListView({
          collection: parents,
          app: self.app,
          model: model
        });
        self.formLayout.rightForm.show(group_new_form);
        var formView = new GroupFormView({
          model: model,
          collection: collection,
          app: self.app
        });
        self.formLayout.leftForm.show(formView);
      });
    },

    edit_group: function(id){
      var self = this;
      var model = GroupModel.findOrCreate({id: id});
      var collection = new DeviceCollection();
      var parents = new DeviceCollection();
      var toolsView = new ToolsView({'optName': 'back'});
      this.app.getRegion('subtools').show(toolsView);
      this.moduleLayout.on('show', function () {
        self.moduleLayout.groupsManager.show(self.formLayout);
        var group_new_form = new GroupDeviceListView({
          collection: parents,
          app: self.app,
          model: model,
        });
        self.formLayout.rightForm.show(group_new_form);
        
        //Shwoing left form view
        var formView = new GroupFormView({
          model: model,
          collection: collection,
          app: self.app,
        });
        
        self.formLayout.leftForm.show(formView);

        model.fetch({
          success: function(m) {
            collection.fetch({
              reset: true,
              data: {
                '__filter[for_group]': m.get("id")
              }
            });
          }
        });
      });
    },

    show_group: function(id) {
      var self = this;
      //var model = IpBlockModel.findOrCreate({id: id});
    }
  });

});
