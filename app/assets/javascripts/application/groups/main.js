define([
  'domReady',
  'sg/jquery',
  'backbone.marionette',
  './controllers/group',
  './router',
  './layouts/page',
  'hbs!./templates/layout'
],

function(
  domReady,
  $,
  Marionette,
  AppController,
  AppRouter,
  FormLayout,
  layoutTemplate
) {

  var layout = new Marionette.Layout({
    template: layoutTemplate,
  });
  
  layout.addRegions({
    groupsManager: "#group_manager",
  });


  return Marionette.Module.extend({

    
    initialize: function(options, moduleName, app) {
      var form_Layout = new FormLayout();
      var router = new AppRouter({
        controller: new AppController({
          app: app,
          module: this,
          router: router,
          moduleLayout: layout,
          formLayout: form_Layout
        })
      });

      router.on('route', function() {
          window.C.page = "ms-groups";
         app.getRegion('main').show(layout);
      });

    },

    onStart: function(options) {
    }
  });

});
