define([
       'sgtools/rails/ui/models/base'
], function(BaseModel){
  var DeviceModel = BaseModel.extend({
  urlRoot: "/api/devices",
    
  });
  // Return the model for the module
  return DeviceModel;
});

