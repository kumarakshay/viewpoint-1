define([
       'sgtools/rails/ui/models/base'
], function(BaseModel){
  var GroupModel = BaseModel.extend({
  urlRoot: "/api/groups",
    errors: [],
    toString: function() {
      return this.get('name');
    } 
  });
  // Return the model for the module
  return GroupModel;
});

