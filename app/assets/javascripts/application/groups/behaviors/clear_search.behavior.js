//TODO: Intigrate this on live table filter row and remove from here,css already added to livetable.css
/**
* DESC: This Behavior class apply on search filter to clear the text
**/
define([
  'backbone.marionette'
], function(Marionette) {
  "use strict";
  var ClearSearchBehavior = Backbone.Marionette.Behavior.extend({
    onShow: function() {
          var ua = window.navigator.userAgent;
          var msie = ua.indexOf("MSIE ");
          var text_search_filter = $(".text_search_filter");       
    /****Start : clear search hack for non IE browsers*************/        
            jQuery(function($) {
            // CLEARABLE INPUT
            function toggleClass(v){return v?'addClass':'removeClass';} 
            $(document.body).on('input', '.clearable', function(){
              $(this)[toggleClass(this.value)]('x');
            }).on('mousemove', '.x', function( e ){
              $(this)[toggleClass(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
            }).on('click', '.onX', function(){
              $(this).removeClass('x onX').val('').change();
              $(this).next().removeAttr("style");
            });
          });         
    /*****************END : clear search **************/
      }
    });
  return ClearSearchBehavior;
});
