define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/device'
], function(_, BaseCollection,  DeviceModel){
  var DeviceCollection = BaseCollection.extend({
    model: DeviceModel,
    url: '/api/vp/devices',
    initialize: function () {
       // Default sort field and direction
      this.sortField = "name";
      this.sortDirection = "ASC";
    }
  });
  // You don't usually return a collection instantiated
  return DeviceCollection;
});
