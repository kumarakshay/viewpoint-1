define([
    'domReady!',
    'sg/jquery',
    'sg/ajax',
    'sg/util',
    'sg/device',
    'sgtools/rails/zenoss/component',
    'sgtools/rails/itsm',
    'application/pure_directives/device',
    'application/events_page',
    'sg/graphs',
    'sg/adapters/d3'
    ],
function(
  domReady,
  $,
  ajax,
  sg_util,
  sg_device,
  sg_component,
  sg_itsm,
  vp_pd_device,
  vp_events_page,
  sg_graphs,
  d3
) {

  return({

    uptime: function($e) {
      var self = this;

      var Digital=$e.html().split(/[dhms]:?/);

      if (Digital.length == 5) {
        var days=parseInt(Digital[0],10),
          hours=parseInt(Digital[1],10),
          minutes=parseInt(Digital[2],10),
          seconds=parseInt(Digital[3],10);

        seconds++;

        if (seconds>59){
          seconds = 0;
          minutes++;
        }
        if (seconds<=9) {
          seconds="0"+seconds;
        }

        if (minutes>59){
          minutes = 0;
          hours++;
        }
        if (minutes<=9) {
          minutes="0"+minutes;
        }

        if (hours>23){
          hours = 0;
          days++;
        }

        $e.html(days+'d:'+hours+'h:'+minutes+'m:'+seconds+'s');

      } else {

      }

      setTimeout(function() {
        self.uptime($('b.uptime'));
      },1000);

    },

    ctx_path: '/api/devices',

    init: function(guid, company_guid) {

      var self = this;

      ajax.ctx_path = [this.ctx_path,guid].join('/');

      $(document).ready(function(){

        // vp_page.init();
        vp_events_page.init();
        
        $('#firewall_logs').click(function(){
           var id = $(this).attr('rel');
           window.location = '/2f/firewall_logs/' + guid; 
        })

        // write url to rel and attach click event to <p>
        $('#components > div > p').each(function(){
          var $api = $(this).attr('rel');
          var $type = $api.split('meta_type=');
          // <p> click event
          $(this).click(function(){
            if (!$(this).hasClass('fetched')) {
              $(this).addClass('fetched');

              // next <div>
              $(this).next('div').each(function(){
                // loading graphic fadein
                var $load_ele = $(this);
                sg_util.loading_on($load_ele);

                $load_ele.append('<div class="results" style="display:none" />');
                var req = sg_component.ajax($api,$load_ele,$type[1]); // replace _ ?
                req.always(function() {
                  sg_util.loading_off($load_ele);
                });
              });
            }
          });
        });

        // slider move
        $('#device_detail_select li').not('.exclude').not('.inactive').each(function($i){
            $(this).click(function(e){
                e.preventDefault();
                $('div.dash_panel').fadeTo(500,1);
                $(this).siblings('li').removeClass('active');
                $('#dash .slider').animate({left:100*-$i+'%'},1000,function(){
                    $('div.dash_panel:eq(' + $i + ')').siblings().fadeTo(500,0);
                });
                sg_util.device_position($i);
            });
        });
        $('.position a').each(function($i){
            $(this).click(function(e){
                e.preventDefault();
                $('div.dash_panel').fadeTo(500,1);


                $('#dash .slider').stop().animate({left:100*-$i+'%'},1000,function(){
                    $('div.dash_panel:eq(' + $i + ')').siblings().fadeTo(500,0);
                });
                sg_util.device_position($i);
            });
        });

        // Device Detail
        // sg_device.renderZenossDetails();
        var z_req = ajax.request('getDeviceZenossDetails',{use_page_context: true});
        z_req.done(function(r) {
          if (r.result) {
            $('#device_time').autoRender(r.result,vp_pd_device.time_widget);

            if (r.result.status === true) {
              $('#device_details dd.status').text('UP');
              $('#device_details dd.status').addClass('status-up').prev('dt').addClass('status-up');
            } else if (r.result.status === false) {
              $('#device_details dd.status').text('DOWN');
              $('#device_details dd.status').addClass('status-down').prev('dt').addClass('status-down');
            } else {
              $('#device_details dd.status').text('UNKNOWN');
            }
            
            // snmp and rack_slot details
            $('#device_details dd.rack_slot').text(r.result.rack_slot);
            $('#device_details dd.snmp_sys_name').text(r.result.snmp_sys_name);
            $('#device_details dd.snmp_location').text(r.result.snmp_location);
            $('#device_details dd.snmp_contact').text(r.result.snmp_contact);
            $('#device_details dd.snmp_descr').text(r.result.snmp_descr);
            $('#device_details dd.snmp_version').text(r.result.snmp_version);
            // Force marquee on the next tick
            // This allows the browser to apply width/height
            $('#aside dl').trigger("setMarquee");
          }
        });

        // Update Incidents, ChangeTickets, and MX Windows
        sg_itsm.updateIncidents(undefined,{company_guid: company_guid});
        sg_itsm.updateChangeTickets(undefined,{company_guid: company_guid});
        sg_itsm.updateMXWindows(undefined,{company_guid: company_guid});

        // Enable data pull for panel links
        // incidents
        $('.incidents_link').click(function(){
          sg_itsm.updateIncidents(undefined,{company_guid: company_guid});
        });


        // changes
        $('.changes_link').click(function(){
          sg_itsm.updateChangeTickets(undefined,{company_guid: company_guid});
        });


        // mx windows
        $('.mx_windows_link').click(function(){
          sg_itsm.updateMXWindows(undefined,{company_guid: company_guid});
        });

        self.uptime($('b.uptime'));

        sg_graphs.device_slider_graphs();

      });

    } // END init

  });
});
