define([
    'sg/jquery',
    'sg/ajax',    
    'sg/util',   
    'sg/graphs',
    'backbone.marionette',    
    '../../livetable/main',          
    '../mockup_json/device_records_generator',
    '../mockup_json/device_columns_generator',
    '../../livetable/builder/column_builder',
    'datatables'
    ], function(
        $,
        ajax,
        util,
        sg_graphs,
        Marionette,       
        LiveTable,
        sampleLogs,
        sampleColumns,
        columnBuilder   
    ) {

        return({

            test_value: "true",

            render_logs: function(columns, logs){
              var self = this;
              var LogList = new Marionette.Application();
              LogList.addRegions ({
               page: "#section"
              });
              LogList.module("FirewallLogs", {
                moduleClass: LiveTable,
                columnObj: columns,
                dataObj: logs,
                // trCallbacks : {rowClick: function(args){ 
                //   window.location = '/devices/' + args.asset_guid}}
              });
             LogList.page.show(LogList.FirewallLogs.layout);
             LogList.start();
            },

            init: function(stub_webservice) { 
              var self = this;         
              // $('#devices_list').dataTable({
              //  "fnDrawCallback": function() {
              //     sg_graphs.interface_init();
              //   }
              // });   
              stub_webservice = stub_webservice || "false";
              $('#section').css({'display':'block', 'top': '110px'});
              if(stub_webservice === "true") {
                var columns = sampleColumns();
                var logs = sampleLogs(100);
                self.render_logs(columns,logs);
              }
              else{
                
                var request = ajax.call('/api/vp_interfaces');
                request.done(function($data){
                  if($data.status != 200){
                    alert('invalid req');
                  } 
                  else{
                    var columns = columnBuilder.dummy_json_logs([{"modelAttribute" : 'device_name', 'title': "Device", "columnType":"text"}, 
                                                                 {"modelAttribute" : 'ip_interface', 'title': "Interface", "columnType":"text"},
                                                                 {"modelAttribute" : 'description', 'title': "Description", "columnType":"text"},
                                                                 {"modelAttribute" : 'internet', 'title': "Internet", "columnType":"text"},
                                                                 {"modelAttribute" : 'network_name', 'title': "Network Name", "columnType":"text"},
                                                                 {"modelAttribute" : 'company_use', 'title': "Company", "columnType":"text"},
                                                                 ]);
                    var logs = $data;
                    self.render_logs(columns,logs);
                  }
                });
              }
            }
        })
})  