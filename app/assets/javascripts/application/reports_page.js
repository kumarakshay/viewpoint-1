define([
  'sg/jquery',
  'sg/ajax',
  'domReady',
  'sg/util',
  'application/noti/noti',
  'jquery.validity',
  'jquery.fileDownload'
], function(
  $,
  ajax,
  domReady,
  util,
  noti
) {

  return {

    obj: {},
    area_obj: {},
    cabinet_obj: {},
    success_msg: {
      checked: "Subscription has been added. We are processing a sample report. You will be notified when it is ready for download.",
      unChecked: "We are processing your report. You will be notified when it is ready for download."
    },

    report_buckets_ajax: function($ele) {
      var self = this;
      ajax.call('/pyxie')
        .done(function($data) {
          var $html = '';
          $.each($data.result, function(i, value) {
            var $prefix = value.name.split('-');
            var $value = value.name.replace($prefix[0] + '-', '');
            var $usage = null;
            // if (value.tags[0] != 'firewall_logs'){
              if (value.tags) {
                if (value.tags[0] == 'usage_reports' || value.tags[0] == 'backup_reports' || value.tags[0] == 'firewall_logs') {
                  $value = value.tags[0];
                  $value = $value.replace(/[_-]/g, ' ');
                  $usage = true;
                } else {
                  if (value.tags[0].indexOf('user_reports') != -1) {
                    $value = "Custom User Reports";
                    // skip this element
                    return true;
                  }
                  $value = $value.replace(/[_-]/g, ' ');
                  $usage = false;
                }
                $html += '<li rel="' + value.name + '|' + $usage + '"><b>' + $value + '</b></li>';
              }
            // }
          });
          $ele.find('ul').html($html).find('li').each(function() {
            $(this).click(function() {
              var $rel = $(this).attr('rel').split('|');
              self.reports_ajax($('#reports tbody'), $rel[0], $rel[1]);
            });
          });
          var $ui = $ele.find('ul'),
            ul_items = $ui.find('li');
          // if (ul_items.length > 10) {
          //   $ui.prepend('<li class="scroll_Up"><div id="up"></div></li>');
          //   $ui.append('<li class="scroll_Down"><div id="down"></div></li>');
          //   // immediate function used to create scoped closure for setintervals
          //   (function($ui) {
          //     var self = this;
          //     var $li = $ui.find('li');
          //     var first = 1;
          //     var last = 11;

          //     for (var i in $li) {
          //       if (i > 10 && i !== "length") {
          //         $($li[i]).hide();
          //       }
          //     }
          //     $ui.find('li.scroll_Down').show();
          //     function scrollMenuUp() {
          //       if (first > 1) {
          //         $($li[first]).show();
          //         $($li[last]).hide();
          //         first--;
          //         last--;
          //       }
          //     }
          //     function scrollMenuDown() {
          //       if (last < ($li.length - 2)) {
          //         $($li[first]).hide();
          //         $($li[last]).show();
          //         first++;
          //         last++;
          //       }
          //     }
          //     $ui.find('li.scroll_Up').mouseenter(function(){
          //       self.up = setInterval(function(){
          //         scrollMenuUp();
          //       }, 100);
          //     }).mouseleave(function(){clearInterval(self.up);});
          //     $ui.find('li.scroll_Down').mouseenter(function(){
          //       self.down = setInterval(function(){
          //         scrollMenuDown();
          //       }, 100);
          //     }).mouseleave(function(){clearInterval(self.down);});
          //   })($ui);
          // }
          if ($ele.find('ul').is(':empty')) {
            $ele.find('div.expand').fadeTo(0, 0.3).css({
              'cursor': 'default'
            }).attr('title', 'no reports for this user');
          }
        });
    },

    report_notifications_ajax: function($ele){

      var self = this;
      ajax.call('api/reqs?job_status=finished&email_me=false')
        .done(function($data) {
          if ($data.result.length > 0){
            $('table.dataTable').each(function() {
              $(this).dataTable().fnDestroy();
            });


          var $html = [];

          $.each($data.result, function(i, value) {
              $row = $('<tr>');
              var myDate = new Date(1000*parseInt(value.created_at,10));
              $row.append('<td><a>' + value.report + ' is available for download.<b style="display: none;" date-created="' + value.created_at + '"></b></a></td>');
              $row.append('<td>' + myDate.toLocaleString() + '</td>');
              $row.append('<td>' + value.created_at + '</td>');

              noti.itemClick($row.find('a'),value);
              if (value.acknowledged === false) {
                $row.find('a').addClass('unacknowledged');
              }

              $html.push($row);
            });



            // make click change set acknowledged to true


            var options = {
                                  "aaSorting": [[ 1, 'desc' ]],
                                  "aoColumns": [
                                    null,
                                    {"iDataSort": 2},
                                    {"bVisible": false}
                                   ]
                                 };


            self.updateTable($ele, $html, options);
          } else {
            $('#noti_table .loading').html('no notifications');
          }
        })
        .always(function() {});
    },

    reports_ajax: function($ele, $api, $usage) {
      var self = this;
      var $prefix = $api.split('-');
      var $bucket_name = $api.replace($prefix[0] + '-', '').replace(/[_-]/g, ' ');

      if ($usage == 'true' && $bucket_name.indexOf('backup reports') == -1) {
        if ($bucket_name.indexOf('firewall logs') > -1) {
          $bucket_name = 'Firewall Logs';
        } else {
          $bucket_name = 'Usage Reports';
        }
      } else if ($bucket_name.indexOf('backup reports') != -1) {
        $bucket_name = 'Backup Reports';
      } else if ($bucket_name.indexOf('user reports') != -1) {
        $bucket_name = 'Custom User Report';
      }
      $ele.closest('table').find('.bucket_name').html($bucket_name + ' - filename');

      ajax.call('/pyxie/' + $api)
        .done(function($data) {

          $('table.dataTable').each(function() {
            $(this).dataTable().fnDestroy();
          });

          var $html = '';

          if ($bucket_name == "Backup Reports") {
            self.processData($ele, $api, $data.result);
          } else {
            $.each($data.result, function(i, value) {
              var display_name = value.filename;
              if (value.filename.indexOf('/') !== -1) {
                temp = value.filename.split('/');
                display_name = temp[temp.length - 1];
              }

              // Day not found?
              if (typeof value.meta.report_day == 'undefined') {
                value.meta.report_day = '01';
              }

              // Convert filesize to display friendly
              value.length = util.file_size_display(value.length);

              if (value.meta.report_date) {
                if (!isNaN(value.meta.report_date * 1000)) {
                  $r = $.datepicker.parseDate('@', (Math.round(value.meta.report_date * 1000)));
                  $r = $.datepicker.formatDate("mm/dd/yy", $r);
                } else {
                  $r = value.value.meta.report_date;
                }
              } else {
                $r = '--';
              }


              $html += '<tr>';
              $html += '<td><a href="' + '/pyxie/' + $api + '/' + value.filename + '">';

              if ($usage == 'true') {
                $html += display_name + '</a></td>';
              } else {
                // $html += value.meta.dlevel0 + ' &gt; ' + value.meta.dlevel1 + ' &gt; ' + value.meta.dlevel2 + '</a></td>';
                $html += display_name + '</a></td>';
              }
              $html += '<td>' + value.length + '</td>';
              if ($usage == 'false') {
                $html += '<td>' + $r + '</td>';
              } else {
                $html += '<td>' + value.meta.report_month + '/' + value.meta.report_day + '/' + value.meta.report_year + '</td>';
              }
              $html += '</tr>';

            });
            self.updateTable($ele, $html);
          }
        })
        .always(function() {});
    },

    processData: function($ele, $api, data) {
      // turn the ajax file objects into an array of
      // filenames and extensions, then create table rows
      // from the array info
      var self = this;
      var fileList = [];

      // can't use for-in, IE8 iterates length as an object
      for (var i = 0; i < data.length; i++) {
        var file = data[i].filename.split('.');
        if (typeof data[i].meta.report_day == 'undefined') {
          data[i].meta.report_day = '01';
        }
        // Convert filesize to display friendly
        var fileSize = util.file_size_display(data[i].length);
        var reportDate = data[i].meta.report_month + '/' +
          data[i].meta.report_day + '/' +
          data[i].meta.report_year;
        self.buildFileList(
          self.findMatch(file, fileList),
          file, fileList, fileSize, reportDate
        );
      }
      var $html = self.createBackupRows($api, fileList);
      self.updateTable($ele, $html);
    },

    buildFileList: function(match, file, fileList, fileSize, reportDate) {
      // creates the array containing filenames and extensions
      var filename = file[0];
      var thisExtension = file[1];
      var fileObject = {};

      if (thisExtension === "pdf") {
        fileSize = fileSize + " (pdf)";
      } else if (thisExtension === "csv") {
        fileSize = fileSize + " (csv)";
      } else if (thisExtension === "txt") {
        fileSize = fileSize + " (txt)";
      } else if (thisExtension === "rst") {
        fileSize = fileSize + " (rst)";
      }

      if (match[0]) {
        fileList[match[1]].name = filename;
        if (thisExtension === "csv") {
          fileList[match[1]].csv = true;
          fileList[match[1]].csvSize = fileSize;
        }
        if (thisExtension === "pdf") {
          fileList[match[1]].pdf = true;
          fileList[match[1]].pdfSize = fileSize;
        }
        if (thisExtension === "txt") {
          fileList[match[1]].txt = true;
          fileList[match[1]].txtSize = fileSize;
        }
        if (thisExtension === "rst") {
          fileList[match[1]].rst = true;
          fileList[match[1]].rstSize = fileSize;
        }
      } else {
        fileObject.name = filename;
        if (thisExtension === "csv") {
          fileObject.csv = true;
          fileObject.csvSize = fileSize;
        }
        if (thisExtension === "pdf") {
          fileObject.pdf = true;
          fileObject.pdfSize = fileSize;
        }
        if (thisExtension === "txt") {
          fileObject.txt = true;
          fileObject.txtSize = fileSize;
        }
        if (thisExtension === "rst") {
          fileObject.rst = true;
          fileObject.rstSize = fileSize;
        }
        fileObject.date = reportDate;
        fileList.push(fileObject);
      }
    },

    findMatch: function(file, fileList) {
      // checks current filename against file array to see
      // if the same file was already processed
      // with a different extension
      var match = [false, -1];
      for (var i = 0; i < fileList.length; i++) {
        if (fileList[i].name === file[0]) {
          match[0] = true;
          match[1] = i;
          return match;
        }
      }
      return match;
    },

    createBackupRows: function($api, fileList) {
      var self = this;
      var html = "";
      for (var i = 0; i < fileList.length; i++) {
        var sizeArr = [];
        html += '<tr><td>';
        // add csv icon/link if csv file was found
        if (fileList[i].csv === true) {
          html += '<a class="mime-icon" href="' + '/pyxie/' +
            $api + '/' + fileList[i].name + ".csv" + '">CSV</a>';
          sizeArr.push(fileList[i].csvSize);
        }
        // add pdf icon/link if pdf file was found
        if (fileList[i].pdf === true) {
          html += '<a class="mime-icon" href="' +
            '/pyxie/' + $api + '/' + fileList[i].name + ".pdf" + '">PDF</a>';
          sizeArr.push(fileList[i].pdfSize);
        }
        // add txt icon/link if txt file was found
        if (fileList[i].txt === true) {
          html += '<a class="mime-icon" href="' +
            '/pyxie/' + $api + '/' + fileList[i].name + ".txt" + '">TXT</a>';
          sizeArr.push(fileList[i].txtSize);
        }
        // add rst icon/link if rst file was found
        if (fileList[i].rst === true) {
          html += '<a class="mime-icon" href="' +
            '/pyxie/' + $api + '/' + fileList[i].name + ".rst" + '">RST</a>';
          sizeArr.push(fileList[i].rstSize);
        }
        // add name of file
        html += '<div>' + fileList[i].name + '</div></td>';
        var combinedSizes = "";
        for (var s = 0; s < sizeArr.length; s++) {
          if (s === (sizeArr.length - 1)) {
            combinedSizes += sizeArr[s];
          } else {
            combinedSizes += sizeArr[s] + ', ';
          }
        }
        // add file size(s)
        html += '<td>' + combinedSizes + '</td>';
        // add report date
        html += '<td>' + fileList[i].date + '</td>';
        html += '</tr>';
      }
      return html;
    },

    updateTable: function($ele, $html, options) {
      $ele.html($html).find('tr:odd').addClass('alternate');

      $('table.delay_apply_datatable').each(function() {
        util.apply_datatable($(this),null,options);
        $(this).attr('style', '');
      });
    },

    sg_validate: function() {
      var self = this;
      // Start validation:
      $.validity.start();

      // Validator methods go here:

      // Dependent values //
      // Which report type 1-4
      $val_report_type = $('#req_report_id').val();
      // Which report subject 'group', 'device'
      $val_report_subject = $('input.report_subject:checked').val();
      // Scheduler, 0 or 1
      $val_report_sched = $('input[name="req[subscription]"]:checked').val();

      // Check report type cases
      if ($val_report_type === "1" || $val_report_type === "2" || $val_report_type === "6" || $val_report_type === "11") {

        // If Group else Device selected
        if ($val_report_subject == 'group') {
          $('#req_group_guid').assert(
            self.notDefault,
            "Please select an option here."
          );
        } else {
          $('#req_device_guid').assert(
            self.notDefault,
            "Please select an option here."
          );
        }

      } else if ($val_report_type == 3) {
        // nothing at this time
      } else if ($val_report_type == 4) {
        // nothing at this time
      } // end report type cases

      // Check if subsription
      if ($val_report_sched == 1) {
        $('#req_frequency').assert(
          self.notDefault,
          "Please select an option here."
        );
        $('#req_delivery_time').assert(
          self.notDefault,
          "Please select an option here."
        );
        $('#req_time_zone').assert(
          self.notDefault,
          "Please select an option here."
        );
        $('#req_email').require("Email is required.").match('email');
      } else {
        // Dates
        $('#req_start_date').require().match('date');
        $('#req_end_date').require().match('date');
      } // end if subscription

      // All of the validator methods have been called:
      // End the validation session:
      var result = $.validity.end();

      // Return whether it's okay to proceed with the Ajax:
      return result.valid;
    },
    
    notDefault: function(e) {
      if (e.value.substring(0, 6) == "Select " || e.value === "") {
        return false;
      } else {
        return true;
      }
    },

    area_and_cabinate: function() {
      var self = this;
      if (self.area_obj["area"] !== undefined) {
        self.fill_area_or_cabinet(self.area_obj["area"], "area");
      } else {
        cabinet_request = self.call_area_cabinet_ajax("area");
      }
      if (self.cabinet_obj["cabinet"] !== undefined) {
        self.fill_area_or_cabinet(self.cabinet_obj["cabinet"], "cabinet");
      } else {
        cabinet_request = self.call_area_cabinet_ajax("cabinet");
      }

    },

    call_area_cabinet_ajax: function(elem) {
      var self = this;
      ajax.call("/reqs/" + elem, {
          dataType: 'json'
        })
        .done(function(c_data) {
          if (elem == "cabinet") {
            self.cabinet_obj[elem] = c_data;
          } else {
            self.area_obj[elem] = c_data;
          }
          self.fill_area_or_cabinet(c_data, elem);
        });
    },

    fill_area_or_cabinet: function(data, ele) {
      select_obj = $('#req_' + ele);
      select_obj.empty().append($('<option selected="selected"></option>').attr("value", "").text("Select" + " " + ele)); //'<option selected="selected" value="">Select $ele</option>');
      $.each(data, function() {
        select_obj.append($('<option></option>').attr("value", this).text(this));
      });
    },

    /* Checks the value of radio buttons and displays the proper select */
    group_or_device: function() {
      var self = this;
      $('.report_subject').each(function() {
        if ($(this).prop('checked') === true) {
          $ele = $(this).attr('value');
          $ajax_ele = $ele;
          if ($ele == 'device') {
            var $report = $('#custom_report #req_report_id option:selected').val();
            if ($report == 1) {
              $ajax_ele = "device_server";
            } else {
              $ajax_ele = "device_sysedge";
            }
          }
          if (self.obj[$ajax_ele] !== undefined) {
            self.append_option(self.obj[$ajax_ele], $ele);
          } else {
            //disabled checkboxes while req is processing
            $(".report_subject").attr("disabled", true);
            request = ajax.call("/reqs/" + $ajax_ele, {
              dataType: 'json'
            });
            request.done(function($data) {
              // eval("var arr_" + $ajax_ele + "=" + $data )
              self.obj[$ajax_ele] = $data;
              self.append_option($data, $ele);
            });
            request.fail(function() {
              //enable checkboxes once req complete
              $(".report_subject").attr("disabled", false);
            });
          }
        }
      });
    },

    append_option: function(data, ele) {
      //enable checkboxes once req complete
      $(".report_subject").attr("disabled", false);
      $data = data;
      $ele = ele;
      var $elem = '#report_subjects select[class=' + $ele + ']';
      $('#report_subjects select[class!=' + $ele + ']').val('').fadeOut(200, function() {
        $($elem).fadeIn(200);
        var $report = $('#report_subjects select[class=' + $ele + ']');
        $report.empty().append($('<option selected="selected"></option>').attr("value", "").text("Select" + ' ' + $ele)); //'<option selected="selected" value="">Select $ele</option>');
        $.each($data, function() {
          if ($ele == 'group') {
            $report.append($('<option></option>').attr("value", this.id).text(this.name));
          } else {
            $report.append($('<option></option>').attr("value", this.asset_guid).text(this.name));
          }
        });
      });
    },

    // group_or_device: function() {
    //   $('.report_subject').each(function(){
    //     if ($(this).prop('checked') === true) {
    //       var $elem = '#report_subjects select[class=' + $(this).attr('value') + ']';
    //       $('#report_subjects select[class!=' + $(this).attr('value') + ']').val('').fadeOut(200,function(){
    //         $($elem).fadeIn(200);
    //       });
    //     }
    //   });
    // },

    /* Toggle Form */
    formToggle: function() {
      var self = this;

      self.group_or_device();

      // Gracefully close progress bar if closing
      $('#wait_progress').hide(); // previous code
      $('#wait_progress > div').css('opacity', 1);

      // Gracefully close scheduler if closing
      $('#req_subscription').prop('checked', false); // previous code
      self.scheduler_toggle(); // previous code

      // Gracefully toggle form
      $('#new_req').slideToggle(); // previous code
    },

    /* Show/Hide groups of objects */
    fade_elements: function($hide, $show) {
      // build array of objects that are currently 'on'
      var $hideThese = '';
      $hide.each(function() {
        $(this).find('select, input, textarea').each(function() {
          $(this).attr('disabled', 'disabled');
        });
        if ($(this).css('display') != 'none') {

          if ($hideThese === '') {
            $hideThese = $(this);
          } else {
            $hideThese.add($(this));
          }
        }
      });
      $hide.hide();
      $show.find('select, input, textarea').each(function() {
        $(this).removeAttr('disabled');
      });
      $show.fadeIn(200, 'linear');
    },

    /* Report Types */
    show_report_fields: function() {
      var self = this;
      var $device_element = $('.set1b');
      var $report = $('#custom_report #req_report_id option:selected').val();
      if ($report == 1 || $report == 11) {
        self.fade_elements($('.setg, .set, .set2, .set3'), $('.set0, .set1, .set1a, .set1b'));
        self.group_or_device();
      }
      if ($report == 2) {
        self.fade_elements($('.setg, .set, .set0, .set1a, .set2, .set3'), $('.set1,.set1b'));
        $('.report_subject.device').prop("checked", true);
        self.group_or_device();
      }
      if ($report == 3) {
        self.fade_elements($('.setg, .set, .set1, .set3, .set5, .set0'), $('.set2'));
      }
      if ($report == 4) {
        self.fade_elements($('.setg, .set, .set1, .set2, .set5, .set0'), $('.set3'));
        self.area_and_cabinate();
      }
      if ($report == 5 || $report == 7 || $report == 8 ||  $report == 9 ) {
        self.fade_elements($('.setg, .set1, .set2, .set3, .set5, .set0'), $('.set'));
      }
      // group only
      if ($report == 6) {
        self.fade_elements($('.setg, .set, .set0, .set1b, .set2, .set3'), $('.set1, .set1a'));
        $('.report_subject.group').prop("checked", true);
        self.group_or_device();
      }
      if ($report == 10 ) {
        self.fade_elements($('.set1, .set3, .set5, .set0'), $('.set, .set2, .setg'));
      }
    },

    set_report_name: function() {
      var $report_name = $('#custom_report #req_report_id option:selected').text();
      $('#req_report_name').val($report_name);
    },

    set_graph: function() {
      var $report = $('#custom_report #req_report_id option:selected').val();
      if($report == 10) {
        var reportType = $('#req_report_type').val();
        var checkbox = $('#req_show_graphs');
        if(reportType === 'csv') {
          $('.setg').hide();
          checkbox.prop('checked', false);
        } else if(reportType === 'pdf') {
          $('.setg').show();
          checkbox.prop('checked', true); }
      }
    },

    /* Obscure covers the given element with a semi transparent block */
    obscure: function($element, $margin) {
      $margin = $margin || '0';
      // Must not be inline and - adding a block inside
      $pos = $element.css('position');

      if ($pos == 'static') {
        $element.css('position', 'relative');
      }
      $element.append('<div class="obscure"><div>');
      $element.find('.obscure').css('padding', 0).fadeIn(400);
    },

    unobscure: function($element) {
      $element.find('.obscure').fadeOut(400, function() {
        $(this).remove();
      });
    },

    /* Toggle Scheduler */
    scheduler_toggle: function() {
      var self = this;

      if ($('#req_subscription').prop('checked') === true) {
        $('.set4').slideDown(400, function() {
          $('.set4').find('input, select, label').fadeTo('slow', 1);
        });
        self.obscure($('.form_cal'), '20px 4px 16px 17px');
      } else {
        $('.set4').find('input, select, label').fadeTo('slow', 0, function() {
          $('.set4').slideUp(400);
        });
        self.unobscure($('.form_cal'));
        $.validity.clear();
      }
    },

    altFormat: 'yy-mm-dd',

    init: function(is_bus_process) {
      var self = this;
      // Initialize code that needs to wait for the DOM to be loaded and ready.
      // Assigning event handlers, classes, and functions to elements are examples.
      domReady(function() {

        //Set the selected page as reports
        window.C.page = "ms-reports";

        // Datepicker
        $(function() {

          $('#section > #loading').fadeOut('slow', function() {
            $('#custom_report').fadeIn('slow');
          });

          $d = new Date();
          $m = ("0" + ($d.getMonth() + 1)).slice(-2);

          $startDefault = $.datepicker.parseDate("d m yy", "1 " + $m + " " + $d.getFullYear());
          $min = "-90d";

          $('#start_date_div').datepicker({
            altFormat: "yy-mm-dd",
            altField: "#req_start_date",
            changeMonth: true,
            numberOfMonths: 1,
            defaultDate: $startDefault,
            onSelect: function(dateStr) {
              var newDate = $(this).datepicker('getDate');
              $('#end_date_div').datepicker('option', 'minDate', newDate);
            }
          });

          $('#end_date_div').datepicker({
            altFormat: "yy-mm-dd",
            altField: "#req_end_date",
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
          });
          // This sets the min/max dates
          // replace last perameter with a date formatted like mm/dd/yy
          $("#end_date_div, #start_date_div").datepicker("option", "minDate", $min);
          $("#end_date_div, #start_date_div").datepicker("option", "maxDate", "0d");

          // make inputs read only
          $('#req_start_date, #req_end_date').attr('readOnly', 'true');
        });


        // user logged in so promote user links in nav
        $userLinks = $('body.users li.sg-mega li.users ul');
        $userHTML = '<ul>' + $userLinks.html() + '</ul>';
        $userLinks.closest('li.sg-mega').find('ul').replaceWith($userHTML);


        // initiate checked option then set the select that shows
        if (!$('.report_subject.group').prop('checked')) {
          if (!$('.report_subject.device').prop('checked')) {
            $('.report_subject.device').prop('checked', true);
          }
        }
        // self.group_or_device();

        // Listen for Group/Device change
        $('.report_subject').change(function() {
          self.group_or_device();
        });


        // // Toggle form
        // $('#form_toggle').click(function(){
        //   self.formToggle();
        // });

        var $device;
        // Initiate sets' display for use by js then attach function to select change
        $('body.reports').each(function() {
          $('.set,.set0, .set1, .set2, .set3, set5').css('display', 'none');
          self.show_report_fields($device);
          $('#custom_report #req_report_id').change(function() {
            self.show_report_fields($device);
            self.set_report_name();
            self.set_graph();
          });
        });

        // Initiate input items in subscription edit as disabled
        $('.inline_form .form_list').find('input, select').prop('disabled', true);
        self.scheduler_toggle();

        // Attach function to "Schedule" checkbox
        $('#req_subscription').change(function() {
          self.scheduler_toggle();
        });

        $('#req_report_type').change(function() {
          self.set_graph();
        });

        // Initiate subscription chackbox as true
        $('.inline_form #req_subscription').prop('checked', true);


        // Quick-fix: remove #ui-datepickder-div
        $('#ui-datepicker-div').remove();


        // Custom Reports close button
        $('.form_submit a').click(function() {
          self.formToggle();
        });

        // Form action for progress bar
        $('#new_req, .edit_req').submit(function(e) {
      e.preventDefault(); //otherwise a normal form submit would occur

      // Check raw polled for group selection and remove it's value
      if ($('#req_report_id').val() == '2') {
        $('#req_group_guid').val('');
      }

      // // add 1 to the field - hack but cleaner than alternative
      // var tempDate,
      //   $tempEndDate = $('#req_end_date');
      // if ($('#end_date_div').length > 0) {
      //   tempDate = $('#end_date_div').datepicker('getDate');
      //   tempDate.setDate(tempDate.getDate() + 1);
      //   $tempEndDate.val($.datepicker.formatDate(self.altFormat, tempDate));
      // }

      // Form submit for old reporting system
      if (is_bus_process === "false") {

        // check for type of report: subscription or adhoc
        var $preparingMessageHtml,
          $failMessageHtml;

        if ($('#req_subscription').prop('checked') === true) {
          $preparingMessageHtml = "<a href='/reqs' style='float:right'>Show all my subscriptions &gt; </a>A sample is being prepared for your review...";
          $failMessageHtml = "<a href='/reqs' style='float:right'>Show all my subscriptions &gt; </a>There was a problem generating your report, please try again. ";
        } else {
          $preparingMessageHtml = "We are preparing your report, please wait...";
          $failMessageHtml = "There was a problem generating your report, please try again.";
        }

        $.sgFileDownload($(this).attr('action'), {
          preparingMessageHtml: $preparingMessageHtml,
          failMessageHtml: $failMessageHtml,
          data: $(this).find('input, select, textarea').serialize(),
          httpMethod: "POST"
        });
      } else {
      
        if (self.sg_validate()){
          $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).find('input, select, textarea').serialize(), // serializes the form's elements.
            success: function(data) {
              $('div.error_messages p.success').html($val_report_sched ? self.success_msg.checked : self.success_msg.unChecked);
              $('div.error_messages').slideDown('slow', function() {
                $(this).find('p.success').delay(500).fadeIn('slow', function() {
                  $(this).delay(6000).fadeOut();
                });
              });
            }
          });
        }
  
      }
      if (typeof tempDate !== 'undefined') {
        // put it back to original value
        tempDate.setDate(tempDate.getDate() - 1);
        $tempEndDate.val($.datepicker.formatDate(self.altFormat, tempDate));
      }
    });
        // Check inlin form's shcedule checkbox
        $('.inline_form #req_subscription').prop('checked', true);
      });

      // Quick-fix: remove #ui-datepickder-div
      $('#ui-datepicker-div').remove();

      // Validation patterns
      $.validity.patterns.date = /^20[0-9]{2}-((1[0-2])|(0[1-9]))-([012]\d|30|31)$/;

      // saved reports
      $('#bucket_select').each(function() {
        self.report_buckets_ajax($(this));
      });

      //all report notifications
      $('#noti_table').each(function(){
        $('#noti_table .loading').fadeIn();
        self.report_notifications_ajax($('#noti_table tbody'));
      });
    }
  };
});