define(['sg/util'],function(util) {
  return {
    'time_widget': {
      '.memory__swap': function(a){
        if (a.context && a.context.memory) {
          return a.context.memory.swap
        }
        return ''
      }
    }
  }
});