define([
    'domReady',
    'sg/jquery',
    'sg/ajax'
  ], function(
      domReady,
      $,
      ajax
  ) {
  return {
    init: function() {
      var self = this;
      domReady(function() {
        var email = $(".dialog_form").attr("rel");
        var openid_req = ajax.call("/api/openid/" + email);
        openid_req.done(function(data){
          $(".dialog_form .user").autoRender(data.user);
          $(".dialog_form .persona").autoRender(data.persona);
          $('#section > #loading').fadeOut('slow',function(){
            $(this).remove();
            $('#admin_form').fadeIn('slow');
          });
        });
      });
    }
  };
});
