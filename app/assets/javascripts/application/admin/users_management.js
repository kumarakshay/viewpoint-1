define([
  'domReady',
  'sg/jquery',
  'sg/ajax',
  'sg/page',
  'application/submit_control',
  'sg/util',
  'jquery.validity',
  'application/jquery.validity.output'
  ], function(
    domReady,
    $,
    ajax,
    sg_page,
    submit_control,
    sg_util,
    validity
    ) {
  return({

    convert_to_portal_user: function($elem){
      var $delete = $elem.parent().parent().find(".delete-btn");
      var $id = $delete.attr('id');
      var $company_id = $delete.attr('rel');
      var confirm_dialog = confirm('Are you sure you want to convert this user to a portal user?');
      if (confirm_dialog === true) {
        window.location = ' /admin/users/' +  $id + '/convert?company_id=' + $company_id;
      }else {
        // any cancel related actions
      }
    },


    delete_me: function($elem){
      var $id = $elem.attr('id');
      var $company_guid = $elem.attr('rel');
      var $row = $elem.parent().parent();
      var confirm_dialog = confirm('Are you sure you want to delete this user?');

      if (confirm_dialog === true) {
        var $path = '/api/itsm/system_users/' + $id + '?company_id='+ $company_guid;
        var request = ajax.request($path,{type: 'DELETE'});
        $elem.parent().addClass('loading');
        $elem.prop('disabled',true);
        $elem.parent().css('cssText', 'background-position: 16% 50% !important');
        request.done(function(){
          $row.css("background", "rgb(236, 206, 203)");// for IE8
          $row.css("background", "rgba(200, 0, 0, 0.5)");// Modern browsers
          $row.fadeOut("slow");
        }).fail(function()  {
          $elem.parent().removeClass('loading');
          $elem.prop('disabled',false);
        });
      } else {
        // any cancel related actions
      }
    },

    contact_user_submit: function($form) {
      var self = this, $type, $path;

      sg_util.phone_reformat();

      // VUPT-4573 - This is hack is added for non-portal user edit profile page so if the country code is
      // not selected then it would save blank value instead of value "-Please Select-" 
      $("select option:selected").filter(function() {
         if (this.value === "-Please Select-") {
           this.value = "";
         }
      });
      
      $('td.save').addClass('loading');
      $('td.save input').prop('disabled',true);
            
      if ($form.hasClass('new')) {
        $type = 'POST';
        $path = '/api/itsm/system_users';
      }else {
        $type = 'PUT';
        $path = '/api/itsm/system_users/' + $form.attr('rel');
      }

      var request = ajax.call(
        $path,
        {data: $form.serialize(), type: $type}
      );
      
      request.done(function($data){
       if ($data.status != 200){
          $('tr.message').addClass('ajax_error').slideDown('slow',function(){
            $(this).find('p.ajax_error').fadeIn('slow');
          });
        } else {
          if ($form.hasClass('new')) {
             //Update form to point to edit action after user added successfully.(#VUPT-1505)
             $form.removeClass('new').addClass('edit');
             $form.attr('rel',$data.result.u_guid);
             $('div.error_messages p.success').html(
            'You have successfully added the new contact record.');
          } else {
               $('div.error_messages p.success').html(
              'Your updates to this contact record have been saved.');
          }
          $('tr.message').slideDown('slow',function(){
            $(this).find('p.success').fadeIn('slow');
          });
        }
      });

      request.fail(function(){
        $('tr.message').addClass('ajax_error').slideDown('slow',function(){
          $(this).find('p.ajax_error').fadeIn('slow');
        });
        // undo disable submit button and show loading icon
        $('#contact_user_form input[type=submit]').removeAttr('disabled').removeAttr('style');
      });

      request.always(function(){
          $('td.save').removeClass('loading');
          $('td.save input').prop('disabled',false);
      });
    },

    get_contact_users: function($company_guid){
      var self = this;
      var url =  '/api/itsm/system_users?__limit=3000&company_id=' + $company_guid;
      var ex = { directive:{
                'tbody tr':{
                  'record<-result':{
                  'td.email':'record.email',
                  'td.first_name':'record.first_name',
                  'td.last_name': function(a){
                              var guid = a.item.u_guid;
                              return '<a class="u_guid" href="/contact_users/' + guid +'/edit?company_id='+ $company_guid +'">'+ a.item.last_name + '</a>';                     
                  },
                  'td.u_guid': function(a){
                              var guid = a.item.u_guid;
                              return '<input class="text-only delete-btn" type="button" id="' + guid + '" rel="' + $company_guid + '" value="×"/>';
                              }
                  },
                }
      }};
      var contact_users = ajax.request(url);
      contact_users.done(function(data){
        if (data.result.length > 0) {
          //alert('found -> ' + data.result.length);
          $.when($('#usr_data').render(data,ex.directive)).done(function() {
            $('#usr_data input.text-only').click(function(){
              self.delete_me($(this));
            });
            $('#usr_data input.convert_contact').click(function(){
              self.convert_to_portal_user($(this));
            });
            //apply datatables
            $('table.delay_apply_datatable').each(function(){
              sg_util.apply_datatable($(this));
              // up to this point the load indicator was showing, so now
              // it's time to remove it right before showing the new results
              $('#section > #loading').fadeOut('slow',function(){
                $(this).remove();
                // fade in the contacts table
                $('#fade_wrapper').fadeIn('slow');
              });
            });
               
          });
        }else{
          // turn off loading if on
          $('#section > #loading').fadeOut('slow',function(){
            $(this).remove();
            $('#usr_data tbody').empty();
            var message = '<tr><td colspan="100">No results were found.</td></tr>';
            $('#usr_data tbody').append(message);
            $('#fade_wrapper').fadeIn('slow');
          });
        }

      });
    },
    
    fill_up_form: function(id){
      var self = this;
      //alert($id);
      //var $edit_id = $('#contact_user_form').attr('rel');
      if (id.length > 0){
        var user = ajax.request('/api/itsm/system_users/'+id);
        user.done(function(data){
          if (data.result) {
            $('#admin_form').autoRender(data.result);
            //The autoRender is not supporting the select box so 
            //added a hack to fix this
            $('#user_phone_number_cc').val(data.result.u_business_phone_cc);
            $('#user_cell_phone_cc').val(data.result.u_mobile_phone_cc);
            $('#user_phone_3_cc').val(data.result.u_alternate_phone_1_cc);
            $('#user_phone_4_cc').val(data.result.u_alternate_phone_2_cc);
            // up to this point the load indicator was showing, so now
            // it's time to remove it right before showing the new results
            $('#section > #loading').fadeOut('slow',function(){
              $(this).remove();
              // fade in the contacts table
              $('#admin_form').fadeIn('slow');
            });
          }else{
            // up to this point the load indicator was showing, so now
            // it's time to remove it right before showing the new results
            $('#section > #loading').fadeOut('slow',function(){
              $(this).remove();
              // fade in the contacts table
              $('#admin_form').fadeIn('slow');
            });
            $('div.error_messages p.ajax_error').html(
              'The user record was found, but an error occurred loading data to the form.' +
              'Please check the record you are attempting to edit is not empty and try again.');
            $('tr.message').addClass('ajax_error').slideDown('slow',function(){
              $(this).find('p.ajax_error').fadeIn('slow');
            });
          }
        });
        user.fail(function(){
            // up to this point the load indicator was showing, so now
            // it's time to remove it right before showing the new results
            $('#section > #loading').fadeOut('slow',function(){
              $(this).remove();
              // fade in the contacts table
              $('#admin_form').fadeIn('slow');
            });
            $('div.error_messages p.ajax_error').html(
              'There was an error loading the record for this user. Please check that ' +
              'you are attempting to edit a valid user and try again.');
            $('tr.message').addClass('ajax_error').slideDown('slow',function(){
              $(this).find('p.ajax_error').fadeIn('slow');
            });
        });
        user.always(function(){
          self.add_form_validation();
        });
      }else{

      }
    },

    //Non portal
    convert_non_portal_form: function(data,company_id)
    {
      var self = this;
      $('#contact_user_form').addClass('new');
      $('#admin_form').autoRender(data);
      self.add_form_validation();
    },

    add_form_validation: function() {
      // VUPT-4573 - This is hack is added for non-portal user edit profile page so if the country code is
      // not selected then it would save blank value instead of value "-Please Select-" 
      $("select option:selected").filter(function() {
         if (this.value === "-Please Select-") {
           this.value = "";
         }
      });
      $.validity.setup({
        // You may change the output mode with this property.
        outputMode: "tooltip_custom"
      });
      $('body#deploy_user_new form#new_user,' +
        'body#deploy_user_edit form.edit_user,' +
        'body#contact_user_new form#contact_user_form, ' +
        'body#contact_user_edit form#contact_user_form,' +
        '#convert_non_portal form#contact_user_form')
      .validity(function() {
        $('#user_first_name').require();
        $('#user_last_name').require();
        $('#user_email').each(function(){
          if ($(this).hasClass('sungard_match')) {
            $(this).match(/\@(sungard\.com|sungardas\.com|sungardsg\.sec|inflow\.com)$/,"Domain is not allowed.").require();
          } else {
            $(this).require().match('email');
          }
        });
        $('#user_secondary_email').each(function(){
          if ($(this).hasClass('sungard_match')) {
            $(this).match(/\@(sungard\.com|sungardsg\.sec|inflow\.com)$/,"Domain is not allowed.");
          } else {
            $(this).match('email');
          }
        });
        if ($('input[name="sungard"]').length === 0) {
          $('#admin_form .phonefield').match(/(^\d{3,13}$)|(^\d{3}[\-\s]?\d{3}[\-\s]?\d{4}$)|(^\d{2}[\-\s]?\d{3}[\-\s]?\d{3}[\-\s]?\d{4}$)|(^\d{3}[\-\s]?\d{3}[\-\s]?\d{3}[\-\s]?\d{3}$)/);
          $('#user_phone_number_cc').require('Country Code is required for business phone number.');
          $('#user_phone_number').require();
          $('#admin_form .phone_ext').match('ext');
          $("#user_business_phone_notes, #user_cell_phone_notes, #user_phone_3_notes, #user_phone_4_notes, #user_notes_1, #user_notes_2, #user_notes_3, #user_notes_4").maxLength(40, 'Phone notes field cannot be longer than 40 characters');
          $('#user_time_zone').require();
        }
        if($('#user_cell_phone').val().length !== 0) {
          $('#user_cell_phone_cc').require('Country Code is required for cell phone number.');
        }
        if($('#user_phone_3').val().length !== 0) {
          $('#user_phone_3_cc').require('Country Code is required for phone 3 number.');
        }
        if($('#user_phone_4').val().length !== 0) {
          $('#user_phone_4_cc').require('Country Code is required for phone 4 number.');
        }
      });
    },

    init: function(company_id) {
      window.C.page = "ms-company-admin";
      var self = this;
      submit_control.ack_check();
      domReady(function() {
        self.add_form_validation();
        // fixes for VUPT-4633
        // $('.phonefield').blur(function() {
        //    sg_util.phone_reformat();
        // });
        //Check If user is entering other phonefield then an asterisk is automatically added to the country code field
        $('#user_cell_phone').blur(function() {
          if ($('#user_cell_phone').val().length !== 0) {
            $("#cellphone").show(); //Slide Down Effect
          } else {
            $('#cellphone').hide();
          }
        });

        $('#user_phone_3').blur(function() {
          if ($('#user_phone_3').val().length !== 0) {
            $("#phone3").show(); //Slide Down Effect
          } else {
            $('#phone3').hide();
          }
        });

        $('#user_phone_4').blur(function() {
          if ($('#user_phone_4').val().length !== 0) {
            $("#phone4").show(); //Slide Down Effect
          } else {
            $('#phone4').hide();
          }
        });

        //Allow only number in phone field
        $(document.body).on('keypress','.phonefield',function(e) {
          var keycode = e.keyCode || e.which,
              phonefield = $(this),
              isAcknoweledge =  $('td.acknoweledge input').is(":checked");
            //Allow only numbers from keyboard and ignor Enter,Backspace,shift, ctrl and arrows key
            if (keycode != 8 && keycode != 13 && keycode != 0 && (keycode < 48 || keycode > 57)) {
              return false;
            }
            //Automatic formate phone number when enter pressed
            if (keycode == 13) {
              phonefield.trigger("blur");
              if (isAcknoweledge) {
                phonefield.closest("form").trigger("submit");
              }
            }
        });
        // If the body id isn't used, this will also trigger on the contact user pages
        // before they are ready. They use a separate call after pure has been applied.
        if ($('body#deploy_user_index table.delay_apply_datatable').length) {
          $.when(sg_util.apply_datatable($('table.delay_apply_datatable')))
            .then(function(){
              $('#section > #loading').fadeOut('slow',function(){
                $(this).remove();
                $('#fade_wrapper').fadeIn('slow');
              });
          });
        }
        
        $('.roles #user_roles_').map(function(){
          $(this).click(function(){
              if (this.value == 'company_admin' && this.checked){
                if($('.roles [value="switch_company"]')){
                  $('.roles [value="switch_company"]').prop('checked','checked');
                }
              }
            });
          }
        );
        // this selector already ensures only the contact index
        // receives the invocation
        $('body#contact_user_index').each(function(){
          self.get_contact_users(company_id);
        });
        // Applies to deploy new, deploy edit, and contact new only.
        // Deploy and contact indexes do similar after datatables loads.
        // Contact edit does similar after form is populated.
        $('body#deploy_user_new, body#deploy_user_edit, body#contact_user_new').each(function(){
          $('#section > #loading').fadeOut('slow',function(){
            $(this).remove();
            // fade in the contacts table
            $('#admin_form').fadeIn('slow');
          });
        });
      });
    }
  });

});
