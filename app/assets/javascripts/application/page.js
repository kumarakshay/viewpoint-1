define(
  [
  'domReady',
  'sg/jquery',
  'sg/ajax',
  'sg/page',
  'sg/util',
  'application/noti/noti',
  'showdown',
  'jquery.multiselect',
  'jquery.ui.datepicker',
  'jquery.fileDownload',
  'es5-shim'
 ], function(
    domReady,
    $,
    ajax,
    sg_page,
    sg_util,
    noti
) {
  return({
    converter: new Showdown.converter(),
    input_change: function($e) {
      var self = this;

      $e.each(function() {
        // Save current value of element
        $(this).data('oldVal', $(this).val());
        $(this).val();

        // Look for changes in the value
        $(this).bind("propertychange keyup input paste", function(event){
          // If value has changed...
          if ($(this).data('oldVal') != $(this).val()) {
           $(this).val();
           // Updated stored value
           $(this).data('oldVal', $(this).val());
            if ($(this).val().length > 2) {
              $(this).css({
               'background-image':"url('/assets/ajax-loader.gif')",
               'background-position':'98% 50%',
               'background-repeat':'no-repeat'
              });
           } else {
              $(this).css({'background-image':"none"});
           }
            self.search_results($('#device_search_results table tbody'),$(this).val());
          }
        });
      });
    },

    helps_ajax: function ($page) {
      var $url = "/helps/show.js?menu=" + $page,
        self = this;
      ajax.call($url, {
        dataType: "html"
      })
      .done(function(html){
         var md = self.converter.makeHtml(html);
       sg_util.big_box('<h2 class="bb-top">help</h2><div class="help_box">'+ md + '</div>');
      })
      .fail(function(jqxhr,status){
        sg_util.big_box('<div class="help_box">No help content for this feature for the following reason:<p>' + status + '</p></div>');
      });
    },

    search_results: function($e,$val) {
      $val = $val+'';
      // need new url
      var $url = '/searches/show.js?query='+$val;

      $("#device_search_results table tbody tr").remove();

      if ($val.length >= 3) {
        ajax.request($url, {
           id: 'viewpoint_master_search',
           dataType: "html",
        })
        .done(function(html){
          var $tdData;
          $e.html(html);
          $("#device_search_results").fadeIn();
          $('form#search input#search_input').css('background-image','none');
          if ($('#search span.total').attr('rel') == 1){
              $('form#search input#search_input').css('background-image',"url('/assets/enter-go.png')");
          }

          $('#search_results tr, #device_search_results tr').each(function(){
            var atag = $(this).find('a'),
              link = atag.attr('href'),
              count = 0;

            atag.removeAttr('href');

            $(this).find('td').each(function(){
              $tdData = $(this).text();
              if ($tdData) {
                if ($tdData === "No records found") {
                $(this).siblings().text('').parent("tr").addClass("no-data");
                } else {
                  $(this).click(function(e){
                    sg_util.click_feedback_overlay();
                    window.location = link;
                  });
                }
              } else {
                $(this).text('--');
                count++;
              }

            });

            if (count > 1) {
              $(this).remove();
            }
          });

        })
        .fail(function(xhr, status, error){
          
          if (status == 'stale' || status == 'abort') {
            // Old Request
          }
          else {
            alert("Unable to load search results: "+status);
          }
        });
      }
    },

    init: function() {
      var self = this,
          marqueeHandler = function(e) {
            sg_util.marquee_reveal($('#aside dl'));
          };
      $('#aside dl').on("setMarquee", marqueeHandler);
      $(document).ready(function() {
        sg_page.init();
        self.input_change($('form#search input#search_input'));
        $('#header > .buttons .help span').click(function(){
          self.helps_ajax($(this).attr('rel'));
         });
        noti.init();
      });
    }
  });
});
