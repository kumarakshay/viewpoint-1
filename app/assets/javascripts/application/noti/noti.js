 define(['underscore', 'sg/jquery'], function(_, $) {

  return {
    
    notificationTemplate: $("<li style='display:none' class='new_notice download'><div class='title'><span></span> is available for download.</div><div class='time'></div></li>"),
    
    ui: {
      count: "#b-notification-count",
      list: "#b-notifications",
      header: ".noti h1"
    },

    apiOptions: {
      url: "/api/reqs?job_status=finished&acknowledged=false&email_me=false",
      default_url: "/api/reqs?job_status=finished&acknowledged=false&email_me=false",
      dataType: "json"
    },

    created: '',

    unacknowledged: 0,

    notifications: [],

    acknowledge: function(jobID){
      var options = {
        url: '/api/reqs/ack?job_id='+jobID,
        type: "PUT"
      };

      $.ajax(options).done(function(data){
        if (data.status === 200) {
          // success
        } else {
          // fail
        }
      });
    },

    itemClick: function(el,data){
      var self = this;
      el.jobID = data.job_id;
      el.click(function(e){
        // stop prop
        e.stopPropagation();
        // check if this has unacknowledged class
        if ($(this).hasClass('unacknowledged')){
          // change acknowledged to true on server
          self.acknowledge(data.job_id);
          // remove unacknowledged class
          $(this).removeClass('unacknowledged');
          $('[data-created=' + data.created_at + ']').closest('.unacknowledged').removeClass('unacknowledged');
          // change count
          self.unacknowledged--;
          // download file
          $(self.ui.count).text(self.unacknowledged);
          // fade out if 0
          if (self.unacknowledged < 1) {
            self.ui.count.fadeTo(500,0);
          }
        }
        window.open(data.destination,"_blank");
        self.updateCount();
      });
    },

    updateCount :function(){
      var evt = document.createEvent('Event');
      evt.initEvent('updateCount', true, true);
      document.dispatchEvent(evt);
    },

    timeAgo: function(time){
      var units = [
        { name: "year", limit: null, in_seconds: 31556926 },
        { name: "month", limit: 31556926, in_seconds: 2629743 },
        { name: "week", limit: 2629743, in_seconds: 604800 },
        { name: "day", limit: 604800, in_seconds: 86400 },
        { name: "hour", limit: 86400, in_seconds: 3600 },
        { name: "minute", limit: 3600, in_seconds: 60 },
        { name: "second", limit: 60, in_seconds: 1 }
      ];

      var currtime = Date.now();
      var diff = ((currtime/1000) - (parseInt(time,10)));
      var text;

      if (isNaN(currtime)) {
        text = "(unspecified creation date)";
      }

      if (diff < 5) {
        text = "now";
      }

      $.each(units,function(i,v){
        var temp;
        if (diff > v.in_seconds && !v.limit){
          temp = Math.floor(diff / v.in_seconds);
          text = "about " + temp + " " + v.name + (temp>1 ? "s" : "") + " ago";
        }
        if (diff < v.limit && diff > v.in_seconds) {
          temp = Math.floor(diff / v.in_seconds);
          text = "about " + temp + " " + v.name + (temp>1 ? "s" : "") + " ago";
        }
      });
      return text;
    },

    build_html: function(data){
      var self = this;
      $.each(data.result,function(index,element){
       self.notifications =  _.reject(self.notifications, function(item){ 
        var result = element.job_id == item.jobID;
        if(result && item.hasClass('unacknowledged')) {
          self.unacknowledged--;
        }
        return result; 
      });


        // check destination

        if (element.destination !== ('null' || null)){

          var temp = self.notificationTemplate.clone();
          var data = {
            job_id: element.job_id,
            destination: element.destination
          };

          // acknowledged behavior
          if (element.acknowledged === false) {
            // count
            self.unacknowledged++;
            // item class
            temp.addClass('unacknowledged');
          }

          // set up click event
          self.itemClick(temp,data);

          // add data
          temp
            // fill in report name
            .find('.title span').text(element.report)
            // calculate elapsed time
            .closest('li').find('.time').attr('data-created',element.created_at);
          // add item
          self.notifications.push(temp);
      }

      });
    },
    
    fetch: function() {
      var self = this;
      
      var options = self.apiOptions;
      var display_array;

      if (self.created !== '') {
        options.url = self.apiOptions.default_url + "&__filter[created_after]=" + self.created;
      } else {
        self.ui.list.empty();
      }

      $.ajax(options).done(function(data){
          // check for results
          if (data.result.length > 0){

            // if there's results
            self.created = data.result[data.result.length-1]['created_at'];

            // moved to function for recalcs and ordering
            self.build_html(data);
            self.ui.list.empty();
            //display it in desc chronological order
            $.each(self.notifications,function(index,element){
              self.ui.list.prepend($(this));
            });

            if (self.unacknowledged > 0) {
              self.ui.count.text(self.unacknowledged).fadeTo(750,1);
            } else {
              self.ui.count.fadeTo(750,0);
            }

            self.ui.list.children('.new_notice').removeClass('new_notice').fadeIn(750,function(){
              // callback
            });

          }
          self.ui.header.addClass('view_all')
            .unbind('click')
            // go to notifications
            .click(function(){
              window.location = "/reports/notifications";
            });
          // no results
          if (self.ui.list.children().length === 0) {
            self.ui.list.html("<li class='no-results'>no notifications</li>").find('li').fadeTo(750,1);
            // self.ui.header.removeClass('view_all')
              // don't go to notifications
              // .unbind('click');
          }

          // time_ago
          $('#b-notifications .time').each(function(){
            $(this).text(self.timeAgo($(this).attr('data-created')));
          });

        });
    },

    init: function(){

      var noti_interval;

      var self = this,
        // delay_short = 3000,
        // delay_long = 12000;

        delay_short = 90000,
        delay_long = 300000;

      $(document).ready(function(){

        self.ui.list = $(self.ui.list);
        self.ui.count = $(self.ui.count);
        self.ui.header = $(self.ui.header);

        self.fetch();

        // $(window)
        //   .focus(function(){
        //     self.fetch();
        //     clearInterval(noti_interval);
        //     noti_interval = setInterval(function(){
        //       self.fetch();
        //     }, delay_short);
        //   }).blur(function(){
        //     clearInterval(noti_interval);
        //     noti_interval = setInterval(function(){
        //       self.fetch();
        //     }, delay_long);
        //   });

      });
    }
  };
});