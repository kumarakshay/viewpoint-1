define(['jquery'
       
], function($){
    var dummy_json = function(iterations) {
        var json_obj = '{"meta": {"total":' + iterations + '},'; 
        json_obj += '"status": 200,';       
        json_obj += '"result":['; // object begins after here

        for (var i = 0; i < iterations; i++){
            var data_group_name = genGroupName();
            var data_total_assets = genTotalAssets();
            var data_edit = genEdit();
            var data_show = genShow();
            var data_delete = genDelete();
            var data_press = genPressButton();
            var data_unlock = '';
            json_obj += '{ "name":"' + data_group_name + '",';
            json_obj += '"total_assets":"' + data_total_assets + '",';
            json_obj += '"show":' + data_show + ',';     
            json_obj += '"edit":' + data_edit + ','; 
            json_obj += '"delete":' + data_delete + ','; 
            json_obj += '"press":' + data_press + ','; 
            if (iterations == (i+1)) {
                json_obj += '"unlock":"' + data_unlock + '"}';
            } else {
                json_obj += '"action":"' + data_unlock + '"},';
            };                      
        };
        json_obj += ']}';

        function genGroupName() {
          var chance = Math.floor((Math.random()*20));
          var nameList = ["Test-Group-1", "Test-Group-2", "Test-Group-3", "Test-Group-4",
                          "Test-Group-5", "Test-Group-6", "Test-Group-7", "Test-Group-8",
                          "Test-Group-9", "Test-Group-10","Test-Group-11","Test-Group-12",
                          "Test-Group-13","Test-Group-14","Test-Group-15","Test-Group-16",
                          "Test-Group-17","Test-Group-18","Test-Group-19","Test-Group-20"];
          return nameList[chance];
        };

        function genTotalAssets() {
          var chance = Math.floor((Math.random()*20));
          var nameList = ["1", "2", "3", "4", "5", 
                          "6", "7", "8", "9", "10",
                          "11", "12", "13", "14", "15",
                          "16", "17", "18", "19", "20"];
          return nameList[chance];
        };

        function genPressButton() {
          var json = '{';
          json += '"callbackName": "clickMe",';
          json += '"title": "Press",';
          json += '"type": "action-button"}';
          return json;
        };

        function genEdit() {
          var json = '{';
          json += '"url": "/groups/4fe0cd33c99aef672b01a695/edit",';
          json += '"title": "Edit",';
          json += '"type": "browser-link"}';
          return json;
        }

        function genShow() {
          var json = '{';
          json += '"url": "/groups/4fe0cd33c99aef672b01a695",';
          json += '"title": "Show",';
          json += '"type": "browser-link"}';
          return json;
        }

        function genDelete() {
          var json = '{';
          json += '"url": "/groups/4fe0cd33c99aef672b01a695",';
          json += '"title": "Delete",';
          json += '"type": "browser-link"}';
          return json;
        }

        // function genDelete() {
        //   var json = '{';
        //   json += '"callbackName": "deleteUser",';
        //   json += '"title": "Delete",';
        //   json += '"type": "action-button"}';
        //   return json;
        // }

        json_obj = $.parseJSON(json_obj);
        return json_obj;
    };
return dummy_json;
});
