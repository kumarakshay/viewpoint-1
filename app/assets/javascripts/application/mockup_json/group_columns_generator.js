define(['jquery'
       
], function($){
    var dummy_json_logs = function() {
        var json_obj = '{"meta": {"total": 8},';  
        json_obj += '"status": 200,';       
        json_obj += '"result":['; 
        // models begin here
        json_obj += '{"modelAttribute":"name", "title":"Name", "columnType":"text"},';
        json_obj += '{"modelAttribute":"total_assets", "title":"Total Assets", "columnType":"text"},';
        json_obj += '{"modelAttribute":"show", "title":"", "columnType":"link", "hideSort": true, "hideFilter": true},';
        json_obj += '{"modelAttribute":"edit", "title":"", "columnType":"link", "hideSort": true, "hideFilter": true},';
        json_obj += '{"modelAttribute":"delete", "title":"", "columnType":"link", "hideSort": true, "hideFilter": true},';
        json_obj += '{"modelAttribute":"press", "title":"", "columnType":"button", "hideSort": true, "hideFilter": true}';
        // models end here
        json_obj += ']}';
           
        json_obj = $.parseJSON(json_obj);
        return json_obj;
    };
return dummy_json_logs;
});
