define(['jquery'
       
], function($){
    var dummy_json_logs = function() {
        var json_obj = '{"meta": {"total": 9},';  
        json_obj += '"status": 200,';       
        json_obj += '"result":['; 
        // models begin here
        json_obj += '{"modelAttribute":"name", "title":"Name", "columnType":"number"},';
        json_obj += '{"modelAttribute":"ip_address_formatted", "title":"Ip Address", "columnType":"number"},';
        json_obj += '{"modelAttribute":"hw_model", "title":"HW Model", "columnType":"dateTime"},';
        json_obj += '{"modelAttribute":"location", "title":"Location", "columnType":"number"},';
        json_obj += '{"modelAttribute":"os_model", "title":"OS Model", "columnType":"number"},';
        json_obj += '{"modelAttribute":"priority", "title":"Priority", "columnType":"number"},';
        json_obj += '{"modelAttribute":"production_state", "title":"Production State", "columnType":"number"},';
        json_obj += '{"modelAttribute":"company_use", "title":"Company", "columnType":"number"}';
        // models end here
        json_obj += ']}';
           
        json_obj = $.parseJSON(json_obj);
        return json_obj;
    };
return dummy_json_logs;
});
