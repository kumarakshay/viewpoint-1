define(['jquery'
       
], function($){
    var dummy_json_logs = function(iterations) {
        var json_obj = '{"meta": {"total":' + iterations + '},'; 
        json_obj += '"status": 200,';       
        json_obj += '"result":['; // object begins after here

        for (var i = 0; i < iterations; i++){
            var name =  '450366862808058' + i;
            var ip_address_formatted =  genIP('10');
            var hw_model = 'BIG-IP deprecated' + i ;
            var location = '/US/GA/ALPHARETTA.001';
            var os_model = 'IOS 12.2' + i;
            var priority = 'Managed';
            var production_state = 'Decommissioned';
            var company_use = 'company_use';
            json_obj += '{ "name":"' + name + '",';
            json_obj += '"ip_address_formatted":"' + ip_address_formatted + '",';
            json_obj += '"hw_model":"' + hw_model  + '",';
            json_obj += '"location":"' + location + '",';     
            json_obj += '"os_model":"' + os_model + '",'; 
            json_obj += '"priority":"' + priority + '",'; 
            json_obj += '"production_state":"' + production_state + '",'; 
            if (iterations == (i+1)) {
                json_obj += '"company_use":"' + company_use + '"}';
            } else {
                json_obj += '"company_use":"' + company_use + '"},';
            };                      
        };
        json_obj += ']}';

        function genProtocol() {
    var chance = Math.ceil((Math.random()*10));
          if (chance < 3) {
      return 51;
          } else if (chance < 6) {
            return "UDP";
          } else {
      return "TCP";
          }
  };

        function genTime() {
            var time = '2013-' + Math.floor((Math.random()*12)+1) +
                "-" + Math.floor((Math.random()*30)+1) + ' ' +
                Math.floor((Math.random()*12)+1) + ":" + 
                Math.floor((Math.random()*59)+1) + ":" + 
                Math.floor((Math.random()*59)+1) + " PM";
            return time;
        };

        function genIP(Octet1, Octet2, Octet3, Octet4) {
            var ip = "";
            if (Octet1) {
                ip += Octet1;
            } else {
                ip += returnIP();
            }
            ip += ".";
            if (Octet2) {
                ip += Octet2;
            } else {
                ip += returnIP();
            }
            ip += ".";
            if (Octet3) {
                ip += Octet3;
            } else {
                ip += returnIP();
            }
            ip += ".";
            if (Octet4) {
                ip += Octet4;
            } else {
                ip += returnIP();
            }                        
            function returnIP(){
                return Math.floor((Math.random()*255)+1);
            }
            return ip;
        };

        json_obj = $.parseJSON(json_obj);
        return json_obj;
    };
return dummy_json_logs;
});
