DB's

Dashboard:
ID, Name, User ID, Description, Default [graph id, graph id, graph id...]

Graphs: 
ID, Name, Metric, Description, Order, Timeframe[day, week, month], [asset_guid, asset_guid, asset_guid...]

Views

Dashboard index: 
lists all with links: show edit and delete

Dashboard show: 
shows dashboard: graphs by order
	.js: shows default dashboard of graphs, allows for re-order

Dashboard edit: 
edits one, allows re-order and widget edit/delete

Dashboard new: 
name, description, default toggle, no widgets to start

_tools: 
used by all views in dashboards/graphs
add new graphs/add new dashboard/back buttons, dropdown list[shows current dashboard name]

_dashboard_form:
used by both new and edit

Graphs new: 
1. initially only allows metric pulldown; fill in name, description and timeframe; shows order
2. metric choice enables second dropdown shows available assets for chosen metric
	- once an asset is selected the metric dropdown is frozen until all devices are deselected
	- limit to five assets? (no cap built in to graph as of now)
3. enable submit when required inputs have value

Grapshs edit:
edit widget

_graph_form:
used by both new and edit