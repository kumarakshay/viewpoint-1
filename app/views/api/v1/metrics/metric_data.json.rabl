object @metric
attributes :uid, :asset_name, :scope, :component_type, :component, :display_name, :metric_name, :metric_alias

child :series => 'series' do
  attributes :value 
  node do |series|
    {:timestamp => series.timestamp.to_f}
  end
 end