collection @records  => :company
node do |c|
  {
    :name => c.name,
    :guid => c.guid,
    :totalUsers => c.user_count
  }
end

