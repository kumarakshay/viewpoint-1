collection @records => :company
node do |c|
  {
    :name => c.name,
    :guid => c.guid,
    :totalUsers => c.user_count,
    :usonly => c.us_only,
    :address1 => nil,
    :address2 => nil,
    :primaryContacts  => 
      {
       :contactName => nil,
       :position => nil,
       :phone => nil,
       :email => nil
      }
  }
end

