devices_target = session[:is_omi] ? "/ui/index.html#/devices" : "/devices"
interfaces_target = session[:is_omi] ? "/ui/index.html#/interfaces" : "/interfaces"
events_target = session[:is_omi] ? "/ui/index.html#/events" : "/events"
env_links = [
  {
    "title"=> "Devices",
    "target"=> devices_target,
    "icon"=> "link-devices",
    "items"=> nil
  }, {
    "title"=> "Interfaces",
    "target"=> interfaces_target,
    "icon"=> "link-interfaces",
    "items"=> nil
  }, {
    "title"=> "Events",
    "target"=> events_target,
    "icon"=> "link-events",
    "items"=> nil
  }
]

event_notification = {
  "title"=> "Event Notifications",
  "target"=> "/event_subscriptions",
  "icon"=> "link-event-sub",
  "items"=> nil
}
if !@current_user.is_omi?
  env_links.push(event_notification)
end

env_health = {
  "title"=> "Environment Health",
  "target"=> "/managedservice/#ehealth",
  "icon"=> "link-environment-health",
  "items"=> nil
}

if !@current_user.is_sungard? && !@current_user.is_omi?
  env_links.push(env_health)
end

firewalls = {
  "title"=> "Firewalls",
  "target"=> "/firewalls",
  "icon"=> "link-firewalls",
  "items"=> nil
}

env_links.push(firewalls)

if current_user.is_sungard?
  env_links.push({
    "title"=> "Ipam",
    "target"=> "/ui/index.html#/ipam",
    "icon" => "link-ipam",
    "item" => nil
  })
end

dns = [
  {
    "title"=> "DNS",
    "target"=> "/ui/index.html#/dns",
    "icon"=> "link-dns",
    "items"=> nil
  },
  {
    "title"=> "DNS Audit Logs",
    "target"=> "/ui/index.html#/dns_audits",
    "icon"=> "link-dns-audits",
    "items"=> nil
  },
  {
    "title"=> "IPv4",
    "target"=> "/ui/index.html#/ipv4",
    "icon"=> "link-ipv4",
    "items"=> nil
  },
  {
    "title"=> "IPv6",
    "target"=> "/ui/index.html#/ipv6",
    "icon"=> "link-ipv6",
    "items"=> nil
  }
]

if can? :read, SGTools::Rails::Infoblox::Dns::Zone
  env_links.push(dns).flatten!
end

if @branding == "allstream"
  @cloud_portal_target = "Allstream Cloud Portal"
  @sungardas_cloud_portal = "https://allstreamcloudcompute.com/portal/login.jsp"
else
  @cloud_portal_target = "SungardAS Cloud Portal"
  @sungardas_cloud_portal = "https://www.edison.sgns.net/portal/login.jsp"
end

tool_links = [
{
  "title" => "SIEM",
  "target" => "/tools/siem/Application.html",
  "icon" => "link-siem",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => @cloud_portal_target,
  "target" => @sungardas_cloud_portal,
  "icon" => "link-siem",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => "US Vaulting",
  "target" => "http://sg-reports.evault.com/singlelogin/login.aspx",
  "icon" => "link-us-vaulting",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => "UK VytalVaults Console",
  "target" => "https://wcc2.sungardvaulting.com/login/login.aspx",
  "icon" => "link-uk-vv-console",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => "UK VytalVault Report",
  "target" => "https://vvreports.sungardvaulting.com/erlogin/login.aspx",
  "icon" => "link-uk-vv-reports",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => "Managed Email Archiving (EAS)",
  "target" => "https://ems.messageone.com/wfe/",
  "icon" => "link-eas",
  "tab" => "_blank",
  "items" => nil
},
{
  "title" => "SE Online Backup Portal",
  "target" => "https://portal.nordicvault.com",
  "icon" => "link-server-replication",
  "tab" => "_blank",
  "items" => nil
}]

hpc_url = {
  "title" => "Hosted Private Cloud",
  "target" => "https://cloudsolutions.sungardas.com/vcac/org/#{session[:user_snt]}/",
  "icon" => "link-us-vaulting",
  "tab" => "_blank",
  "items" => nil
}

qlick_sense_url = {
  "title" => "Qlik Sense",
  "target" => "https://bi.sungardas.com",
  "icon" => "link-us-vaulting",
  "tab" => "_blank",
  "items" => nil
}

if(session[:user_snt] && ["sahd","su7s","phgh","fol0","insu","hegp","cess","tr0i"].include?(session[:user_snt].downcase))
  tool_links.push(hpc_url)
end

if current_user.is_sungard? 
 tool_links.push(qlick_sense_url)
end

legacy_portal = {
  "title" => "Legacy Portal (IE Only)",
  "target" => APP_CONFIG['legacy_portal'],
  "icon" => "link-siem",
  "tab" => "_blank",
  "items" => nil
}

if current_user.is_sungard?
  tool_links.push(legacy_portal)
end

report_links = [
{
  "title" => "Reports",
  "target" => "/ui/index.html#/reports",
  "icon" => "link-reports",
  "items" => nil
}]

patching_links = [
{
  "title" => "Patching Schedule",
  "target" => "/ui/2f/index.html#/patching-schedule",
  "icon" => "link-schedule-manager",
  "items" => nil
},
{
  "title" => "Windows Policy Manager",
  "target" => "/ui/2f/index.html#/patching-policy",
  "icon" => "link-policy-manager",
  "items" => nil
},
{
  "title" => "Servers",
  "target" => "/ui/2f/index.html#/patching-servers",
  "icon" => "link-patching-servers",
  "items" => nil
}
]

if (current_user)
  if !session[:is_omi] || current_user.is_sungard?
    target = "/reqs"
  else
    target = "/ui/index.html#/reports/subscriptions"
  end
  report_links.push({
    "title" => "Subscriptions",
    "target" => target,
    "icon" => "link-subs",
    "items" => nil
  })
end

if current_user && current_user.access_restricted?
  report_links.push({
      "title" => "Restricted Contracts",
      "target" => "/ui/index.html#/restricted_contracts",
      "icon" => "link-contracts",
      "items" => nil
    },
    {
      "title" => "Restricted Invoices",
      "target" => "/ui/index.html#/restricted_invoices",
      "icon" => "link-invoices",
      "items" => nil
    })
end

application_links =
  [
    {
      :title => "Launchboard",
      :target => @homepage_url,
      :icon => "link-launchboard",
      :items => nil
    },
    {
      :title => "Ticketing",
      :target =>  @get_sn_url,
      :icon => "link-ticketing",
      :tab => "_blank",
      :items => nil
    },
    {
      "title"=> "Environment",
      "target"=> "#",
      "icon"=> "link-environment",
      "active"=> true,
      "items"=> env_links
    },
    {
      "title" => "Reports",
      "target" => "#",
      "icon" => "link-reports",
      "items" => report_links
    },
    {
      "title" => "Tools",
      "target" => "#",
      "icon" => "link-tools",
      "items" => tool_links
    },

  ]

  groups = {
    "title" => "Groups",
    "target" => "/managedservice/#groups",
    "icon" => "link-groups",
    "items" => nil
  }

  ncl_dashboard = {
    "title" => "OS APP Dashboard",
    "target" => "/ui/index.html#/os-app-dashboard",
    "icon" => "link-reports",
    "items" => nil
  }

  automated_patching = {
     "title" => "Automated Patching",
     "target" => "#",
     "icon" => "link-update-manager",
     "items" => patching_links
  }

 if @current_user && @current_user.company_guid.downcase == "d811fa8b-4288-4f6d-b369-072f2c4c1d21"
    application_links.push(ncl_dashboard)
 end

if !@current_user.is_sungard?
  application_links.push(groups)
end

if !APP_CONFIG['vp_cauldron']
  if @current_user.is_sungard?
    application_links.push(automated_patching)
  else
    if @current_user.portal_patching_access? && @sn_permissions[:server_change].to_i == 1
      application_links.push(automated_patching)
    end
  end
end


#Default portal selector tab
portal_tabs = [{ :title =>"Applications",:active => true}]
admin_links =[]

vp_admin = {
    "title" => "Viewpoint Admin",
    "target" => "/sungard",
    "icon" => "link-company-admin",
    "items" => nil
}

sungard_as_admin = {
  "title" => "Sungard AS Admin",
  "target" => "/sungard",
  "icon" => "link-company-admin",
  "items" => nil
}

company_admin = {
  "title" => "Company Admin",
  "target" => "/admin/users",
  "icon" => "link-company-admin",
  "items" => nil
}

hpc_admin = {
  "title" => "HPC Network Admin",
  "target" => "/ui/2f/index.html#/hpc_admin",
  "icon" => "link-company-admin",
  "items" => nil
}

#conditions for showing administrative tab in tool-selector
if(current_user.admin_role?)
  portal_tabs.push({:title => "Administrative",:active => false})
end
if (current_user.is_viewpoint_admin?)
  admin_links.push(vp_admin)
end
if (current_user.is_sungard_admin?) and (!current_user.is_viewpoint_admin?)
   admin_links.push(sungard_as_admin)
end

if (current_user.is_company_admin?) and (!current_user.is_sungard?)
 admin_links.push(company_admin)
end

if (current_user.is_sungard? and current_user.role?('hpc_network_admin')) 
 admin_links.push(hpc_admin)
end

node :tools do |c|
  {
    :tabList  =>
      {
        :header => portal_tabs,
        :body => {
          :Applications => application_links,
          :Administrative => admin_links

        }

      }
  }
end
