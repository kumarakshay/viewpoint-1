object @record
attributes :id, :user_guid, :report, :message_id, :job_id, :job_status, :acknowledged, :destination, :email_me
node(:created_at) {|u|u.created_at.to_time.to_i}
