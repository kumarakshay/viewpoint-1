collection @dats, :object_root => false
attributes :id, :name, :company_guid, :active, :description, :updated_at, :deleted_at
node(:asset_list) { |dat| dat.asset_list.uniq }