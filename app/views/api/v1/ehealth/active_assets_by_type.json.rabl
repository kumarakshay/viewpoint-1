collection @records,:object_root => false
attributes :status,:host,:mx_windows,:guid
node(:incident) { |u| u.incidents }
node(:changes) {|u|u.req_changes}
node(:avail) {|u|u.avail.to_s + "%"}