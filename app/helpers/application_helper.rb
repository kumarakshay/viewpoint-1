module ApplicationHelper

  # This helper return array of roles display_name for user
  def display_user_roles(user)
     Role.where(:name => user.roles).all.collect(&:display_name)
  end  

  def get_report_name(report_id)
    dat = Statement.where(report_id: report_id).first.name
  end
  
  def get_device_name(asset_guid)
    dat = Device.where(asset_guid: asset_guid).first
    if dat
      name = dat.name
    else
      name = ""
    end
  end  
  
  def get_group_name(group_guid)
    dat = Group.where(id: group_guid).first
    if dat
      name = dat.name
    else
      name = ""
    end
  end

  def submit_button(content, options = {})
    content_tag :button, content, options.merge(:type => 'submit', :'data-disable-with' => 'please wait')
  end

  def format_ticketing_date(ticket_date)
    timezone = current_user.timezone rescue "PST8PDT"
    ticket_date.to_datetime.in_time_zone(timezone).strftime('%F %H:%M:%S') rescue ticket_date
  end

  # the session[:masquerade] is a string
  def get_sn_url()
    if current_user
      if (current_user.is_sungard? || @masquerade)
        url = APP_CONFIG['service_now_url'] + "/saml_redirector.do?sysparm_uri=navpage.do"
      elsif (current_user.is_allstream_user? and !(request.url.match('allstream.com').nil?))
        url = APP_CONFIG['service_now_allstream'] + "/saml_redirector.do?sysparm_uri=snc/sg/home.do"
      else
        url = APP_CONFIG['service_now_url'] + "/saml_redirector.do?sysparm_uri=sg/home.do"
      end
    else
      url = APP_CONFIG['service_now_url']
    end
    return url
  end

  def get_kb_url()
    APP_CONFIG['knowledge_base']
  end

  def get_cloud_url()
    APP_CONFIG['cloud_url']
  end
end
