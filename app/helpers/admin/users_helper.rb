module Admin::UsersHelper

  def available_roles(match)
    active_roles = Role.active_roles(current_user.roles)
    if match == 'sungard_match'
      active_roles = active_roles.all.delete_if{|role|Role.all_customer_roles.all.collect(&:name).include? role.name  }
    else
      active_roles = active_roles.all.delete_if{|role| ['sungard_admin','viewpoint_admin','hpc_network_admin'].include? role.name }
    end
    active_roles
  end

  def can_be_converted_to_non_portal(usr)
   ['E71312DC-AAF1-4BF2-941D-B297B1D326AB','76C6F530-40C1-446D-A5D5-A66E78605149'].include?(usr.company_guid.upcase) ? false : true   
  end

  def sungard_match(user)
    sungard_match = ''
    if user.is_sungard? or (session[:temp_company_guid] && session[:temp_company_guid] == '76C6F530-40C1-446D-A5D5-A66E78605149')
      sungard_match = 'sungard_match'
    end
    sungard_match
  end

end