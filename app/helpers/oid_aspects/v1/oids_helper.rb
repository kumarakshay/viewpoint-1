module OidAspects
  module V1
    module OidsHelper

      def create
        if openid = request.env[Rack::OpenID::RESPONSE]
          case openid.status
          when :success
            logger.info("Success status received from OpenID trying to update/auto-provision an user")
            begin
              ax = OpenID::AX::FetchResponse.from_success_response(openid)
              email = ax.get_single('http://axschema.org/contact/email')
              user = User.where(:email => email, :active => true).first
              received_attrs = fetch_exchange_attributes(ax)
              user ||= User.update_identifier(received_attrs)
              Rails.logger.info("received attribute #{received_attrs}")
              # auto provisioning a user
              if user.blank?
                if received_attrs[:email] =~ /@sungardas.com/i
                  received_attrs[:portal_name] = "sungard"
                  # UUID gem manages guid creation
                  uuid = UUID.new
                  z = uuid.generate
                  received_attrs[:guid] = z
                end
                logger.info("Received attribute - #{received_attrs}")
                user = User.create!(received_attrs)
                logger.info("Auto provisioned #{received_attrs[:email]} with #{received_attrs}")
              end
              # operator_guid is used by pyxie for auth
              session[:user_guid] = user.id
              session[:operator_guid] = user.id
              logger.info("authorized access for #{received_attrs[:email]}")
              redirect_to(session[:redirect_to] || home_path)
              return
            rescue Exception => e
              logger.error("Error occured while creating/updating or fetching details after SSO -  #{e.inspect} #{openid.inspect}")
              render :file => 'shared/sso' # , :layout => 'application'
            end
          when :failure
            logger.error("Failure status received from OpenId Unable to authorized access #{openid.inspect}")
            #render :file => 'shared/unauthorized', :layout => 'basic'
            redirect_to welcome_path
            return
          end
        else
          redirect_to "#{APP_CONFIG['open_id_url']}"
        end
      end

      def oid_redirect
        respond_to do |format|
          format.html {
            response.headers['WWW-Authenticate'] = Rack::OpenID.build_header(
              :identifier =>  APP_CONFIG['open_id_url'],
              :required => ["http://axschema.org/contact/email",
                            "http://axschema.org/company/name",
                            "http://axschema.org/company/guid",
                            "http://axschema.org/contact/web/default",
                            ],
              :optional => ["http://axschema.org/namePerson/first",
                            "http://axschema.org/namePerson/last",
                            ],
              :return_to => oid_url,
              :method => 'POST'
            )
            head :unauthorized
          }
          format.any {
            head :unauthorized
          }
        end
      end

      private

      def fetch_exchange_attributes(ax)
        # Brought time zone as a mandatory attribute and fetching its value.
        received_attrs = {:email => ax.get_single('http://axschema.org/contact/email'),
                          :company_name => ax.get_single('http://axschema.org/company/name'),
                          :company_guid => ax.get_single('http://axschema.org/company/guid'),
                          :portal_name => ax.get_single('http://axschema.org/contact/web/default')}
        { :first_name => 'http://axschema.org/namePerson/first',
        :last_name => 'http://axschema.org/namePerson/last' }.each do |key,value|
          begin
            unless ax.get_single(value).blank?
              received_attrs[key] = ax.get_single(value)
            end
          rescue Exception => e
            logger.info("Unable to fetch value from openid response for optional field #{key}")
          end
        end
        received_attrs
      end

      def create_audit_log(user)
        audit = {}
        audit["user_email"] = user.email
        audit["action"] = "login"
        audit["auditable_type"] = 'User'
        Audit.create(audit)
      end

    end
  end
end
