Feature: Ticket Landing
  In order to see ticket information
       A Viewpoint user
  Should be able to sign in through SSO
     And See ticket landing as their home page

Scenario: Deep Links are wrapped in permissions
   Given I exist as a logged in user
    Then I should be see Deep Links
     And Deep Link Incident is available for all
     And Deep Link Change is available for all
     And Deep Link Request is available for all
     And Deep Link LILO/Site access is restricted
     And Deep Link Notification Matrix is restricted
     And Deep Link Authorization Matrix is restricted

Scenario: User interacts with Deep Links
 Given I exist as a logged in user
   And I have the correct privs
  Then I should be able to click through into Service Now

Scenario: User sees unclickable Deep Links
 Given I exist as a logged in user
   And I don’t have the correct privs
  Then I should not be able to click the link
   And I won’t see any message indicating that I don’t have the permission

Scenario: User sees Asset Health
  Given I should see a circle graph
    And That graph shows the Health Score
    And I should be able to see the health score grouped by asset types
    And I should be able to see the health score details by asset types
    And I should be able to see the health score grouped by user defined groups
    And I should be able to see the health score details by user defined groups

# this should be an api call - by company guid

Scenario: User sees Ticket List
  Given I should see a list of tickets
    And The time range is always Time.now - 30.days
    And the columns are sortable
    And the columns are searchable
    And the columns are Ticket Number, State, Summary, Created, and Updated
    For Ticket Type Incidents
    For Ticket Type Requests
    For Ticket Type Change

# this should be an api call - by company guid

Scenario: User sees Ticket Statistics
   Given I should see a circle graph
     For Time range of Time.now - 30.days
 Display Summarized counts by Active, Closed and Total
     For Ticket Type Incidents (INC)
     And Ticket Type Requests (REQ)
     And Ticket Type Change (CHG)
     And the details of each contain a clickable radio button
     And Clicking on the detail with update the circle graph with the counts that ticket state
     And Clicking on the detail will update the Ticket List to display that ticket state

Scenario: User interacts with Ticket Stats
 Given I can interact with Ticket Stats
   And data is always for Time.now - 30.days
   When I click Incidents
   Then Ticket list should update with Incidents
   When I click Requests
   Then Ticket list should update with Requests
   When I click Changes
   Then Ticket list should update with Changes

Scenario: User views the latest 20 devices that have been viewed previously
Given I exist as a logged in user
I get the list of devices that I viewed previously
Then I open up any device profile page
Then I click “viewpoint” from upper-left corner to come back to Landing page, I’ll see the device just been viewed is listed out at the top
