Feature: Restrict user 
  In order to restrict user by doing any activity if he/she gets deleted or disabled user needs to be login to apllication from one place and admin also be logged in from 
  other place

    Scenario: admin deleted user
      Given I exist as a user with any admin role
        And I can delete user 
	   When I delete that user
       Then that user session immediately get destroyed and he will be signout from application
	   
       
    Scenario: admin disabled user
      Given I exist as a user with any admin role
        And I can disable user 
     When I disable that user
       Then that user session immediately get destroyed and he will be signout from application and not able to login again to application

