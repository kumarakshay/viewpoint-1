Feature: Add/Remove Subscription for events 
  Active company users can add/remove event subscription through email confirmation 

Scenario: User wants to add event subscription
 Given I exist as a company user 
  When I log into the application 
   And I navigate to the event subscription form
  Then I should be able to see form where I can provide email address while creating new  event subscription
  When I submit the form
  Then I will receive an email containing a subscription confirmation link at given email address
  When I click on confirmation link
  Then It will enable my event subscription and listed it on corresponding index page

Scenario: User wants to remove event subscription
 Given I exist as a company user 
  When I log into the application 
   And I navigate to the event subscription index page
  Then It should show me all active subscriptions along with 'unsubscribe' button
  When I click on 'unsubscribe' button
  Then I will receive an email containing a unsubscribe link at given email address
  When I click on unsubscribe link
  Then It will remove my event subscription and take me to the index page
   Where I am not able to see removed subscription