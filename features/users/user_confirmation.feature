Feature: User Confirmation
  In order to confirm an user
  A user should have confirmation link and a valid password that system can accept.
  A confirmation link is valid for 24 hrs.

  Scenario: If confirmation link is over 24 hours old
    When I put in valid password and confirmation password and click confirm
    Then I see the same page with error message "Please check with administrator, your activation token has expired"

  Scenario: If confirmation link is not older than 24 hours
    When I put in valid password and confirmation password and click confirm     
    Then I should successfully access the welcome page

  Scenario: In case an user requests for a new confirmation token.
    When I as an admin click on resend confirmation link for a particular user.
    Then User would receive and email with a valid confirmation link. 