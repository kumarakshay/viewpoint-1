Feature: Switch company role for an user 
  In order to use switch company permission a user must have the "switch company" role

Scenario: User logged in as a viewpoint/sungard/company admin 
  Given I exist as a user with viewpoint/sungard/company admin role
    And I open any user settings of non sungard user
   Then I see the role "Switch Company" 
   When I select "switch company" permission and I the record
   Then The user has role "switch company"

Scenario: An external user logged in into viewpoint 
  Given I exist as a user with switch company role
    And I would like to view child company data
   When I click the gear icon in the menu bar
   Then I see the link "Switch Company" 
   When I click "Switch Company" link 
   Then I can see a list of child companies 
   When I click on any company in the given list        
   Then I am redirected to a welcome page 
    And I see the UI element "Working In .." selected company
    And I can navigate the application in that company

Scenario: A user wants to return to the default company    
   When I click once again on the gear icon 
   Then I see the "My-Company" link
   When I click on the link "My-Company" link 
   Then I am redirected back to the parent company 
    And "Working in Company" UI element will be gone

Scenario: A sungard user masquerades as an user having switch company rights.
  When I click once again on the gear icon
  Then I see Masquerade with a tick icon in front of it, as well I see switch company below it in the drop 
       down menu
  When I click on the 'switch company' menu item
  Then I see list of all child companies. 
  When I click on any company in the given list        
  Then I am redirected to a welcome page 
  And  I see the UI element "Masqueraded as <email sungard user>" and second UI element next to it "for 
       company <company name>" 
  And I can navigate the application in that company

