Feature: CRUD for Contact user. 
  In order to create contact user you should have admin rights (viewpoint/sungard/company admin.)

Scenario-A: User logged in as a company admin wants to view list of contact users 
  Given  I exist as a user with company admin role
    And  I click gear icon
    Then I see menu item "Manage non-portal users" 
    When I click on this menu item 
    Then I am redirected to index page displaying contact users for his company.

Scenario-B: User logged in as a Sungard/Viewpoint admin wants to view list of contact users 
  Given  I exist as a user with Sungard/Viewpoint admin role
    And  I click gear icon
    Then I see menu item "Company management" 
    When I click on company management menu item 
    Then I would see list of companies and next to it two links "users" and "contact users".
    When I click the "contact users" link then I am redirected to index page displaying contact  
          user for that company. 

Scenario: User logged in as an admin wants to add a contact user 
  Given  I am on index page of contact users as in scenario(A/B) above   
    When I click the link "add contact" in the top header bar
    Then I see a form with fields for adding contact user data.
    When I fill in all mandatory details and submit the form by clicking "save profile"
    Then I would see a flash message saying user details saved. 
    When I click on the back button in header bar.      
    Then I am redirected once again to index page.

Scenario: User logged in as an admin wants to edit a contact user 
  Given  I am on index page of contact users as in scenario(A/B) above   
    When I click the "edit" link next to any user detail
    Then I see a form with fields showing data for that contact user.
    When I fill change details and submit the form by clicking "save profile"
    Then I would see a flash message saying "Your updates to this contact record have been saved". 
    When I click on the back button in header bar.      
    Then I am redirected once again to index page.

Scenario: User logged in as an admin wants to delete a contact user 
  Given I am on index page of contact users as in scenario(A/B) above   
    When I click the "delete" button next to any user detail
    Then I see a confirmation asking to confirm delete action.
    When I confirm by clicking "yes"
    Then That contact user would be deleted.