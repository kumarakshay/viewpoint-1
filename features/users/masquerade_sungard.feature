Feature: Masquerade
  All SunGard users can masquerade into any company

    Scenario: A SunGard user logged in into viewpoint 
      Given I exist as a sungard user I automatically have masquerading permission
        And I would like to masquerade as any company user
      When I click masquerading link under the gear icon
        Then I can see list of all active companies
      When I click on users link in front of any company
        Then I can see list of users for that selected company
      When I click on particular user 
        Then I am redirected to a page showing confirmation about masquerading

    Scenario: A SunGard user logged in into viewpoint and is masquerading as a user having switch company 
      role

      Given I exist as a sungard user masquerading as user with switch comapny rights
      When I click on the gear icon  
        Then I would see switch company as an additional link under in addition to Un-Masquerade.
      When I click switch company link under the gear icon
        Then I can see list of active companies where I am allowed to take a look
      When I click on any company in the given list        
        Then I am redirected to a welcome page showing with the top right hand corner saying text   
            "masqueraded as impersonate@viewpoint.com for company ...(selected company) " 
      When I click once again on the gear icon 
        Then I would see another entry saying my-company to switch back to default company where I belong
      When I click on the link "my company" link under the gear icon
        Then I am redirected back to the parent company where I am working. 
        Then The  left hand top corner will not have a message "masqueraded as ...(masqueraded user) "    