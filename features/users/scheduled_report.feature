Feature: Add/Remove subscription for reports
  Active company users can add/remove report subscription through email confirmation 

Scenario: User wants to add report subscription
 Given I exist as a company user 
  When I log into the application 
   And I navigate to the report subscription page
  Then I should be able to see form where I can provide email address while creating new  report subscription
  When I submit the form
  Then I will receive an email containing a subscription confirmation link at given email address
  When I click on confirmation link
  Then It will enable my report subscription and display it on corresponding index page

Scenario: User wants to remove report subscription
 Given I exist as a company user 
  When I log into the application 
   And I navigate to the report subscription page
  Then It should show me all active report subscriptions along with 'unsubscribe' button
  When I click on 'unsubscribe' button
  Then I will receive an email containing a unsubscribe link at given email address
  When I click on unsubscribe link
  Then It will remove my report subscription and take me to the corresponding index page
   Where I am not able to see removed subscription