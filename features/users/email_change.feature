Feature: Change user email address
  Active company users can change their email address 

Scenario: User wants to change email address 
 Given I exist as a company user 
  When I log into the application 
   And I navigate to the edit user profile
  Then I should be able to change my email address
   And Validation of unique and correctly formatted email is enforced
  Then I submit the form

Scenario: User validates the email change
 Given I have initiated an email change
  Then I will receive an email containing a confirmation link at the new address
   And I will receive an email at the current address indicating an email change has been initiated
  When I click on confirmation link
  Then It will take me to the email change confirmation page
   And It will display a message stating 'Change has been made'
   And There will be a link to the application login form

Scenario: Sungard user initiates a email change
  Given I exist as a sungard user 
   Then I can masquerade as any company user
    And I can change email address 
    And The 'user wants to change their email' workflow will be initiated 
