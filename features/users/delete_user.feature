Feature: Delete user 
  In order to delete user should require admin(viewpoint, sungard, company) permission 

    Scenario: User logged in as a viewpoint/sungard admin 
      Given I exist as a user with viewpoint/sungard admin role
        And I can delete user of any company
	   When I go to user listing page
       Then I can see delete link in front of every user 
	   When I click on delete link for deleting user 
	   Then It should ask me for confirmation
       When I confirm delete action 
       Then User will deleted from application as well as from openid server.
       When I delete user	   
	   Then I am not able to see that user in user listing page.
       
    Scenario: User logged in as a company admin 
      Given I exist as a user with company admin role
         And I can delete any user from my company
	   When I go to user listing page
       Then I can see delete link in front of every user
	   When I click on delete link for deleting user 
	   Then It should ask me for confirmation
       When I confirm delete action 
       Then User will deleted from application as well as from openid server.
       When I delete user	   
	   Then I am not able to see that user in user listing page.
	   
     Scenario: User logged in as normal user 
      Given I exist as a user without admin role
       When I am login to application 
	   Then I am not able to see user listing page and delete action link.
       