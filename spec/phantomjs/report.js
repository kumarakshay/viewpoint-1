var page = new WebPage(), testindex = 0, loadInProgress = false;
var base_url = "https://vpdev.sgns.net/~sunny.bogawat/vp/";
var site_url = base_url + "home";
var login_url = base_url + "admin/sessions/new";
var report_sub_url = base_url + "reqs"
var nowin = '';
var cur_time = '';

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.onLoadStarted = function() {
  loadInProgress = true;
  console.log("load started - " + nowin);
  cur_time = Date.now();
};

page.onLoadFinished = function() {
  loadInProgress = false;
  console.log('load finished loading time ' + (Date.now() - cur_time) + ' msec');
};

logResources = true;

//Here are array of steps which is perfom on complete report lifecycle
var steps = [
              //Load Login Page
              function(){
                nowin ='Display login';
                page.open(login_url, function(status){
                  page.injectJs('jquery-1.6.1.min.js');
                  if (status !== 'success')
                   {
                      console.log('FAIL to load the address');
                   }
                  else
                   {
                      page.evaluate(function(){
                       document.getElementById('email').value = "jhoch@subaru.com";
                       document.getElementById('login_form');
                      });
                   }
                });
              },
              ///  submit login 
              function(){
                nowin ='Submit login';
                page.evaluate(function(){
                  document.getElementById('login_form').submit();
                  return; 
                });
                
              },
              ///  Checking if welcome message is there. 
              function() {
                nowin ='Checking home page contents';
                // Output content of page to stdout after form has been submitted
                page.evaluate(function() {
                  console.log("Page Title " + document.title);                         
                  console.log("Your name is " + document.querySelector('div.tools h2').innerHTML); 
                  //console.log(document.querySelectorAll('html')[0].outerHTML);
                });
              },              

              //After sucessfull login go to dashboard page
              function(){
                nowin ='Opening dashboards page';
                  page.open(site_url,  function(status){
                      if (status !== 'success')
                       {
                          console.log('Unable to load the dashboard!');
                          phantom.exit();
                       }
                      else
                       {
                          console.log('Loading dashboard page....');
                          page.clipRect = {top:0, left:0, width:1024, height:1024};
                          page.render('dashboard.png');  
                       }
                  });
              },

             //collect all dashboard links
             function(){
              page.open(site_url,function(){
                    var results = page.evaluate(function(){
                      var list = document.querySelectorAll('a'), links = [], i;
                      for (i = 0; i < list.length; i++) {
                          links.push(list[i].innerText);
                      }
                      return links;
                    });
                  //console.log("****************Link found************************")
                  //console.log(results.join('\n'));
                  //console.log("****************Link found************************")
              });
            },

            //Clicks on report link
            function(){
              page.evaluate(function(){
                  var ev = document.createEvent("MouseEvents");
                  ev.initEvent("click", true, true);
                  document.querySelector(".reports a[href]").dispatchEvent(ev);
                  console.log("Clicked on Reports link")
              });
            },

            //After going to reports page capture reports page by making heading background white
            //Open report subscription form
            function(){
              page.evaluate(function (){
                var field = document.getElementById("reports");
                field.style.backgroundColor = "#fff";
                var ev = document.createEvent("MouseEvents");
                ev.initEvent("click", true, true);
                if (document.querySelector("#form_toggle").dispatchEvent(ev) == true)
                {
                  console.log("Clicked on custom report link")
                }
                else
                {
                  console.log("Failed to Clicked on custom reports link")
                }

              });
              page.clipRect = {top:0, left:0, width:1024, height:1024};
              page.render('report.png');
             
            },

            // function(){
            //     setInterval(function(){page.render('report.png');},100000);
                    
            //       },

            // Fill report subscription form with all required details  
            function(){
//                    page.render('report.png');
                    setInterval(function(){
                    page.evaluate(function (){
                      nowin ='Opening custom reports drop down..';
                      var ev = document.createEvent("MouseEvents");
                        console.log("Custom reports form opened ---")
                        ev.initEvent("click", true, true);
                        document.querySelector("#req_subscription").dispatchEvent(ev);
                        
                        document.getElementById('req_report_id').selectedIndex=1;
                        document.getElementById('req_device_guid').selectedIndex=1;
                        console.log("Clicked on subscription checkbox")
                        document.getElementById('req_frequency').selectedIndex=1;
                        document.getElementById('req_delivery_time').selectedIndex=1;
                        document.getElementById('req_time_zone').selectedIndex=1;
                    });
                    page.clipRect = {top:0, left:0, width:1024, height:1024};
                    page.render("custom_report_form_selection.png");
                  },100);
                
            },

            //Submit report subscription form
            function(){
              setTimeout(function (){
                  page.evaluate(function (){
                    var ev = document.createEvent("MouseEvents");
                    ev.initEvent("click", true, true);
                    document.querySelector("input[type=submit]").dispatchEvent(ev);
                    console.log("Submitting custom report form");
                  });
              }, 5000);
            },

            //After submitting form capture "wait for progress" div
            function(){
                    page.evaluate(function (){
                      document.getElementById('wait_progress').style.backgroundColor = "#ffffff";
                      console.log("Capturing custom reports page after filling data");
                    });
                    page.clipRect = {top:0, left:0, width:1024, height:1024};
                    page.render("custom_report_form.png");
            }, 

            //Open report subscription index page and capture the new subscribed report.
            function(){
              page.open(report_sub_url,function(){
                page.evaluate(function(){
                 page.clipRect = {top:0, left:0, width:1024, height:1024};
                 page.render("reports.png");
              });
             });
            }
        ];


interval = setInterval(function() {
  if (!loadInProgress && typeof steps[testindex] == "function") {
    console.log("step " + (testindex + 1));
    steps[testindex]();
    testindex++;
  }
  if (typeof steps[testindex] != "function") {
    console.log("test complete!");
    phantom.exit();
  }
}, 70);