require 'spec_helper'

describe EventSubscription do 
  it "has a valid factory" do
    FactoryGirl.create(:event_subscription).should be_valid
  end

  context "validations" do
  	subject {FactoryGirl.build(:event_subscription)}
  	it {should validate_presence_of(:name).with_message('cannot be empty') }
  	it {should validate_presence_of(:email).with_message('invalid format') }
  	(0..6).each do |val|
  		it {should allow_value(val).for(:severity)}  		
  	end
  	(7..9).each do |val|
  		it {should_not allow_value(val).for(:severity)}  		
  	end
    it 'should validate presence of asset_list_included' do 
      subject.all_asset = false
      should validate_presence_of(:asset_list).with_message('Cannot be empty') 
    end
  end

  it 'asset_list should be empty if all asset is selected' do
    eSub = FactoryGirl.create(:event_subscription, :all_asset => true)
    eSub.asset_list.should be_empty()    
  end

  it 'should strip off empty assets from asset_list' do
    eSub = FactoryGirl.create(:event_subscription, :asset_list => ["1","","3"])
    eSub.should have(2).asset_list    
  end

  context "scopes"  do
    before :each do
      @asset_list = (0..6).to_a
      @company = mock_model(Company).as_null_object
      @company.stub!(:asset_list).and_return(@asset_list)
      @eSub = FactoryGirl.create(:event_subscription, :all_asset => true)
      Company.stub!(:with_guid).with(@eSub.company_guid).and_return(@company)
    end

    it 'should load with all assets of the company if all_asset flag is true' do
      event = EventSubscription.load_event_subscription @eSub.id
      event.id.should eql(@eSub.id)
      event.should have(@asset_list.length).asset_list
      # TODO....  Don't know why these methods are not getting called at all...
      #Company.should_receive(:with_guid).with(@eSub.company_guid).and_return(@company)  
      #EventSubscription.should_receive(:load_all_assets_if_required).once
    end

  end

  def create_esubs(n=1)
  (0..n).each{ FactoryGirl.create}
  end
end



  