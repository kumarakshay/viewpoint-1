require 'spec_helper'

describe Role do 
  it "has a valid factory" do
    FactoryGirl.create(:role).should be_valid
  end
  context "validations" do
  	subject {FactoryGirl.build(:role)}
  	it {should validate_presence_of(:name).with_message('cannot be empty') }
    it {should validate_presence_of(:description).with_message('cannot be empty') }
  	it {should validate_uniqueness_of(:position) }
  end
  

  context "scopes" do 
  	before :each do
      Role.destroy_all
      FactoryGirl.create(:role, {:name => 'viewpoint_admin',:position => 1})     
      FactoryGirl.create(:role, {:name => 'sungard_admin',:position => 2})     
      FactoryGirl.create(:role, {:name => 'company_admin',:position => 3})     
    end

    it "should show active roles allowed to me" do      
      {'viewpoint_admin' => ['viewpoint_admin','sungard_admin','company_admin'],
       'sungard_admin' => ['sungard_admin','company_admin'],
       'company_admin' => ['company_admin']} .each do |k,v|
          names = Role.active_roles([] << k ).collect(&:name)
          names.should == (v)
       end
    end

    it 'should show allowed roles if combination roles present' do 
       {['viewpoint_admin','sungard_admin' ]=> ['viewpoint_admin','sungard_admin','company_admin'],
       ['sungard_admin','company_admin' ]=> ['sungard_admin','company_admin' ],
       ['company_admin'] => ['company_admin']} .each do |k,v|
          names = Role.active_roles(k).collect(&:name)          
          names.should == (v)          
       end
    end

    it 'should create new role with max position' do 
      r = FactoryGirl.create(:role) 
      r.position.should == Role.count
    end

  end

  
	
end