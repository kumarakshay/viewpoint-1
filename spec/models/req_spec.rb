require 'spec_helper'

describe Req do
  it "has a valid factory" do
    create(:req).should be_valid
  end
  describe "time conversions" do      
    context "displaying time" do
      it "returns a Time.now in local time" do
        @req1 = create(:req, time_zone: "London")
        time_in_zone = Time.now.utc.in_time_zone(@req1.time_zone)
        puts "time_in_london: #{time_in_zone}"
      end
      it "should do it" do
        # @subscription = create(:req, time_zone: "London", delivery_time: "01:00")
        @subscription = create(:req, time_zone: "Eastern Time (US & Canada)", delivery_time: "14:00")
        @zone = ActiveSupport::TimeZone[@subscription.time_zone]        
        delivery_time_string = "09/04/12 " + @subscription.delivery_time
        delivery_time = DateTime.strptime(delivery_time_string, "%m/%d/%y %H:%M") 
        utc_time = Time.parse(delivery_time.to_s).utc
        new_utc_time = utc_time - @zone.utc_offset
        puts "  delivery_time: #{@subscription.delivery_time}"  
        puts "zone.utc_offset: #{@zone.utc_offset}"  
        puts "        new_utc: #{new_utc_time}"
        puts "      time only: #{new_utc_time.strftime("%H:%M")}"
      end
    end    
  end
end

# ActiveSupport::TimeZone + Time.zone_offset
# http://stackoverflow.com/questions/9798245/how-to-get-correct-time-zone-offset-in-rails-2-3