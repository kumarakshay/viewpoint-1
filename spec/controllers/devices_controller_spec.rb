require 'spec_helper'

describe DevicesController do
  before(:each) do  	
    controller.stub!(:authenticated?).and_return(true)
	  controller.stub!(:current_user).and_return(mock_user)
    mock_user.stub!(:is_sungard?).and_return(false)
  end	
  
  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
  	it "renders unauthorized when not in acl" do
  	  mock_device = mock_model(Device).as_null_object
  	  controller.stub!(:view_device_details?).with("#{mock_device.id}").and_return(false)    
      get :show, :id => mock_device.id
      response.should render_template("shared/unauthorized")
  	end
  end

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
    @mock_user.stub!(:asset_list).and_return([])
    @mock_user
  end
end