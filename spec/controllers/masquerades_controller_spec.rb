require 'spec_helper'

describe MasqueradesController do
  
  it "should use MasquerdesController" do
    controller.should be_an_instance_of(MasqueradesController)
  end
  
  it "should require authentication to access" do
      get 'index'
      response.should redirect_to new_oid_path
  end

  describe "GET 'index'" do
    before :each do
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      get :index
    end

    it "renders the :index view" do
      response.should render_template :index
    end

    it "shoud return all companies if search parameter blank?" do
      @company = [mock_model(Company), mock_model(Company)]
      controller.stub!(:authenticated?).and_return(true)
      Company.should_receive(:all).with(:limit => 50).and_return @company
      get :index, :search =>{}
    end

    it "should return searched related company" do
      @company = [mock_model(Company), mock_model(Company)]
      controller.stub!(:authenticated?).and_return(true)
      Company.should_receive(:where).with(:name =>/sungard/i).and_return(Company)
      Company.should_receive(:all).and_return @company
      get :index, :search =>'sungard'
    end
  end

  describe "GET show" do
    before :each do      
      controller.stub!(:authenticated?).and_return(true)
      Company.stub!(:where).and_return(mock_company)
      mock_company.stub_chain(:first, :name) {}
      User.should_receive(:where).and_return User
      User.should_receive(:sort).and_return User
      User.should_receive(:all).and_return true
      get :show
    end    
    it { response.should be_success }
    it { response.should render_template('show') }
  end


  def mock_company
    @mock_company ||= mock_model(Company).as_null_object
  end

   def mock_user
    @mock_user ||= mock_model(User).as_null_object
   end
end
