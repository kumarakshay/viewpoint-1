require 'spec_helper'

describe ReqsController do

  it "should use ReqsController" do
    controller.should be_an_instance_of(ReqsController)
  end
  
  it "should require authentication to access" do
      get 'index'
      response.should redirect_to new_oid_path
  end

  before :each do
  	@group =[mock_model(Group), mock_model(Group)]
  end

  describe "GET 'index'" do
    before :each do
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      Req.should_receive(:where).with(:user_guid => mock_user.guid, :subscription =>true).and_return(Req)
      Group.should_receive(:dat_for_dropdown).with(mock_user.company_guid).and_return(@group)
      Device.should_receive(:dat_for_dropdown).with(mock_user.asset_list).and_return(true)
      Device.should_receive(:dropdown_by_device_class).with(mock_user.asset_list, "SysEdge").and_return(true)
      Device.should_receive(:dropdown_by_device_class).with(mock_user.asset_list, "Server").and_return(true)
      Power.should_receive(:find_distinct_area).with(mock_user.company_guid).and_return(true)
      Power.should_receive(:find_distinct_cabinet).with(mock_user.company_guid).and_return(true)
	  get :index
    end
        
    it "renders the :index view" do
      response.should render_template :index
    end
  end

  describe "GET 'show'" do
    before :each do  
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user) 
      Req.should_receive(:find).with(mock_req.id.to_s).and_return(mock_req)
      Audit.should_receive(:create).and_return(true)
      Adhoc.new.should_receive(:gen_ehealth_link).with(mock_req).and_return(mock_req)
      Adhoc.new.should_receive(:get_ehealth_report).with("", mock_req.id.to_s)
    end   

    it "should sucessfully returns file"  do
      pending do
        get :show, :id => mock_req.id
      end
    end
  end

  describe "POST create" do

    before :each do
      controller.stub!(:authenticated?).and_return(true)
      Req.delete_all
    end

    context "with valid attributes" do
      it "creates a new req" do
        expect{
          post :create, req: FactoryGirl.attributes_for(:req)
        }.to change(Req,:count).by(1)
      end

      it "redirects to the new req" do
        post :create, req: FactoryGirl.attributes_for(:req)
        response.should redirect_to Req.last
      end
    end

    context "with blank attributes not save then  " do
      it "should render new" do
        Req.should_receive(:new).and_return(mock_req)
        mock_req.stub(:save).and_return(false)
        post :create, req: {}
        response.should render_template :new
      end
    end 
  end


  describe "PUT Reqs/:id" do

    before(:each) do
      controller.stub!(:authenticated?).and_return(true)
    end

    context "with valid params" do
      before(:each) do
        @req = mock_model(Req, :update_attributes => true)
        Req.stub!(:find).with("1").and_return(@req)
      end
      
      it "should find req and return object" do
        Req.should_receive(:find).with("1").and_return(@req)
        put :update, :req =>{}, :id => 1
      end
      
      it "should update the req object's attributes" do
        @req.should_receive(:update_attributes).and_return(true)
        put :update, :req =>{}, :id => 1
      end
      
      it "should redirect to the req's show page" do
        put :update, :req =>{}, :id => 1
        response.should redirect_to(req_url(@req))
      end
    end

    describe "with invalid params" do
      before(:each) do
        @req = mock_model(Req, :update_attributes => false)
        Req.stub!(:find).with("1").and_return(@req)
      end

      it "should find Req and return object" do
        Req.should_receive(:find).with("1").and_return(@req)
        put :update, :req =>{}, :id => 1 
      end

      it "should update the Req object's attributes" do
        @req.should_receive(:update_attributes).and_return(false)
        put :update, :req =>{}, :id => 1 
      end

      it "should render the edit form" do
        put :update, :req =>{}, :id => 1 
        response.should render_template('edit')
      end
    end
  end

  describe "DELETE destroy" do
    before :each do
      controller.stub!(:authenticated?).and_return(true)
    end

    it "destroys the requested req" do
      Req.stub(:find).with("37") { mock_req }
      mock_req.should_receive(:destroy)
      delete :destroy, :id => "37"
    end

    it "redirects to the req list" do
      Req.stub(:find) { mock_req }
      delete :destroy, :id => "1"
      response.should redirect_to(reqs_url)
    end
  end

  def mock_user
     @mock_user ||= mock_model(User).as_null_object
  end  

  def mock_req
    @mock_req ||= mock_model(Req).as_null_object
  end

end
