require 'spec_helper'

describe GroupsController do

  it "should use GroupsController" do
    controller.should be_an_instance_of(GroupsController)
  end

  before :each do
    controller.stub!(:authenticated?).and_return(true)
    controller.stub!(:current_user).and_return(mock_user)
  end


  describe "GET #index" do
    before :each do
      @groups = [mock_model(Group),mock_model(Group)]
      Group.should_receive(:all).with(:company_guid => mock_user.company_guid).and_return @groups
      get :index
    end

    it "populates an array of group" do      
      assigns[:groups].should == @groups 
    end
    
    it "renders the :index view" do
      response.should render_template :index
    end
  end  

  describe "GET show" do
    before :each do      
      Group.stub!(:find).with("#{mock_group.id}").and_return(mock_group)    
      get :show, :id => mock_group.id
    end    
    it { response.should be_success}
    it { response.should render_template('show') }
  end

  describe "POST create" do

    context "with valid attributes" do
      it "creates a new group" do
        expect{
          post :create, group: FactoryGirl.attributes_for(:group)
        }.to change(Group,:count).by(1)
      end

      it "redirects to the new group" do
        Group.delete_all
        post :create, group: FactoryGirl.attributes_for(:group)
        response.should redirect_to Group.last
      end
    end

    context "with invalid attributes" do
      it "does not save the new group" do
        [{},{name:"abc"},{asset_list:[1,2,3]}].each do |params|
          expect{
            post :create, group: {}
          }.to_not change(Group,:count)
        end
      end
      
      it "re-renders the new method" do
        post :create, group: {}
        response.should render_template :new
      end
    end 
  end

  describe "PUT groups/:id" do

    before :each do 
      Group.stub!(:find).with("#{mock_group.id}").and_return(mock_group)
    end

    describe "with valid params" do
      before(:each) do
        mock_group.stub!(:update_attributes).and_return(true)
      end
        
      it "should find group and return object" do
        Group.should_receive(:find).with("#{mock_group.id}").and_return(mock_group)
        put :update, :id => mock_group.id, :group => {}
      end

      it "should redirect to show page after update" do
        put :update, :id => mock_group.id, :group => {}
        response.should redirect_to(group_url(mock_group))
      end
    end
    
    describe "with invalid params" do
      before(:each) do
        mock_group.stub!(:update_attributes).and_return(false)
      end

      it "should render the edit form" do
        put :update, :id => mock_group.id, :group => {}
        response.should render_template('edit')
      end

      it "should call update the group object's attributes" do
        mock_group.should_receive(:update_attributes).and_return(false)
        put :update, :id => mock_group.id, :group => {}
      end

      
#      it "should have a flash notice" do
#        put :update, :id => mock_group.id, :group => {}
#        flash[:notice].should_not be_blank
#      end
    end
  end

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_group
    @mock_group ||= mock_model(Group).as_null_object
  end

end

