require 'spec_helper'
require './lib/service_now_client'

shared_examples "a company" do
  describe "Assets Summary for a Company", :type => :request do
    it "returns JSON for company asset summary'" do
      snc = ServiceNowClient.new
      snc.company_asset_summary(@company_guid)
      assert_kind_of ServiceNowClient, snc
    end
  end

  # shared_examples "a company" do
  #   describe "Tickets Summary for a Company", :type => :request do
  #     it "returns JSON for company ticket summary'" do
  #       snc = ServiceNowClient.new
  #       snc.company_ticket_summary(@company_guid)
  #       assert_kind_of ServiceNowClient, snc
  #     end
  #   end

  # shared_examples "a company" do
  #   describe "Change Tickets for Company", :type => :request do
  #     it "returns JSON for company_change_request'" do
  #       snc = ServiceNowClient.new
  #       snc.company_change_request(@company_guid)
  #       assert_kind_of ServiceNowClient, snc
  #     end
  #   end

  # describe "Incidents for Company", :type => :request do
  #   it "returns JSON for change tickets" do
  #     get "/api/companies/#{@company_guid}/change_tickets"
  #   end
  # end

  # describe "MX Windows for Company", :type => :request do
  #   it "returns MX Windows" do
  #     get "/api/companies/#{@company_guid}/mx_windows"
  #   end
  # end
end


describe "Subaru" do
  it_behaves_like "a company"

  before do
    @company_guid = 'D9EFA335-F8E5-4271-9541-BB5D7590AEE5'
  end

end
