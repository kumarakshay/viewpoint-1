FactoryGirl.define do
  factory :group do
    name { Faker::Name.first_name }
    company_guid { Faker::Name.last_name }
    asset_list ["1","2"]
  end
end