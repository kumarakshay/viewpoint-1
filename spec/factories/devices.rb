FactoryGirl.define do
  factory :device do
    name { Faker::Name.first_name }
    ip_address_formatted { Faker::Name.last_name }
    asset_guid { Faker::Name.first_name }
  end
end