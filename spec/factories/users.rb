FactoryGirl.define do
  factory :user do
    guid 'D1433C17-4864-463C-80E0-AE387871F308'
    company_guid 'E71312DC-AAF1-4BF2-941D-B297B1D326AB'
    company_name 'SUNGARD Availability Services - UK'
    portal_name 'sasuk'
    app_settings 'nil'
    active true
    vp_admin false
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { "#{first_name}.#{last_name}@example.com".downcase }
    phone_number {Faker::PhoneNumber.phone_number}
  end
end

# <User _id: BSON::ObjectId('4f7c4214c99aef7849006be5'), 
# active: true, app_settings: {}, company_guid: "E71312DC-AAF1-4BF2-941D-B297B1D326AB", 
# company_name: "SUNGARD Availability Services - UK", created_at: Wed, 04 Apr 2012 12:44:03 UTC +00:00, 
# email: "ryan.bell@sungard.com", 
# guid: "D1433C17-4864-463C-80E0-AE387871F308", 
# portal_name: "sasuk", updated_at: Wed, 04 Apr 2012 12:44:03 UTC +00:00>