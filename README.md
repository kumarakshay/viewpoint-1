## Viewpoint

# But first, why?

* Deep analytic view into your devices including Network, Servers, and Storage Area Network.
* Real-time and historical event viewer of device activity.
* Performance Data views including real-time and historical data.
* Customizable Dashboard for individual users to build a view of selectable widgets.

## Testing our project

For this project, we're going to use RSpec to test our gem. We write tests to ensure that everything goes according to plan and to prevent future-us from 
building a time machine to come back and kick our asses. 

## Deploying a branch to staging

    cap staging deploy:update

## Bump    
