import React from 'react'
import ReactDOM from 'react-dom'
require(['ot-react-components', 'api'], function (OtReactComponent, Api) {

const UnifiedHeader = OtReactComponent.Header;
const Footer = OtReactComponent.Footer;
let lastModifiedTS = '';
let finalUrl = '';
let getNotificationApi = '/api/reqs?job_status=finished&acknowledged=false&email_me=false';
let getLatestNotificationApi = '/api/reqs?job_status=finished&acknowledged=false&email_me=false&__filter[created_after]=';
var id_requests = {};
let branding= '';

class HeaderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.getUser();
    this.getTool();
    this.getPortalLinks();
    this.state = {
      assetResults: null,
      groupResults: null,
      filteredCompanies: {},
      sendCompanyUsers: {},
      user: {},
      tools: null,
      portalLinks:{},
      notifications: [],
      notificationCount: 0,
      ticketingBaseUrl: '',
      isOnePortal: true
    };
  }

  componentWillMount() {
    if (lastModifiedTS) {
      finalUrl = getLatestNotificationApi + lastModifiedTS;
    }
    else {
      finalUrl = getNotificationApi;
    }
    this.getNotification();
  }

  componentDidMount() {
    // Show impersonate blue bar when header component is mounted 
    document.getElementById('tools-bar').style.visibility = "visible";
  }

  reload_url(url)
  {
     if(window.location.pathname +window.location.hash == url){
        window.location.reload()
      }
      else{
      window.location.href = url;
      }
  }

  fetch(resultVar, defaultValue, url, method, data, options) {
    var self = this;
    const p = Api.sendRequest(url, method, data, options);
    p.then(function success(response) {
      self.setState({
        [resultVar]: response.result,
      });
    }, function error() {
      self.setState({
        [resultVar]: defaultValue,
      });
    });
  }

  getChildCompanies(){
    const self = this;
    const url = '/api/portal/switch_companies';
    const p = Api.sendRequest(url);
    p.then(function success(response) {
      self.setState({
        filteredCompanies: response.result,
      });
    });
  }

  getUser(){
    var self = this;
    const url = '/api/current_users';
    const p = Api.sendRequest(url);
      p.then(function success(response) {
        if (response.result.current_user) {
          self.setState({user: response.result});
          if (response.result.session.company_has_childs && response.result.current_user.vp_roles.switch_company){
            self.getChildCompanies();
          }
        }
      }, function error() {
         self.setState({
            user: {},
          });
      });
  }

  getNotification() {
    const self = this;
    var remoteReq;
    const results = Api.sendRequest(finalUrl);
    results.then(function serverResponse(res) {
      if (res) {
        if(typeof(res.result) === 'number'){
          this.setState({ notifications: [] });
          this.updateCount(0);
      }
      else{
        this.setState({ notifications: res.result });
        res.result.sort(function sortNoti(a, b) { return b.created_at - a.created_at });
        this.updateCount(res.result.length);
        lastModifiedTS = res.result[res.result.length - 1].created_at;
      }
    }
    }.bind(this));
  }

  updateCount(count){
    this.setState({notificationCount: count});
  }

  updateNotification(jobID) {
    const self = this;
    const remoteReq = Api.sendRequest('/api/reqs/ack?job_id=' + jobID, 'PUT');
    remoteReq.then(function serverResponse(res) {
      if (res.status === 200) {
        return({ success: true });
      } else {
        return({ success: false });
      }
    });
  }

  getTool(){
    var self = this;
    const url = '/api/portal/tool_selectors';
    this.fetch("tools", {}, url);
  }

  getPortalLinks() {
    var self = this;
    const url = '/api/portal/portal_selectors';
    this.fetch("portalLinks", {}, url);
  }

  getSearchData(str) {
    this.setState({
      groupResults: null,
      assetResults: null,
    });
    this.getAssetRecords(str);
    this.getGroupRecords(str);
  }

  getAssetRecords(str) {
    const url = '/api/portal/search/device?query='+ str;
    this.fetch("assetResults", [], url, undefined, undefined, {
      id: 'asset_search',
    });
  }

  getGroupRecords(str) {
    const url =  '/api/portal/search/group?query='+ str;
    this.fetch("groupResults", [], url, undefined, undefined, {
      id: 'group_search',
    });
  }

  getImpersonateDetails(name, id){
    const self = this;
    const url =  '/api/portal/masquerades/new/'+ id;
    const p = Api.sendRequest(url);
     p.then(function serverResponse(response) {
      self.reload_url("/my_board")
    });
  }

  impersonateCompanySearch(companyName) {
    const self = this;
    const url =  '/api/portal/masquerades?search='+ companyName;
    this.fetch("filteredCompanies", {}, url, undefined, undefined, {
      id: 'company_search',
    });
  }

  saveUserProfileDetails(user_id, dataObj, cb) {
     // this function will update the user information.
    const self = this;
    const url = '/api/portal/users/' + user_id;
    const remoteReq = Api.sendRequest(url,'PUT',JSON.stringify(dataObj));
    remoteReq.then(function serverResponse(res) {      
      if (cb && typeof cb === 'function') {
        cb();
        self.getUser();
      }
    }, function error() {
       if (cb && typeof cb === 'function') {
          cb(true);
        }  
    });
  }

  impersonateUsersSearch(CompanyGuid) {
    const self = this;
    const url =  '/api/portal/masquerades/' + CompanyGuid;
    this.fetch("sendCompanyUsers", {}, url, undefined, undefined, {
      id: 'user_search',
    });
  }

  unmasquerade(){
    const self = this;
    const remoteReq = Api.sendRequest('/api/portal/unmasquerade');
    remoteReq.then(function serverResponse(response) {
      self.reload_url("/my_board")
    });
  }

  changePassword(cb) {
    const url = this.state.ticketingBaseUrl + '/api/portal/forgot_password/'
                + this.state.user.current_user.guid;
    const p = Api.sendRequest(url);
      p.then(function success(response) {
      if (response.errors) {
        if (cb && typeof cb === 'function') {
          cb(true);
        }
      }
      else {
        if (cb && typeof cb === 'function') {
          cb();
        }
      }
    }, function error() {
        if (cb && typeof cb === 'function') {
          cb(true);
        }  
      });
  }

  company_masquerade(guid){
    const self = this;
    const url =  '/api/portal/switch_company/new/'+ guid;
    const p = Api.sendRequest(url);
    p.then(function success(response) {
      window.location.reload(true);
    }, function error() {
      
    });
  }

  company_unmasquerade(){
    const self = this;
    let url;
    let redirect_url;
    if(this.state.user.company_masquerade)
    {
      url =  '/api/portal/home_company'
    }
    else{
      url =  '/api/portal/unmasquerade';
      redirect_url = "/my_board";
    }
    const p = Api.sendRequest(url);
    p.then(function success(response) {
      if(redirect_url){
        self.reload_url(redirect_url)
      }else{
        window.location.reload()
      }
    }, function error() {
      
    });
  }

  render() {
    return (
      <div className="otrc">
        <UnifiedHeader
          user={this.state.user}
          changePassword = {this.changePassword.bind(this)}
          portalLinks = {this.state.portalLinks}
          toolsLinks = {this.state.tools}
          getImpersonateDetails = {this.getImpersonateDetails.bind(this)}
          companyList = {this.state.filteredCompanies}
          userList = {this.state.sendCompanyUsers}
          unmasquerade = {this.unmasquerade.bind(this)}
          impersonateCompanySearch ={this.impersonateCompanySearch.bind(this)}
          impersonateUsersSearch ={this.impersonateUsersSearch.bind(this)}
          updateNotification = {this.updateNotification.bind(this)}
          notiCount = {this.state.notificationCount}
          getNotification = {this.getNotification.bind(this)}
          updateCount = {this.updateCount.bind(this)}
          notiRecords = {this.state.notifications}
          getData={this.getSearchData.bind(this)}
          groupResults={this.state.groupResults} 
          assetResults={this.state.assetResults}
          ticketingBaseUrl = {this.state.ticketingBaseUrl}          
          infoConfig = {infoConfig}
          saveUserProfileDetails = {this.saveUserProfileDetails.bind(this)}
          company_masquerade={this.company_masquerade.bind(this)}
          isOnePortal={this.state.isOnePortal}
          company_unmasquerade = {this.company_unmasquerade.bind(this)}  />
        </div>
    )
  }
}

ReactDOM.render(
  <HeaderContainer />,
  document.getElementById('u_menu')
);

class FooterContainer extends React.Component {
  render() {
    return (
      <div>
        <Footer />
      </div>
    )
  }
  
}

ReactDOM.render(
  <FooterContainer branding="{response.result.branding}" />, document.getElementById('u_footer')
)

});
