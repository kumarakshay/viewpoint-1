Viewpoint::Application.routes.draw do



  ############################# VP5 Proxy Routing ###################################
  #Common Portal Landing page internal and external users proxy through home controller
  match '/my_board', to: 'welcome#landing_page', :as => :my_board, via: [:get]
  match '/my_tutorials', to: 'welcome#tutorials_page', :as => :my_tutorials, via: [:get]
  #For DNS-IPAM release(6.15.0) we will remove old DNS link and always redirect user to new DNS/IPAM pages
  # match "/dns" => redirect("/ui/index.html#/dns"), :as => :dns
  match '/dns', to: 'welcome#dns_page', :as => :vp_dns, via: [:get]
  # redirect old vpp4 reports to new vp5 reports
  # match "/reports" => redirect("/ui/index.html#/reports"), :as => :reports
  match "/reports", to: 'welcome#reports_page', :as => :reports, via: [:get]
  match "/restricted_contracts", to: 'welcome#restricted_contracts', :as => :restricted_contracts, via: [:get]
  match "/restricted_invoices", to: 'welcome#restricted_invoices', :as => :restricted_invoices, via: [:get]
  match "/ipam", to: 'welcome#ipam', :as => :ipam, via: [:get]
  match "/patching_schedule", to: 'welcome#patching_schedule', :as => :patching_schedule, via: [:get]
  match "/patching_policy", to: 'welcome#patching_policy', :as => :patching_policy, via: [:get]
  match "/patching_servers", to: 'welcome#patching_servers', :as => :patching_servers, via: [:get]
  match "/sg_cloud_portal", to: 'welcome#sg_cloud_portal', :as => :sg_cloud_portal, via: [:get]
  match "/vp_firewalls", to: 'welcome#vp_firewalls', :as => :vp_firewalls, via: [:get]
  match "/ipv4", to: 'welcome#ipv4', :as => :ipv4, via: [:get]
  match "/ipv6", to: 'welcome#ipv6', :as => :ipv6, via: [:get]
  match "/company_dns_audits", to: 'welcome#dns_audits', :as => :dns_audits, via: [:get]
  match "/hpc_admin", to: 'welcome#hpc_admin', :as => :hpc_admin, via: [:get]
  match "/command_runner/:asset_guid", to: 'welcome#command_runner', :as => :command_runner, via: [:get]
  match "/osappdashboard", to: 'welcome#osappdashboard', :as => :osappdashboard, via: [:get]
  match "/pyxie_reports/:folder", to: 'welcome#pyxie_reports', :as => :pyxie_reports, via: [:get]

  resources :interfaces
  resources :reports do
    collection do
      get '/notifications' => 'reports#notifications'
    end
  end
  resources :searches
  resources :events
  resources :histories
  resources :groups
  resources :masquerades
  resources :company_masquerades
  resources :event_subscriptions
  resources :helps
  resources :graphs
  resources :dashboards
  resources :ticketing
  resource :oid

  resources :reqs do
    collection do
      get 'group'
      get 'device_sysedge'
      get 'device_server'
      get 'area'
      get 'cabinet'
    end
  end

  match 'auth/:provider/callback', to: 'oids#create', via: [:get, :post]
  match 'auth/failure', to: 'sessions#failure', via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  # match 'auth/viewpoint', to: 'oids#create', via: [:get, :post]

  scope path: '/api', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1', constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true)  do

      # Below are the test api routes for testing performance
      get '/ehealth/test_ticketing_incidents' => "ehealth#test_ticketing_incidents"
      get '/ehealth/test_ticketing_requests' => "ehealth#test_ticketing_requests"
      get '/ehealth/test_ticketing_change_requests' => "ehealth#test_ticketing_change_requests"
      get '/ehealth/test_ticketing_maintenance' => "ehealth#test_ticketing_maintenance"
      get '/ehealth/test_events' => "ehealth#test_events"
      get '/ehealth/:asset_guid/test_active_asset_status' => "ehealth#test_active_asset_status"
      get '/ehealth/:asset_guid/test_active_asset_incidents' => "ehealth#test_active_asset_incidents"
      get '/ehealth/:asset_guid/test_active_asset_req_changes' => "ehealth#test_active_asset_req_changes"
      get '/ehealth/:asset_guid/test_active_asset_mx_windows' => "ehealth#test_active_asset_mx_windows"
      get '/ehealth/:company_guid/test_active_assets' => "ehealth#test_active_assets"

      get '/ehealth/events' => "ehealth#events"
      get '/ehealth/backups' => "ehealth#backups"
      get '/ehealth/backups_for_month' => "ehealth#backups_for_month"
      get '/ehealth/backup_months' => "ehealth#backup_months"
      get '/ehealth/environment_healths' => "ehealth#environment_healths"
      get '/ehealth/groups_environment_health' => "ehealth#groups_environment_health"
      get '/ehealth/asset_types' => "ehealth#asset_types"
      get '/ehealth/:asset_type/active_assets_by_type' => "ehealth#active_assets_by_type"
      get '/ehealth/active_assets' => "ehealth#active_assets"
      get '/ehealth/:group_id/group_active_assets' => "ehealth#group_active_assets"
      get '/ehealth/ticketings' => "ehealth#ticketings"
      get "/group_glances", to: "group_glances#index"
      get "/vp/devices", to: "generic_devices#index"
      get "/recent_assets", to: "recent_assets#index"

      get '/ticket_landing/ticket_stats' => "ticket_landing#ticket_stats"
      get '/ticket_landing/asset_health' => "ticket_landing#asset_health"
      get '/ticket_landing/recent_assets' => "ticket_landing#recent_assets"
      get '/ticket_landing/tickets' => "ticket_landing#tickets"
      get '/ticket_landing/asset_health' => "ticket_landing#asset_health"
      get '/ticket_landing/groups_health' => "ticket_landing#groups_health"
      get '/ticket_landing/sn_user_roles' => "ticket_landing#sn_user_roles"

      get "/device_command/:asset_guid", to: "commands#device_command"
      get "/device_search", to: "searches#device_search"

      get "/device_command/:asset_guid", to: "commands#device_command"
      get '/device_objects/:asset_guid', to: 'commands#device_objects'
      get '/device_object_groups/:asset_guid', to: 'commands#device_object_groups'
      get '/device_groups/:asset_guid', to: 'commands#device_groups'
      get '/device_services/:asset_guid', to: 'commands#device_services'
      get "/delete_user",to: "vp_users#delete_user"
      get "/device_search", to: "searches#device_search"

      get "/device_reports", to: "device_reports#index"
      get "/network_interface_subscriptions", to: "device_reports#network_interface"
      get "/device_availability_subscriptions", to: "device_reports#device_availability"

      get "firewalls/archive", to: "firewalls#archive"
      get "/vp_devices/by_ip", to: "vp_devices#by_ip"

      resources :company_subscriptions
      resources :device_subscriptions
      resources :internet_usages
      resources :groups
      resources :devices
      resources :device_glances
      resources :group_availabilities
      resources :power_reports
      resources :event_subscriptions
      resources :msa_subscriptions do
        collection do
          get 'company_device_availability_pdf'
          get 'company_device_availability_csv'
          get 'company_network_availability_pdf'
          get 'company_network_availability_csv'
          get 'company_power_availability_pdf'
          get 'company_power_availability_csv'
          get 'company_availability_pdf'
          get 'network_path_performance_pdf'
          get 'company_availability_csv'
          get 'group_availability'
          get 'network_path_performance_csv'
        end
      end
      resources :openid, constraints: {id: /.+/} # allow for email as param
      resources :vp_devices
      resources :vp_interfaces
      resources :current_users
      resources :foos
      resources :bars
      resources :reqs, only: [:index,:create,:destroy] do
        collection do
          put 'ack'
          get 'subscriptions'
        end
      end
      resources :statements do
        collection do
          get 'groups'
          get 'device_sysedge'
          get 'device_server'
          get 'areas'
          get 'cabinets'
        end
      end

      resources :event_subscriptions do
        collection do
          match '/event_subscriptions' => 'event_subscriptions#index', via: [:get]
        end
      end

      resources :locations do
        collection do
          match '/locations' => 'locations#index', via: [:get, :post]
        end
      end

      get "/vp/devices", to: "generic_devices#index"

      ########## Viewpoint API for Unified Portal ##################
      namespace :portal do
        get "/masquerades",to: "masquerades#index"
        get "/masquerades/:company_guid", to: "masquerades#show"
        get "/masquerades/new/:id",to: "masquerades#new"
        get "/unmasquerade", to: "masquerades#destroy"

        get "/switch_companies",to: "company_masquerades#index"
        get "/switch_company/new/:id",to: "company_masquerades#new"
        get '/home_company', to: 'company_masquerades#destroy', as: 'home_company'
        get '/search/:collection', to: "search#search"
        get '/tool_selectors', to: "tool_selectors#index"
        get '/portal_selectors', to: "portal_selectors#index"
        get '/quick_links', to: "tool_selectors#quick_links"

        get "forgot_password/:user_guid", to: "users#forgot_password"
        get "country_codes", to: "users#country_codes"
        get "timezones", to: "users#timezones"
        get "sn_permissions", to: "users#sn_permissions"

        post 'request_logger',to: "request_logger#index"

        scope '/mock' do
          get 'vp_tools_selector', to: 'mock#vp_tools_selector'
          get 'raas_tools_selector', to: 'mock#raas_tools_selector'
          get 'get_info_html', to: 'mock#get_info_html'
          get 'help_sections', to: 'mock#help_sections'
          get 'get_compliance_status', to: 'mock#compliance_status'
          get 'get_capacity_status', to: 'mock#capacity_status'
        end

        #resources :users
        resources :users, only: [:update]
      end
      resources :audits do
        collection do
          get 'login_statistic'
        end
      end
      ########## END Viewpoint API for Unified Portal ##################
      # original calls
      # match 'event_histories'         => 'event_histories#index', via: [:post]
      # match 'event_histories_company' => 'event_histories_company#index', via: [:post]
      # match 'events'                  => 'events#index', via: [:post]
      # match 'events_company'          => 'events_company#index', via: [:post]
      # match 'event_logs/:event_id'    => 'event_logs#index', via: [:get]
    end
    scope :module => 'api/v1/obr_reporting' , constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true) do
      resources :statement_obr, only: [:index]
      resources :req_obr
    end
  end

  #This route is created for AWS lambda to post the report created notification to viewpoint
  #Parameter {filename,job_id}
  scope path: '/public', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1/public', constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true)  do
      post '/process_lambda',to: "req_lambdas#process_report"
    end
  end

  scope path: '/ticketingService', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1/portal', constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true)  do

      get '/companies/:company_guid/tasks', to: 'ticketing#companies_tasks'
      get '/companies/:company_guid/tasks/search/', to: 'ticketing#companies_tasks_search'
      get '/companies/:company_guid/tasks/:number', to: 'ticketing#companies_tasks_details'

      get '/cti/services', to: 'ticketing#cti_service'
      get '/cti/prompts', to: 'ticketing#cti_prompts'
      get '/cti/services/:service_id/prompts', to: 'ticketing#cti_prompt'
      get '/cti/assignmentGroup', to: 'ticketing#cti_assignment_group'

      get '/lookup/location/search', to: 'ticketing#lookup_location_search'
      get '/lookup/company/search', to: 'ticketing#lookup_company_search'
      get '/lookup/assignmentGroup/search', to: 'ticketing#lookup_assignment_group_search'
      get '/lookup/ci/search', to: 'ticketing#lookup_ci_search'
      get '/lookup/user/search', to: 'ticketing#lookup_user_search'

      get '/tasks/:ticket_sys_id/comments', to: 'ticketing#ticketing_comments'
      put '/tasks/:ticket_sys_id', to: 'ticketing#update_task'
      post '/task', to: 'ticketing#task'

      get '/ticket_landing/:company_guid/tickets', to: 'ticket_landing#tickets'
      get '/ticket_landing/:company_guid/ticket_stats', to: 'ticket_landing#ticket_stats'
    end
  end

  scope path: '/', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1/omi' , constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true) do
      get 'api/omi/companies', to: 'companies#index'
      get 'api/omi/companies/:id', to: 'companies#show'
      get 'api/omi/nodes', to: 'nodes#index'
      get 'api/omi/nodes/:id', to: 'nodes#show'
      get '/cmdb/node/:asset_guid', to: 'omi#node'
      get '/cmdb/node/:asset_guid/relates', to: 'omi#node_related'
      get '/cmdb/node', to: 'omi#nodes'
      get '/cmdb/interface', to: 'omi#interfaces'
      get '/cmdb/tql/:asset_guid', to: 'omi#tql'
      get '/company', to: 'omi#companies'
      get '/company/relations', to: 'omi#company_relations'
      get '/company/:company_guid', to: 'omi#company'
      get '/company/:company_guid/relations', to: 'omi#company_relation'
      get 'monitoring/events/:id' , to: 'omi#event'
      get 'monitoring/events/customer/:company_guid' , to: 'omi#company_events'
      get 'monitoring/events/customer/:company_guid/counts' , to: 'omi#company_events_count'
      get 'monitoring/events' , to: 'omi#events'
      get 'monitoring/perfdata/:device_guid', to: 'omi_reporting#perfdata'
      get 'monitoring/perfdata/interface/:sg_nnmi_interface_uuid', to: 'omi_reporting#interface_graph'
      
      get 'cmdb/rosetta', to: 'omi_reporting#rosetta'
      get 'device_graph/:device_guid', to: 'omi_reporting#graph_data'
      get 'network_device_graph/:device_guid', to: 'omi_reporting#network_graph_data'
      get 'api/device_graph/metrics', to: 'omi_reporting#graph_metrics'
      get 'osappdashboard/AvgCpuMemoryUtilization',to: 'dashboard#monthly_average_cpu_memory'
      get 'osappdashboard/AvgFileSystemUtilization',to: 'dashboard#monthly_used_file_system'
      get 'osappdashboard/maxUtilizationCpu',to: 'dashboard#max_utilization_cpu'
      get 'osappdashboard/maxUtilizationMemory',to: 'dashboard#max_utilization_memory'
      get 'osappdashboard/maxUtilizationFileSystem',to: 'dashboard#max_utilization_file_system'
      get 'osappdashboard/baseDataCpuMemory',to: 'dashboard#base_data_cpu_memory'
      post 'hpc/processWorkflows',to: 'hpc#process_workflows'

    end
    scope :module => 'api/v1/patch_management', constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true) do
      get '/patchManagement/OS', to: 'operating_systems#os_list'
      get '/patchManagement/getServerId', to: 'servers#server_id_by_name'
      get '/patchManagement/schedule/byCust', to: 'schedules#customer_schedules'
      get '/patchManagement/policy/OS/:os_id', to: 'policies#policies_by_os', constraints: { os_id: /[^\/]+/ }
      get '/patchManagement/server/byOsIdCustomerName', to: 'schedules#add_to_patch'
      post '/patchManagement/schedule/add', to: 'schedules#add_schedule'
      post '/patchManagement/validateKB', to: 'schedules#validate_kb'
      get '/patchManagement/scheduleServer/byScheduleId', to: 'schedules#update_schedule_servers'
      get '/patchManagement/scheduleServer/byScheduleCustOS', to: 'schedules#add_schedule_servers'
      put '/patchManagement/scheduleServer/update', to: 'schedules#update_schedule'
      put '/patchManagement/schedule/delete', to: 'schedules#delete_schedule'
      get '/patchManagement/schedule/scheduleStatus', to: 'schedules#schedule_status'
      get '/patchManagement/schedule/exists', to: 'schedules#schedule_name_exist'
      get '/patchManagement/changeTicket/schedule-count', to: 'schedules#schedule_count'
      get '/patchManagement/changeTicket/details', to: 'schedules#details'
      get '/patchManagement/server/byChangeNumber', to: 'schedules#byChangeNumber'
      get '/patchManagement/wsusPolicy', to: 'policies#customer_policies'
      post '/patchManagement/wsusPolicy/add', to: 'policies#add_policy'
      put '/patchManagement/wsusPolicy/delete', to: 'policies#delete_policy'
      put '/patchManagement/wsusPolicy/update', to: 'policies#update_policy'
      get '/patchManagement/wsusruletype/custOsId', to: 'policies#selection_rule'
      get '/patchManagement/server/createPolicy/addPolicyServerToPatch', to: 'policies#add_servers'
      get '/patchManagement/server/updatePolicy/addPolicyServerToPatch', to: 'policies#update_servers'
      get '/patchManagement/server/updatePolicy/updatePolicyServerToPatch', to: 'policies#remove_servers'
      get '/patchManagement/policyGroupName/id', to: 'policies#check_group_name'
      get '/patchManagement/wsusCustSubgrp/id', to: 'policies#check_group_name'
      get '/patchManagement/customers/search', to: 'customers#search'
      get "/patchManagement/server/serverCentric", to: "servers#serverCentric"
    end
  end

  scope path: '/api/v1', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1/um', constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true)  do

      match '/scheduledEntries', to: 'update_managers#scheduledEntries', via: [:get, :post]
      put '/scheduledEntries/:id', to: 'update_managers#scheduledEntries'

      match '/latestGoldPolicy', to: 'update_managers#latestGoldPolicy', via: [:get]
      
      match '/goldPolicies', to: 'update_managers#goldPolicies', via: [:get, :post]
      get '/goldPolicies/:id', to: 'update_managers#goldPolicies'

      get '/patches/:id', to: 'update_managers#patches'
      post '/patches', to: 'update_managers#patches'

      post '/tickets', to: 'update_managers#tickets'
      get  '/tickets/:id', to: 'update_managers#ticket'
      get '/tickets/comments/:id', to: 'update_managers#tickets_comments'
      put '/tickets/comments/:id', to: 'update_managers#update_comment'

      get '/servers', to: 'update_managers#servers'
    end
  end

  scope path: '/', defaults: {format: 'json'}, as: :api do
    scope :module => 'api/v1/dns_ipam' , constraints: SGTools::Rails::REST.api_constraints_class.new(version: 1, default: true) do
      resources :dns_audits, only: [:create, :index]
      get 'allrecords', to: 'infoblox#get_dns_ipam'
      get 'record:cname', to: 'infoblox#get_dns_ipam'
      get 'nsgroup', to: 'infoblox#get_dns_ipam'
      get 'zone_auth', to: 'infoblox#get_dns_ipam'
      get 'company_zones', to: 'infoblox#get_zone_auth'
      get 'record:a', to: 'infoblox#get_dns_ipam'
      get 'record:aaaa', to: 'infoblox#get_dns_ipam'
      get 'record:cname', to: 'infoblox#get_dns_ipam'
      get 'record:ptr', to: 'infoblox#get_dns_ipam'
      get 'record:mx', to: 'infoblox#get_dns_ipam'
      get 'record:srv', to: 'infoblox#get_dns_ipam'
      get 'record:naptr', to: 'infoblox#get_dns_ipam'
      get 'record:txt', to: 'infoblox#get_dns_ipam'
      get 'ip_subnet_list/WHERE/:query', to: 'infoblox#get_ip_list', constraints: { query: /[^\/]+/ }
      get 'ip6_subnet6_list/WHERE/:query', to: 'infoblox#get_ip_list', constraints: { query: /[^\/]+/ }

      post 'zone_auth', to: 'infoblox#post_dns_ipam'
      post 'record:a', to: 'infoblox#post_dns_ipam'
      post 'record:aaaa', to: 'infoblox#post_dns_ipam'
      post 'record:cname', to: 'infoblox#post_dns_ipam'
      post 'record:ptr', to: 'infoblox#post_dns_ipam'
      post 'record:mx', to: 'infoblox#post_dns_ipam'
      post 'record:srv', to: 'infoblox#post_dns_ipam'
      post 'record:naptr', to: 'infoblox#post_dns_ipam'
      post 'record:txt', to: 'infoblox#post_dns_ipam'

      put 'record:a/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:cname/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:aaaa/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:mx/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:srv/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:naptr/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }
      put 'record:txt/:ref/default', to: 'infoblox#put_dns_ipam', constraints: { ref: /[^\/]+/ }

      delete 'record:a/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:cname/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:aaaa/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:ptr/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:mx/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:srv/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:naptr/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      delete 'record:txt/:ref/default', to: 'infoblox#delete_dns_ipam', constraints: { ref: /[^\/]+/ }
      # ipam endpoints
      get '/ipam/getCollection', to: 'ipam#get_collection_by_name'
      get '/ipam/getCollection/:id', to: 'ipam#get_collection_by_id', constraints: {id: /[^\/]+/}
      get '/ipam/getCollection/getAllDocuments', to: 'ipam#get_all_documents'
      get '/ipam/getCollection/getDocOnFieldValue', to: 'ipam#get_wildcard_values'
      get '/ipam/exists/collection', to: 'ipam#check_collection_exists'
      get '/ipam/exists/collection/field', to: 'ipam#check_field_exists'
      get '/ipam/exists/collection/field/Transics', to: 'ipam#check_value_exists'
      get '/ipam/getCollection/count', to: "ipam#get_collection_count"
      get '/ipam/getCollection/getDocOnFieldValue', to: "ipam#get_doc_field"

      post '/ipam/addDocuments', to: 'ipam#add_document'
      put '/ipam/updateDocuments', to: 'ipam#update_document'
      delete '/ipam/deleteDocument', to: 'ipam#delete_document'
    end
  end


  namespace :sungard do
    resources :welcome, :helps, :api_keys, :statements, :menus, :roles, :event_subscriptions, :compliance, :compliance_compute, :companies, :ehealth_companies, :parent_child
   
    match 'status' => 'status#index'
    match 'login_count' => 'status#login_count'
    match 'echo' => 'status#echo'
    match 'acl' => 'status#acl'
    root :to => 'welcome#index'
  end

  namespace :tools do
    get 'service_now', to: 'service_now#index'
    get 'knowledge_base', to: 'knowledge_base#index'
    get 'ids', to: 'ids#index'
    get 'netids', to: 'net_ids#index'
    get 'seim', to: 'seim#index'
    # get 'test', to: 'test#index'
  end

  resources :devices do
    collection do
      get '/core_devices' => 'devices#core_devices'
      get "/:id" => "devices#show"
      get '/device_details/:global_id' => "devices#device_details"
      # itsm widget routes
      get '/:device_id/widgets/device_time'   => 'widgets#device_time', constraints: {device_id: /[^\/]+/}
    end
  end

  resources :sessions
  scope 'graphs/:graph_id' do
    resources :oder,
      :as         => 'graph_order',
      :controller => 'graphs_order',
      :only       => [:create]
  end

  get 'export/devices' => 'export#devices'
  get 'export/interfaces' => 'export#interfaces#export'
  get 'export/events' => 'export#events'
  get 'export/live_events' => 'export#live_events'

  get 'home' => 'welcome#index'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'unmasquerade', to: 'masquerades#destroy', as: 'unmasquerade'
  get 'home_company', to: 'company_masquerades#destroy', as: 'home_company'

  # static pages
  get 'privacy' => 'say#privacy'
  get 'services_data' => 'say#services_data'
  get 'terms' => 'say#terms'
  get 'safe_harbor' => 'say#safe_harbor'
  
  match 'welcome' => 'welcome#landing_page'
  match "/pyxie" => ViewpointPyxieServer, :anchor => false
  match 'update_company_status' => 'companies#update', via: [:post]

  namespace :admin do
    resources :welcome
    resources :sessions, :only => [:create,:new ] unless Rails.env == 'production'
    resources :user_password_resets, :only => [:create]
    resources :user_password_resets, :only => [:create]
    resources :users do
      member do
        get 'change_password'
        put "update_password"
        put "resend_confirmation"
        put 'toggle_unlock'
        put 'toggle_active'
        get 'change_email'
        get 'change_email_success'
        put "update_email"
        get 'convert'
        get 'convert_non_portal'
        get 'cleanup'
      end
    end
    root :to => 'welcome#index'
  end

  namespace :services do
    resources :confirmations
    resources :password_resets
    resources :email_resets
  end

  get "/rs_user_guide" => "application#rs_user_guide", :as => "raas_user_guide"
  get "/user_guide" => "application#user_guide", :as => "viewpoint_user_guide"
  get "/user_guide_allstream" => "application#user_guide_allstream", :as => "viewpoint_user_guide_allstream"
  get "/user_guide_omi" => "application#user_guide_omi", :as => "viewpoint_user_guide_omi"
  get "/tutorials/customer_catalog" => "application#customer_catalog"
  get "/tutorials/remote_intelligent_hands" => "application#remote_intelligent_hands"
  get "/tutorials/secure_site_access" => "application#secure_site_access"
  get "/tutorials/create_ticket" => "application#create_ticket"
  get "/tutorials/ticket_updates" => "application#ticket_updates"
  get "/tutorials/modify_authorizations" => "application#modify_authorizations"
  get "/tutorials/creating_reports" => "application#creating_reports"
  get "/tutorials/notification_matrix" => "application#notification_matrix"
  get "/tutorials/providers_list" => "application#providers_list"
  get "/tutorials/mx_window" => "application#mx_window"
  get "/tutorials/contract_management" => "application#contract_management"
  get "/tutorials/self_service_guide" => "application#self_service_guide"
  
  #Mobile Tutorials 
  get "/tutorials/mobile_guide/:name" => "application#mobile_guides"

  #Video Tutorials
  get "/tutorials/video_guide/:name" => "application#video_guides"

  resources :firewalls,:only => [:index], path: '/firewalls'

  # get "/logs/firewalls", to: "firewalls#index"
  # get "/beta/groups", to: "groups#beta_groups"

  namespace "beta" do
    resources :devices
    resources :interfaces
  end

  # 2f is essential for tw0-factor authentication
  match "/2f/auth", to: "auths#index"

  root :to => 'welcome#index'

  match "/managedservice", :to => "welcome#index"

  match "/401", to: "errors#unauthorized"
  match "/404", to: "errors#not_found"
  match "/500", to: "errors#error"
end
