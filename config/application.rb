require File.expand_path('../boot', __FILE__)
require 'rails/all'
require 'openid/store/mongo'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module Viewpoint
  class Application < Rails::Application

    # cache 
    config_file = Rails.root.join('config/mongo.yml')
    if config_file.file?
        our_config = YAML.load(ERB.new(config_file.read).result)
        MongoMapper.setup(our_config, Rails.env)
    end
    config.cache_store = [
        :mongo_cache_store, 
        :TTL, :db => MongoMapper.database,
        :serialize => :always, 
        :collection_opts => {
            :read => :nearest
        }
    ]

    config.cacherank.database = MongoMapper.database
    
    #For custom error pages
    config.exceptions_app = self.routes
    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"
    config.autoload_paths += %W(#{config.root}/lib)
    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]
    config.middleware.use "Rack::OpenID",
      OpenID::Store::Mongo.new(:collection => MongoMapper.database['rack_openid'])

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    config.active_record.whitelist_attributes = true

    # Enable the asset pipeline
    config.assets.enabled = true

    #Enable cross origin support
    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :put, :options, :delete]
      end
    end

    # USAGE_REPORTS = "/data/reports/"
    # STUB_USER = false
   
    CUSTOMER_NETWORK_CONFIG = OpenStruct.new({
        url: 'http://sgs-api-dev.sgns.net/servicecenter/ports/internet?stream=yes',
        host: 'sgs-api-dev.sgns.net',
        auth: 'c2dtY191c2VyOmVkYWhsLXJ1bHo=\n'
    })

    ZENOSS_CONFIG_WORKER = OpenStruct.new({
        auth: 'bWF0dC5wYXphcnluYTpUbHVid3didHIxIQ==',
        urls: {'dev1.zen.sgns.net'=>'https://dev1.zen.sgns.net'},
        network: 'https://net.zen.sgns.net'
    })

    SERVICE_CENTER_API_CONFIG = OpenStruct.new({
        use_ssl: false,
        url: 'http://sgs-api-dev.sgns.net',
        auth: 'c2dtY191c2VyOmVkYWhsLXJ1bHo=\n'
    })
   
    puts config.paths['config']
  end

end
