# Load the rails application
require File.expand_path('../application', __FILE__)
# importing certificate from staging.
require "openid/fetchers"

OpenID.fetcher.ca_file = "#{Rails.root}/config/ca-certificates.crt"

# Initialize the rails application
Viewpoint::Application.initialize!
