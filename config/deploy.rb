#$:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
require 'capistrano/ext/multistage'
require 'net/ssh/gateway'
require './lib/cap_notify.rb'

set :application, "viewpoint"
set :stages, %w(new_env production staging qa dev vagrant oneportal_uat)
set :default_stage, "staging"
set :branch, fetch(:branch, "master")
set :copy_exclude, ["config/deploy.rb","lib/tasks/rspec.rake", "doc", "spec"]
set :keep_releases, 4
set :git_enable_submodules, 1

set :ssh_options, {
  :forward_agent => true,
}

# prod does not have access to github.com, deploy via copy
# set :scm, :none
# set :repository, "."
# set :deploy_via, :copy
set :scm, :git
set :repository, "git@bitbucket.org:mountdiablo/viewpoint.git"

# use a ssh wrapper that will not use key checking
set :default_environment, {
  'GIT_SSH' => "/usr/bin/git_ssh"
}

namespace :passenger do
  desc "Restart Application"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt", roles: :app
  end
end

namespace :config do
  task :copy do
    run "ln -nfs #{shared_path}/docs/vp_user_guide.pdf #{release_path}/downloads/sg_vp_user_guide.pdf"
    run "ln -nfs #{shared_path}/docs/vp_user_guide_omi.pdf #{release_path}/downloads/sg_vp_user_guide_omi.pdf"
    run "ln -nfs #{shared_path}/docs/vp_user_guide.zip #{release_path}/downloads/sg_vp_user_guide.zip"
    run "ln -nfs #{shared_path}/docs/rs_user_guide.pdf #{release_path}/downloads/sg_rs_user_guide.pdf"
  end

  task :copy_documents do
    run "ln -nfs #{shared_path}/docs/self_service_customer_catalog.pdf #{release_path}/downloads/tutorials/self_service_customer_catalog.pdf"
    run "ln -nfs #{shared_path}/docs/remote_intelligent_hands.pdf #{release_path}/downloads/tutorials/remote_intelligent_hands.pdf"
    run "ln -nfs #{shared_path}/docs/secured_site_access.pdf #{release_path}/downloads/tutorials/secured_site_access.pdf"
    run "ln -nfs #{shared_path}/docs/how_to_create_a_ticket.pdf #{release_path}/downloads/tutorials/how_to_create_a_ticket.pdf"
    run "ln -nfs #{shared_path}/docs/looking_for_ticket_updates.pdf #{release_path}/downloads/tutorials/looking_for_ticket_updates.pdf"
    run "ln -nfs #{shared_path}/docs/how_to_modify_authorizations.pdf #{release_path}/downloads/tutorials/how_to_modify_authorizations.pdf"
    run "ln -nfs #{shared_path}/docs/creating_reports.pdf #{release_path}/downloads/tutorials/creating_reports.pdf"
    run "ln -nfs #{shared_path}/docs/notification_matrix.pdf #{release_path}/downloads/tutorials/notification_matrix.pdf"
    run "ln -nfs #{shared_path}/docs/service_delivery_providers_list.pdf #{release_path}/downloads/tutorials/service_delivery_providers_list.pdf"
    run "ln -nfs #{shared_path}/docs/MX_Windows.pdf #{release_path}/downloads/tutorials/MX_Windows.pdf"
    run "ln -nfs #{shared_path}/docs/contract_management.pdf #{release_path}/downloads/tutorials/contract_management.pdf"    
    run "ln -nfs #{shared_path}/docs/self_service_guide.pdf #{release_path}/downloads/tutorials/self_service_guide.pdf"
    
    run "ln -nfs #{shared_path}/docs/mobile-authorizations_management.pdf #{release_path}/downloads/tutorials/mobile-authorizations_management.pdf"
    run "ln -nfs #{shared_path}/docs/mobile-managing_notification_matrix.pdf #{release_path}/downloads/tutorials/mobile-managing_notification_matrix.pdf"
    run "ln -nfs #{shared_path}/docs/mobile-managing_requests_and_incidents.pdf #{release_path}/downloads/tutorials/mobile-managing_requests_and_incidents.pdf"
    run "ln -nfs #{shared_path}/docs/mobile-requesting_site_access.pdf #{release_path}/downloads/tutorials/mobile-requesting_site_access.pdf"
    run "ln -nfs #{shared_path}/docs/mobile-service_now_on_mobile.pdf #{release_path}/downloads/tutorials/mobile-service_now_on_mobile.pdf"
    #Video Tutorials
    run "ln -nfs #{shared_path}/docs/video/self-service.mp4 #{release_path}/downloads/tutorials/self-service.mp4"
    run "ln -nfs #{shared_path}/docs/video/user-management.mp4 #{release_path}/downloads/tutorials/user-management.mp4"
    run "ln -nfs #{shared_path}/docs/video/authorizations-secure-access.mp4 #{release_path}/downloads/tutorials/authorizations-secure-access.mp4"
    run "ln -nfs #{shared_path}/docs/video/open-manage-tickets.mp4 #{release_path}/downloads/tutorials/open-manage-tickets.mp4"
    run "ln -nfs #{shared_path}/docs/video/create-manage-notifications.mp4 #{release_path}/downloads/tutorials/create-manage-notifications.mp4"
    run "ln -nfs #{shared_path}/docs/video/snow-email-opt-out.mp4 #{release_path}/downloads/tutorials/snow-email-opt-out.mp4"
    run "ln -nfs #{shared_path}/docs/video/create-reports.mp4 #{release_path}/downloads/tutorials/create-reports.mp4"
    run "ln -nfs #{shared_path}/docs/video/res.mp4  #{release_path}/downloads/tutorials/res.mp4"
  end
end

namespace :bundle do
  desc "run bundle install and ensure all gem requirements are met"
  task :install do
    run "cd #{release_path} && bundle install  --without=test"
  end
end

namespace :rake do
  desc "Run a task on a remote server."
  task :precompile_assets do
    run("cd #{release_path} && bundle exec rake assets:precompile RACK_ENV=#{rails_env} RAILS_ENV=#{rails_env}", roles: :app)
  end
end

set :notify_emails, [
  "amit.gijare@sungardas.com",
  "sunny.bogawat@sungardas.com",
  "rashi.sinha@sungardas.com",
  "priyanka.ukirde@sungardas.com",
  "as.OSS.ReleaseEngineering@sungardas.com"
]

namespace :deploy do
  desc "Send email notification"
  task :send_notification do
    Notifier.deploy_notification(self).deliver
  end

  desc "Symlink shared configs"
  task :symlink_config do
    run "ln -nfs #{shared_path}/config/config.yml #{release_path}/config/config.yml"
  end

  desc "Symlink shared report customers"
  task :symlink_report_customers do
    run "ln -nfs #{shared_path}/data/service_mgt_customers.csv  #{release_path}/db/data/import/service_mgt_customers.csv"
    run "ln -nfs #{shared_path}/config/omi_companies.csv #{release_path}/db/data/omi/omi_companies.csv"
    run "ln -nfs #{shared_path}/config/cloudfront/pk-APKAJTWQM4BZJNT4DHBQ.pem #{release_path}/config/cloudfront/pk-APKAJTWQM4BZJNT4DHBQ.pem"
    run "ln -nfs #{shared_path}/config/cloudfront/rsa-APKAJTWQM4BZJNT4DHBQ.pem #{release_path}/config/cloudfront/rsa-APKAJTWQM4BZJNT4DHBQ.pem"
    run "ln -nfs #{shared_path}/config/backup_report_companies.csv  #{release_path}/db/data/import/backup_report_companies.csv"
    run "ln -nfs #{shared_path}/config/portal_patching.csv  #{release_path}/db/data/portal_patching/portal_patching.csv"
    run "ln -nfs #{shared_path}/config/tdr_companies.csv  #{release_path}/db/data/import/tdr_companies.csv"
    run "ln -nfs #{shared_path}/config/apm_companies.csv  #{release_path}/db/data/import/apm_companies.csv"
  end
end

before 'deploy:symlink', 'bundle:install', 'rake:precompile_assets'
after 'deploy:update', 'config:copy', 'config:copy_documents', 'passenger:restart', 'deploy:symlink_config', 'deploy:symlink_report_customers' ,'deploy:send_notification'
