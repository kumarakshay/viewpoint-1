if Rails.env != 'test'
  email_settings = YAML::load(File.open("#{Rails.root.to_s}/config/email.yml"))
  unless email_settings[Rails.env].nil?
    ActionMailer::Base.smtp_settings = email_settings[Rails.env] 
    ActionMailer::Base.default_url_options[:host] =  email_settings[Rails.env][:host_url]
    ActionMailer::Base.default_url_options[:protocol] =  'https'
  end
end
