Rails.logger.info("---- Initializing spotlight client")
sp_config_file = Rails.root.join('config/spotlight_client.yml')
if sp_config_file
	sp_client_config = YAML.load(File.read(sp_config_file))[Rails.env]
	if sp_client_config
		SpotlightClient.config do |config|
			config.site =  sp_client_config["site"]
			config.api_key = sp_client_config["api_key"]
			config.create_model :Firewall
			config.create_model :Device
		end
		#config.spolight_client_config = sp_client_config
	end
end
