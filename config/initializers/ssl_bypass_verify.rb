OpenSSL::SSL.const_set(:VERIFY_PEER, OpenSSL::SSL::VERIFY_NONE)
# OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

Rack::OAuth2.debug!
Rack::OAuth2.http_config do |config|
  config.ssl_config.verify_mode = OpenSSL::SSL::VERIFY_NONE
end

OpenIDConnect.debug!
OpenIDConnect.http_config do |config|
  config.ssl_config.verify_mode = OpenSSL::SSL::VERIFY_NONE
end
