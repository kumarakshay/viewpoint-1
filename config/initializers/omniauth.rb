OmniAuth.config.logger = Rails.logger

if Figaro.env.open_id_version == 'V2'
  Rails.application.config.middleware.use OmniAuth::Builder do
    provider :openid_connect, {
      name: :viewpoint,
      scope: [:openid, :email, :profile, :address, :cloud, :phone, :userGuid],
      response_type: :code,
      client_options: {
        port: 443,
        scheme: "https",
        host: "#{Figaro.env.sso_v2_authentication_server}",
        identifier: "#{Figaro.env.sso_v2_viewpoint_api_key}",
        secret: "#{Figaro.env.sso_v2_viewpoint_api_secret}",
        redirect_uri: "#{Figaro.env.redirect_uri}",        
        authorization_endpoint: "/service/oauth2/authorize?realm=/SungardAS",
        token_endpoint: "/service/oauth2/access_token?realm=/SungardAS",
        userinfo_endpoint: "/service/oauth2/userinfo?realm=/SungardAS",
        jwks_uri: "/service/jwks.json"       
      },
    }

    provider :openid_connect, {
      name: :allstream,
      scope: [:openid, :email, :profile, :address, :cloud, :phone, :userGuid],
      response_type: :code,
      client_options: {
        port: 443,
        scheme: "https",
        host: "#{Figaro.env.sso_v2_allstream_authentication_server}",
        identifier: "#{Figaro.env.sso_v2_allstream_api_key}",
        secret: "#{Figaro.env.sso_v2_allstream_api_secret}",
        redirect_uri: "#{Figaro.env.allstream_redirect_uri}",   
        authorization_endpoint: "/service/oauth2/authorize?realm=/Allstream",
        token_endpoint: "/service/oauth2/access_token?realm=/Allstream",
        userinfo_endpoint: "/service/oauth2/userinfo?realm=/Allstream",
        jwks_uri: "/service/jwks.json"       
      },
    }
  end

  OmniAuth.config.full_host = Proc.new do |env|
    "https://#{env['HTTP_HOST']}"
  end

  Rails.application.config.middleware.use Rack::Config do |env|  
    env['rack.url_scheme'] = 'https'    
    env['HTTPS'] = 'on'
  end

  module OpenIDConnect
    class SGResponse < ResponseObject::UserInfo::OpenID
        attr_optional(
          :applications,
          :roles,
          :groups,
          :userGuid,
          :company_guid
        )
    end

    class AccessToken
      def userinfo!
        hash = resource_request do
          get client.userinfo_uri
        end
        Rails.logger.info hash
        SGResponse.new hash
      end
    end
  end
end  