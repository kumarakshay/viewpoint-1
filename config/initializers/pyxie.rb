require 'pyxie'
require 'pyxie/access_control/open'
require 'sgtools/pyxie/access_control/viewpoint' 

pyxie = Pyxie::Server
pyxie.configure() {|c| c.set :environments, %w{development test production staging qa oneportal_uat} }
pyxie.config_file(Rails.root.join('config', 'pyxie.yml'))
pyxie.register(SGTools::Pyxie::AccessControl::Viewpoint)

class ViewpointPyxieServer < pyxie
end

