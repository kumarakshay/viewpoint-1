set :rvm_ruby_string, 'ruby-1.9.3-p125@sgmc_app'
role :app, "phlnu273", "phlnu274"
set :deploy_to,"/home/sgmc/#{application}"
set :user, 'sgmc'
set :use_sudo, false
set :rails_env, "production"

namespace :deploy do
  task :start do ; end
  task :stop do ; end


  before 'deploy:update_code' do
    servers = find_servers_for_task(current_task)
    puts "servers: #{servers.join(', ')}"
    servers.each do |server|
      conn = Net::SSH.start(server.to_s,'sgmc')
      conn.forward.remote(22, "bitbucket.org", 11233)
      conn.forward.remote(22, "github.com", 11234)
      Thread.new do
        conn.loop {true}
      end
    end
  end
end
