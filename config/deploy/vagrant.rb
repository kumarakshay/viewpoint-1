# set :rvm_ruby_string, 'ruby-1.9.3-p125@sgmc_app'
role :app, "localhost"
set :port, "2223"
set :deploy_to,"/home/sgmc/#{application}"
set :user, 'sgmc'
set :use_sudo, false
set :rails_env, "development"