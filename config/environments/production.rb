Viewpoint::Application.configure do

  APP_CONFIG = YAML.load_file("#{Rails.root}/config/application.yml")[Rails.env]

  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify
  config.assets.digest = true
end

class Viewpoint::Application
  ZENOSS_CONFIG_WORKER.urls = {'cust2'=>'https://cust2.zen.sgns.net', 'uk2'=>'https://uk2.zen.sgns.net'}
  ZENOSS_CONFIG_WORKER.network = 'https://net2.zen.sgns.net'
  SERVICE_CENTER_API_CONFIG = OpenStruct.new({
    use_ssl: false,
    url: 'https://sgs-api.sgns.net',
    auth: 'c2dtY191c2VyOnNnbWM0ZXZlcg==\n'
  })
    
  USAGE_REPORTS = "/data/reports/"
  GA.tracker = "UA-93696326-1"
  STUB_WEB_SERVICES = false
end
