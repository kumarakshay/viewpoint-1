Viewpoint::Application.configure do
  APP_CONFIG = YAML.load_file("#{Rails.root}/config/application.yml")[Rails.env]
  config.assets.digest = true
end

class Viewpoint::Application  

  USAGE_REPORTS = "/data/reports/"
  ZENOSS_CONFIG_WORKER.urls = {'cust2'=>'https://cust2-stage.zen.sgns.net', 'uk2'=>'https://uk2-stage.zen.sgns.net'}
  ZENOSS_CONFIG_WORKER.network = 'https://net2-stage.zen.sgns.net'
 
  SERVICE_CENTER_API_CONFIG = OpenStruct.new({
    use_ssl: true,
    url: 'https://sgs-api.sgns.net',
    auth: 'c2dtY191c2VyOnNnbWM0ZXZlcg==\n'
  })
  
  GA.tracker = "UA-93696326-2"
end
