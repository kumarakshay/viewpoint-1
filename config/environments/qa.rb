Viewpoint::Application.configure do
  # APP_CONFIG = YAML.load_file("#{Rails.root}/config/aph.yml")[Rails.env]
  APP_CONFIG = YAML.load_file("#{Rails.root}/config/application.yml")[Rails.env]

end

class Viewpoint::Application  

  USAGE_REPORTS = "/data/reports/"
  
  ZENOSS_CONFIG_WORKER.urls = {'cust2'=>'https://cust2-stage.zen.sgns.net', 'uk2'=>'https://uk2-stage.zen.sgns.net'}
  ZENOSS_CONFIG_WORKER.network = 'https://net2-stage.zen.sgns.net'
 
  SERVICE_CENTER_API_CONFIG = OpenStruct.new({
    use_ssl: true,
    url: 'https://sgs-api.sgns.net', 
    auth: 'c2dtY191c2VyOnNnbWM0ZXZlcg==\n'
  })
  
  # JHoch@subaru.com
  STUB_USER = 'FB404A21-FACF-4029-B20D-9F38BDA51872'

  GA.tracker = "UA-38487100-2"
end