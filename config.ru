# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)

require 'utf8-cleaner'
use UTF8Cleaner::Middleware

# https://github.com/rails/rails/issues/6933
if Viewpoint::Application.config.relative_url_root
  Viewpoint::Application.routes.default_url_options[:script_name] = Viewpoint::Application.config.relative_url_root
end

run Viewpoint::Application
