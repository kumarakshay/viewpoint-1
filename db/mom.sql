DROP TABLE IF EXISTS `alert_state`;
CREATE TABLE `alert_state` (
  `evid` char(36) NOT NULL,
  `userid` varchar(64) NOT NULL DEFAULT '',
  `rule` varchar(255) NOT NULL DEFAULT '',
  `lastSent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`evid`,`userid`,`rule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail` (
  `evid` char(36) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  KEY `evididx` (`evid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `heartbeat`;
CREATE TABLE `heartbeat` (
  `device` varchar(128) NOT NULL,
  `component` varchar(128) NOT NULL DEFAULT '',
  `timeout` int(11) DEFAULT '0',
  `lastTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device`,`component`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `dedupid` varchar(255) NOT NULL,
  `evid` char(36) NOT NULL,
  `device` varchar(128) NOT NULL,
  `component` varchar(255) DEFAULT '',
  `eventClass` varchar(128) DEFAULT '/Unknown',
  `eventKey` varchar(128) DEFAULT '',
  `summary` varchar(128) NOT NULL,
  `message` varchar(4096) DEFAULT '',
  `severity` smallint(6) DEFAULT '-1',
  `eventState` smallint(6) DEFAULT '0',
  `eventClassKey` varchar(128) DEFAULT '',
  `eventGroup` varchar(64) DEFAULT '',
  `stateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `firstTime` bigint(20) DEFAULT NULL,
  `lastTime` bigint(20) DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  `prodState` smallint(6) DEFAULT '0',
  `suppid` char(36) NOT NULL,
  `manager` varchar(128) NOT NULL,
  `agent` varchar(64) NOT NULL,
  `DeviceClass` varchar(128) DEFAULT '',
  `Location` varchar(128) DEFAULT '',
  `Systems` varchar(255) DEFAULT '',
  `DeviceGroups` varchar(255) DEFAULT '',
  `ipAddress` char(15) DEFAULT '',
  `facility` varchar(8) DEFAULT 'unknown',
  `priority` smallint(6) DEFAULT '-1',
  `ntevid` smallint(5) unsigned DEFAULT '0',
  `ownerid` varchar(32) DEFAULT '',
  `deletedTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clearid` char(36) DEFAULT NULL,
  `DevicePriority` smallint(6) DEFAULT '3',
  `eventClassMapping` varchar(128) DEFAULT '',
  `monitor` varchar(128) DEFAULT '',
  `suppressionCount` int(11) DEFAULT '0',
  `suppressionSeconds` int(11) DEFAULT '0',
  `DeviceImpact` int(11) DEFAULT NULL,
  `origin_id` varchar(255) DEFAULT '',
  `origin_evid` char(36) DEFAULT '',
  `enriched` tinyint(1) DEFAULT '0',
  `sn_sysid` varchar(256) DEFAULT NULL,
  `sn_number` varchar(256) DEFAULT NULL,
  `assetGuid` varchar(60) DEFAULT NULL,
  `companyGuid` varchar(60) DEFAULT NULL,
  KEY `device` (`device`),
  KEY `severityidx` (`severity`),
  KEY `firstTime` (`firstTime`),
  KEY `lastTime` (`lastTime`),
  KEY `evid` (`evid`),
  KEY `originidx` (`origin_id`,`origin_evid`),
  KEY `assetguididx` (`assetGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `evid` char(36) NOT NULL,
  `userName` varchar(64) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `text` text,
  KEY `evididx` (`evid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queue_info`;
CREATE TABLE `queue_info` (
  `name` varchar(255) NOT NULL,
  `receivetimeout` int(11) NOT NULL DEFAULT '30',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `queue_items`;
CREATE TABLE `queue_items` (
  `orderidx` int(11) NOT NULL AUTO_INCREMENT,
  `queuename` varchar(255) NOT NULL,
  `id` varchar(255) NOT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visibletime` timestamp NULL DEFAULT NULL,
  `body` mediumtext,
  PRIMARY KEY (`orderidx`),
  KEY `queuenameidx` (`queuename`),
  KEY `ididx` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11782922 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sn_cti`;
CREATE TABLE `sn_cti` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `sn_category` varchar(256) DEFAULT NULL,
  `sn_type` varchar(256) DEFAULT NULL,
  `sn_item` varchar(256) DEFAULT NULL,
  `support_org` varchar(32) DEFAULT NULL,
  `last` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=811 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sn_incident`;
CREATE TABLE `sn_incident` (
  `sysid` varchar(256) NOT NULL,
  `number` varchar(32) DEFAULT NULL,
  `last` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sysid`),
  KEY `number_idx` (`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `dedupid` varchar(255) NOT NULL,
  `evid` char(36) NOT NULL,
  `device` varchar(128) NOT NULL,
  `component` varchar(255) DEFAULT '',
  `eventClass` varchar(128) DEFAULT '/Unknown',
  `eventKey` varchar(128) DEFAULT '',
  `summary` varchar(128) NOT NULL,
  `message` varchar(4096) DEFAULT '',
  `severity` smallint(6) DEFAULT '-1',
  `eventState` smallint(6) DEFAULT '0',
  `eventClassKey` varchar(128) DEFAULT '',
  `eventGroup` varchar(64) DEFAULT '',
  `stateChange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `firstTime` double DEFAULT NULL,
  `lastTime` double DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  `prodState` smallint(6) DEFAULT '0',
  `suppid` char(36) NOT NULL,
  `manager` varchar(128) NOT NULL,
  `agent` varchar(64) NOT NULL,
  `DeviceClass` varchar(128) DEFAULT '',
  `Location` varchar(128) DEFAULT '',
  `Systems` varchar(255) DEFAULT '',
  `DeviceGroups` varchar(255) DEFAULT '',
  `ipAddress` char(15) DEFAULT '',
  `facility` varchar(8) DEFAULT 'unknown',
  `priority` smallint(6) DEFAULT '-1',
  `ntevid` smallint(5) unsigned DEFAULT '0',
  `ownerid` varchar(32) DEFAULT '',
  `clearid` char(36) DEFAULT NULL,
  `DevicePriority` smallint(6) DEFAULT '3',
  `eventClassMapping` varchar(128) DEFAULT '',
  `monitor` varchar(128) DEFAULT '',
  `suppressionCount` int(11) DEFAULT '0',
  `suppressionSeconds` int(11) DEFAULT '0',
  `DeviceImpact` int(11) DEFAULT NULL,
  `origin_id` varchar(255) DEFAULT '',
  `origin_evid` char(36) DEFAULT '',
  `enriched` tinyint(1) DEFAULT '0',
  `sn_sysid` varchar(256) DEFAULT NULL,
  `sn_number` varchar(256) DEFAULT NULL,
  `assetGuid` varchar(60) DEFAULT NULL,
  `companyGuid` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`dedupid`),
  KEY `evididx` (`evid`),
  KEY `deviceidx` (`device`),
  KEY `originidx` (`origin_id`,`origin_evid`),
  KEY `clearidx` (`clearid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;