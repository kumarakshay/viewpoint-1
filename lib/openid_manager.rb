module OpenidManager
  class << self

    def get_user(email)
      data = {
        'auth_token' => APP_CONFIG['open_id_auth_token']
      }
      openid_get_url = APP_CONFIG['open_id_get_url'] + "/" + email
      access_openid(openid_get_url, data, 'Get')
   end
    
    # Calls openid with a post request for given set of data.
    def access_openid(site_url, data, req_type = 'Post')
      uri = URI.parse(APP_CONFIG['open_id_base_url']  + site_url)
      req = eval "Net::HTTP::#{req_type}.new(uri.path)"
      req.set_form_data(data)
      fire_request(uri,req,data)
    end
    
    # Makes and actual request to openid server. 
    def fire_request(uri, req, data)
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme =='https'
        pem = File.read("#{Rails.root}/config/ca-certificates.crt")
        http.use_ssl = true
        http.cert = OpenSSL::X509::Certificate.new(pem)      
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER      
      end
      res = http.start do |httpvar|      
        httpvar.request(req)
      end
      Rails.logger.add_metadata(request_to: "#{uri.to_s}") if Rails.logger.respond_to?(:add_metadata)
      Rails.logger.add_metadata(request_response_type: "#{res}") if Rails.logger.respond_to?(:add_metadata)
      Rails.logger.add_metadata(request_response_body: "#{res.body}") if Rails.logger.respond_to?(:add_metadata)
      json_res(res)
    end

    def json_res(res)
      if res.kind_of? Net::HTTPSuccess
        JSON.parse(res.body)
      else
        {'status' => 'failure'}.to_json
      end
    end
  end
end
