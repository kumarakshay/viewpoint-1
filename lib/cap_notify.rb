require 'action_mailer'
require 'active_support/core_ext/hash/indifferent_access'

class Notifier < ActionMailer::Base
    default :from => "noreply@sungardas.com"

    def deploy_notification(cap_vars)
      now = Time.now
      msg = "Viewpoint having branch <b>#{cap_vars.branch}</b> has been deployed to #{cap_vars.stage} on #{now.strftime("%m/%d/%Y")} at #{now.strftime("%I:%M %p")} "
      mail(:to => cap_vars.notify_emails, :subject => "Viewpoint legacy portal deployed to #{cap_vars.stage}") do |format|
        format.text { render :text => msg}
        format.html { render :text => "<p>" + msg + "<\p>"}
      end
    end
end
