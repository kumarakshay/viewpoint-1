namespace :db do  
  desc "CRUD on portal user for a company"
  task :test_portal_user_workflow=> :environment do
    ContactUtil.delete_portal_user
  	puts "Total portal users count before create:- #{ContactUtil.portal_user_count}"
  	status = ContactUtil.create_portal_user
  	puts "Total portal users count after create:- #{ContactUtil.portal_user_count}"
    if status
      ContactUtil.update_portal_user
      puts "Total portal users count before delete:- #{ContactUtil.portal_user_count}"
      ContactUtil.delete_portal_user
      puts "Total portal users count after delete:- #{ContactUtil.portal_user_count}"
    end  
  end
 
  desc "CRUD on non-portal user for a company"
  task :test_non_portal_user_workflow => :environment do
    puts "Total non-portal users count before create:- #{ContactUtil.np_user_count_for_a_company}"
    status = ContactUtil.create_np_user
    puts "Total non-portal users count after create:- #{ContactUtil.np_user_count_for_a_company} "
    if status
      ContactUtil.update_np_user
      puts "Total non-portal users count before delete:- #{ContactUtil.np_user_count_for_a_company}"
      ContactUtil.delete_np_user
      puts "Total non-portal users count after delete:- #{ContactUtil.np_user_count_for_a_company}"
    end
  end
end

class ContactUtil
  COMPANY_GUID="99D01800-E9A8-430A-98CE-8EAF850DB6C6"
  COMPANY_NAME="SAS OSS Tools"
  USER_GUID = (UUID.new).generate

  PORTAL_USER_HASH = {
  	"first_name"=>"portal",
	  "last_name"=>"user",
	  "email"=>"portal.user@fakedomain.com", 
	  "secondary_email"=>"portal.sec@fakedomain.com", 
	  "phone_number"=>"000 000 0000",
	  "phone_number_ext"=>"000", 
	  "business_phone_notes"=>"Business notes",
	  "cell_phone"=>"111 111 1111",
	  "cell_phone_ext"=>"111", 
    "cell_phone_notes"=>"Notes", 
    "phone_3"=>"222 222 2222", 
    "phone_3_ext"=>"222", 
    "phone_3_notes"=>"Phone 3 notes", 
    "phone_4"=>"333 333 3333", 
    "phone_4_ext"=>"333",
    "company_name" => COMPANY_NAME,
    "company_guid" => COMPANY_GUID,
    "guid" => USER_GUID,
    "phone_4_notes"=>"phone 4 notes", 
    "time_zone"=>"(GMT + 01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", 
    "roles"=>["company_admin", "switch_company"],
    "active"=>"true"
  }

  NP_USER_HASH = { 
    :first_name => 'non-portal',
    :last_name =>  'user',
    :email => "non-portal.user@fakedomain.com",
    :u_alternate_email => "non-portal.user@test.com",
    :active => 'true',
    :locked_out => 'true',
    :mobile_phone => '9192939495',
    :u_mobile_phone_ext => "000",
    :u_mobile_phone_notes => "Mobile Phone Notes",
    :u_active_contact => 'true',
    :u_alternate_phone_1 => '2345678761',
    :u_alternate_phone_1_ext => "111",
    :u_alternate_phone_1_notes => "Phone 1 notes",
    :u_alternate_phone_2 => '2345678762',
    :u_alternate_phone_2_ext => "222",
    :u_alternate_phone_2_notes =>"Phone 2 notes",
    :phone => '1234567890',
    :u_business_phone_notes => "Business Phone notes",
    :u_legacy_contact_id => "#{USER_GUID}",
    :company => "#{COMPANY_GUID}"
  }

  def self.portal_user_count
  	User.where(:company_guid => COMPANY_GUID).count
  end

  def self.delete_portal_user
  	user = User.first(:email => PORTAL_USER_HASH["email"])
  	unless user.blank?
  	  MyOpenid.delete_user(user.email)
      user.destroy
    end
  end

  def self.create_portal_user
    status = false
  	user = User.new(PORTAL_USER_HASH)
  	params ={"user" => PORTAL_USER_HASH}
   	res = MyOpenid.register_user(params, user)
   	json_res = JSON.parse(res.body)
    user.set_attributes(json_res)
    if ((json_res["status"] == "success") && user.save && user.save_sn_data!(PORTAL_USER_HASH))
      puts "portal user created successfully"
      status = true
    else
      puts "There is some problem for creating portal user:-"
      puts "#{user.inspect}"
    end
    status
  end

  def self.update_portal_user
  	user = User.first(:email => PORTAL_USER_HASH["email"] )
  	PORTAL_USER_HASH["phone_number"] = "999 999 9999"
  	PORTAL_USER_HASH["phone_3"]  = "888 888 8888"
    puts "Portal user phone_number and phone_3 before update:- #{user.phone_number} and #{user.phone_3}"
  	if (user.update_attributes(PORTAL_USER_HASH) && user.save_sn_data!(PORTAL_USER_HASH))
      user.reload
      puts "Portal user phone_number and phone_3 after update:- #{user.phone_number} and #{user.phone_3}"
  	else
  	  puts "There is some problem for updating portal user:-"
  	  puts "#{user.inspect}"
  	end
  end

  # Non Portal User Operation 
  def self.np_user_count_for_a_company
    SGTools::ITSM::SystemUser.where("company.u_portal_id" => COMPANY_GUID ,"locked_out" => true, "active" => true).count
  end  

  def self.delete_np_user
  	u = SGTools::ITSM::SystemUserWriter.new(:company => COMPANY_GUID)
	  u.first_name = 'updated firstname'
    u.last_name = 'updated lastname'
	  u.email = "non-portal.user@fakedomain.com"
	  u.u_legacy_contact_id = USER_GUID
	  u.active = false
	  u.locked_out = true
  	if u.save!
  	   puts "non-portal user deleted successfully"
  	else
  	  puts "There is some problem for deleting non-portal user:-"
  	  puts "#{u.inspect}"
  	end
  end 

  def self.create_np_user
    status = false
    u = SGTools::ITSM::SystemUserWriter.new(NP_USER_HASH)
    if u.save!
      status = true
      puts "Non-portal user created successfully"
    else
      puts "There is some problem for creating non-portal user:-"
      puts "#{u.inspect}"
    end
    status
  end  

  def self.update_np_user
    user = SGTools::ITSM::SystemUser.get_users(:u_guid => USER_GUID).first 
    puts "non-portal user first_name and last_name before update:- #{user.first_name} #{user.last_name}"
    u = SGTools::ITSM::SystemUserWriter.new(NP_USER_HASH)
    u.first_name = 'updated firstname'
    u.last_name =  'updated lastname'
    if u.save!
      user = SGTools::ITSM::SystemUser.get_users(:u_guid => USER_GUID).first 
      puts "non-portal user first_name and last_name after update:- #{user.first_name} #{user.last_name}"
    else
      puts "There is some problem for updating non-portal user:-"
      puts "#{u.inspect}"
    end
  end  
end


