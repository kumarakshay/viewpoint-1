namespace :db do
	desc "This task would create menu and help items for default group"
	task :correct_delivery_time_of_reports => :environment do
		begin
			reqs = Req.all.select{|n| n if n.delivery_time.present? && n.delivery_time.include?(".")}
			puts "^^^^^ req count -  #{reqs.count}"
			reqs.each do |req|
				req.update_attribute("delivery_time", req.delivery_time.sub!(".", ":"))
			end
		rescue
			Rails.logger.error $!
			raise
		end
	end
end