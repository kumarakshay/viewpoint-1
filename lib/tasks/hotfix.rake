class Util
  def self.filter_params(params={})
    fil_hash = {}
    fil_hash[:guid] = params["userid"].upcase
    fil_hash[:email] = params["uid"]
    fil_hash[:portal_name] = params["portalname"]
    fil_hash[:company_name] = params["companyname"]
    fil_hash[:company_guid] = params["companyid"].upcase
    fil_hash[:first_name] = params["firstname"] if not_blank_unknown(params["firstname"] )
    fil_hash[:last_name] = params["lastname"] if not_blank_unknown (params["lastname"])
    fil_hash
  end

  def self.name_attributes(params={})
    fil_hash = {}
    fil_hash[:first_name] = params[:first_name] if not_blank_unknown(params[:first_name] )
    fil_hash[:last_name] = params[:last_name] if not_blank_unknown (params[:last_name])
    fil_hash
  end

  def self.not_blank_unknown(attribute)
    state = false
    if not attribute.blank? and not attribute == "Unknown"
      state =  true
    end
    state
  end
end

namespace :db do

  desc "Hotfix 2.01.09 import"
  task :hotfix_09 => :environment do
    created, updated, total = 0,0,0
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      file = "#{Rails.root}/db/data/hotfix/lp-users-for-viewpoint-20130617-3.txt"
      header =[]
      @file = IO.read(file).force_encoding("ISO-8859-1")
      @file.each_line do |line|
        data = line.split(/\t/)
        data.collect(&:strip!)
        if total == 0
          header = data
        else
          act_params = Hash[header.zip data]
          params = Util.filter_params(act_params)
          name_params = Util.name_attributes(params)
          begin
            # Load user only if we have name attributes to update.
            user = User.find_by_email(/^#{params[:email]}$/i) unless name_params.empty?
            if user.blank?
              User.create!params
              created +=1
            else
              user.update_attributes(name_params)
              updated +=1
            end
          rescue Exception => e
            Rails.logger.error("Error occured while creating user with attributes #{params} - #{e}")
            Rails.logger.error $!
          end
        end
        total +=1
        print '.' if total%250==0
      end
    rescue
      Rails.logger.error $!
      raise
    end
    Rails.logger.info("Total records in txt file - #{total}")
    Rails.logger.info("Records created           - #{created}")
    Rails.logger.info("Records updated           - #{updated}")
  end

  desc "De-activate groups belonging to Sungard companies"
  task :deactivate_sungard_groups => :environment do
    failure, updated, total = 0,0,0
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      puts "Starting operation"
      groups =  Group.where(:company_guid => {'$in' =>
                                              ['76C6F530-40C1-446D-A5D5-A66E78605149',
                                               'E71312DC-AAF1-4BF2-941D-B297B1D326AB']})
      groups.each do |group|
        total +=1
        group.active = false
        begin
          group.save
          updated +=1
        rescue Exception => e
          failure +=1
          Rails.logger.error("Error occured while deactivating group #{group.id} - #{e}")
          Rails.logger.error $!
        end
      end
    rescue
      Rails.logger.error $!
      raise
    end
    msg = "Total records - #{total} \n Records updated - #{updated} \n Failures  - #{failure}"
    Rails.logger.info( msg )
    puts(msg)
  end

  desc "This task would set last login value default to current time for all active users"
  task :set_last_login_at => :environment do
    begin
      updated = 0
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      users = User.where(:active => true).all
      users.each do |user|
        begin
          user.update_attribute(:last_login_at, Time.now)
          updated +=1
        rescue Exception => e
          Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}"
        end
      end
    rescue
      Rails.logger.error $!
      raise
    end
    msg = "Total users updated - #{updated}"
    Rails.logger.info( msg )
    puts(msg)
  end

  desc "This task will update the support Organization for the company"
  task :update_support_org => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      sn_client =  ServiceNowRestClient.new
      results = sn_client.support_org_companies
      unless results.blank?
        results.each do |result|
          if result['u_portal_id'].present? && result['u_support_organization'].present?
            Company.collection.update({:guid => result['u_portal_id']},{:$set=>{support_org: result['u_support_organization']}})
          end
        end
      end
    rescue Exception => e
      Rails.logger.error $!
      raise
      Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}"
    end
  end

  desc "This task would downcase the company guid from the collection compliance_devices"
  task :downcase_company_guid_from_compliance_devices => :environment do
    begin
      updated = 0
      Rake::Task[:environment].invoke
      puts  "Starting operation"
      arr = ComplianceDevice.all
      puts "Total Compliance Devices found #{arr.count}"
      arr.each do |obj|
        obj.company_guid = obj.company_guid.downcase
        obj.save!
        updated +=1
      end
      puts "Total users updated - #{updated}"
    rescue
      Rails.logger.error $!
      raise
      Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}"
    end
  end
end
