class Util
    def self.downcase_req_device_guid
      Rails.logger.info "Starting operation for Req collection"
      puts "Starting operation for Req collection"
      reqs = Req.where(:device_guid.ne => nil).all
      count = 0
      reqs.each do |req|
        begin
          device_guid = req.device_guid
          unless device_guid.blank?
            req.update_attribute(:device_guid, device_guid.downcase) 
            count = count + 1
          end
        rescue Exception => e
          Rails.logger.error "Error occured while updating Req:-  #{e.backtrace.to_s}" 
          puts "Error occured while updating Req collection:-  #{e.backtrace.to_s}"
        end
      end 
      Rails.logger.info "Total updated device_guid for Req collection is:- #{count}" 
      puts "Total updated device_guid for Req collection is:- #{count}"      
    end

    def self.downcase_group_asset_list
      Rails.logger.info "Starting operation for Group collection"
      puts "Starting operation for Group collection"
      groups = Group.all
      count = 0 
      groups.each do |group|
        begin
          asset_list = group.asset_list
          unless asset_list.blank?
            group.update_attribute(:asset_list, asset_list.map(&:downcase)) 
            count = count + 1
          end  
        rescue Exception => e
          Rails.logger.error "Error occured while updating Group collection:-  #{e.backtrace.to_s}" 
          puts "Error occured while updating Group collection:-  #{e.backtrace.to_s}"
        end
      end 
      Rails.logger.info "Total updated asset_list for Group collection:- #{count}"
      puts "Total updated asset_list for Group collection:- #{count}"      
    end

    def self.downcase_event_subscription_asset_list
      Rails.logger.info "Starting operation for EventSubscription collection"
      puts "Starting operation for EventSubscription collection"
      event_subscriptions = EventSubscription.all
      count = 0 
      event_subscriptions.each do |es|
        begin
          asset_list = es.asset_list
          unless asset_list.blank?
            es.update_attribute(:asset_list, asset_list.compact.map(&:downcase)) 
            count = count + 1
          end  
        rescue Exception => e
          Rails.logger.error "Error occured while updating EventSubscription collection:-  #{e.backtrace.to_s}" 
          puts "Error occured while updating EventSubscription collection:-  #{e.backtrace.to_s}" 
        end
      end       
      Rails.logger.info "Total updated asset_list for EventSubscription collection:- #{count}"
      puts "Total updated asset_list for EventSubscription collection:- #{count}"
    end

    def self.downcase_company_asset_list
      Rails.logger.info "Starting operation for Company collection"
      puts "Starting operation for Company collection"
      companies = Company.all
      count = 0
      companies.each do |company|
        begin
          asset_list = company.asset_list
          unless asset_list.blank?
            company.update_attribute(:asset_list, asset_list.map(&:downcase)) 
            count = count + 1
          end
        rescue Exception => e
          Rails.logger.error "Error occured while updating assets for Company collection:-  #{e.backtrace.to_s}"
          puts "Error occured while updating asset_list for Company collection:-  #{e.backtrace.to_s}"  
        end
      end
      Rails.logger.info "Total updated asset_list for Company collection:- #{count}" 
      puts "Total updated asset_list for Company collection:- #{count}"      
    end

    def self.downcase_graph_assets
      Rails.logger.info "Starting operation for Graph collection"
      puts "Starting operation for Graph collection"
      graphs = Graph.all
      count = 0
      graphs.each do |graph|
        begin
          assets = graph.assets
          unless assets.blank?
            graph.update_attribute(:assets, assets.map(&:downcase)) 
            count = count + 1
          end
        rescue Exception => e
          Rails.logger.error "Error occured while updating assets for Graph collection:-  #{e.backtrace.to_s}"
          puts "Error occured while updating assets for Graph collection:-  #{e.backtrace.to_s}"  
        end
      end
      Rails.logger.info "Total updated assets for Graph collection:- #{count}" 
      puts "Total updated assets for Graph collection:- #{count}"      
    end

end


namespace :db do  
  desc "This task would update all asset guids into lowercase for number of collections"
  task :downcase_all_asset_guid => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_req_device_guid
      Util.downcase_group_asset_list
      Util.downcase_event_subscription_asset_list
      Util.downcase_company_asset_list
      Util.downcase_graph_assets
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  task :downcase_req_device_guid => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_req_device_guid
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  task :downcase_group_asset_list => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_group_asset_list
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  task :downcase_event_subscription_asset_list => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_event_subscription_asset_list
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  task :downcase_company_asset_list => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_company_asset_list
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  task :downcase_graph_assets => :environment do
    begin
      Rake::Task[:environment].invoke
      Util.downcase_graph_assets
    rescue
      Rails.logger.error $!
      raise
    end     
  end

end 