namespace :db do
  desc "load backup report for current month for backup companies only"
  task :load_backup_report => :environment do
    p "======Starting backup Report======="
    ct = Time.now
    file = "#{Rails.root}/db/data/import/backup_report_companies.csv"
    CSV.foreach(file, :headers => true, encoding: "bom|utf-8") do |row|
      company_guid = row.to_hash['Guid'].to_s
      company_name = row.to_hash['Name'].to_s
      company = company_guid.blank? ? nil : Company.find_by_guid(company_guid)
      if company.present?
        begin
          exsting_backups = Ehealth::BackupReport.where(company_guid: company_guid).all
          current_month_backup = exsting_backups.select{|backup| backup if backup.date.to_date >= Time.now.beginning_of_month.to_date}
          current_month_backup.map(&:delete) unless current_month_backup.empty?
          t = Ehealth::BackupReport.load_backup_data(company_guid)
          p "Total Backup record found for #{company_guid}:- #{t.count}"
        rescue Exception=> e
          puts "*****************#{e.message}**** error while getting bacup data for company guid = #{company_guid}  #{company_name}"
        end
      end
    end
    p "--Total Time taken for backup report is #{Time.now-ct}.--"
  end

  desc "load_previous_months_backup_data_for_company"
  task :load_previous_months_backup_data_for_company => :environment do
    ct = Time.now
    company_guid = ENV["company_guid"]
    p "Loading Backup for company - #{company_guid}"
    exsting_backups = Ehealth::BackupReport.where(company_guid: company_guid).all
    exsting_backups.map(&:delete) unless exsting_backups.blank?
    begin
      t = Ehealth::BackupReport.load_backup_data(company_guid,true)
      p "Total Backup record found :- #{t.count}"
    rescue Exception => e
      puts "*****************#{e.message}**** error while getting bacup data for company guid = #{company_guid}  #{company["name"]}"
    end
    p "--Total Time taken for backup report is #{Time.now-ct}.--"
  end

  desc "load backup data for all companies"
  task :load_previous_months_backup_data_for_all_company => :environment do
    Ehealth::BackupReport.delete_all
    count = 0
    file = "#{Rails.root}/db/data/import/backup_report_companies.csv"
    CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
      company_guid = row.to_hash['Guid'].to_s
      company_name = row.to_hash['Name'].to_s
      company = company_guid.blank? ? nil : Company.find_by_guid(company_guid)
      if company.present?
        begin
          count = count + 1
          t = Ehealth::BackupReport.load_backup_data(company_guid,true)
          p "*******Backup added for total number of companies: #{count} current company with guid: #{company_guid} have #{t.count} backups"
        rescue Exception => e
          puts "********#{company_guid}*********#{e.message}**** error while getting bacup data for company guid = #{company_guid}  #{company_name}"
        end
      end
    end
  end

  desc "clean ehealth data"
  task :clean_ehealth_data => :environment do
    Ehealth::ActiveAsset.delete_all
    Ehealth::Ticketing.delete_all
    Ehealth::BackupReport.delete_all
  end
end
