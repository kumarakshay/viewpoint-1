namespace :db do  
  desc "Create admin users"
  task :create_admin_users => :environment do
    created, total = 0,0
    Rake::Task[:environment].invoke
    Rails.logger.info "Starting operation"
    # companies = Company.where(name: /sungard/i).all
    companies = Company.all
    companies.each do |c|
      u = User.where(company_guid: c.guid).count
      if u == 0
        email = "viewpoint.administrator-#{c.guid}@sungardas.com"
        puts "new user required for #{c.name}-#{c.guid}"        
        u = User.create(
           :active => true, 
           :company_guid => c.guid, 
           :company_name => c.name, 
           :email => email, 
           :first_name => 'fname', 
           :last_name => 'lname')
        if u.save!
          created +=1
        end
      end
    end
    Rails.logger.info "Total company count #{companies.count}"
    Rails.logger.info("Records created           - #{created}")
  end
end