class BulkLoadMailer < ActionMailer::Base
  default :from => 'noreply@sungardasas.com'
  def send_bulk_load_mail(model, model_count, worker_count, diference)
    body = "After three attempts, bulk load has continued to fail. Details as belows -
      Time - #{Time.now}
      Environment - #{Rails.env}
      Counts - Before bulk load #{model.to_s} records count: #{model_count}.
                  After bulk load #{model.to_s} records received via web service: #{worker_count} 
      Total records difference: #{diference}
      
    Please initiate bulk load manually.

    Thanks,
    OMS Tools Team."

    to = ::Receiver.all.collect(&:email).join(",")
    mail(:to => to,
         :subject => "IMPORTANT MUST READ #{model.to_s} bulk load fail (#{Rails.env})", :body => body)
  end
end

namespace :load do

  task :sanity_check  => :environment do
    unless ENV['sanity_check'].blank?
      if ENV['sanity_check'] == "true"
        APP_CONFIG['bulk_load_sanity_check'] = true
      else
        APP_CONFIG['bulk_load_sanity_check'] = false
      end
    end
  end

  desc "Test rake task"
  task :init_start => :sanity_check do
    p "------Rake task is working fine-------"
  end


  desc "run bulk load"
  task :bulk_load => :environment do
    Worker::BulkLoad.run
  end
   

  desc "Load Company"
  task :company => :sanity_check do
    Worker::CompanyWorker.build_new_collection
  end
   
  desc "Load Device"
  task :device => :sanity_check do
    Worker::DeviceWorker.build_new_collection
  end

  desc "Load command"
  task :command => :sanity_check do
    Worker::CommandRunnerWorker.build_new_collection(Device)
  end

  desc "Load Interface"
  task :interface => :sanity_check do
    Worker::InterfaceWorker.build_new_collection
  end
  
  desc "Load PortalMap"
  task :portal_map => :sanity_check do
    Worker::RawPortalMapWorker.build_new_collection
  end 

  desc "Load all"
  task :all => :sanity_check do
    Worker::RawPortalMapWorker.build_new_collection
    Worker::CompanyWorker.build_new_collection
    Worker::DeviceWorker.build_new_collection
    Worker::InterfaceWorker.build_new_collection
    Worker::PowerWorker.build_new_collection
  end

  desc "Load DeviceDetailReportWorker" 
  task :device_detail_report_worker => :sanity_check do
    Worker::DeviceDetailReportWorker.bulk_load
  end

  desc "Load Power"
  task :power => :sanity_check do
    Worker::PowerWorker.build_new_collection
  end

  desc "Update Production IP from Spotlight"
  task :production_ip_hotfix, [:company_guid] => :environment do |t, args|
    if args[:company_guid]
      Worker::ProductionIpHotfixWorker.update_device_records(args[:company_guid])
    else
      Worker::ProductionIpHotfixWorker.update_device_records
    end
  end

  desc "load bulk_load notification receivers for dev environment"
  task :receivers_for_dev => :environment do
    create_receivers(["sunny.bogawat@sungardas.com","amit.gijare@sungardas.com"])
  end

  desc "load bulk_load notification receivers for qa environment"
  task :receivers_for_qa => :environment do
    create_receivers(["sunny.bogawat@sungardas.com","amit.gijare@sungardas.com"])
  end

  desc "load bulk_load notification receivers for staging environment"
  task :receivers_for_stage => :environment do
    create_receivers(['parvez.pathan@sungardas.com','sunny.bogawat@sungardas.com',
                      'amit.gijare@sungardas.com','matt.pazaryna@sungardasas.com'
                      ])
  end

  desc "load bulk_load notification receivers for production environment"
  task :receivers_for_prod => :environment do
    create_receivers(["sunny.bogawat@sungardas.com","amit.gijare@sungardas.com",
                      "matt.pazaryna@sungardas.com","parvez.pathan@sungardas.com",
                      "dl.oss.operations@sungardas.com"])
  end

  def create_receivers(emails)
    Receiver.destroy_all
    emails.each do |email|
      begin
        r = Receiver.new(:email => email)
        r.save
      rescue Exception => e
        Rails.logger.error "Error occured while creating receiver- #{e.backtrace.to_s}"
      end
    end
  end
end
