namespace :db do
  desc "This task would delete all subscriptions for users who are inactive viewpoint acccount"
  task :delete_subscription_for_inactive_users => :environment do
    begin
      user_emails = []
      Rake::Task[:environment].invoke
      puts "Starting operation"
      subscriptions = Req.where(subscription: true).group_by(&:email)
      subscriptions.each do |email, records|
        begin
          user = User.find_by_email(email)
          if !user.present? || (!user.active? && user.deleted_at.present?)
            user_emails.push(email)
            records.map(&:delete) 
          end
        rescue Exception => e
          Rails.logger.error "Error occured while deleting subscriptions of user -  #{e.backtrace.to_s}" 
        end
      end
      puts "inactive_user_count - #{user_emails.length}**************user_emails - #{user_emails}"
    rescue
      Rails.logger.error $!
      raise
    end
  end
end