namespace :db do
  desc "This task would clean group records and remove devices are not there in zenoss"
  task :clean_groups => :environment do
    begin
      Rake::Task[:environment].invoke
      puts "Starting to clean groups"
      Rails.logger.info "Starting to clean groups"
      cnt, error = 0,0
      Group.active.each do |grp|
        company = Company.where(guid: grp.company_guid).first
        unless company.blank?
          org_assets = company.asset_list & grp.asset_list
          new_assets =[]
          model = company.is_omi? && !APP_CONFIG['vp_cauldron'] ? OmiDevice : Device
          devices = model.where(:asset_guid => org_assets).all
          begin
            unless devices.blank?
              new_assets = devices.inject([]){|a,v| a << v.asset_guid}
              grp.asset_list = new_assets
              grp.save!
              cnt += 1
            end
          rescue Exception => e
            error += 1
            Rails.logger.error "Error occured while updating group #{grp.id} \n #{e.backtrace.to_s}"
          end
        end
      end
      puts "Updated #{cnt} groups had errors for #{error} groups"
      Rails.logger.info "Updated #{cnt} groups had errors for #{error} groups"
    rescue
      Rails.logger.error $!
      raise
    end
  end

  desc "This task would find all faux users without guid and assign them a new guid"
  task :set_guid_for_faux_user => :environment do
    begin
      Rake::Task[:environment].invoke
      cnt,error = 0,0
      users_without_guid = []
      vp_users =  User.where('email' => /viewpoint.administrator-/).all
      vp_users.each do |user|
        if user.guid.blank?
          begin
            uuid = UUID.new
            z = uuid.generate
            user.guid = z
            user.save!
            cnt += 1
            p "user #{user.email} updated with guid #{user.guid}"
          rescue Exception => e
            error += 1
            users_without_guid << user.email
            Rails.logger.error "Error occured while updating user #{user.email} \n #{e.backtrace.to_s}"
          end
        end
      end
      puts "Total #{cnt} users updated successfully"
      puts "Total #{error} faulty users #{users_without_guid}"
    rescue
      Rails.logger.error $!
      raise
    end
  end
end
