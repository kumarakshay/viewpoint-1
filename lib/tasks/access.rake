namespace :db do
  desc "This task will import hosting data center companies into access table for restricting SN access"
  task :import_csv_companies => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting to import csv data"
      count = 0
      Access.destroy_all
      path = "#{Rails.root}/db/data/import"
      file = "#{path}/vXchange_customers.csv"
      CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
        params = row.to_hash
        dat = Access.new({:company_name =>params["Name"],:company_guid => params["MS Portal GUID"],:access_code =>["VUPT-3761"]})
        begin
          dat.save
          count += 1
        rescue Exception => e
          Rails.logger.error "Error occured while adding company #{row[0]} -  #{e.backtrace.to_s}"
        end
      end
      puts "Finished importing of #{count} companies"
    rescue
      Rails.logger.error $!
      raise
    end
  end
end
