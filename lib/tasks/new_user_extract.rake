# This is a utility script that is running on a cron
class UserMailer < ActionMailer::Base
  default :from => "matt.pazaryna@sungardas.com"
  def welcome_email(email, body)
    mail(:to => email, :subject => "New Users list", :body => body)
  end
end

namespace :admin do  
  desc "email new user list"
  task :export_users => :environment do
    audits = User.where(:created_at => {'$gt'=> 1.days.ago.midnight,'$lt'=> Time.now }).order(:created_at.asc).all
    header = 'created_at,email,company_name,company_guid,first_name,last_name'
    body = (header + "\n")
    audits.each do |a| 
      body << "#{a.created_at},#{a.email},#{a.company_name},#{a.company_guid},#{a.first_name},#{a.last_name}\n" 
    end
    UserMailer.welcome_email("matt.pazaryna@sungardas.com", body).deliver
    UserMailer.welcome_email("wadiya.wynn@sungardas.com", body).deliver
    UserMailer.welcome_email("david.wofford@sungardas.com", body).deliver
  end
end