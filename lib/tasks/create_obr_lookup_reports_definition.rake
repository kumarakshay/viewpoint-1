namespace :db do
  desc "This task would create default OBR reports"
  task :create_obr_lookup_reports_definition => :environment do
    begin
      Rake::Task[:environment].invoke
      StatementObr.destroy_all
      data =  ActiveSupport::JSON.decode(File.read(Rails.root+"db/data/obr/#{Rails.env}_obr.json")) if defined?(data)
      data.each do |row|
        begin
          report = StatementObr.new(row)
          report.save
          puts "Created report '#{report.name}' successfully"
        rescue Exception => e
          Rails.logger.error "Error occured while creating custom report-  #{e.backtrace.to_s}"
        end
      end
    rescue
      Rails.logger.error $!
      raise
    end
  end
end
