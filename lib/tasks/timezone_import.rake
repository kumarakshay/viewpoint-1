namespace :db do  
  desc "This task would create iana time zone on mongo database"
  task :modify_timezones => :environment do
    puts "Starting adding IANA attribute to existing timezones"
    Rails.logger.info "Starting to import timezones in IANA format"
    count = 0
    total_timezones = Timezone.all.count
    missing_values = []
    begin 
      sc_timezones = {
        "(GMT - 05:00) Eastern Time (US & Canada)" => 'EST5EDT', 
        "(GMT - 06:00) Central Time (US & Canada)" => 'CST6CDT', 
        "(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London" => 'GMT',  
        "(GMT + 05:30) Calcutta, Chennai, Mumbai, New Delhi" => 'Asia/Calcutta', 
        "(GMT - 07:00) Arizona" => 'America/Phoenix', 
        "(GMT - 07:00) Mountain Time (US & Canada)" => 'MST7MDT', 
        "(GMT - 06:00) Central America" => 'CST6CDT', 
        "(GMT - 08:00) Pacific Time (US & Canada); Tijuana" => 'PST8PDT', 
        "(GMT + 01:00) Brussels, Copenhagen, Madrid, Paris" => 'Europe/Paris', 
        "(GMT + 01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna" => 'Europe/Vatican', 
        "(GMT - 09:00) Alaska" => 'America/Anchorage', 
        "(GMT - 10:00) Hawaii" => 'US/Hawaii', 
        "(GMT) Casablanca, Monrovia" => 'GMT', 
        "(GMT + 03:00) Moscow, St. Petersburg, Volgograd" => 'Europe/Moscow', 
        "(GMT - 03:00) Brasilia" => 'America/Sao_Paulo', 
        "Central Time (US & Canada)" => 'CST6CDT', 
        "(GMT - 04:00) Atlantic Time (Canada)" => 'America/Thule', 
        "(GMT + 01:00) Belgrade, Bratislava, Budepest, Ljubljana, Prague" => 'Europe/Bratislava', 
        "(GMT - 06:00) Saskatchewan" => 'America/Regina', 
        "(GMT + 10:00) Canberra, Melbourne, Sydney" => 'Australia/Sydney', 
        "(GMT + 08:00) Kuala Lumpur, Singapore" => 'Etc/GMT-8', 
        "(GMT + 05:00) Islmabad, Karachi, Tashkent" => 'Etc/GMT-5', 
        "(GMT - 05:00) Indiana (East)" => 'America/Indianapolis', 
        "(GMT - 05:00) Bogota, Lima, Quito" => 'Etc/GMT+5', 
        "(GMT - 04:00) Caracas, La Paz" => 'Etc/GMT+4', 
        "Eastern Time (US & Canada)" => 'EST5EDT', 
        "(GMT + 02:00) Helsinki, Riga, Tallinn" => 'Europe/Kiev', 
        "(GMT + 02:00) Jerusalem" => 'Asia/Jerusalem', 
        "Alaska"  => 'America/Anchorage', 
        "(GMT + 07:00) Bangkok, Hanoi, Jakarta" => 'Etc/GMT-7', 
        "(GMT + 08:00) Beijing, Chongging, Hong Kong, Urumqi" => 'Asia/Macau', 
        "(GMT + 02:00) Athens, Istanbul, Minsk" => 'Europe/Bucharest', 
        "(GMT - 04:00) Santiago" => 'America/Santiago', 
        "(GMT - 03:30) Newfoundland" => 'America/St_Johns', 
        "(GMT + 11:00) Magadan, Solomon Is., New Caledonia" => 'Etc/GMT-11', 
        "(GMT + 02:00) Cairo" => 'Africa/Cairo', 
        "(GMT + 05:45) Kathmandu" => 'Asia/Katmandu', 
        "(GMT - 01:00) Azores" => 'Atlantic/Azores', 
        "American Samoa" => 'Pacific/Apia', 
        "(GMT + 01:00) Sarajevo, Skopje, Sofija, Vlnius, Warsaw, Zagreb" => 'Europe/Warsaw', 
        "(GMT + 02:00) Harare, Pretoria" => 'Etc/GMT-2', 
        "(GMT + 12:00) Auckland, Wellington" => 'Pacific/Auckland', 
        "(GMT + 08:00) Perth" => 'Australia/Perth', 
        "(GMT + 06:00) Astana, Dhaka" => 'Etc/GMT-6', 
        "(GMT - 02:00) Mid Atlantic" => 'Etc/GMT+2',
        "(GMT + 08:00) Irkutsk, Ulaan Bataar" =>'Asia/Irkutsk',
        "America/Los_Angeles" => 'PST8PDT',
        "(GMT + 04:00) Abu Dhabi, Muscat" => 'Asia/Muscat',
        "(GMT + 09:00) Seoul" => 'Asia/Seoul',
        "(GMT + 09:00) Osaka, Sapporo, Tokyo" => 'Asia/Tokyo',
        "(GMT + 10:00) Vladivostok" => 'Asia/Vladivostok',
        "Bogota" => "America/Bogota",
        "Hawaii" => "US/Hawaii",
        "Mexico City" => "America/Mexico_City",
        "Midway Island" => "Pacific/Midway",
        "(GMT + 12:00) Fiji, Kamchatka, Marshall Is." => "Pacific/Fiji",
        "International Date Line West" => 'UTC'
      }    	
      Rake::Task[:environment].invoke
      Timezone.all.each do |tz|
        if sc_timezones.include?(tz.label)
          begin
            tz.iana_value = sc_timezones[tz.label]
            tz.save!
            count = count + 1            
          rescue Exception => e            
            missing_values << tz.label
            puts "Unable to save iana specific value for #{tz.label}"
            Rails.logger.error "Unable to save iana specific value for #{tz.label} \nBacktrace:\n\t#{e.backtrace.join("\n\t")}"
          end
        else
          missing_values << tz.label
          puts "Unable to find iana specific value for #{tz.label}"
        end
      end
    rescue
      Rails.logger.error $!
      raise
    end
    unless missing_values.empty?
      Rails.logger.info "Unable to find timezone information for #{missing_values.join(',')}"
      Rails.logger.info "Unable to update  #{total_timezones - count}  records"
    end
    Rails.logger.info "Finished updating timezones. Total records updated #{count}"
    puts "Finished importing timezones. Total records imported #{count}"
  end 

  desc "This task would generate new timezone table with iana values, label and value"
  task :populate_timezone_collection => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      Timezone.delete_all
      @records =   JSON.parse(File.read("#{Rails.root}/db/data/import/timezones.json"))
      @records.each do |record|
       time_zone = Timezone.new(record)
       time_zone.save!
    end
      puts "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
      puts "Total timezone imported #{Timezone.all.count}"
      puts"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
    rescue
      Rails.logger.error $!
      raise
    end
  end
end