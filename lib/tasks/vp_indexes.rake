namespace :db do  
  desc "This task would create indexes on mongo database"
  task :create_vp_indexes => :environment do
    puts "Starting to create indexes"
    Rails.logger.info "Starting to create indexes"
    begin
      Rake::Task[:environment].invoke
      puts  "Created index in User.collection #{User.collection.create_index([['email',  Mongo::ASCENDING],['deleted_at',  Mongo::ASCENDING]])}"
      puts  "Created index in User.collection #{User.collection.create_index([['company_guid',  Mongo::ASCENDING], ['deleted_at',  Mongo::ASCENDING]])}"
      puts  "Created index in Device.collection #{Device.collection.create_index([['asset_guid',  Mongo::ASCENDING]])}"
      puts  "Created index in Company.collection #{Company.collection.create_index([['guid',  Mongo::ASCENDING]])}"
      puts  "Created index in PortalMapWorker.collection #{PortalMapWorker.collection.create_index([['parent_company_snt',  Mongo::ASCENDING]])}"
    rescue
      Rails.logger.error $!
      raise
    end
    Rails.logger.info "Finished creating indexes"
    puts "Finished creating indexes"
  end 
end
