namespace :db do  
  desc "This task would create roles"
  task :create_roles => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting to create role"
      Role.destroy_all
      names = {'viewpoint_admin' => ['Viewpoint Admin','viewpoint_admin','OSSTools-Viewpoint-Viewpoint Admin'],
               'sungard_admin' => ['Sungard Admin','sungard_admin','OSSTools-Viewpoint-Sungard Admin'],
               'company_admin' => ['Company Admin','company_admin','OSSTools-Viewpoint-Company Admin'],
               'infoblox_dns_customer_reporter' => ['DNS Customer Reporter','dns_customer_reporter','OSSTools-Viewpoint-DNS-Customer Reporter'],
               'infoblox_dns_customer_engineer' => ['DNS Customer Engineer','dns_customer_engineer','OSSTools-Viewpoint-DNS-Customer Engineer'],
               'switch_company' => ['Switch Company','switch_company','OSSTools-Viewpoint-Switch Company'],
               'firewall_viewer' => ['Firewall Viewer', 'firewall_viewer','OSSTools-Viewpoint-Firewall Viewer'],
               'command_runner_show_command' => ['Command Runner Show Command ', 'command_runner_show_command', 'OSSTools-Viewpoint-Command-Runner-Show Command'],
               'command_runner_change_command' => ['Command Runner Change Command', 'command_runner_change_command', 'OSSTools-Viewpoint-Command-Runner-Change Command'],
               'view_billing_and_contracting' => ['View Billing and Contracting', 'view_billing_and_contracting', 'OSSTools-Viewpoint-View Billing And Contracting'],
               'hpc_network_admin' => ['HPC Network Admin', 'hpc_network_admin', 'OSSTools-Viewpoint-HPC-Network Admin'],
               'hpc_customer_admin' => ['HPC Customer Admin', 'hpc_customer_admin', 'OSSTools-Viewpoint-HPC-Customer Admin']
               }
      names.each do |name, desc|
        role = Role.new(:name => name, :display_name => desc[0], :description => desc[1], :sso_mapped_name => desc[2])
        begin
          role.save
          puts "Created role '#{role.name}'"
        rescue Exception => e
          Rails.logger.error "Error occured while creating role -  #{e.backtrace.to_s}"
        end
      end
    rescue
      Rails.logger.error $!
      raise
    end
  end

  desc "Update role from Firewall Change Command to Command Runner Chnage Command"
  task :correct_firewall_change_command_role => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting to update firewall_change_command role"
      count = 0;
      User.all.each do |user|
        unless user.is_sungard?
          user_roles = user.roles
          if user_roles.include?("firewall_change_command")
            user.roles = user_roles.map!{ |x| x == "firewall_change_command" ? 'command_runner_change_command' : x }
            user.save!
            count = count + 1;
          end
        end
      end
      puts "total user update --- #{count}"
    rescue
      Rails.logger.error $!
      raise
    end
  end

  desc "One time task to give admin right"
  task :set_admin_roles => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"

      sungard_admins = [ "anna.weyerman", "valarie.barros", "cesar.bejarano", "xande.craveiro-lopes",
                         "alanta.mcclendon ", "ryan.romswinckel", "brian.cartwright", "megan.strohfus",
                         "brandon.bohyer ", "jane.mcclure ", "robert.monroe ", "shane.erickson ",
                         "aaron.smith-pearson ", "cameron.hopkin ", "claire.steir ", "joshua.colky ",
                         "mari.eastin ", "tammy.burns", "damon.do",
                         "margaret.lucchesi", "debi.kahn", "john.wild", "teri.dugan", "michele.small ",
                         "ronnie.chambers", "wadiya.wynn", "gary.clark", "david.wofford", "divya.warble",
                         "jo.burch", "jason.jordan", "paul.ferguson", "patrick.bays", "thomas.blevins",
                         "dewey.garwood", "david.finley", "jerry.clingan ", "joseph.bradly", "kevin.harlow ",
                         "scott.schaffer ", "kevin.mcgrath", "will.mccracken", "damien.stuart", "matt.pazaryna"]
      viewpoint_admin = ["kevin.mcgrath", "matt.pazaryna", "wadiya.wynn" ]

      sungard_admins.each do |usr|
        begin
          email ="#{usr}@sungardas.com"
          user = User.first(:email => { :$regex => /^#{email}$/i })
                                        if user.blank?
                                          puts "Unable to find user #{usr} "
                                        else
                                          user.roles << 'company_admin'  unless user.role?('sungard_admin')
                                          if viewpoint_admin.include?(usr)
                                            user.roles << 'viewpoint_admin' unless user.role?('viewpoint_admin')
                                          end
                                          user.save
                                          puts "Updated #{user.email} with roles => #{user.roles}"
                                        end
                                        rescue Exception => e
                                          Rails.logger.error "Error occured while updating user - #{usr} #{e.backtrace.to_s}"
                                        end
                                        end
                                        rescue
                                          Rails.logger.error $!
                                          raise
                                        end
                                        end

                                        desc "Assign switch company role to a company admin if company has child"
                                        task :assign_switch_company_role => :environment do
                                          begin
                                            Rake::Task[:environment].invoke
                                            Rails.logger.info "Starting to assign switch_company to company_admin"
                                            snts = PortalMapWorker.distinct(:parent_company_snt)
                                            snts.each do |snt|
                                              #break if s_count == 2
                                              company = Company.where(:snt => snt).fields('guid','name').first
                                              #print "found company #{company.inspect} \n"
                                              next if company.blank?
                                              admins = User.where(:company_guid => company.guid).where(:roles =>{:$in => ['company_admin']})
                                              count = 0
                                              admins.each do |admin|
                                                unless admin.role?('switch_company')
                                                  admin.roles << 'switch_company'
                                                  begin
                                                    admin.save
                                                    count += 1
                                                  rescue Exception => e
                                                    Rails.logger.error $!
                                                  end
                                                end
                                              end
                                              puts "Assgined rights to  #{count} user/s in company #{company.name}"
                                            end
                                          rescue
                                            Rails.logger.error $!
                                            raise
                                          end
                                        end
                                        end
