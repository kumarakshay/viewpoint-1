require 'sgtools/pyxie/access_control/base'

module SGTools
  module Pyxie
    module AccessControl
      module Viewpoint
        module Helpers
          include AccessControl::Base::Helpers

          def authenticate!
            session_data = request.cookies['_sgmc_app_session']

            if params['token'] == 'ccd5995f61224fae48f78fcb5a17a82d'
              @user_roles = ['super_admin']
              @user_acl_ids = [params["company_guid"]]
            elsif session_data
              vp_session_data, digest = session_data.split("--")
              vp_session_data = nil  unless digest == OpenSSL::HMAC.hexdigest(OpenSSL::Digest::SHA1.new, '58c61e1be95fe00340fa0e6815dbeac5f3e2424a2958172c3991b71fe04e3cedc7c1efc933350e8d4e4e91468afe1a78119cbe8e7dea79eb1636e28542108da1', vp_session_data)

              begin
                vp_session_data = vp_session_data.unpack("m*").first
                decrypt_data = Marshal.load(vp_session_data)
                logger.debug("Decrypted Cookie: #{decrypt_data}")
                vp_session_data = decrypt_data
                
                user = User.find(vp_session_data['operator_guid'])
                company_guid = user.company_guid                
                unless vp_session_data['company_mas_guid'].blank?
                  company_guid = vp_session_data['company_mas_guid'] 
                end
                @user_acl_ids = [user.id.to_s, user.guid, company_guid.downcase, company_guid]
                logger.debug("User ACL IDs: #{@user_acl_ids.inspect}")
                @user_roles = []
              
              rescue Exception => e
                logger.error(e)
                halt 401, "Access Denied"
              end
            else
              halt 401, "Access Denied"
            end
          end

          def protected!(permission, *options)
            super(permission, *options) do
              puts @user_roles.inspect
              if @user_roles.include?('super_admin')
                true
              else 
                send("#{permission}?",*options) 
              end
            end
          end

          def list_buckets?
            true
          end

          def bucket_read?(bucket)
            if (bucket && bucket['acl'] && bucket['acl']['read'] && bucket['acl']['read'].is_a?(Array))
              match = (bucket['acl']['read'] & @user_acl_ids)
              return (!match.nil? and match.count > 0)
            end
            return false
          end

          def object_read?(bucket, grid_file)
            ok = bucket_read?(bucket)
            return ok if ok
            #Check object ACL
            # TODO
            return false
          end

          def object_insert?(bucket)
            if (bucket && bucket['acl'] && bucket['acl']['write'] && bucket['acl']['write'].is_a?(Array))
              return !(bucket['acl']['write'] & @user_acl_ids).nil?
            end
            return false 
          end

          def object_update?(bucket, grid_file)
            if (bucket && bucket['acl'] && bucket['acl']['write'] && bucket['acl']['write'].is_a?(Array))
              return !(bucket['acl']['write'] & @user_acl_ids).nil?
            end
            return false 
          end

        end

        def self.registered(app)
          app.helpers SGTools::Pyxie::AccessControl::Viewpoint::Helpers

          app.before_list_buckets_bucket do |bucket|
            bucket.delete('acl')
          end

        end
      end
    end
  end
end
