module MyOpenid
  class << self

  def get_open_id_url_for(api_url)
     APP_CONFIG['open_id_base_url'] + api_url
  end  

  # Registers new user with openid.  
  def register_user(params, current_user)
    data = register_user_data(params, current_user)
    create_url = get_open_id_url_for(APP_CONFIG['open_id_api_url'])
    res = access_openid(create_url, data)
  end

  # Preparation of form data used for user registration at open-id.
  # Any data expecting to go to OpenID must be set here.
  def register_user_data(params, current_user)
    Rails.logger.add_metadata(new_user_guid: "#{current_user.guid}") if Rails.logger.respond_to?(:add_metadata)
    data = {'user[email]' => params["user"]['email'],
    'user[active]' => params["user"]["active"],
    'user[company_name]' => "#{current_user.company_name}",
    'user[company_guid]' => "#{current_user.company_guid}",
    'user[guid]' => "#{current_user.guid}",
    'firstname' => params['user']['first_name'],
    'lastname' => params['user']['last_name'],
    'timezone' => "#{current_user.time_zone}",    
    'telephone' => params['user']['phone_number'],
    'auth_token' => APP_CONFIG['open_id_auth_token'] }
    data
  end
  
  # Confirm an user with openid.
  def openid_confirm(user)
    confirmation_url = get_open_id_url_for(APP_CONFIG['open_id_confirmation_url'])
    data = set_confirmation_data(user)
    res = access_openid(confirmation_url, data)
  end
  
  # Calls openid with a post request for given set of data.
  def access_openid(site_url, data)
    uri = URI.parse(site_url)
    req = Net::HTTP::Post.new(uri.path)
    req.set_form_data(data)
    fire_request(uri,req,data)
  end

  # Preparation of form data used for user confirmation at open-id.
  # User password and confirmation token finds itself as important attribute of the packet.
  def set_confirmation_data(user)
    data = {'user[password]' => user.password,
    'user[password_confirmation]' => user.password_confirmation,
    'user[confirmation_token]' => user.confirmation_token,
    'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    data
  end

  # Request is made to openid for issuing a reset token to user with supported email.
  def send_password_reset(email)
    reset_url = get_open_id_url_for(APP_CONFIG['open_id_reset_url'])
    data = reset_data(email)
    res = access_openid(reset_url, data)
  end

  # Preparation of form data used for having a reset token from open-id.
  # User email finds itself as important attribute of the packet.
  def reset_data(email)
    data = { 'user[email]' => email,
      'auth_token' => APP_CONFIG['open_id_auth_token']
    }
  end

  # Confirmation of reset password for user is communicated with openid.
  def confirm_password_reset(user)
    reset_url = get_open_id_url_for(APP_CONFIG['open_id_reset_url'])
    data = confirm_reset_data(user)
    res = access_openid_update_data(reset_url, data)
  end

  # Preparation of form data used for reset user password.
  # User password and reset token finds itself as important attribute of the packet.
  def confirm_reset_data(user)
    data = {
      'user[reset_password_token]' => user.reset_password_token,
      'user[password]' => user.password,
      'user[password_confirmation]' => user.password_confirmation,
      'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    data
  end
  
  # updates an user data with openid. 
  def update_user(params, user)
    data = update_user_data(params, user)
    update_url = get_open_id_url_for(APP_CONFIG['open_id_update_url'])
    res = access_openid_update_data(update_url, data)
  end

  # Preparation of form data used for updating an user. 
  def update_user_data(params, user)
    data = {'user[email]' => "#{user.email}",
    'user[active]' => params["user"]["active"],
    'user[company_name]' => "#{user.company_name}",
    'user[company_guid]' => "#{user.company_guid}",
    'persona[company_name]' => "#{user.company_name}",
    'persona[company_guid]' => "#{user.company_guid}",
    'persona[firstname]' => params['user']['first_name'],
    'persona[lastname]' => params['user']['last_name'],
    'persona[timezone]' => params['user']['time_zone'],
    'persona[telephone]' => params['user']['phone_number'],
    'auth_token' => APP_CONFIG['open_id_auth_token'] }
    data
  end
   
  def change_password(user)
    data = change_password_data(user)
    change_password_url = get_open_id_url_for(APP_CONFIG['open_id_api_url'])
    res = access_openid_update_data(change_password_url, data)
  end

  def change_password_data(user)
    data = {
    'user[email]' => user.email,
    'user[password]' => user.password,
    'user[password_confirmation]' => user.password_confirmation,
    'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    data
  end

  def change_email(user)
    data = data = {
    'user[email]' => user.email,
    'user[new_email]' => user.pending_email,
    'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    change_email_url = get_open_id_url_for(APP_CONFIG['open_id_change_email_url'])
    res = access_openid_update_data(change_email_url, data)
  end

  def confirm_email(user)
    data = data = {
    'user[email]' => user.email,
    'user[new_email]' => user.pending_email,
    'user[confirmation_token]' => user.confirmation_token,
    'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    confirm_email_url = get_open_id_url_for(APP_CONFIG['open_id_confirm_email_url'])
    res = access_openid(confirm_email_url, data)
  end

  # Create a put request sent across to openid
  def access_openid_update_data(site_url, data)
    uri = URI.parse(site_url)
    req = Net::HTTP::Put.new(uri.path)
    req.set_form_data(data)
    fire_request(uri,req,data)
  end
  
  # Makes and actual request to openid server. 
  def fire_request(uri, req, data)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme =='https'
      pem = File.read("#{Rails.root}/config/ca-certificates.crt")
      http.use_ssl = true
      http.cert = OpenSSL::X509::Certificate.new(pem)      
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER      
    end
    res = http.start do |httpvar|      
      httpvar.request(req)
    end
    Rails.logger.add_metadata(request_to: "#{uri.to_s}") if Rails.logger.respond_to?(:add_metadata)
    #Rails.logger.add_metadata(request_data: "#{data}") if Rails.logger.respond_to?(:add_metadata)
    Rails.logger.add_metadata(request_response_type: "#{res}") if Rails.logger.respond_to?(:add_metadata)
    Rails.logger.add_metadata(request_response_body: "#{res.body}") if Rails.logger.respond_to?(:add_metadata)
    res
  end
  
  # When blank time_zone is defaulted to US/Eastern.
  def timezone(time_zone)
    if time_zone.blank?
      time_zone = '(GMT - 05:00) Eastern Time (US &amp; Canada)'
      # time_zone = ''
    end
    time_zone
  end
  
  # Unlocks user in openid by setting failed_attempts and locked_at attributes to none
  def unlock_user(email)
    data = {
      'user[email]' => email,
      'user[failed_attempts]' => 0,
      'user[locked_at]' => nil,
      'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    update_url = get_open_id_url_for(APP_CONFIG['open_id_update_url'])
    res = access_openid_update_data(update_url, data)
    res    
  end

  def delete_company(company_guid)
    data = {'auth_token' => APP_CONFIG['open_id_auth_token'],:company_guid => company_guid }
    openid_delete_url = get_open_id_url_for(APP_CONFIG['open_id_delete_url'])
    uri = URI.parse(openid_delete_url)
    req = Net::HTTP::Delete.new(uri.path)
    req.set_form_data(data)
    fire_request(uri,req,data)
  end

  # TODO - replace this with the actual call 
  def user_profile(email)
    openid_get_url = get_open_id_url_for(APP_CONFIG['open_id_get_url'])
    data = {'auth_token' => APP_CONFIG['open_id_auth_token'], :email => email}
    uri = URI.parse(openid_get_url)
    req = Net::HTTP::Get.new(uri.path)
    req.set_form_data(data)
    res = fire_request(uri,req,data)
    Rails.logger.add_metadata(get_uri: "#{openid_get_url}") if Rails.logger.respond_to?(:add_metadata)
    Rails.logger.add_metadata(get_data: "#{data}") if Rails.logger.respond_to?(:add_metadata)
    # res = User.first.to_json
    res = fire_request(uri,req,data)
  end

  def get_user(email)
    data = {
      'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    openid_get_url = get_open_id_url_for(APP_CONFIG['open_id_get_url']) + "/" + email
    uri = URI.parse(openid_get_url)
    req = Net::HTTP::Get.new(uri.path)
    req.set_form_data(data)
    fire_request(uri,req,data)
  end

  # Request is made to openid for issuing a confirmation token to user with supported email.
  def resend_confirmation(email)
    reset_url = get_open_id_url_for(APP_CONFIG['open_id_resend_confirm_url'])
    data = reset_data(email)
    res = access_openid_update_data(reset_url, data)
  end

  def delete_user(email)
    data = {
      'auth_token' => APP_CONFIG['open_id_auth_token']
    }
    openid_delete_url = get_open_id_url_for(APP_CONFIG['open_id_user_delete_url']) + "/" + email
    uri = URI.parse(openid_delete_url)
    req = Net::HTTP::Delete.new(uri.path)
    req.set_form_data(data)
    fire_request(uri,req,data)
  end

 end
end
