#!/usr/bin/env ruby

ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")
require 'sgtools_amqp'

@log ||= Logger.new("#{Rails.root}/log/bus_consumer.log", 'daily')
original_formatter = Logger::Formatter.new
@log.formatter = proc { |severity, datetime, progname, msg|
  original_formatter.call(severity, datetime, progname, msg.dump)
}

$running = true

Signal.trap("TERM") do
  @log.warn "TERM Signal Received!"
  $running = false
end

# grab process pid to append to q name
x = Process.pid

consumer = SGTools::Amqp::ReportConsumer.new("report_status_listener-#{x}", queue_opts={:auto_delete => true})

consumer.begin do |message|
  @log.info "job id: #{message.job_id}"
  @log.info "status: #{message.status}"
  @log.info "message: #{message.inspect}" 
  Req.update_req_status(message)
end

while($running) do
  sleep 5
end