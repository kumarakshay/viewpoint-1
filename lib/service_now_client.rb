require 'savon'
require 'logger'
require 'yaml'

class ServiceNowClient

  @@setup = false

  def initialize
    self.setup()
    #Custom logging for reports
    @@my_logger ||= Logger.new("#{Rails.root}/log/service_now_calls.log")
    original_formatter = Logger::Formatter.new
    @@my_logger.formatter = proc { |severity, datetime, progname, msg|
      original_formatter.call(severity, datetime, progname, msg.dump)
    }
  end

  def setup(env = Rails.env)
    if !self.setup?
      @@config = self.load_config()
      @@env_config = @@config[env]
      raise StandardError, "Env #{env} not found in config" if @@env_config.nil?
      # symbolize the keys, which Bunny expects
      @@env_config.keys.each {|key| @@env_config[(key.to_sym rescue key) || key] = @@env_config.delete(key) }
      # raise StandardError, "'exchange' key not found in config" if !@@env_config.has_key?(:exchange)
      @@setup = true
    end
  end

  # Load configuration from config/amqp.yml
  def load_config
    if defined?(::Rails)
      config_file = ::Rails.root.join('config/sgtools_itsm.yml')
      if config_file.file?
        YAML.load(ERB.new(config_file.read).result)
      else
        nil
      end
    end
  end

  # def test
  #   p @@env_config
  # end

  # Is the connection set up?
  def setup?
    @@setup
  end

  def company_asset_summary(company_guid)
    retry_attempts = 0
    results =[]
    begin
      ct = Time.now
      client = Savon.client(wsdl: "#{@@env_config[:client_url]}/CompanyAssetSummary.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
                            log: true, 
                            log_level: :debug,
                            pretty_print_xml: true,
                            open_timeout: 300,
                            read_timeout: 300)
      response = client.call(:execute, message: {company: company_guid })
    rescue HTTPClient::ReceiveTimeoutError => e
      if retry_attempts > 0
        retry_attempts -= 1
        sleep 2
        retry
      else
        Rails.logger.info "--Unable to Load ServiceNow Company Asset Summary"
        puts "--Unable to Load ServiceNow Company Asset Summary"
        raise
      end
    end
    results = JSON.parse(response.hash[:envelope][:body][:execute_response][:arrayval])
    results
  end

  def company_ticket_summary(company_guid)
    ct = Time.now
    client = Savon.client(wsdl: "#{@@env_config[:client_url]}/CompanyTicketSummary.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
                          log: true, log_level: :debug, pretty_print_xml: true)
    response = client.call(:execute, message: {company: company_guid})
    results = response.hash[:envelope][:body][:execute_response]
    results
  end

  #This method return the user roles for grant_access, notification, authorization in service now
  #This roles is further used in ticket landing for showing the top action buttons
  def get_user_roles(user_guid)
    ct = Time.now
    client = Savon.client(wsdl: "#{@@env_config[:client_url]}/GetUserRoles.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
                          log: true, log_level: :debug, pretty_print_xml: true)
    response = client.call(:execute, message: {user: user_guid})
    results = response.hash[:envelope][:body][:execute_response]
    results
  end

  # def company_change_request(company=nil)
  #   ct = Time.now
  #   client = Savon.client(wsdl: "#{@@env_config[:client_url]}/change_request.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
  #                         log: true, log_level: :debug, pretty_print_xml: true)
  #   response = client.call(:get_records, message: {company: {u_portal_id: company}})
  #   results = response.hash[:envelope][:body][:get_records_response][:get_records_result]
  #   # require_attr = eval config['CHANGE_REQ_ATTRIBUTES']
  #   # dummy = {}
  #   # unless results.blank?
  #   #   results.each do |result|
  #   #     require_attr.each do |attr|
  #   #       dummy[attr] = result[attr]
  #   #     end
  #   #   end
  #   @@my_logger.info "Total Change Requests Found for a company:- #{results.count}"
  #   @@my_logger.info "Total time taken for fetching change_request data:- #{Time.now-ct}."
  #   results
  # end

  # def company_incident(company=nil)
  #   p @@env_config
  #   p "************"
  #   ct = Time.now
  #   client = Savon.client(wsdl: "#{@@env_config[:client_url]}/incident.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
  #                         log: true, log_level: :debug, pretty_print_xml: true)
  #   response = client.call(:get_records, message: {company: {u_portal_id: company}})
  #   results = response.hash[:envelope][:body][:get_records_response][:get_records_result]
  #   @@my_logger.info "Total Incidents Found :- #{results.count}"
  #   @@my_logger.info "Total time taken for fetching incidents data:- #{Time.now-ct}."
  #   results
  # end

  # def company_mx_window(company=nil)
  #   ct = Time.now
  #   client = Savon.client(wsdl: "#{@@env_config[:client_url]}/u_maintenance_window_ci.do?wsdl", basic_auth: [ @@env_config[:client_basic_auth_user], @@env_config[:client_basic_auth_password] ],
  #                         log: true, log_level: :debug, pretty_print_xml: true)
  #   response = client.call(:get_records, message: {sc_company: {u_portal_id: company}})
  #   results = response.hash[:envelope][:body][:get_records_response][:get_records_result]
  #   @@my_logger.info "Total Mx Windows Found :- #{results.count}"
  #   @@my_logger.info "Total time taken for fetching incidents data:- #{Time.now-ct}."
  #   results
  # end
end
